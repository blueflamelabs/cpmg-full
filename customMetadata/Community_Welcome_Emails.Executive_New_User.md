<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Executive - New User</label>
    <protected>false</protected>
    <values>
        <field>Channel__c</field>
        <value xsi:type="xsd:string">Executive</value>
    </values>
    <values>
        <field>Default_Email_Sender__c</field>
        <value xsi:type="xsd:string">salesforce@cpmgevents.com</value>
    </values>
    <values>
        <field>Email_Template_ID__c</field>
        <value xsi:type="xsd:string">00X1C000001ifuw</value>
    </values>
    <values>
        <field>Sender_Name__c</field>
        <value xsi:type="xsd:string">CPMG</value>
    </values>
    <values>
        <field>Status__c</field>
        <value xsi:type="xsd:string">Attending Event Contact</value>
    </values>
    <values>
        <field>User_Type__c</field>
        <value xsi:type="xsd:string">New User</value>
    </values>
</CustomMetadata>
