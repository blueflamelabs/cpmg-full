({
    doInit: function(component, event, helper) {
        helper.doInitHelper(component, event,'Name');
    }, 
    getSelectedCampaignRecords: function(component, event, helper) {
        helper.doInitHelper(component, event,'Name');
    },
    getSelectedRecords: function(component, event, helper) {
        var campRec = component.get("v.listOfSelectedCampaigns");      
        var selectedRecords = [];        
         for (var i = 0; i < campRec.length; i++) {          
                selectedRecords.push(campRec[i]);
            }                
        helper.SelectedHelper(component, event, selectedRecords);
    },
    getDeSelectedRecords: function(component, event, helper) {
        var campRec = component.get("v.listOfDeSelectedCampaigns");      
        var selectedRecords = [];        
         for (var i = 0; i < campRec.length; i++) {          
                selectedRecords.push(campRec[i]);
            }                
        helper.DeSelectedHelper(component, event, selectedRecords);
    },
    /*navigateToRecord12 : function() {
            var url = window.location.href; 
            var value = url.substr(0,url.lastIndexOf('/') + 1);
            window.history.back();
            return false;
    },*/
      navigateToRecord : function(component, event, helper) {
         var contactsId = component.get("v.recordId");
         var basUrl=component.get('v.baseUrl');
         if (component.get('v.theme_ui') === 'Theme3') {
          // salesforce classic
           window.location.href = basUrl + '/' + contactsId         
          } else {
          // lightning experience
             var lightUrl= basUrl.replace('my.salesforce.com','lightning.force.com/lightning/r/Contact');
             // alert(lightUrl);
            window.location.href = lightUrl + '/' + contactsId+ '/view' ;
        }
    },

   searchKeyChange: function(component, event,helper) { 
     
       helper.SearchlHelper(component, event);
  },
   campEventhandler: function(component, event, helper) {
        var campRec = event.getParam("campaignsSelect");       
        var selectedRecords = component.get("v.listOfSelectedCampaigns");
        var campRecFlag = event.getParam("campaignAdd");
       if(campRecFlag){
     		selectedRecords.push(campRec); 
       }else{
       		selectedRecords.splice(selectedRecords.indexOf(campRec),1)
       }
       component.set("v.listOfSelectedCampaigns", selectedRecords); 
    },
    campEventhandlerRemove: function(component, event, helper) {
        var campRec = event.getParam("campaignsSelect");       
        var selectedRecords = component.get("v.listOfDeSelectedCampaigns");
        var campRecFlag = event.getParam("campaignAdd");
       if(campRecFlag){
     		selectedRecords.push(campRec); 
       }else{
       		selectedRecords.splice(selectedRecords.indexOf(campRec),1)
       }
       component.set("v.listOfDeSelectedCampaigns", selectedRecords); 
    },
    tabSelected: function(component,event,helper) {
        //alert(component.get("v.selTabId"));
        var selelectedTabId = component.get("v.selTabId");
        component.set("v.selelectedTabId", selelectedTabId);
          //alert(component.get("v.selelectedTabId"));
    },   
    sortFirstName: function(component, event, helper) {
       component.set("v.selectedTabsoft","Name");
       helper.sortHelper(component, event,"Name");
    },
    sortRecordType: function(component, event, helper) {
       component.set("v.selectedTabsoft","RecordType");
       helper.sortHelper(component, event,"RecordType.Name");
    },
    sortFirstNameRemove: function(component, event, helper) {
       component.set("v.selectedTabsoft","Name");
       helper.sortHelperRemove(component, event,"Name");
    },
    sortRecordTypeRemove: function(component, event, helper) {
       component.set("v.selectedTabsoft","RecordType");
       helper.sortHelperRemove(component, event,"RecordType.Name");
    }
     
})