({
	createRecord : function(component, event, helper) {
       var AccId = component.get("v.recordId");
       var action = component.get('c.showContactRecord');
       action.setParams({'AcctId': AccId});
       action.setCallback(this, function(response) {            
           var state = response.getState();
           if (state === "SUCCESS") {
               var objRecord = response.getReturnValue();
               if(objRecord != null){
                   var createRecordEvent = $A.get("e.force:createRecord");
                   createRecordEvent.setParams({
                       "entityApiName": "Contact",
                       "recordTypeId": objRecord,
                       'defaultFieldValues': {
                           'AccountId' : AccId,
                       }
                   });
                   createRecordEvent.fire();
               }
           }
       });
       $A.enqueueAction(action);
	}
})