({
    
	helperMethod : function() {
		
	},
    YesActionHelper: function(component, event, helper,fromButton) {
		var selectAccount =  component.find("AccountNames");  
        var ConId = component.get("v.recordId"); 
       // var ConId = '0030m00000OjQD5AAN';
        var actionApex = component.get("c.ContactInfo");
        actionApex.setParams({'ContactId':ConId});
        actionApex.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
               var allValues = response.getReturnValue();
                if(fromButton === 'yes'){
                    allValues.objContact.AccountId = '';
                    allValues.objContact.Email = '';
                    allValues.objContact.Title = '';
                    allValues.objContact.Phone = '';
                    allValues.objContact.MobilePhone = '';
                    allValues.objContact.MailingCity = '';
                    allValues.objContact.MailingCountry = '';
                    allValues.objContact.MailingPostalCode = '';
                    allValues.objContact.MailingState = '';
                    allValues.objContact.MailingStreet = '';
                }
                component.set('v.wrapperList', allValues);
                component.set('v.Accounts',allValues.listOfAccount);
               
            }
        });
        $A.enqueueAction(actionApex);
	},
     insertContact: function(component, event, helper) {
        var wraList = component.get("v.wrapperList");
        var contact = component.get("v.contact");
        var toastEvent = $A.get('e.force:showToast');
        var createAction = component.get('c.createContactRecord');
        createAction.setParams({
            'newContact': contact,
            'AccId':wraList.objPreAccountId,
        });
        createAction.setCallback(this, function(response) {   
            var state = response.getState();
            if(state === 'SUCCESS') {
                var ContactId = response.getReturnValue();
                component.set("v.ContactNameSingle",ContactId); 
                var selectContact =  component.find("ContactNames"); 
                var wraList = component.get("v.wrapperList"); 
                var actionConApex = component.get("c.allContact");
                actionConApex.setParams({'objwrap':wraList});
                actionConApex.setCallback(this, function(response1) {
                    var state = response1.getState();
                    if (state === "SUCCESS"){
                        var conList =  response1.getReturnValue();
                        var opts = [];
                        opts.push({"class": "optionClass", label: "None", value: ""});
                        for(var i=0;i< conList.length;i++){
                            if(conList[i].Id != ContactId)
                           		opts.push({"class": "optionClass", label: conList[i].Name, value: conList[i].Id});
                            else
                                opts.push({"class": "optionClass", label: conList[i].Name, value: conList[i].Id,selected: "true"});
                        }
                        selectContact.set("v.options", opts); 
                        component.set('v.Contacts',conList);
                        component.set("v.ApplyAllOppShow",true);
                    }
                });
                $A.enqueueAction(actionConApex);
                var modal = component.find("contactModal");
                var modalBackdrop = component.find("contactModalBackdrop");
                $A.util.removeClass(modal,"slds-fade-in-open");
                $A.util.removeClass(modalBackdrop,"slds-backdrop_open");
            }
        });
        $A.enqueueAction(createAction);
         var modal = component.find("contactModal");
         var modalBackdrop = component.find("contactModalBackdrop");
         $A.util.removeClass(modal,"slds-fade-in-open");
         $A.util.removeClass(modalBackdrop,"slds-backdrop_open");
    },
    OpportunityInfo : function(component, event, helper,ButtonName) {
        var wraList = component.get("v.wrapperList"); 
        var actionOPPApex = component.get("c.OpportunityInfo");
        actionOPPApex.setParams({'AccId':wraList.objContact.Id});
        actionOPPApex.setCallback(this, function(responseOPP) {
            var state = responseOPP.getState();
            if (state === "SUCCESS"){
                component.set("v.wrapperOpportunityList",responseOPP.getReturnValue());
                var objMap = new Map()
                for(var i=0;i< responseOPP.getReturnValue().length;i++){
                    objMap.set(responseOPP.getReturnValue()[i].objOppMap.Id, responseOPP.getReturnValue()[i].objOppMap);
                }
                component.set("v.wrapperOpportunityMapMaster",objMap);
                if(responseOPP.getReturnValue().length == 0 && ButtonName == 'Yes'){
                    component.set('v.SelectedPage','NotAvaiOpp');
                }
                if(responseOPP.getReturnValue().length == 0 && ButtonName == 'noAction'){
                    component.set('v.SelectedPage','NotAvaiOpp');
                }
            }
        });
        $A.enqueueAction(actionOPPApex);
    },
    ConformationInfo : function(component, event, helper,ButtonName) {
        var wraList = component.get("v.wrapperList"); 
        var actionOPPApex = component.get("c.OrderInfo");
        actionOPPApex.setParams({'AccId':wraList.objContact.Id});
        actionOPPApex.setCallback(this, function(responseOPP) {
            var state = responseOPP.getState();
            if (state === "SUCCESS"){
                component.set("v.wrapperOrderList",responseOPP.getReturnValue());
                
                var objMap = new Map()
                for(var i=0;i< responseOPP.getReturnValue().length;i++){
                    objMap.set(responseOPP.getReturnValue()[i].objMasOrd.Id, responseOPP.getReturnValue()[i].objMasOrd);
                }
                component.set("v.wrapperOrderMapMaster",objMap);
                if(responseOPP.getReturnValue().length == 0 && ButtonName == 'Yes'){
                    component.set('v.SelectedPage','NotAvaiCon');
                }
            }
        });
        $A.enqueueAction(actionOPPApex);
    },
    createContactGone: function(component, event, helper) {
        var wraList = component.get("v.wrapperList");
        var contact = component.get("v.contact");
        var toastEvent = $A.get('e.force:showToast');
        var createAction = component.get('c.createContactRecord');
        createAction.setParams({
            'newContact': contact,
            'AccId':wraList.objPreAccountId,
        });
        createAction.setCallback(this, function(response) {   
            var state = response.getState();
            if(state === 'SUCCESS') {
                var ContactId = response.getReturnValue();
                component.set("v.ContactNameSingle",ContactId); 
                var selectContact =  component.find("ContactNameGones"); 
                var wraList = component.get("v.wrapperList"); 
                var actionConApex = component.get("c.allContact");
                actionConApex.setParams({'objwrap':wraList});
                actionConApex.setCallback(this, function(response1) {
                    var state = response1.getState();
                    if (state === "SUCCESS"){
                        var conList =  response1.getReturnValue();
                        var opts = [];
                        opts.push({"class": "optionClass", label: "None", value: ""});
                        for(var i=0;i< conList.length;i++){
                            if(conList[i].Id != ContactId)
                           		opts.push({"class": "optionClass", label: conList[i].Name, value: conList[i].Id});
                            else
                                opts.push({"class": "optionClass", label: conList[i].Name, value: conList[i].Id,selected: "true"});
                        }
                        selectContact.set("v.options", opts); 
                        component.set('v.Contacts',conList);
                        component.set("v.ApplyAllOrderShow",true);
                    }
                });
                $A.enqueueAction(actionConApex);
                var modal = component.find("contactModalGone");
                var modalBackdrop = component.find("contactModalBackdropGone");
                $A.util.removeClass(modal,"slds-fade-in-open");
                $A.util.removeClass(modalBackdrop,"slds-backdrop_open");
            }
        });
        $A.enqueueAction(createAction);
        var modal = component.find("contactModalGone");
        var modalBackdrop = component.find("contactModalBackdropGone");
        $A.util.removeClass(modal,"slds-fade-in-open");
        $A.util.removeClass(modalBackdrop,"slds-backdrop_open");
    },
    SaveAllUpdateRecordHelper : function(component, event, helper,ButtonName) {
        component.set("v.LostOrderMessage","");
        component.set("v.isCloseLostOrderMessage",false);
        component.set("v.isCloseLostOrder",false);
        var AccIdUpdate = component.get("v.selectedRecord").Id;
        //var OrdInfoCheck = component.get("v.wrapperOrderList"); 
        var ConId = component.get("v.recordId");
        var AccOrCon = component.get("v.wrapperList"); 
        var OppInfo = component.get("v.wrapperOpportunityList"); 
        var OrdInfo = component.get("v.wrapperOrderList"); 
        var isSave = component.get("v.isSaveAgain");
        var NtChangeOrder = component.get("v.oppNtChangeRecOrder");
        var ChangeOrder = component.get("v.oppChangeRecOrder");
        var actionConApex = component.get("c.SaveAllUpdate");
        //alert(AccIdUpdate);
        //alert(AccOrCon.objContact.AccountId);
        if(AccIdUpdate == undefined)
            AccIdUpdate	= AccOrCon.objContact.AccountId;
        actionConApex.setParams({'AccIds':AccIdUpdate,'objwrap':AccOrCon,'objWrapOpp':OppInfo,'objWrapOrd':OrdInfo,'isSaveAgain':isSave,'objOrderListID':NtChangeOrder,'ChangeOrderListID':ChangeOrder,'OrderButton':ButtonName});
        actionConApex.setCallback(this, function(response1) {
            var state = response1.getState();
            var succ =  response1.getReturnValue();
            if (succ === "SUCCESS"){
                component.set("v.oppNtChangeRecOrder",'');
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": ConId,
                    "slideDevName": "detail"
                });
                navEvt.fire();
            }else{
                var obj = JSON.parse(response1.getReturnValue());
                console.log('#######objobj##3333 '+obj);
                if(obj != null && obj.ListIds.length > 0){
                    var opts = [];
                    for(var i=0;i< obj.ListIds.length;i++){
                        opts.push(obj.ListIds[i].Id);
                    }
                	var messError = 'The selected contact already has an confirmation for that event, Please select a different Contact or this confirmation will be marked cancelled after clicking on next.';
                    component.set("v.LostOrderMessage",messError);
                    component.set("v.isCloseLostOrderMessage",true);
                    component.set("v.oppNtChangeRecOrder",opts);
                    var ChangeList = component.get("v.oppChangeRecOrder");
                    var optsChange = [];
                    for(var i=0;i< ChangeList.length;i++){
                        var isAvailable = false;
                        for(var j=0;j< obj.ListIds.length;j++){
                            if(ChangeList[i] == obj.ListIds[j].Id){
                                isAvailable = true;
                                break;
                            }
                    	}
                        if(isAvailable == false){
                            optsChange.push(ChangeList[i]);
                        }
                    }
                    component.set("v.oppChangeRecOrder",optsChange);
                    component.set("v.isSaveAgain",true);
                }else{
                    //alert(response1.getReturnValue());
                    var errorMe = response1.getReturnValue()
                    component.set("v.LostOrderMessage",errorMe);
                    component.set("v.isCloseLostOrderMessage",true);
                }
                
            }
        });
        $A.enqueueAction(actionConApex); 
    },
    //Lookup Functionlity
    
    searchRecordsHelper : function(component, event, helper, value) {
        var wraList = component.get("v.wrapperList");
		$A.util.removeClass(component.find("Spinner"), "slds-hide");
        component.set('v.message','');
        component.set('v.listOfSearchRecords',null);
		// Calling Apex Method
        if(wraList != null){
            var action = component.get("c.fetchLookUpValues");
            action.setStorable();
            // set param to method  
            action.setParams({
                'searchKeyWord': value,
                'ObjectName' : component.get("v.objectAPIName"),
                'RecordTypeNames' : wraList.objContact.RecordType.Name
            });
            action.setCallback(this,function(response){
                var result = response.getReturnValue();
                if(response.getState() === 'SUCCESS') {
                    // To check if any records are found for searched keyword
                    if(result.length > 0) {
                        // To check if value attribute is prepopulated or not
                        if( !$A.util.isEmpty(value) ) {
                            component.set('v.listOfSearchRecords',result);        
                        } else {
                            var index = result.findIndex(x => x.Name === value)
                            if(index != -1) {
                                var selectedRecord = result[index];
                            }
                            component.set('v.selectedRecord',selectedRecord);
                        }
                    } else {
                        component.set('v.message','No Records Found');
                    }
                } else if(response.getState() === 'INCOMPLETE') {
                    component.set('v.message','No Server Response or client is offline');
                } else if(response.getState() === 'ERROR') {
                    // If server throws any error
                    var errors = response.getError();
                    if (errors && errors[0] && errors[0].message) {
                        component.set('v.message', errors[0].message);
                    }
                }
                // To open the drop down list of records
                if( !$A.util.isEmpty(value) )
                    $A.util.addClass(component.find('resultsDiv'),'slds-is-open');
                $A.util.addClass(component.find("Spinner"), "slds-hide");
            });
            $A.enqueueAction(action);
        }    
	},
    ContactsHyginRecordInfo : function(component, event, helper) {
        var actionOPPApex = component.get("c.ContactsHyginRecord");
        actionOPPApex.setCallback(this, function(responseConHy) {
            var state = responseConHy.getState();
            if (state === "SUCCESS"){
                var opts = [];
                opts.push({"class": "optionClass", label: "None", value: ""});
                for(var i=0;i< responseConHy.getReturnValue().length;i++){
                        opts.push({"class": "optionClass", label: responseConHy.getReturnValue()[i].split("###")[0], value: responseConHy.getReturnValue()[i].split("###")[1]});
                }
                component.set("v.ContactsHygin",opts);
            }
        });
        $A.enqueueAction(actionOPPApex);
    },
})