trigger PrimaryContactRole on Opportunity (after insert,after update) {
    PrimaryContactRoleCTRL objPriContRolhandler = new PrimaryContactRoleCTRL();
    if(Trigger.isAfter){
      if(Trigger.isInsert){
          objPriContRolhandler.PrimaryContactRoleDelete(Trigger.new);
          objPriContRolhandler.PrimaryContactRoleCreate(Trigger.new);
      }
      if(Trigger.isUpdate){
         Map<Id,Opportunity> objMapOpp = new Map<Id,Opportunity>();
           for(Opportunity objOpp : Trigger.new){
               if(objOpp.Primary_Contact__c != Trigger.oldMap.get(objOpp.Id).Primary_Contact__c){
                   objMapOpp.put(objOpp.id,objOpp);
               }
           }
           if(objMapOpp.size() > 0)
               objPriContRolhandler.PrimaryContactRoleUpdate(objMapOpp);
       }
    }
    
}