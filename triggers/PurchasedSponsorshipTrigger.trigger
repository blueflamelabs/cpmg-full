trigger PurchasedSponsorshipTrigger on Purchased_Sponsorship__c (after insert, after update, before delete) {

	List<Event_Sponsorships__c> eventSponsorshipsToUpdate = new List<Event_Sponsorships__c>();


	if (Trigger.isUpdate || Trigger.isInsert) {
		for (Purchased_Sponsorship__c ps : (List<Purchased_Sponsorship__c>)Trigger.new) {
			if (Trigger.isUpdate) {
				if (ps.Sponsorship__c != NULL && Trigger.oldMap.get(ps.Id).Sponsorship__c != ps.Sponsorship__c) { //if changed
					eventSponsorshipsToUpdate.add(new Event_Sponsorships__c( Id = ps.Sponsorship__c, Purchased__c = true) );
					eventSponsorshipsToUpdate.add(new Event_Sponsorships__c( Id = Trigger.oldMap.get(ps.Id).Sponsorship__c, Purchased__c = false) );
				} else if (ps.Sponsorship__c == NULL && Trigger.oldMap.get(ps.Id).Sponsorship__c != NULL) {
					eventSponsorshipsToUpdate.add(new Event_Sponsorships__c( Id = Trigger.oldMap.get(ps.Id).Sponsorship__c, Purchased__c = false) );
				}

			} else if (Trigger.isInsert && ps.Sponsorship__c != NULL) {
				eventSponsorshipsToUpdate.add(new Event_Sponsorships__c( Id = ps.Sponsorship__c, Purchased__c = true) );
			}
		}
	} else if (Trigger.isDelete) {
		for (Purchased_Sponsorship__c ps : (List<Purchased_Sponsorship__c>)Trigger.old) {
			if (ps.Sponsorship__c != NULL) {
				eventSponsorshipsToUpdate.add(new Event_Sponsorships__c( Id = ps.Sponsorship__c, Purchased__c = false) );
			}
		}
	}

	update eventSponsorshipsToUpdate;
}