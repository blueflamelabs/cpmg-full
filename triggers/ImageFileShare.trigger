trigger ImageFileShare on Image__c (after insert,after update) {
    
    if(Trigger.isInsert && Trigger.isAfter){
        ImageFileShareCTRL.fileInsertVisibility(Trigger.New);
    }
    if(Trigger.isUpdate && Trigger.isAfter){
        List<Image__c> objImageList = new List<Image__c>();
        for(Image__c objImage : Trigger.New){
            if(String.isNotBlank(objImage.File_ID__c) && objImage.File_ID__c != Trigger.oldMap.get(objImage.Id).File_ID__c){
                objImageList.add(objImage);
            }
        }
        if(objImageList.size() > 0){
            ImageFileShareCTRL.fileInsertVisibility(objImageList);
        }
    }
}