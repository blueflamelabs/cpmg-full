trigger updatePostedDateOrderbyPlanningSeries on Planning_Series__c (after insert,after update) {
    if(Trigger.isInsert && Trigger.isAfter){
        ScheduleAlertNewPostOrderPlanningseries.updateOrderbyPlaningSeries(Trigger.newMap.keySet());
    }
    if(Trigger.isUpdate && Trigger.isAfter){
        Map<Id,Planning_Series__c> objMapSer = new Map<Id,Planning_Series__c>();
        for(Planning_Series__c objPlanSer : Trigger.new){
            if(String.isNotBlank(String.valueOf(objPlanSer.Posted_Date__c)) && 
                    objPlanSer.Posted_Date__c != Trigger.oldMap.get(objPlanSer.id).Posted_Date__c){
                objMapSer.put(objPlanSer.id,objPlanSer);
            }
        }
        if(objMapSer.size() > 0){
            ScheduleAlertNewPostOrderPlanningseries.updateOrderbyPlaningSeries(objMapSer.keySet());
        }
    }
}