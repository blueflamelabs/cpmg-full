@IsTest
public class preventEventQuestion_Test{
    static testmethod void preventEventQuestionUnitTest(){
        Id execAccRtype =  Schema.SObjectType.Question__c.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
        Question__c objques = TestDataGenerator.createQuestion(true,'test',100,false,'Category',execAccRtype,false,false);
        Question__c objques1 = TestDataGenerator.createQuestion(true,'test1',100,false,'Category',execAccRtype,false,false);
        Event_Question__c objeventqu = TestDataGenerator.createEventQuestion(true,'Test',prod,'Category',objques,false,2,'Primary Area of Interest');
        
        
            
        Test.startTest();
           
           Event_Question__c objeventqu1 = TestDataGenerator.createEventQuestion(true,'Test',prod,'Company',objques,false,2,'Company Short Description');
           objeventqu1.Phase__c = 'Personal';
           objeventqu1.Display_Type__c = 'Professional Biography';
           
           List<Event_Question__c> lstEvntQu1 = new List<Event_Question__c>();
           lstEvntQu1.Add(objeventqu1);
            
           PreventEventQuestionCTRL objPrevent1 = new PreventEventQuestionCTRL();
           objPrevent1.preventInsert(lstEvntQu1);
         
        Test.stopTest();
        
        
        
    }
}