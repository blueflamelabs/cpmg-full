/*
*Created By     : 
*Created Date   : 22/07/2019 
*Description    : 
*Test Class Name: CancellationsOrderPRCTRL
Modification History :
============================================================================================
V.No    Date            By                  Description
============================================================================================
*/
@IsTest
public class CancellationsOrderPRCTRL_Test{
    static testMethod void CancellationsOrderPRCTRL_testMethod(){
    
     Id conRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        
        Account objAcc = TestDataGenerator.createTestAccount(true,'Test Supplier Account');
        
        Contact objCon = TestDataGenerator.createTestContact(true,'Test FName','Last Name','testexec@test.com',objAcc,conRecType);

        Profile profilId = [SELECT Id FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User userCPMG = TestDataGenerator.CreateUser(true,profilId.Id,'Test','Exec','2225252525','test1Exec1234@cpmg.com',objCon.Id);
        
        Product2 objProd = TestDataGenerator.createTestProduct(true,'Test Name',True);
        
        Event_Staff__c objEventStf = TestDataGenerator.createEventStaff(true,objProd,userCPMG,'Developer');
        
        Opportunity objOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),ObjAcc,'Closed Won',objProd,'Onsite Renewal');
        
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,objProd,objAcc,objOpp,objCon, execPartRecordType);
        Map<Id,Event_Account_Details__c> objMapPR = new Map<Id,Event_Account_Details__c>();
        Order objOrder = new Order(AccountId = objAcc.Id, BillToContact = objCon, Status = 'Attending Event Contact',Event_Account_Detail__c = partRecord.id, OpportunityId = objOpp.Id, Primary_Event_Contact__c = true, EffectiveDate = objOpp.CloseDate);
        insert objOrder;
        partRecord.Status__c = 'Cancelled';
        update partRecord;
         Test.startTest();
            
            
            CancellationsOrderPRCTRL extension = new CancellationsOrderPRCTRL();
            CancellationsOrderPRCTRL.canOrder(objMapPR);
         Test.stopTest();  
    }
}