public class MarkGoneContactCTRL {
    
    
    @AuraEnabled 
    public static List<String> ContactsHyginRecord(){
        List<String> objHyi = new List<String>();
        Schema.DescribeFieldResult fieldResult = Contact.Reach_Hygiene__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            objHyi.add(pickListVal.getLabel()+'###'+pickListVal.getValue());
        }     
        return objHyi;
    }
    
    @AuraEnabled(cacheable=true)
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName,String RecordTypeNames) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = searchKeyWord + '%';
        
        List < sObject > returnList = new List < sObject > ();
        String AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(RecordTypeNames).getRecordTypeId();
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5   
        String sQuery = '';
        if(String.isNotBlank(searchKeyWord)){
            sQuery =  'select id, Name from Account where RecordTypeId =:AccRecordTypeId and Name LIKE: searchKey order by Name ASC';
        }else{
            sQuery = 'select id, Name from Account where RecordTypeId =:AccRecordTypeId order by Name ASC limit 10';
        }
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        System.debug('&&&&&&&&&& '+returnList);
        return returnList;
    }
    
    public class wrapperClass{
        @AuraEnabled public Contact objContact{get;set;}
        @AuraEnabled public List<Account> listOfAccount{get;set;}
        @AuraEnabled public String objPreAccountId{get;set;}
        public wrapperClass(Contact objContact,List<Account> listOfAccount,String objPreAccountId){
            this.objContact = objContact;
            this.listOfAccount = listOfAccount;
            this.objPreAccountId = objPreAccountId;
        }
        public wrapperClass(){}
    }
    
    @AuraEnabled 
    public static wrapperClass ContactInfo(Id ContactId){
        Contact objCon = [Select Id,Gone__c,Reach_Hygiene__c,AccountId,Account.Name,Title,Email,Phone,
                          MobilePhone,RecordTypeId,RecordType.Name,
                          MailingAddress,MailingCity,MailingCountry,MailingPostalCode,MailingState,MailingStreet
                          from Contact where Id=:ContactId];
        String AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(objCon.RecordType.Name).getRecordTypeId();
        List<Account> objAccList = [Select Id,Name,RecordTypeId from Account where RecordTypeId =:AccRecordTypeId order by name asc limit 50000];
        wrapperClass objWrap = new wrapperClass(objCon,objAccList,objCon.AccountId);
        return objWrap;
    }
    @AuraEnabled 
    public static wrapperClass addressOfAccount(String AccId,wrapperClass objwrap){
        System.debug('$$$$$$$$objwrap.objContact.AccountId$$$$$$4 '+objwrap.objContact.AccountId);
        Account objAcc = [Select Id,BillingAddress,BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet from Account where Id =:AccId];
        objwrap.objContact.MailingCountry = objAcc.BillingCountry;
        objwrap.objContact.MailingState = objAcc.BillingState;
        objwrap.objContact.MailingCity = objAcc.BillingCity;
        objwrap.objContact.MailingStreet = objAcc.BillingStreet;
        objwrap.objContact.MailingPostalCode = objAcc.BillingPostalCode;
        return objwrap;
    }
    @AuraEnabled
    public static List<Contact> allContact(wrapperClass objwrap){
        System.debug('$$$$$$$$objwrap.objContact.AccountId$$$$$$4 '+objwrap.objPreAccountId);
        List<Contact> objConList = [Select Id,Name from Contact where AccountId =:objwrap.objPreAccountId order by Name asc];
        return objConList;
    }
    @AuraEnabled 
    public static String ContactRecTypeId(String AccId){
        Account objAcc = [Select Id,RecordType.Name from Account where Id =:AccId];
        String ContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(objAcc.RecordType.Name).getRecordTypeId();
        return ContactRecordTypeId;
    }
    @AuraEnabled
    public static String createContactRecord(Contact newContact,String AccId) {
        newContact.AccountId = AccId;
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true; 
        Database.insert(newContact, dml);
        String ConId = String.valueOf(newContact.Id);
        return ConId;        
    }
     @AuraEnabled
    public static Account createAccountRecord(Account newAcc,String RecTypeName) {
        String AccRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(RecTypeName).getRecordTypeId();
        newAcc.RecordTypeId = AccRecordTypeId;
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true; 
        Database.insert(newAcc, dml);
        String AccId = String.valueOf(newAcc.Id);
        Account objAcc = [Select Id,Name from Account where Id =: AccId];
        return objAcc;        
    }
    
    public class wrapperOpporuntiyClass{
        @AuraEnabled public Opportunity objOpp{get;set;}
        @AuraEnabled public Opportunity objOppMap{get;set;}
        public wrapperOpporuntiyClass(Opportunity objOpp,Opportunity objOppMap){
            this.objOpp = objOpp;
            this.objOppMap = objOppMap;
        }
        public wrapperOpporuntiyClass(){}
    }
    
    @AuraEnabled
    public static List<wrapperOpporuntiyClass> OpportunityInfo(String AccId) {
        List<wrapperOpporuntiyClass> objWraOppList = new List<wrapperOpporuntiyClass>();
        List<Opportunity> objOppList = [Select Id,ContactId,Product__c,Name,Lost_Reason_Details__c,Returning_Customer__c,Account.Name,Primary_Contact__c,Product__r.Name,CloseDate,StageName from Opportunity where Primary_Contact__c =: AccId and StageName != 'Closed Won' and StageName != 'Closed Lost'];
        System.debug('$$$$$$$$$$4 '+AccId);
        System.debug('$$$$$$objOppList$$$$4 '+objOppList);
        for(Opportunity objOpp : objOppList){
            objWraOppList.add(new wrapperOpporuntiyClass(objOpp,objOpp));
        }
        return objWraOppList;
    }
    public class wrapperConforClass{
        @AuraEnabled public Order objOrd{get;set;}
        @AuraEnabled public Order objMasOrd{get;set;}
        public wrapperConforClass(Order objOrd,Order objMasOrd){
            this.objOrd = objOrd;
            this.objMasOrd = objMasOrd;
        }
        public wrapperConforClass(){}
    }
    @AuraEnabled
    public static List<wrapperConforClass> OrderInfo(String AccId) {
        List<wrapperConforClass> objWraOrdList = new List<wrapperConforClass>();
        List<Order> objOrdList = [Select Id,EffectiveDate,Primary_Event_Contact__c,Event_Account_Detail__c,OrderNumber,Account.Name,BillToContactId,Order_Event__r.Name,Status,AccountId from Order where BillToContactId =: AccId and Status != 'Cancelled' and Order_Event__r.Start_Date__c >TODAY];
        for(Order objOrd : objOrdList){
            objWraOrdList.add(new wrapperConforClass(objOrd,objOrd));
        }
        return objWraOrdList;
    }
    @AuraEnabled
    public static String SaveAllUpdate(String AccIds,wrapperClass objwrap,List<wrapperOpporuntiyClass> objWrapOpp,List<wrapperConforClass> objWrapOrd,Boolean isSaveAgain,List<Id> objOrderListID,List<Id> ChangeOrderListID,String OrderButton) {
        String Succ = 'SUCCESS';
        try{
            // if(OrderButton == 'NoOrder'){
            //isSaveAgain = false;
            //}
            System.debug('############3 '+AccIds);
            System.debug('############3 '+objwrap.objContact.AccountId);
            Contact objCon = objwrap.objContact;
            objCon.AccountId = AccIds;
            if(objwrap.objPreAccountId == AccIds)
                objCon.Gone__c = true;
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.allowSave = true;
            dml.DuplicateRuleHeader.runAsCurrentUser = true; 
            
            List<Opportunity> objOppList = new List<Opportunity>();
            for(wrapperOpporuntiyClass objWraOpp : objWrapOpp){
                Opportunity objOpp = objWraOpp.objOpp;
                //objOpp.ContactId = objWraOpp.objOpp.Primary_Contact__c;
                objOpp.Returning_Customer__c = 'Renewal';
                if(objOpp.StageName == 'Closed Lost'){
                    objOpp.Lost_Reason__c = 'No longer with company';
                    objOpp.Lost_Reason_Details__c = 'Lost';    
                }
                System.debug('$$$$$objOpp$$$$$$4 '+objOpp);
                objOppList.add(objOpp);
            }
            if(objOppList.size() > 0){
                Set<Id> objEvId = new Set<Id>();
                for(Opportunity objOpp : objOppList){
                    objEvId.add(objOpp.Product__c);
                }
                List<Product2> objPro = [Select Id,Schedule_URL__c from Product2 where Schedule_URL__c = null and Id IN :objEvId];
                for(Product2 objP : objPro){
                    objP.Schedule_URL__c = 'test';
                }
                if(objPro.size() > 0){
                    update objPro;
                }
                
            }
            List<Order> objOrderList = new List<Order>();
            SeT<Id> objPRParentId = new Set<Id>();
            Set<Id> objPrimaryId = new Set<Id>();
            Set<Id> EventId = new Set<Id>();
            Set<Id> AccountsId = new Set<Id>();
            Set<Id> OrderIdsId = new Set<Id>();
            for(wrapperConforClass objWraOrd : objWrapOrd){
                OrderIdsId.add(objWraOrd.objOrd.Id);
                objOrderList.add(objWraOrd.objOrd);
                objPRParentId.add(objWraOrd.objOrd.Event_Account_Detail__c);
                if(objWraOrd.objOrd.BillToContactId != objCon.Id)
                    objPrimaryId.add(objWraOrd.objOrd.BillToContactId);
                EventId.add(objWraOrd.objOrd.Order_Event__c);
                AccountsId.add(objWraOrd.objOrd.AccountId);
            }
            List<Order> objOrderListExist = [Select Id,BillToContactId from Order Where BillToContactId IN : objPrimaryId and Order_Event__c IN : EventId and AccountId IN :AccountsId];
            System.debug('$$$$$$$objOrderListExist$$$$$$$ '+objOrderListExist);
            Map<Id,List<Order>> objPRRecdMapIdOrder = new Map<Id,List<Order>>();
            List<Order> objOrderCountPR = [Select Id,Event_Account_Detail__c from Order where Event_Account_Detail__c IN : objPRParentId];
            for(Order objOrPr : objOrderCountPR){
                if(objPRRecdMapIdOrder.keySet().Contains(objOrPr.Event_Account_Detail__c) == false){
                    objPRRecdMapIdOrder.put(objOrPr.Event_Account_Detail__c,new List<Order>{objOrPr});    
                }else{
                    List<Order> objOrderLists = objPRRecdMapIdOrder.get(objOrPr.Event_Account_Detail__c);
                    objOrderLists.add(objOrPr);
                    objPRRecdMapIdOrder.put(objOrPr.Event_Account_Detail__c,objOrderLists);    
                }  
            }
            Set<Id> objPRRecdId = new Set<Id>();
            for(Id objPrID : objPRRecdMapIdOrder.keySet()){
                if(objPRRecdMapIdOrder.get(objPrID).size() == 1){
                    objPRRecdId.add(objPrID);
                }
            }
            System.debug('######objPRRecdId###33 '+objPRRecdId);
            List<Event_Account_Details__c> objPrRecList = [Select Id,Status__c,Opportunity__c from Event_Account_Details__c where Id IN : objPRRecdId and Status__c !='Cancelled'];
            for(Event_Account_Details__c objEveAccDetail : objPrRecList){
                objEveAccDetail.Status__c = 'Cancelled';
            }
            
            if(objOrderListExist.size() > 0 && isSaveAgain == false && OrderButton == 'YesOrder'){
                List<Order> objOrderListCheck = new List<Order>();
                for(Order objOr : objOrderList){
                    for(Order objOrs : objOrderListExist){
                        if(objOr.BillToContactId == objOrs.BillToContactId){
                            objOrderListCheck.add(objOr);
                            break;
                        }
                    }
                }
                System.debug('##objOrderListCheck## '+objOrderListCheck);
                Map<String, List<Order>> mapToSerialize = new Map<String, List<Order>>();
                mapToSerialize.put('ListIds',objOrderListCheck);
                Succ = JSON.serialize(mapToSerialize);
                return Succ;
            }
            
            if(objOrderList.size() > 0){
                if(objwrap.objContact.RecordType.Name == 'Executive'){
                    List<Order> objOrderNewInser = new List<Order>();
                    for(Order objOr : objOrderList){
                        if(objOr.BillToContactId != objCon.Id){
                            Order objNewOrder = objOr.Clone();
                            objNewOrder.BillToContactId = objCon.Id;
                            objNewOrder.Status =  'Cancelled';
                            objNewOrder.Primary_Event_Contact__c =  false;
                            objOrderNewInser.add(objNewOrder);
                        }
                    }
                   if(objOrderNewInser.size() > 0)
                        insert objOrderNewInser;
                   if(objOrderList.size() > 0)
                        update objOrderList; 
                   if(objPrRecList.size() > 0)
                       update objPrRecList;
                    
                }else{
                    try{
                        List<Order> objOrderListCheck = new List<Order>();
                        for(Order objOr : objOrderList){
                            for(Order objOrs : objOrderListExist){
                                if(objOr.BillToContactId == objOrs.BillToContactId){
                                    objOr.Status =  'Cancelled';
                                    objOr.BillToContactId = objCon.Id;
                                    objOr.Primary_Event_Contact__c =  false;
                                    objOrderListCheck.add(objOr);
                                    break;
                                }
                            }
                        }
                        System.debug('@@@@@@@@@@@objOrderList@@@@ '+objOrderList);
                        System.debug('@@@@@@@@@@@objCon.Id@@@@ '+objCon.Id);
                        if(objPrRecList.size() > 0)
                            update objPrRecList; 
                       // update objOrderList; 
                        Map<Id,Order> objListUpdateByform = new Map<Id,Order>();
                        for(Order objOr : objOrderList){
                            if(objOr.BillToContactId != objCon.Id){
                                if(!objListUpdateByform.keySet().Contains(objOr.Id)){
                                    objListUpdateByform.put(objOr.Id,objOr);
                                }
                                
                            }
                        }
                        if(objListUpdateByform.size() > 0)
                            update objListUpdateByform.values();
                        
                        List<Order> objOrderNewInser = new List<Order>();
                        for(Order objOr : objOrderList){
                            if(objOr.BillToContactId != objCon.Id){
                                Order objNewOrder = objOr.Clone();
                                objNewOrder.BillToContactId = objCon.Id;
                                objNewOrder.Status =  'Cancelled';
                                objNewOrder.Primary_Event_Contact__c =  false;
                                objOrderNewInser.add(objNewOrder);
                            }
                        }
                        System.debug('#####objOrderNewInser###33 '+objOrderNewInser);
                        if(objOrderNewInser.size() > 0)
                            insert objOrderNewInser;
                        update objOrderListCheck;
                    }catch(Exception ex){
                        String Succ1 = ex.getMessage();
                        if(Succ1.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION, Suppliers with Attending or Cancelled status cannot be the Primary Event Contact'))
                        {
                            Succ = 'Suppliers with Attending or Cancelled status cannot be the Primary Event Contact';
                            return Succ;
                        }else{
                            System.debug('#######Succ1Succ1Succ1Succ1###### '+Succ1);
                        }
                    }
                }
            }
            
            List<User> objUserList = [Select Id,Email from user where ContactId =:objwrap.objContact.Id];
            for(User objUser : objUserList){
                objUser.Email = objwrap.objContact.Email;
            }
            if(objUserList.size() > 0){
                update objUserList;    
            }
            //Database.update(objCon, dml);
            if(objwrap.objPreAccountId != AccIds){
                List<AccountContactRelation> objAccConRelaRecord = [Select id,EndDate,IsActive,StartDate,Employment_Status__c,Email_Address__c,Title__c,ContactId,AccountId
                                                                    from AccountContactRelation where ContactId =:objCon.Id and AccountId =:objwrap.objPreAccountId];
                
                for(AccountContactRelation objAccRe : objAccConRelaRecord){
                    objAccRe.EndDate = date.today();
                    objAccRe.Employment_Status__c = 'Inactive';
                }
                update objAccConRelaRecord;
            }else{
                List<AccountContactRelation> objAccConRelaRecord = [Select id,EndDate,IsActive,StartDate,Employment_Status__c,Email_Address__c,Title__c,ContactId,AccountId
                                                                    from AccountContactRelation where ContactId =:objCon.Id and AccountId =:AccIds];
                
                for(AccountContactRelation objAccRe : objAccConRelaRecord){
                    objAccRe.EndDate = date.today();
                    objAccRe.Employment_Status__c = 'Inactive';
                }
                update objAccConRelaRecord;
            }
            
            
            //ChangeOrderListID
            List<Order> objOrderChangeList = [Select Id,OpportunityId from Order where Id IN : ChangeOrderListID];
            Set<Id> oppIds = new Set<Id>();
            for(Order objOrd : objOrderChangeList){
                oppIds.add(objOrd.OpportunityId);
            }
            System.debug('########ChangeOrderListID##33 '+ChangeOrderListID);
            Map<Id,String> objMapOppIdStatus = new Map<Id,String>();
            List<Opportunity> objOppListOldQuery = [Select Id,StageName from Opportunity where Id IN : oppIds];
            for(Opportunity objOpp : objOppListOldQuery){
                objMapOppIdStatus.put(objOpp.Id,objOpp.StageName);
            }
            System.debug('##########3 objMapOppIdStatus '+objMapOppIdStatus);
            for(Opportunity objOppUpdate : objOppList){
                if(objMapOppIdStatus.keyset().Contains(objOppUpdate.Id)){
                    objOppUpdate.StageName = objMapOppIdStatus.get(objOppUpdate.Id);
                }
            }
            update objOppList; 
            
            if(objPrRecList.size() > 0){
                Set<Id> objOppId = new Set<Id>();
                for(Event_Account_Details__c objPR : objPrRecList){
                    objOppId.add(objPR.Opportunity__c);
                }
                if(objOppId.size() > 0){
                    List<Opportunity> objOppLostList = [Select id,Primary_Contact__c,(Select id from Event_Account_Details__r),StageName,Returning_Customer__c,
                                                        Lost_Reason__c,Lost_Reason_Details__c from Opportunity where Id IN : objOppId];
                    for(Opportunity objOpp : objOppLostList){
                        if(objOpp.Event_Account_Details__r.size() == 1 && objOpp.Primary_Contact__c == objCon.Id){
                            if(String.isNotBlank(objOpp.Returning_Customer__c))
                                objOpp.Returning_Customer__c = 'Renewal';
                            objOpp.StageName = 'Closed Lost';
                            objOpp.Lost_Reason__c = 'No longer with company';
                            objOpp.Lost_Reason_Details__c = 'Lost';   
                        }
                    }
                    update objOppLostList;
                }
                
            }
            Database.update(objCon, dml);
        }
        catch(Exception ex){
            String Succ1 = ex.getMessage();
            if(Succ1.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION, An Opportunity for this event and contact already exists. You may not create a duplicate'))
            {
                Succ = 'An Opportunity for this event and contact already exists. You may not create a duplicate.'; 
            }else{
                Succ = ex.getMessage();
            }
        }
        return Succ;
    }
}