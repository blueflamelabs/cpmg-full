@isTest
private class EventFeedController_Test {

	@isTest
	private static void testName() {
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Prod',true);
		prod.News__c =' THIS CONTAINS NEWS';
		update prod;

		Test.startTest();
			EventFeedController eFeed = new EventFeedController();
			eFeed.isSupplier = false;
			eFeed.eventId = prod.Id;

			eFeed.getCurrEvent();
		Test.stopTest();
	}
}