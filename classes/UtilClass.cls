public with sharing class UtilClass {
    ///Remove

	/*public UtilClass() {
		
	} */
	private static List<ProfileWrapper> profWrappers{get;set;}

	public static String GenerateRandomString(Integer len)
    {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len)
        {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            randStr += chars.substring(idx, idx + 1);
        }
        return randStr;
    }

     public static UserPermissions getUserPermissions(){
        Id userId = UserInfo.getUserId();
        User u = [SELECT ID, COntactID FROM USER WHERE ID =:userID];
        System.debug('USER U : '+ u);
        Contact currentCon = [SELECT Id,RecordType.Name FROM Contact where Id =: u.ContactId LIMIT 1];
        System.debug('Current COn: '+ currentCon);
        return new UserPermissions(currentCon.RecordType.Name,true);
    }

    
     //Used to format external web links for the community
    public static String formatExternalLinkULR(String url){
        if(String.isNotBlank(url)){
            if(url.containsIgnoreCase('http://') || url.containsIgnoreCase('https://')){
                return url;
            } else{
                return 'http://'+url;
            }
        } else{
            return null;
        }
    }

	//Used to generate a ProfileWrapper from the Event Account Detail
	public static List<ProfileWrapper> transformToProfileWrapper(List<Event_Account_Details__c> partRecords){
		profWrappers = new List<ProfileWrapper>();
		for(Event_Account_Details__c partRecord: partRecords){
			profWrappers.add(new ProfileWrapper(partRecord,null,null,null));
		}

		return profWrappers;
	}
}