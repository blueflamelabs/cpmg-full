public without sharing class MeetingRequestController {

    //public MeetingRequestDto[] selectedMeetingRequests{get;set;}
    //public MeetingRequestDto[] availableMeetingRequests {get;set;}
    public Product2 currentEvent {get;set;}
    public UserPermissions permissions {get;set;}
    public Contact currentContact {get;set;}
    public Account currentAccount {get;set;}
    public Event_Account_Details__c currentPartRecord {get;set;}
    public Integer selectedOneOnOne {get;set;}
    public Integer selectedBoardroom {get;set;}
    public Integer maxOneOnOne {get;set;}
    public MultiComponentForm parentForm {get;set;}
    public List<Event_Account_Details__c> availableRequests {get;set;}
    public List<Event_Ranking__c> currentSelections {get;set;}
  public static Boolean hasBuildMeetingRequest = false;
 // public Boolean displayProfileModal {get;set;}
  //public Map<Id,ProfileWrapper> prIdProfilesMap {get;set;}
  public Community_Settings__c commSettings {get;set;}
  //public Map<
  //public List<ProfileWrapper> prIdProfilesMap {get;set;}

  //New Functionality
  private Map<String,Contact_Event_Answers__c> answersByPRAndQType {get;set;}
  private Map<Id,Event_Account_Details__c> allPRsForOppositeRType {get;set;}
  //private Map<String,List<Event_Ranking__c>> partRecordIdEventRankingMap{get;set;}
  private Map<String,Event_Ranking__c> partRecordIdEventRankingMap{get;set;}
  //private Map<Id,List<Contact_Event_Answers__c>> partRecordIdProfileQuestions {get;set;}
  public MeetingRequestWrapper[] selectedMeetingRequests{get{
                                    if(selectedMeetingRequests == null){
                                      try{
                                        buildMeetingRequests(currentEvent.Id, currentPartRecord.Id);

                                      }catch(Exception ex){
                                        System.debug('Error: '+ ex.getMessage()+'. On Line: '+ ex.getLineNumber()+'. Stack Trace: '+ ex.getStackTraceString());
                                      }
                                    }
                                    return selectedMeetingRequests;
                                  }set;}
 /* public MeetingRequestWrapper[] availableMeetingRequests {get{
                                      if(selectedMeetingRequests == null){
                                      try{
                                        buildMeetingRequests(currentEvent.Id, currentPartRecord.Id);

                                      }catch(Exception ex){
                                        System.debug('Error: '+ ex.getMessage()+'. On Line: '+ ex.getLineNumber()+'. Stack Trace: '+ ex.getStackTraceString());
                                      }
                                    }
                                    return selectedMeetingRequests;
                                  }set;} */
  public MeetingRequestWrapper[] availableMeetingRequests {get;set;}
  public Integer numberOneOnOnes {get;set;}
  public Integer numberBoardRooms {get;set;}
  private Map<Id,SObject> deletedMeetingRequests{get;set;}
  private List<Event_Ranking__c> upsertMeetingRequests {get;set;}
  public String eventId {get;set;}
  public String partRecordId {get;set;}
  public String currentPartRecordId {get;set;}
  public List<ProfileWrapper> profilesList {get;set;}
  public List<Schema.FieldSetMember> accountFormFields{get;set;}
  public Map<String, Account_Form_Display_Settings__c> accountUserSpecifiedFieldNames { get; set; }
  public Set<Id> categoryQuestionId {get;set;}
  public List<SelectOption> categorySelectOptions {get;set;}
  public String categoryFilter {get;set;}

    public MeetingRequestController() {
    //System.debug('permissions: '+ permissions);
    //if($ApexPages.CurrentPage.Name == 'profilePage'){
    
    //}

    accountUserSpecifiedFieldNames = new Map<String, Account_Form_Display_Settings__c>(Account_Form_Display_Settings__c.getAll());
    accountFormFields = SObjectType.Account.FieldSets.User_Specified_Fields.getFields();
    
    for(Schema.FieldSetMember field :accountFormFields){
    
      if(!accountUserSpecifiedFieldNames.containsKey(field.getFieldPath())) {
        accountUserSpecifiedFieldNames.put(field.getFieldPath(), new Account_Form_Display_Settings__c(Read_Only__c = false, Field_Display_Name__c = field.getLabel()));
      }
        //System.debug('Field: ' + accountUserSpecifiedFieldNames.get(field.getFieldPath()));
    }
    
        if(ApexPages.currentPage().getParameters().containsKey('eId')){
      eventId =ApexPages.currentPage().getParameters().get('eId');
    }
    if(ApexPages.currentPage().getParameters().containsKey('prId')){
      partRecordId =ApexPages.currentPage().getParameters().get('prId');
    }
    if(ApexPages.currentPage().getParameters().containsKey('currentprId')){
      currentPartRecordId =ApexPages.currentPage().getParameters().get('currentprId');
    }
    /*if(eventId != null && currentPartRecordId != null){
      try{
      //buildProfileWrappers(eventId,currentPartRecordId);
      }catch(Exception ex){
        System.debug('Error: '+ ex.getMessage());
        System.debug('Stack Trace: '+ ex.getStackTraceString());
      }
    } */
    }
  public void buildProfileWrappers(Id eventId, Id currentPartRecord){
   /* profilesList = new List<ProfileWrapper>();
    getAllPRsForOppositeRType(new Set<Id>{eventId});
    getParticipationRecordQuestionAnswers(new Set<Id>{eventId});
    getAllIncludeInProfileQuestions(eventId, currentPartRecordId);
    prepareMeetingRequestWrappers(); */
    //if(partRecordId != null){
     // profilesList.add( prIdProfilesMap.get(partRecordId));
    //}else{
     // profilesList.addAll(prIdProfilesMap.values());
    //}
    System.debug('Profiles List: '+ profilesList);
  }
  public void buildMeetingRequests(Id eventId, Id currentPartRecordId){
    if(!MeetingRequestController.hasBuildMeetingRequest){
    this.numberBoardRooms = (currentPartRecord.Package_Selection__r.Number_of_Boardroom_Selections_Needed__c != null)?Integer.valueOf(currentPartRecord.Package_Selection__r.Number_of_Boardroom_Selections_Needed__c):10*Integer.valueOf(currentPartRecord.Badge_Total__c);
    this.numberOneOnOnes = (currentPartRecord.Package_Selection__r.Number_of_One_On_One_Selections_Needed__c != null)?Integer.valueOf(currentPartRecord.Package_Selection__r.Number_of_One_On_One_Selections_Needed__c):5;
    //this.displayProfileModal = false;
      MeetingRequestController.hasBuildMeetingRequest = true;
        getParticipationRecordQuestionAnswers(new set<Id>{eventId});
        getAllPRsForOppositeRType(new Set<Id>{eventId});
        getAllEventRankingsForPR(eventId,currentPartRecordId);
       // getAllIncludeInProfileQuestions(eventId, currentPartRecordId);
        prepareMeetingRequestWrappers();
    }
  }

  //Gets Short, and Category Category Answers
  private void getParticipationRecordQuestionAnswers(Set<Id> eventIds){
      Set<String> questionTypes = new Set<String>{'Company Short Description'};
      answersByPRAndQType = ContactEventAnswerHelper.getLongAndShortProfiles1(EventIds, questionTypes);
      System.debug('answersByPRAndQType answersByPRAndQType '+answersByPRAndQType );
  }

  private void getAllPRsForOppositeRType(Set<Id> eventIds){
      System.debug('#######permissionspermissions### '+permissions);
      if(permissions == null){
        List<Id> eventIdsList = new List<Id>(eventIds);
        permissions = UtilClass.getUserPermissions();
        permissions.getEventStaff(eventIdsList[0]);
      }
      String rType = permissions.isSupplier ? 'Executive' : 'Supplier';
      System.debug('###########rType########## '+rType);
      if(rType == 'Supplier'){
       allPRsForOppositeRType = new Map<Id,Event_Account_Details__c>();
       Map<Id,Event_Account_Details__c> objEventAccountDetails = ParticipationRecordHelper.getAllPRRecordsByEventForMeetingRequests(eventIDs, rType);
       Set<Id> objEventAccID = new Set<Id>();
       Set<Id> objEventAccIDCompany = new Set<Id>();
       //Need Differentiate Supplier
       Map<String,Event_Account_Details__c> objEventAccountDetailsList = new Map<String,Event_Account_Details__c>();
       List<Order> objOrderList = [select id,Company_Display_Name__c,BillToContactId,Event_Account_Detail__c,Status,Company_Name__c from Order where Event_Account_Detail__c IN : objEventAccountDetails.keySet() and Status != 'Cancelled' and Primary_Event_Contact__c = true];
       for(Order objOrder : objOrderList){
           if(String.isNotBlank(objOrder.Company_Display_Name__c)){
               if(!objEventAccountDetailsList.keySet().Contains(objOrder.Company_Display_Name__c)){
                     objEventAccountDetailsList.put(objOrder.Company_Display_Name__c,objEventAccountDetails.get(objOrder.Event_Account_Detail__c));  
               }else{
                   objEventAccID.add(objEventAccountDetailsList.get(objOrder.Company_Display_Name__c).id);
                   objEventAccID.add(objOrder.Event_Account_Detail__c);
               }      
           }  
           else{
               if(!objEventAccountDetailsList.keySet().Contains(objOrder.Company_Name__c)){
                   objEventAccountDetailsList.put(objOrder.Company_Name__c,objEventAccountDetails.get(objOrder.Event_Account_Detail__c));
               }else{
                   objEventAccIDCompany.add(objEventAccountDetailsList.get(objOrder.Company_Name__c).id);
                   objEventAccIDCompany.add(objOrder.Event_Account_Detail__c);
               }  
           }  
       } 
       System.debug('$$$$$$$objEventAccIDCompany$$$4 '+objEventAccIDCompany);
       List<Contact_Event_Answers__c> objContactEventAns = [SELECT Id,Answer__c, Question__c, Participation_Record__c, Confirmation__c,Confirmation__r.Company_Display_Name__c, Question__r.Event__c, 
                                                           Question__r.Display_Type__c, Question__r.Phase__c,
                                                           Question__r.Question_Display__c, Question__r.Question__c
                                                           FROM Contact_Event_Answers__c WHERE Question__r.Event__c In :eventIds and Participation_Record__c IN : objEventAccID order by LastModifiedDate ASC];
                                                           
       for(Contact_Event_Answers__c objConEvent : objContactEventAns){
            if(String.isNotBlank(objConEvent.Answer__c))
               objEventAccountDetailsList.put(objConEvent.Confirmation__r.Company_Display_Name__c,objEventAccountDetails.get(objConEvent.Participation_Record__c));
       }  
       
       
       List<Contact_Event_Answers__c> objContactEventAnsCompany = [SELECT Id,Answer__c, Question__c, Participation_Record__c, Confirmation__c,Confirmation__r.Company_Name__c, Question__r.Event__c, 
                                                           Question__r.Display_Type__c, Question__r.Phase__c,
                                                           Question__r.Question_Display__c, Question__r.Question__c
                                                           FROM Contact_Event_Answers__c WHERE Question__r.Event__c In :eventIds and Participation_Record__c IN : objEventAccIDCompany order by LastModifiedDate ASC];
                                                           
       for(Contact_Event_Answers__c objConEvent : objContactEventAnsCompany){
          if(String.isNotBlank(objConEvent.Answer__c))
               objEventAccountDetailsList.put(objConEvent.Confirmation__r.Company_Name__c,objEventAccountDetails.get(objConEvent.Participation_Record__c));
       }  
           
                                                        
       for(Event_Account_Details__c pRecord : objEventAccountDetailsList.values()){
            if(pRecord.Account__r.Type != 'Vendor'){
               allPRsForOppositeRType.put(pRecord.id,pRecord);
            }
        }
          
      }else if(rType == 'Executive'){
          Set<Id> objConId = new Set<Id>();
          allPRsForOppositeRType = new Map<Id,Event_Account_Details__c>();
          Map<Id,Event_Account_Details__c> objAllPREventAcctountSupplier = ParticipationRecordHelper.getAllPRRecordsByEventForMeetingRequests(eventIDs, rType);
          for(Event_Account_Details__c objEventAccount : objAllPREventAcctountSupplier.values()){
             if(objEventAccount.Account__r.Type != 'Vendor'){
              objConId.add(objEventAccount.Primary_Contact__c);
            }
          }
          List<Order> objOrderList = [select id,BillToContactId,Event_Account_Detail__c,Status from Order where BillToContactId IN : objConId and Event_Account_Detail__c IN : objAllPREventAcctountSupplier.keySet() and Status != 'Cancelled' and Primary_Event_Contact__c = true];
          for(Order objOrder : objOrderList){
              allPRsForOppositeRType.put(objOrder.Event_Account_Detail__c,objAllPREventAcctountSupplier.get(objOrder.Event_Account_Detail__c));
          }
      }  
  }

  private void getAllEventRankingsForPR(Id currentEventId, Id partRecordId){
      partRecordIdEventRankingMap = EventRankingHelper.getEventRankingsForEvent(currentEventId, partRecordId);
      System.debug('partRecordIdEventRankingMap partRecordIdEventRankingMap '+partRecordIdEventRankingMap );
  }
  /*private void getAllIncludeInProfileQuestions(Id currentEventId, Id partRecordId){
      partRecordIdProfileQuestions = ContactEventAnswerHelper.getProfileQuestionAnswers(new Set<Id>{currentEventId}, new Set<Id>{partRecordId});
  } */
  private void prepareMeetingRequestWrappers(){
     categorySelectOptions = new List<SelectOption>{new SelectOption('All','All')};
      selectedMeetingRequests = new List<MeetingRequestWrapper>();
      availableMeetingRequests = new List<MeetingRequestWrapper>();
     // prIdProfilesMap = new Map<Id,ProfileWrapper>();
      categoryQuestionId = new Set<Id>();
      selectedOneOnOne = 0;
      selectedBoardroom=0;

    System.debug('$$$$$$$$$$$$$44 '+allPRsForOppositeRType);

      for(Event_Account_Details__c partRecord: allPRsForOppositeRType.values()){
          MeetingRequestWrapper meetingRequest;
          Contact_Event_Answers__c shortAnswer = ContactEventAnswerHelper.getAnswerByPRandQType(partRecord.Id, 'Short',  answersByPRAndQType);
          Contact_Event_Answers__c category = ContactEventAnswerHelper.getAnswerByPRandQType(partRecord.Id, 'Category',  answersByPRAndQType);
          System.debug('#########category.Question__r.Question__c#33 '+category.Question__r.Question__c);
          categoryQuestionId.add(category.Question__r.Question__c);
          //List<Contact_Event_Answers__c> profileQuestions = partRecordIdProfileQuestions.get(partRecord.Id);
          ProfileWrapper profile = new ProfileWrapper(partRecord, shortAnswer, category, null);
          //prIdProfilesMap.put(partRecord.Id,profile);
          //System.debug('Part Record Event Ranking: '+ partRecordIdEventRankingMap);
          if(partRecordIdEventRankingMap != null){
            if(partRecordIdEventRankingMap.containsKey(String.valueOf(currentPartRecord.Id)+String.valueOf(partRecord.Id))){
              meetingRequest = new MeetingRequestWrapper(profile,partRecordIdEventRankingMap.get(String.valueOf(currentPartRecord.Id)+String.valueOf(partRecord.Id)));
              selectedMeetingRequests.add(meetingRequest);
              incrementCounts(meetingRequest);
            }else{
               meetingRequest = new MeetingRequestWrapper(profile,EventRankingHelper.createMeetingRequest(currentPartRecord, partRecord, currentEvent, currentContact));
               availableMeetingRequests.add(meetingRequest);
             }
          }  
      }
      if(categoryQuestionId.size() > 0) categorySelectOptions.addAll(QuestionsHelper.getCategoryOptions(categoryQuestionId));
      if(selectedMeetingRequests.size() > 0) selectedMeetingRequests.sort();
      if(availableMeetingRequests.size() > 0) availableMeetingRequests.sort();
     // System.debug('prIdProfilesMap' + prIdProfilesMap);
     System.debug('######categoryQuestionId###3 '+categoryQuestionId);
      System.debug('Select Options : '+ categorySelectOptions);
  }
  /*
  private List<MeetingRequestWrapper> getSelectedMeetingRequests(){
    return selectedMeetingRequests;
  }
  private List<MeetingRequestWrapper> getAvailableMeetingRequest(){
    return availableMeetingRequests;
  } */

  public void updateMeetingRequestSelectionOnChange() {
    deletedMeetingRequests = new Map<Id,SObject>();
    upsertMeetingRequests = new List<Event_Ranking__c>();
    categoryFilter = categoryFilter;

    Integer i = 0;
    
     while (i < availableMeetingRequests.size()) {
      //MeetingRequestWrapper mWrapper = availableMeetingRequests.get(i);
      MeetingRequestWrapper mWrapper = availableMeetingRequests[i];
      if(mWrapper.hasOneOnOne || mWrapper.hasBoardroom) {
        System.debug('Before Ranking: '+ mWrapper.meetingRequest);
        //if sometthing selected, move selection to selected list
        MeetingRequestWrapper removed = availableMeetingRequests.remove(i);
        if(removed.meetingRequest.Rank__c == null) removed.meetingRequest.Rank__c = selectedMeetingRequests.size() +1; //last
        removed.meetingRequest.Type__c = determineType(removed);
        selectedMeetingRequests.add(removed);
        System.debug('After Request Logic: '+ mWrapper.meetingRequest);
      } 
      i++;
    } 
    if(selectedMeetingRequests.size() > 0) selectedMeetingRequests.sort();
    // de - select

    i = 0;
    
    while (i < selectedMeetingRequests.size()) {
      MeetingRequestWrapper mRequest = selectedMeetingRequests.get(i); 

      if(!mRequest.hasOneOnOne && !mRequest.hasBoardroom) {
        
        MeetingRequestWrapper removed = selectedMeetingRequests.remove(i);
        removed.meetingRequest.Type__c = determineType(removed);
        availableMeetingRequests.add(removed);
        if(removed.meetingRequest.Id != null){
            deletedMeetingRequests.put(removed.meetingRequest.Id,removed.meetingRequest);
          }
      } 
      i++;
    }

    selectedOneOnOne = 0;
    selectedBoardroom = 0;
    selectedMeetingRequests.sort();
    i=1;
    for(MeetingRequestWrapper selected : selectedMeetingRequests){
      if(selected.hasBoardroom){
        selected.meetingRequest.Rank__c = i;
        i++;
      } else {
        selected.meetingRequest.Rank__c = null;
      }
      
      selected.meetingRequest.Type__c = determineType(selected);
      incrementCounts(selected);
      upsertMeetingRequests.add(selected.meetingRequest);
      
    }
    
    if(availableMeetingRequests.size() > 0) availableMeetingRequests.sort();
    SYstem.debug('Selected: '+ selectedMeetingRequests);
    System.debug('Available: '+ availableMeetingRequests);
    System.debug('Category Filter: '+ categoryFilter);
    if(selectedMeetingRequests.size() > 0 ) upsert upsertMeetingRequests;
    if(deletedMeetingRequests.size() > 0 ) delete deletedMeetingRequests.values();

    for(MeetingRequestWrapper mrWrapper: availableMeetingRequests){
      if(mrWrapper.meetingRequest.Id != null){
        mrWrapper.meetingRequest.Id = null;
      }
    }
  }

  private String determineType(MeetingRequestWrapper mRequest){
      String mType ='';
      if(mRequest.hasBoardroom && mRequest.hasOneOnOne){
        mType = 'Boardroom and One-on-One';
      } else if( mRequest.hasBoardroom){
        mType='Boardroom';
      } else if(mRequest.hasOneOnOne){
        mType='One-on-One';
      } else{
        mType = null;
      }
      return mType;
  } 

  public PageReference submitMeetingRequests(){
      System.debug('###########currentContact###33 '+currentContact);
    
    boolean isValidated = false;
    upsertMeetingRequests = new List<Event_Ranking__c>();
    deletedMeetingRequests = new Map<Id,SObject>();
      if(selectedMeetingRequests.size() > 0){
        selectedMeetingRequests.sort();
        System.debug('SOrted Meeting Requests: '+ selectedMeetingRequests);
        Integer rank = 1;
        for(MeetingRequestWrapper mRWrapper : selectedMeetingRequests){
          if(mRWrapper.hasBoardroom || mRWrapper.hasOneOnOne){
            System.debug('M Request Wrapper: '+mRWrapper);
            mRWrapper.meetingRequest.Type__c = determineType(mRWrapper);
            if(mRWrapper.hasBoardroom){
              mRWrapper.meetingRequest.Rank__c = rank;
              rank++;
              }else{
                mRWrapper.meetingRequest.Rank__c = null;
              }
            upsertMeetingRequests.add(mRWrapper.meetingRequest);
            
          }

        }
      }
      if(availableMeetingRequests.size() > 0){
        for(MeetingRequestWrapper mRWrapper : availableMeetingRequests){
          if(mRWrapper.meetingRequest.Id != null&& !mRWrapper.hasOneOnOne && !mRWrapper.hasBoardroom){
            deletedMeetingRequests.put(mRWrapper.meetingRequest.Id,mRWrapper.meetingRequest);
          }
        }
      }
      System.debug('Selected: '+ upsertMeetingRequests);
      System.debug('Deleted Meeting Requests: '+ deletedMeetingRequests);
      isValidated = validateRequests();
      if(isValidated){
        sendMeetingRequestEmail();
        currentPartRecord.Registration_Part_2_Submitted__c = true;
        currentPartRecord.Boardroom_Ranking__c = true;
        update currentPartRecord;
      }else{
          currentPartRecord.Boardroom_Ranking__c = true;
          update currentPartRecord;
      }
      
      if(upsertMeetingRequests.size()> 0) upsert upsertMeetingRequests;
      //if(deletedMeetingRequests.size() > 0) delete deletedMeetingRequests.values();
      //buildMeetingRequests(currentEvent.Id, currentPartRecord.Id);
    
        String reportURl = (UserInfo.getProfileId() == commSettings.Community_User_Profile__c)?'/pdf_registrationPage?':'apex/pdf_registrationPage?';
        PageReference pageRef = new PageReference('/' + ApexPages.currentPage().getUrl().substringBetween('apex/', '?'));
        pageRef.getParameters().put('eId', this.currentEvent.Id );
        pageRef.setRedirect(true);
        return pageRef;
  }

 /* public void sortSelectedRequest(){
    if(selectedMeetingRequests != null && selectedMeetingRequests.size() >0) selectedMeetingRequests.sort();
  } */

  private void incrementCounts(MeetingRequestWrapper mRequest){
    if(mRequest.hasOneOnOne) {
          selectedOneOnOne++; 
    }
    if(mRequest.hasBoardroom) {
        selectedBoardroom++;
    }
  }

  /*public void openProfileModal(){
      displayProfileModal = true;
  }
  public void closeProfileModal(){
      displayProfileModal = false;
  } */

  public void sendMeetingRequestEmail(){
    System.debug('Save and Submit called');
   // Community_Settings__c commSettings =  Community_Settings__c.getOrgDefaults();
    String reportURl = (UserInfo.getProfileId() == commSettings.Community_User_Profile__c)?'/pdf_MeetingRequestConfirmation':'apex/pdf_MeetingRequestConfirmation';
    //List<Contact> objConList = new List<Contact>();
    //objConList.add(currentContact);
    Set<String> objPrimaryEmail = new Set<String>();
    List<Order> contactcurrentList = [select id,Primary_Event_Contact__c,Company_Display_Name__c,Event_Account_Detail__c,Account.Name,BillToContact.email,OpportunityId  from order where BillToContactId =:currentContact.id  and Order_Event__c =:currentEvent.Id  limit 1 ];
    Set<Id> objConIdList = new set<Id>();
    List<Order> contactMasterList = [select id,Status,Company_Display_Name__c,Event_Account_Detail__c,BillToContact.email,BillToContactId,Grant_Edit_Permission__c,Primary_Event_Contact__c,Order_Event__c,Company_Name__c from order where BillToContactId !=:currentContact.id and (Primary_Event_Contact__c=true OR Status = 'Attending Event Contact' OR Status = 'Preliminary' OR (Grant_Edit_Permission__c = true and Status = 'Complete')) and 
                Order_Event__c =:currentEvent.Id and Event_Account_Detail__c =:contactcurrentList[0].Event_Account_Detail__c and Status != 'Cancelled'];
    
    for(Order objOrder : contactMasterList){
        objConIdList.add(objOrder.BillToContactId);
        if(objOrder.Primary_Event_Contact__c == true)
            objPrimaryEmail.add(objOrder.BillToContactId);
    }
    
    List<Contact> objConListSendMail = [Select id,FirstName,LastName,Email,Name from Contact where Id IN : objConIdList];
    objConListSendMail.add(currentContact);
    if(contactcurrentList.size() > 0){
       if(contactcurrentList[0].Primary_Event_Contact__c == true){
            objPrimaryEmail.add(currentContact.Id);
       }
    }
    
    EmailPdfReport pdfReport = new emailPdfReport(currentEvent,currentContact,reportURl,permissions,null,objConListSendMail,objPrimaryEmail);
    PageReference pageRef = new PageReference('/' + ApexPages.currentPage().getUrl().substringBetween('apex/', '?'));
    pageRef.getParameters().put('eId', this.currentEvent.Id );
    pageRef.getParameters().put('p2','true');
    //pageRef.setAnchor('p1');
    pageRef.setRedirect(true);
    try{
    pdfReport.sendMeetingRequestEmail();
    //verifyPart1Reg();
  

    }catch(customException ex){
      System.debug('Custom Exception: '+ ex);
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Validation Error: '+ex));
      //return null;
    }
    //return pageRef;
  }

  public Boolean validateRequests(){
    Boolean valid = false;
    if(permissions.isExecutive){
      valid = true;
    }
    if(permissions.isSupplier && (selectedOneOnOne == numberOneOnOnes) && (selectedBoardroom >= numberBoardRooms)){
      valid = true;
    }
    return valid;
  }

  public void setCategoryFilter(){
    this.categoryFilter = categoryFilter;
  }

  public String getCategoryFilter(){
    return categoryFilter;
  }


}