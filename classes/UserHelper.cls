public with sharing class UserHelper {
    
    public static Map<Id,User> getContactIdUserMap(Set<id> contactIds){
        Map<Id,User> contactIdUserMap = new Map<Id,User>();
        for(User u :[SELECT id, ContactId, UserName, Email FROM User where contactId IN :contactIds]){
            contactIdUserMap.put(u.contactId,u);
        }
        return contactIdUserMap;
    }

    public static User CreateUser(Id ProfileId, String firstName, String LastName, String Phone, String email, Id contactId) {
        System.debug('Enter CreateUser');
        List<string> split = Email.split('@', 0);
        System.debug('Split: '+ split);
        System.debug('FirstName: '+ firstName);
        System.debug('LastName: '+ lastName);
        System.debug('Phone: '+ phone);
        System.debug('Email: '+ email);
        System.debug('Contact Id: '+ contactId);
        //System.debug((UserInfo.getProfileId() == ProfileId)?true:false);
        User u = new User();
        
        System.debug('Mid CreateUser');
        u.ProfileId = profileId;
        u.Alias = split[0].length() > 5 ? split[0].substring(0, 5) : split[0];
        u.FirstName = FirstName;
        u.Phone = Phone;
        u.Email = Email;
        u.EmailEncodingKey = 'UTF-8';
        u.LastName = LastName;
        u.LanguageLocaleKey = 'en_US';
        u.LocalesIdKey = 'en_US';
        u.ContactId = ContactId;
        u.TimezonesIdKey = 'America/Los_Angeles';
        u.Username = email; 
        u.CommunityNickname = split[0] +'@'+ split[1] + UtilClass.GenerateRandomString(1);
        if (u.CommunityNickname.length() > 28) {
            u.CommunityNickname = u.CommunityNickname.substring(0, 28);
        }
        u.CommunityNickname = u.CommunityNickname + '_cpmg';
        
        return u;
    }

    @InvocableMethod(label ='Update User Name on Contact')
    public static void updateUserNameOnContact(List<User> newUsers)
    {
        Set<Id> uIds = new Set<Id>();
        for(User u : newUsers){
            uIds.add(u.Id);
        }
        UserHelper.updateUNameOnContact(uIds);
    }

    //@Future
    public static void updateUNameOnContact(Set<Id> newUsers){
        List<Contact> consToUpdate = new List<Contact>();
        //System.debug('Attempting Contact Community UserName Update');
        for(User u: [SELECT Id, ContactId, UserName,Email FROM USER WHERE Id in :newUsers]){
            if(u.ContactId != null){
                consToUpdate.add(new Contact(Id= u.ContactId, Community_User_Name__c = u.Username));    
            }
        }

        if(!consToUpdate.isEmpty()){
            //System.debug('Contacts to Update: '+ consToUpdate);
            System.enqueueJob( new SObjectHelper('Contact',consToUpdate));
            //SObjectHelper.updateRecords(consToUpdate);
        }
    }

    
}