public with sharing class ConnectingPointTemplateController {
    public String eventid { get; set; }
    public Product2 tempEvent {get;set;}
    public User userContactType {get;set;}
    public boolean regActive {get;set;}
    public Date part1Open {get;set;}
    public Boolean isEmailSeries {get;set;}
    public Boolean isEmailSeriesNotification {get;set;}
    public Boolean isRegistration {get;set;}
    public ConnectingPointTemplateController() {
       isRegistration = true;
        eventId = ApexPages.currentPage().getParameters().get('eId');
        if(String.isNotBlank(eventId)){ 
            tempEvent = EventHelper.getEvent(eventId);
            
            //regActive= (tempEvent.Registration_Part_1_Start__c <= System.Today())?true:false;
        } else{
            Id userId = UserInfo.getUserId();
            User currentUser = [SELECT Id, Name, ContactId, Contact.RecordTypeId FROM User where Id = :userId];
            if(currentUser.contactId != null){
                List<Order> orders = [SELECT Id, Order_Event__c,Status FROM Order WHERE (Order_Event__r.Start_Date__c >= Today AND BillToContactId =:currentUser.ContactId and Status != 'Cancelled')  ORDER BY Order_Event__r.Start_Date__c ASC LIMIT 1];
                if (orders != null && orders.size() > 0) {
                    Id pId = orders.get(0).Order_Event__c;
                    tempEvent = EventHelper.getEvent(pId);
                }
            } 
            if(tempEvent == null) tempEvent = EventHelper.getClosestEventByDate();
        }
        userContactType = [SELECT Id, Contact.RecordType.Name FROM User where ID = :UserInfo.getUserId()];
        
        if(userContactType.ContactId != null){
            Contact objCon = [Select id,Name,RecordTypeId,RecordType.Name from Contact where Id =: userContactType.ContactId limit 1];
            Id recTypeId = Schema.getGlobalDescribe().get('Planning_Series__c').getDescribe().getRecordTypeInfosByName().get(objCon.RecordType.Name).getRecordTypeId();
            Integer countEmailSeries =  [select count() from Planning_Series__c where Posted_Date__c !=null and RecordTypeId=:recTypeId
                                        and Event__c =:tempEvent.Id and Event__r.Planning_Series__c = true];
            
            if(countEmailSeries > 0)
                isEmailSeries = true;
                
                
             List<Order> objOrderList = [Select id,Alert_New_Post__c,Order_Event__c,BillToContactId from Order 
                                    where Order_Event__c =:tempEvent.Id and BillToContactId =:objCon.Id and Alert_New_Post__c = true];  
             if(countEmailSeries > 0 && objOrderList.size() > 0){
                 isEmailSeriesNotification = true;
             }                          
        }
        part1Open = (tempEvent != null && String.isNotBlank(userContactType.Contact.RecordType.Name) && userContactType.Contact.RecordType.Name == 'Supplier')?tempEvent.Registration_Part_1_Open_for_Supplier__c: tempEvent.Registration_Part_1_Open_for_Executive__c;
       // regActive = (tempEvent != null && tempEvent.Registration_Part_1_Start__c <= System.Today())?true:false;
        regActive = (part1Open != null && part1Open <= System.Today())?true:false;
        
        if(tempEvent != null && String.isNotBlank(userContactType.Contact.RecordType.Name)){
            if((userContactType.Contact.RecordType.Name == 'Supplier' && tempEvent.Registration_Part_1_Open_for_Supplier__c >System.Today() && tempEvent.Registration_Part_2_Open_For_Supplier__c >System.Today()) || 
                    (userContactType.Contact.RecordType.Name == 'Executive' && tempEvent.Registration_Part_1_Open_for_Executive__c >System.Today() && tempEvent.Registration_Part_2_Open_For_Executive__c >System.Today())){
                isRegistration = false;        
            }
        }

    }

    public Integer getCurrentPageIndex() {
        String currentPage = ApexPages.currentPage().getUrl();
        if(currentPage.containsIgnoreCase('CommunitiesHomePage')) return 1;
        if(currentPage.containsIgnoreCase('CommunitiesTravel')) return 2;
        if(currentPage.containsIgnoreCase('EventDirectory')) return 4;
        if(currentPage.containsIgnoreCase('CommunitiesMap')) return 5;
        if(currentPage.containsIgnoreCase('Registration')) return 6;
        if(currentPage.containsIgnoreCase('Events')) return 7;
        if(currentPage.containsIgnoreCase('EmailSeries')) return 8;
        return 0;
    }
}