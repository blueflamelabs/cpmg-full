@isTest
private class ImageHelper_test {
    
    @isTest static void testGetImagesByContact() {
        
        Account acct = new Account(Name='SFDC Account');
        insert acct;

        ID acctID = acct.ID;

        Contact con = new Contact(
        FirstName='Joe',
        LastName='Smith',
        Phone='415.555.1212',
        AccountId=acctID);

        Image__c myImage = new Image__c(Image_Type__c='Headshot', Contact__c = con.Id);
        insert myImage;

        test.startTest();
        List<Image__c> result = ImageHelper.getImagesByContact(con.Id);
        System.assert(resultContainsImg(result, myImage));
        test.stopTest();
    }
    
    @isTest static void testGetImagesByAccount() {
        Account acct = new Account(Name='SFDC Account');
        insert acct;

        ID acctID = acct.ID;

        Contact con = new Contact(
        FirstName='Joe',
        LastName='Smith',
        Phone='415.555.1212',
        AccountId=acctID);

        Image__c myImage = new Image__c(Image_Type__c='Logo', Contact__c = con.Id, Company__c=acct.Id);
        insert myImage;

        test.startTest();
        List<Image__c> result = ImageHelper.getImagesByAccount(acct.Id);
        System.assert(resultContainsImg(result, myImage));
        test.stopTest();
    }

    @isTest static void testGetImagesByParticipationRecords(){
        Account acct = new Account(Name='SFDC Account');
        insert acct;

        Contact con = new Contact(
        FirstName='Joe',
        LastName='Smith',
        Phone='415.555.1212',
        AccountId=acct.Id);


        Product2 myEvent = new Product2(Name='testEvent',User_Link_Expiration_for_Executives__c=10,User_Link_Expiration_for_Suppliers__c=10
);
        insert myEvent;

        Event_Account_Details__c myEventAccountDetail = new Event_Account_Details__c(Event__c=myEvent.Id, Account__c=acct.Id);
        insert myEventAccountDetail;

        Image__c myImage = new Image__c(
            Image_Type__c='Logo', 
            Contact__c = con.Id, 
            Company__c=acct.Id,
            Sponsorship_Image__c = true,
            Participation_Record__c = myEventAccountDetail.Id);
        insert myImage;

        Set<Id> recordIds = new Set<Id>();

        recordIds.add(myEventAccountDetail.Id);


        test.startTest();
        List<Image__c> result = ImageHelper.getImagesByParticipationRecords(recordIds);
        System.assert(resultContainsImg(result, myImage));
        test.stopTest();
    }

    private static boolean resultContainsImg(List<Image__c> result, Image__c myImage){
        for(Image__c img : result){
            if(img.Id == myImage.Id){
                return true;
            }
        }
        return false;
    }


    @isTest static void testCreateImage(){

        Account acct = new Account(Name='SFDC Account');
        insert acct;

        Contact con = new Contact(
        FirstName='Joe',
        LastName='Smith',
        Phone='415.555.1212',
        AccountId=acct.Id);


        Product2 myEvent = new Product2(Name='testEvent',User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert myEvent;

        Event_Account_Details__c myEventAccountDetail = new Event_Account_Details__c(Event__c=myEvent.Id, Account__c=acct.Id);
        insert myEventAccountDetail;


        Document myDocument = new Document(
            Name='testDocument',
            Body=Blob.valueOf('test body'),
            FolderId= UserInfo.getUserId());
        insert myDocument;

        Pricebook2 priceBook = new Pricebook2(Name='testPriceBook');

        Order myOrder = new Order(AccountId = acct.Id, OwnerId = UserInfo.getUserId(), Pricebook2Id = priceBook.Id, Status='Attending');

        Image__c result = ImageHelper.createImage(
            'testname', 
            acct.Id,
             con.Id, 
             myEvent, 
             myEventAccountDetail.Id, 
             myOrder.Id, 
             myDocument, 
             'type', 
              true);
        
        System.assertNotEquals(null, result);

    }
    
}