public without sharing class PurchasedSponsorshipHelper {

	public static List<Purchased_Sponsorship__c> getPurchasedSponsorshipsForPortalImages(String eventId){
		return [SELECT ID,
					   Event__c,
					   Sponsorship__c, 
					   Event_Account_Detail__c, 
					   Sponsorship__r.Display_On_Portal__c 
				FROM Purchased_Sponsorship__c where Event__c = :eventId and Sponsorship__r.Display_On_Portal__c=True];
	}
}