/*
    This class is Used to get a collection of Images that were uploaded by the current contact
*/
public without sharing class ImageHelper {

    public static List<Image__c> getImagesByContact(String contactId){
        return [SELECT Id, Contact__c, Event__c, File_Id__c, Selected__c FROM Image__c where Contact__c =: contactId and Image_Type__c='Headshot'];
    }

    public static List<Image__c> getImagesByAccount(String accountId){
        return [SELECT Id, Company__c, Event__c, File_id__c, Selected__c FROM Image__c where Company__c =:accountId and Image_Type__c='Logo'];
    }

    public static List<Image__c> getImagesByParticipationRecords(Set<Id> participationRecordIds){
        return [SELECT Id, Company__c, Company__r.Website, Company__r.Name, File_Id__c, Image_Type__c FROM Image__c where Participation_Record__c in : participationRecordIds and Image_Type__c='Logo' and Sponsorship_Image__c = true];
    }

    public static Image__c createImage(String p_name, Id accountId, id ContactId, Product2 event, Id ParticipationRecordId, Id orderId, Document doc, String type, Boolean selected){
          return new Image__c(Name = p_name,
                          Company__c = accountId,
                          Confirmation__c = orderId,
                          Contact__c = contactId,
                          Event__c = event.Id,
                          File_Id__c = doc.Id,
                          File_Name__c = doc.Name,
                          Image_Type__c =   type,
                          Participation_Record__c = participationRecordId,
                          Selected__c = selected);
  }
}