public without sharing class EventRankingHelper {
    
    public static Map<String,Event_Ranking__c> eventRankingMap {get;set;}

    public static Map<String,Event_Ranking__c> getEventRankingsForEvent(Id eventId, Id currentPRId){
        //eventRankingMap = new Map<String,List<Event_Ranking__c>>();
        eventRankingMap = new Map<String,Event_Ranking__c>();

        for(Event_Ranking__c meetingRequest : [SELECT Id, Comment__c, Event__c, 
                            Participation_Record_Registrant__c, 
                                                    Participation_Record_Registrant_Match__c, Rank__c, Type__c
                                            FROM Event_Ranking__c where 
                                            Event__c = :eventId and 
                                            Participation_Record_Registrant__c =: currentPRId]){
            //List<Event_Ranking__c> tempList = new List<Event_Ranking__c>();
            String tempKey = String.valueOf(meetingRequest.Participation_Record_Registrant__c) + String.valueOf(meetingRequest.Participation_Record_Registrant_Match__c);
            /*if(eventRankingMap.containsKey(tempKey)){
                tempList = eventRankingMap.get(tempKey);
                tempList.add(meetingRequest);
                eventRankingMap.put(tempKey, tempList);
            }else{ */
                //eventRankingMap.put(tempKey, new List<Event_Ranking__c>{meetingRequest});
                eventRankingMap.put(tempKey, meetingRequest);
            //}

        }
        return eventRankingMap;

    }

    public static Event_Ranking__c createMeetingRequest(Event_Account_Details__c registrantPR, Event_Account_Details__c matchedPR, Product2 currentEvent, Contact currentContact){
        return new Event_Ranking__c(
            Participation_Record_Registrant__c = registrantPR.Id
            ,Participation_Record_Registrant_Match__c = matchedPR.Id
            , Event__c = currentEvent.Id
            ,Registrant__c = currentContact.Id
            );
    }
}