@isTest
private class GenerateOpportunitiesController_Test {
    
    static testMethod void test_GenerateOppControllerConstructor() {
        // Implement test code
        PageReference pageRef = Page.Generate_Opportunities;

        Product2 prod = new Product2(Name = 'Test Event', Start_Date__c =System.Today(), ProductCode =' TE 17',User_Link_Expiration_for_Executives__c = 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert prod;

        Campaign_Opp_Settings__c campSettings = new Campaign_Opp_Settings__c(Days_To_Event__c = 21, StageName__c ='Prospect');
        insert campSettings;

        Campaign camp = new Campaign(Name ='Test Event Campaign', StartDate = System.Today(), Event__c = prod.Id );
        insert camp;

        test.StartTest();
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id', camp.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(camp);
            GenerateOpportunitiesController goc = new GenerateOpportunitiesController();
           
        test.StopTest();
    }
    
    @isTest static void test_GenerateOppController_Redirect() {
        // Implement test code

        PageReference pageRef = Page.Generate_Opportunities;
        
        Product2 prod = new Product2(Name = 'Test Event', Start_Date__c =System.Today(), ProductCode =' TE 17',User_Link_Expiration_for_Executives__c = 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert prod;

        Campaign_Opp_Settings__c campSettings = new Campaign_Opp_Settings__c(Days_To_Event__c = 21, StageName__c ='Prospect');
        insert campSettings;
        Account a = new Account(Name = 'Test Account 1', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId());
        insert a;
        Contact c = new Contact(FirstName ='Test', LastName ='Executive', AccountId = a.Id,RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId(), email ='test@test.com');
        Contact c2 = new Contact(FirstName ='Test2', LastName ='Executivetest', AccountId = a.Id,RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId(), email ='test2@test.com');
        Contact c3 = new Contact(FirstName ='Not Interested', LastName ='testExecutive', AccountId = a.Id,RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId(), email ='test3@test.com');
        insert c;
        insert c2;
        insert c3;
        
        Opportunity objOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),a,'Closed Won',Prod,'Onsite Renewal');
         objOpp.Primary_Contact__c = c.id;
         update objOpp;
        Campaign camp = new Campaign(Name ='Test Event Campaign', StartDate = System.Today(), Event__c = prod.Id );
        insert camp;

        CampaignMember cm1 = new CampaignMember(CampaignId = camp.Id, contactId = c.Id, Status = 'Sent');
        CampaignMember cm2 = new CampaignMember(CampaignId = camp.Id, contactId = c2.Id, Status = 'Sent');
        CampaignMember cm3 = new CampaignMember(CampaignId = camp.Id, contactId = c3.Id, Status = 'Not Interested');
        insert cm1;
        insert cm2;
        insert cm3;

        test.StartTest();
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id', camp.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(camp);
            GenerateOpportunitiesController goc = new GenerateOpportunitiesController(sc);
            goc.StartBactJob();
            goc.getMessage();
        test.StopTest();
    }
    
}