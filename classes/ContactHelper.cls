public without sharing class ContactHelper {
    /*public ContactHelper() {
        
    } */

    public static Contact commitCommunityContact(Contact c){
        System.debug('Contact to Create: '+ c);
        //insert c;
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = true;
        dml.DuplicateRuleHeader.runAsCurrentUser = true; 
        Database.insert(c, dml);
        return c;
    }

    public static Contact upsertCommunityContact(Contact c){
        if(String.isNotBlank(c.Id)){
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.allowSave = true;
            dml.DuplicateRuleHeader.runAsCurrentUser = true; 
            Database.update(c, dml);
        }else{
            Database.DMLOptions dml = new Database.DMLOptions();
            dml.DuplicateRuleHeader.allowSave = true;
            dml.DuplicateRuleHeader.runAsCurrentUser = true; 
            Database.insert(c, dml);
        }
        //upsert c;
        return c;
    }

    public static Map<Id,Contact> getContactMap(Set<id> contactIds){
        return new Map<Id,Contact>([SELECT Id, User_Create_Confirmation__c,Email, FirstName, LastName,Phone, AccountId, Account.Type,RecordTypeId FROM Contact Where Id IN :contactIds]);
    }

    public static Contact getContact(Id contactId){
        return [SELECT Id, FirstName,Email,LastName,AccountId, Account.Name, Title, MobilePhone, Community_User_Name__c FROM Contact where Id =:contactID];
    }

    
    public static boolean getContactByEmail(String email){
        List<Contact> conList = [Select Id, Email FROM Contact where Email =:email];
        return (conList.size()>0?true:false);
    }
    
    @InvocableMethod(Label ='Update User Email and UserName')
    public static void updateUserEmailAndUserName(List<Contact> updatedContacts){
        System.debug('Executing Update User Email and UserName');
        Set<Id> conIds = new Set<Id>();
        for(Contact con: updatedContacts){
            conIds.add(con.Id);
        }
        ContactHelper.updateUserEmailAndUserNameHelper(conIds);
    }
    @Future
    private static void updateUserEmailAndUserNameHelper(Set<Id> conIds){
        //Set<Id> conIds = new Set<Id>();
        Map<Id,User> contactUserMap;
        /*for(Contact con: contacts){
            conIds.add(con.Id);
        } */
        if(!conIds.isEmpty()){
            contactUserMap = UserHelper.getContactIdUserMap(conIds);
        }
        List<User> usersToUpdate = new List<User>();
        if(contactUserMap != null){
            for(Contact con: [SELECT Id, Email FROM Contact where Id in :conIds]){
                if(contactUserMap.containsKey(con.Id)){
                    User u = contactUserMap.get(con.Id);
                    u.put('UserName', con.Email);
                    u.put('Email',con.Email);
                    usersToUpdate.add(u);
                }
            }
        }
        if(!usersToUpdate.isEmpty()){
            //SObjectHelper.updateRecords(usersToUpdate);
            SobjectHelper sobHelper = new SObjectHelper('User',usersToUpdate);
            System.enqueueJob(sobHelper);
        }

    }
}