public class CancellationsOrderCTRL{
    public static void canOrder(Set<Id> objOppId){
        List<Event_Account_Details__c> objPR = [Select id,Opportunity__c,Status__c 
                                                    from Event_Account_Details__c where Opportunity__c IN : objOppId];
        for(Event_Account_Details__c objAccDetails : objPR){
            objAccDetails.Status__c = 'Cancelled';
        }    
        if(objPR.size() > 0)
            update objPR;                 
    }
    public static void RespondedCamMemberStatus(Set<Id> objOppId){
        Set<String> objAccountName = new Set<String>();
        Set<Id> objProductId = new Set<Id>();
        List<Opportunity> objOppList= [Select id,AccountId,Account.Name,Product__c from Opportunity where Id IN : objOppId];
        for(Opportunity objOpp : objOppList){
            objAccountName.add(objOpp.Account.Name);
            objProductId.add(objOpp.Product__c);
        }
        List<CampaignMember> objCamMember = [SELECT CampaignId,Campaign.Event__c,CompanyOrAccount,
                                             Status,Name,Id FROM CampaignMember where CompanyOrAccount IN : objAccountName and Campaign.Event__c IN : objProductId];
        
        Set<Id> objCampId = new Set<Id>();
        for(CampaignMember objCmMem : objCamMember){
            objCampId.add(objCmMem.CampaignId);
        } 
        
        Map<Id,String> objCamStatusCheck = new Map<Id,String>();
        List<CampaignMemberStatus> objCamMebStatus = [SELECT CampaignId,HasResponded,Label FROM CampaignMemberStatus where HasResponded = true and CampaignId IN : objCampId order by LastModifiedDate desc];
        for(CampaignMemberStatus objCamSta : objCamMebStatus){
            objCamStatusCheck.put(objCamSta.CampaignId,objCamSta.Label);
        }
        
        for(CampaignMember objCmMem : objCamMember){
            if(objCamStatusCheck.keySet().Contains(objCmMem.CampaignId)){
                objCmMem.Status = objCamStatusCheck.get(objCmMem.CampaignId);
            }
        }                               
        update objCamMember; 
    }
}