public class updateAccountContactRelationCTRL{
    public static void updateAccConRela(Set<Id> accId,Map<Id,Contact> objMapContact,Map<Id,Contact> objOldMapContact){
         List<AccountContactRelation> objAccConRelaRecord = [Select id,EndDate,IsActive,StartDate,Employment_Status__c,Email_Address__c,Title__c,ContactId,AccountId
                                                         from AccountContactRelation where ContactId IN : objMapContact.keySet() and
                                                         AccountId IN : accId];
                                                         
         for(AccountContactRelation objAccConRe : objAccConRelaRecord){
             objAccConRe.Email_Address__c = objMapContact.get(objAccConRe.ContactId).Email;
             objAccConRe.Title__c = objMapContact.get(objAccConRe.ContactId).Title;
             if(objMapContact.get(objAccConRe.ContactId).AccountId != objOldMapContact.get(objAccConRe.ContactId).AccountId)
                 objAccConRe.StartDate = date.today();
             objAccConRe.Employment_Status__c = 'Active';
             objAccConRe.IsActive = true;
             objAccConRe.EndDate = null;
         }    
         
         Set<Id> objAccOldId = new Set<Id>();
         for(Contact objConNew : objMapContact.values()){
             if(objConNew.AccountId != null && objConNew.AccountId != objOldMapContact.get(objConNew.id).AccountId){
                 objAccOldId.add(objOldMapContact.get(objConNew.id).AccountId);
             }
         }
         
         List<AccountContactRelation> objAccConRelaRecordOld = [Select id,Email_Address__c,Title__c,EndDate,IsActive,ContactId,Employment_Status__c 
                                                         from AccountContactRelation where ContactId IN : objMapContact.keySet() and 
                                                         AccountId IN : objAccOldId];
                                                         
         for(AccountContactRelation accCont: objAccConRelaRecordOld){ 
             //accCont.Email_Address__c = objMapContact.get(accCont.ContactId).Email;
             //accCont.Title__c = objMapContact.get(accCont.ContactId).Title;
              accCont.EndDate = date.today();
              accCont.Employment_Status__c = 'Inactive';
              accCont.IsActive = false;
         }  
         
         
         if(objAccConRelaRecord.size() > 0)
             update objAccConRelaRecord; 
         if(objAccConRelaRecordOld.size() > 0)
             update objAccConRelaRecordOld;  
             
                    
    }
    
    public static void updateContactTitle(List<Contact> objConList){
       ControllingTrigger.isRunAgain = true;
       Set<Id> objConid = new Set<Id>();  
       Id conRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();                                        
       for(Contact objCon : objConList){
           if(objCon.RecordTypeId == conRecordTypeId){
               objConid.add(objCon.Id);
           }    
       }
       if(objConid.size() > 0){
           List<Title_Reference_Table__c> objTitleRef = [Select id,Value_to_Replace__c,Replace_with_Value__c,
                                                            Add_Comma_After_Replaced_Value__c,Review_Order__c from Title_Reference_Table__c order by Review_Order__c asc];
           List<Contact> objUpdateCon = [Select id,Title,Seniority__c from Contact where Id IN : objConid];
           for(Contact objCon : objUpdateCon){
               if(String.isNotBlank(objCon.Title)){
                 if(objTitleRef.size() > 0){
                   Boolean isAvailable = false;
                   for(Title_Reference_Table__c objTReTable : objTitleRef){
                       if(objCon.Title.Contains(objTReTable.Value_to_Replace__c)){
                           isAvailable = true;
                           objCon.Seniority__c = objTReTable.Replace_with_Value__c;
                           String subValue = objCon.Title.substring(objTReTable.Value_to_Replace__c.length(), objCon.Title.length()).Trim();
                           if(objTReTable.Add_Comma_After_Replaced_Value__c == true)
                               objCon.Title = objTReTable.Replace_with_Value__c+', '+subValue;
                           else
                               objCon.Title = objTReTable.Replace_with_Value__c+subValue;    
                           break;
                       }
                   }
                   if(isAvailable == false){
                       objCon.Seniority__c = '';
                   }
                   isAvailable = false;
                 }else{
                     objCon.Seniority__c = '';
                 }
               }else{
                   objCon.Seniority__c = '';
               }
           }   
           if(objUpdateCon.size() > 0){
               update objUpdateCon;
               System.debug('$$$$$$$$ objUpdateCon %%%%%%%% '+objUpdateCon);
           }
           System.debug('@@@objTitleRef@@@@ '+objTitleRef);
       }
    }
    
    
}