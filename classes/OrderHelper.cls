public with sharing class OrderHelper {
	public static boolean isAttending(Order order, Order_Statuses__c settings) {
      	List<String> attendingStatuses = settings.Attending_picklist_values__c.split('\\|');

		for(Integer i = 0; i < attendingStatuses.size(); i++) {
			if(attendingStatuses.get(i).equalsIgnoreCase(order.Status)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isEventContact(Order order, Order_Statuses__c settings) {
      	if(settings != null){
		List<String> attendingStatuses = settings.Event_Contact_picklist_values__c.split('\\|');
		
			for(Integer i = 0; i < attendingStatuses.size(); i++) {
				if(attendingStatuses.get(i).equalsIgnoreCase(order.Status)) {
					return true;
				}
			}
		}
		return false;
	}
	public static Order getConfirmation(Id contactId, Id eventId){
		return [SELECT Id,Hotel_Confirmation__c, Airline_and_flight_URL__c, Ground_Transportation__c, status,
			Airline_and_flight__c FROM ORder where BillToContactId=:contactID and Order_Event__c =:eventId LIMIT 1];
	}
	//THIS WILL GET ALL CONFIRMATIONS FOR AN EVENT. WE NEVER want confirmations that have a vendor account.
	public static List<Order> getUserConfirmations(Id eventId, String contactRTypeId){
		String query = 'SELECT Id, Event_Account_Detail__c, Order_Event__c,'+
						'BillToContact.RecordTypeId,BillToContactId, Order_Event__r.isActive, Send_Email__c'+
						+', BillToContact.Account.Type, Email_Notification_User__c FROM Order WHERE BillToContact.Account.Type <> \'Vendor\' AND Send_Email__c <> True';
			 if(eventId != null) query += ' AND Order_Event__c =\''+eventId+'\'';
			 if(String.isNotBlank(contactRTypeId)) query +=' AND BillToContact.RecordTypeId =\''+contactRTypeId+'\'';
			 System.debug('Query String: '+ query);
		//List<Order> eventConfirmations = new List<Order>([SELECT Id, Event_Account_Detail__c, Order_Event__c,
						//BillToContact.RecordTypeId,BillToContactId, Order_Event__r.isActive, Send_EMail__c FROM Order]);
		List<Order> eventConfirmations = (List<Order>)Database.query(query);
		System.debug('Identified Event Confirmations: ' + eventConfirmations);
		return eventConfirmations;
	}
}