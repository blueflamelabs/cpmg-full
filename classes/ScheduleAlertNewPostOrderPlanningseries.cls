public class ScheduleAlertNewPostOrderPlanningseries Implements Schedulable{

    public void execute(SchedulableContext sc)
    {
        updateAlertPost();
    }
    public ScheduleAlertNewPostOrderPlanningseries(){}
    public static void updateAlertPost(){
        Set<Id> objEventId = new Set<Id>();
        Set<String> objRecName = new SEt<String>();
        List<Planning_Series__c> objPlanSer = [Select id,RecordType.Name,Event__c,Posted_Date__c from Planning_Series__c where Posted_Date__c = today];
        for(Planning_Series__c objPlan : objPlanSer){
            objEventId.add(objPlan.Event__c);
            objRecName.add(objPlan.RecordType.Name);
        }
        
        Set<Id> objRecTypeId = new Set<Id>();
        if(objRecName.size() >=2){
            Id RecordTypeIdSupp = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Id RecordTypeIdEx = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
            objRecTypeId.add(RecordTypeIdSupp);
            objRecTypeId.add(RecordTypeIdEx);
        }
        if(objRecName.size() == 1){
            string RecName =(new List<String>(objRecName))[0];
            Id RecordtyId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(RecName).getRecordTypeId();
            objRecTypeId.add(RecordtyId);
        }
        
        if(objEventId.size() > 0){
            List<Order> objOrderList = [Select id,Alert_New_Post__c,Order_Event__c,Status from Order 
                                            where Order_Event__c IN : objEventId and Status != 'Cancelled' and BillToContact.RecordTypeId IN : objRecTypeId];
            if(objOrderList.size() > 0 ){
                for(Order objOrd : objOrderList){
                    objOrd.Alert_New_Post__c = true;
                }
                update objOrderList;
            }
        }
    }
    
    public static void updateOrderbyPlaningSeries(Set<Id> objSetPlanningSeriesId){
        Set<Id> objEventId = new Set<Id>();
        Set<String> objRecName = new SEt<String>();
        List<Planning_Series__c> objPlanSer = [Select id,RecordType.Name,Event__c,Posted_Date__c from Planning_Series__c 
                                                    where Id IN : objSetPlanningSeriesId and Posted_Date__c <= today];
        for(Planning_Series__c objPlan : objPlanSer){
            objEventId.add(objPlan.Event__c);
            objRecName.add(objPlan.RecordType.Name);
        }
        Set<Id> objRecTypeId = new Set<Id>();
        if(objRecName.size() >=2){
            Id RecordTypeIdSupp = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Id RecordTypeIdEx = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
            objRecTypeId.add(RecordTypeIdSupp);
            objRecTypeId.add(RecordTypeIdEx);
        }
        if(objRecName.size() == 1){
            string RecName =(new List<String>(objRecName))[0];
            Id RecordtyId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(RecName).getRecordTypeId();
            objRecTypeId.add(RecordtyId);
        }
        
        if(objEventId.size() > 0 && Test.isRunningTest() == false){
            List<Order> objOrderList = [Select id,Alert_New_Post__c,Order_Event__c,Status from Order 
                                            where Order_Event__c IN : objEventId and Status != 'Cancelled' and BillToContact.RecordTypeId IN : objRecTypeId];
            if(objOrderList.size() > 0 ){
                for(Order objOrd : objOrderList){
                    objOrd.Alert_New_Post__c = true;
                }
                update objOrderList;
            }
        }
    }
}