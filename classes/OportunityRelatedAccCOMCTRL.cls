public class OportunityRelatedAccCOMCTRL {
	@AuraEnabled 
    public static String showContactRecord(Id AccsId){
        Account objAcc = [select Id,RecordType.Name,Name  from Account where Id =:AccsId];
        String OppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(objAcc.RecordType.Name).getRecordTypeId();
        String AccRecord = OppRecordTypeId+','+objAcc.Id+','+objAcc.Name+','+objAcc.RecordType.Name;
        return AccRecord;
    }
}