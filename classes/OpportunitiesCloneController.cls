public class OpportunitiesCloneController {
    public Id oppId{set;get;}
    public String isContactRoleClone{set;get;}
    public String isErrorMessage{set;get;}
    public OpportunitiesCloneController(ApexPages.StandardController std) {
        oppId = (Id)std.getId();
    }
    public PageReference CloneOpportunity(){
     if(oppId != null){
            String queryOpp = 'SELECT ';
            Map<String, Schema.SObjectField> fieldMapOpportunity = Schema.SObjectType.Opportunity.fields.getMap();
            for(Schema.SObjectField OppField : fieldMapOpportunity.Values()){
                schema.describefieldresult dfr = OppField.getDescribe();
                queryOpp += dfr.getname() + ',';
            }
           Opportunity objSourceOpp =  Database.query(queryOpp.removeEnd(',')+' FROM Opportunity WHERE Id =\'' + oppId + '\'');
           Opportunity objOpp = [Select id,Product__c,Product__r.Family,Product__r.Year__c from Opportunity where Id =: oppId];
           String CurrentYear = String.valueOf(Integer.valueOf(objOpp.Product__r.Year__c)+1);
           
           //String CreateEventName = objOpp.Product__r.Family+' '+CurrentYear;
           
           List<Product2> objProd = [select id,Name,Family,Year__c,Test_Event__c from Product2 where Family =: objOpp.Product__r.Family and Year__c =: CurrentYear and Test_Event__c = false];
           Opportunity CloneOpp = objSourceOpp.Clone();
           if(objProd.size() > 0)
               CloneOpp.Product__c = objProd[0].id;
           else
               CloneOpp.Product__c = null;
           Date CloseDateOPP = objSourceOpp.CloseDate;
           Date CurrentDate = CloseDateOPP.addYears(1);
           CloneOpp.CloseDate = CurrentDate;
           CloneOpp.StageName = 'Initial';
           CloneOpp.Lost_Reason__c = null;
           CloneOpp.Lost_Reason_Details__c = null;
           CloneOpp.Amount = null;
           CloneOpp.Supplier_Opp_Count__c = null;
           if(isContactRoleClone == 'false')
               CloneOpp.Primary_Contact__c = null;
         try{    
           insert CloneOpp;
         }catch(Exception ex){
             if(ex.getMessage().contains('An Opportunity for this event and contact already exists')){
                 isErrorMessage = 'An Opportunity for this event and contact already exists. You may not create a Duplicate.';
                 return null;
             }
         }  
           List<OpportunityContactRole> OppContactRoleDelete = [select id from OpportunityContactRole where OpportunityId =: CloneOpp.id];
           if(OppContactRoleDelete != null && OppContactRoleDelete.size() > 0)
               delete OppContactRoleDelete;
           if(isContactRoleClone == 'true'){
               List<OpportunityContactRole> CloneOppContactRole = new List<OpportunityContactRole>();
               List<OpportunityContactRole> OppContactRole = [select id,IsPrimary,Role,ContactId,OpportunityId  from OpportunityContactRole where OpportunityId =: oppId];
               for(OpportunityContactRole objOppContact : OppContactRole){
                   OpportunityContactRole objOppCon = new OpportunityContactRole();
                   objOppCon.IsPrimary = objOppContact.IsPrimary;
                   objOppCon.Role = objOppContact.Role;
                   objOppCon.ContactId = objOppContact.ContactId ;
                   objOppCon.OpportunityId = CloneOpp.id;
                   CloneOppContactRole.add(objOppCon);
               }
              if(CloneOppContactRole != null && CloneOppContactRole.size() > 0)
                  insert CloneOppContactRole;
           }
        PageReference acctPage = new PageReference('/'+CloneOpp.id);
        acctPage.setRedirect(true);
        return acctPage;
      }
      return null;
    }
}