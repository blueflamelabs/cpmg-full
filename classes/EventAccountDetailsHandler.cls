public with sharing class EventAccountDetailsHandler {

    private set<Id> eventIds {get;set;}
    //private set<Id> packageIds {get;set;}

    private Map<Id,Product2> eventMap {get;set;}
    public static boolean hasExecuteProductSoldProcess = false;
    private Map<Id, Event_Account_Details__c> allEADsMap {get;set;}

   public void executeProductSoldProcess(Map<Id,Event_Account_Details__c> newEVAMap){
        EventAccountDetailsHandler.hasExecuteProductSoldProcess = true;
        getEventAndProductIDs(newEVAMap);
        getEvents();
        getAllEADs();
        calculateProductSoldFields();
        commitProductUpdates();
    }
    
    public void getEventAndProductIDs(Map<Id,Event_Account_Details__c> newEVAMap){
    
        eventIds = new Set<id>();
        //packageIds = new Set<Id>();
        
        for(Event_Account_Details__c ead: newEVAMap.values()){
            if(ead.Event__c != null && !eventIds.contains(ead.Event__c)) eventIds.add(ead.Event__c);
            //if(ead.Package_Selection__c != null && !packageIds.contains(ead.Package_Selection__c)) packageIds.add(ead.Package_Selection__c);
        }
    }
    
    private void getEvents(){
        eventMap = new Map<Id,Product2>();
        if(eventIds.size() > 0){
            for(Product2 prod:[SELECT Id, Badges_Sold__c, Board_Rooms_Sold__c, Theatres_Sold__c FROM Product2 where ID in :eventIds]){
                prod.Badges_Sold__c = 0;
                prod.Board_Rooms_Sold__c= 0;
                prod.Theatres_Sold__c = 0;
                eventMap.put(prod.Id, prod);
            }
        }
    }

    private void getAllEADs(){
        allEADsMap = new Map<Id, Event_Account_Details__c>();
        if(eventIds.size() > 0){
            for(Event_Account_Details__c ead: [SELECT Id, Event__c, Badge_Total__c,Boardrooms__c,Theatres__c FROM Event_Account_Details__c WHERE event__c in : eventIds]){
                allEADsMap.put(ead.Id, ead);
            }
        }
    }
    /*
    private void getEventPackageInventories(){
        eventPackageInventoryMap = new Map<Id,Event_Package_Inventory__c>();
        if(packageIds.size() > 0){
            eventPackageInventoryMap = [SELECT ID FROM Event_Package_Inventory__c WHERE Id in: packageIds];
        }
    } */
    
    private void calculateProductSoldFields(){
        if(allEADsMap.size() > 0){
            for(Event_Account_Details__c ead: allEADsMap.values()){
                if(eventMap.containsKey(ead.Event__c)){
                    Product2 prod = eventMap.get(ead.Event__c);
                    prod.Badges_Sold__c = determineNumberSold(Integer.valueOf(prod.Badges_Sold__c), Integer.valueOf(ead.Badge_Total__c));
                    prod.Board_Rooms_Sold__c = determineNumberSold(Integer.valueOf(prod.Board_Rooms_Sold__c), Integer.valueOf(ead.Boardrooms__c));
                    prod.Theatres_Sold__c = determineNumberSold(Integer.valueOf(prod.Theatres_Sold__c), Integer.valueOf(ead.Theatres__c));
                    eventMap.put(prod.ID, prod);
                }
            }
        }
    }
    
    private void commitProductUpdates(){
        if(eventMap.size() > 0){
            update eventMap.values();
        }
    }
    

    private Integer determineNumberSold(Integer productField, Integer eadField){
        //Integer numberSold;
        if(productField == 0){
            productField = eadField;
        }else{
            productField += eadField;
        }
        return productField;
    }
}