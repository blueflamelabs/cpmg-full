public with sharing class GeneralUtilities {
	public static final Pattern validPhoneNumber = Pattern.compile('^[+]?(?:[0-9]*[\\.\\s\\-\\(\\)]|[0-9]+){3,24}$');


	public static boolean isValidPhoneNumber(String phoneNumber) {
      return validPhoneNumber.matcher(phoneNumber).matches();
  }

  public static String getFormattedPhoneNumber(String phoneNumber) {
   if(phoneNumber.length() == 10 && phoneNumber.isNumeric()) {
      // Is a US phone number without the formatting, so formatting.
      return String.format('({0}) {1}-{2}',
        new String[]{phoneNumber.substring(0, 3), phoneNumber.substring(3, 6), phoneNumber.substring(6, 10)});
    } else {
      // Assume that it is formated. If it gets here there are so many
      // international numbers that it would be very challenging to validate.
      return phoneNumber;
    }
  }

  public static void sendNotificationEmail(ID theObject, String subject, String bodyText, String toEmail) {

    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
    email.setSubject(subject);
    email.setPlainTextBody(bodyText);
    email.setWhatId(theObject);
    email.setToAddresses(new String[] {toEmail});

    Messaging.reserveSingleEmailCapacity(1);

    Messaging.sendEmail(new Messaging.Email[] { email } );
  }

  /**
   * Gets a database query with all of the fields.
   *
   * fields: Fields from the fieldset. Optional
   * otherFields: A set of field names that need to be in it. Required.
   * sobjectType: The type of sobject
   * whereClause: The entire where clause minus the "where"
   */
   // TODO: Test this
  public static String getSObjectSelectDatabaseQuery(List<Schema.FieldSetMember> fields, Set<String> otherFields, String sobjectType, String whereClause) {
    String query = 'SELECT ';

    for(Schema.FieldSetMember field : fields) {
      otherFields.add(field.getFieldPath());
    }

    boolean isFirst = true;
    for(String fieldName : otherFields) {
      if(!isFirst)
        query += ', ';
      isFirst = false;
      query += fieldName;
    }
    if(sobjectType=='Contact' && !query.contains('AccountId')){
      query+=',AccountId';
    }

    return query + ' FROM ' + sobjectType + ' WHERE ' + whereClause;
  }
  /*
    This uses the field set definition. If it is marked as required, we add it to the map, and return the map.
  */
  public static Map<String,String> getRequiredFieldsFromFieldSet(List<Schema.FieldSetMember> fields){
    Map<String,String> requiredFieldMap = new Map<String,String>();
    for(FieldSetMember fsMem : fields){
      if(fsMem.getRequired()){
        requiredFieldMap.put(fsMem.getFieldPath(), fsMem.getFieldPath());
      }
    }
    return requiredFieldMap;
  }
  /*
  This will validate th
  */
  public static Boolean validateRequiredFields(Map<String,String> requiredFieldMap, SObject objectToVerify){
      Map<String, Object> populatedObjectMap = objectToVerify.getPopulatedFieldsAsMap();
      System.debug('Populated Object Field Map: '+ populatedObjectMap);
      Boolean hasError = false;
      for(String s : requiredFieldMap.values()){
        if(!populatedObjectMap.containsKey(s)){
          hasError = true;
          throw new customException('The following Field is Required: '+s+'. Please fill it out to complete registration.');
        }
      }
    return hasError;
  }
  public static QuestionWrapper[] combineAllQuestions(Set<registrationQuestionsController> regQuestionControllers){
      List<QuestionWrapper> allRegistrationQuestions = new List<QuestionWrapper>();
      for(registrationQuestionsController regQuestController : regQuestionControllers){
        for(QuestionWrapper qw: regQuestController.questionsList){
          allRegistrationQuestions.add(qw);
        }
      }
      return allRegistrationQuestions;

  } 
}