public class EventActiveCTRL{
    
    public Product2 objProduct{set;get;}
    public boolean ischeck{set;get;}
    public EventActiveCTRL( ApexPages.StandardController stdController ) {
        objProduct = (Product2)stdController.getRecord(); 
        List<product2> listPro  = [select id,isActiveCheck__c,IsActive from Product2 where isActiveCheck__c = true and IsActive = false and id =:objProduct.id];
        if(listPro.size()>0){
            ischeck = true;
        }      
    }
   public void updateProduct(){
       List<product2> listPro  = [select id,isActiveCheck__c,IsActive from Product2 where id =:objProduct.id];
       for(Product2 objPro : listPro){
           objPro.isActiveCheck__c = false;
       }
       update listPro;
   }
   public void updateNotProduct(){
       List<product2> listPro  = [select id,isActiveCheck__c,IsActive from Product2 where id =:objProduct.id];
       for(Product2 objPro : listPro){
           objPro.IsActive = true;
           objPro.isActiveCheck__c = false;
       }
       update listPro;
   }
}