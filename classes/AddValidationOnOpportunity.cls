/*******************************/
/***
    Who  : VSCIDev
    What : Add validation for duplicate opportunity based on account's record type 
***/
/*******************************/
public class AddValidationOnOpportunity{
    public static void addValidation(list<opportunity> triggerNew){
        set<Id> accountId = new set<Id>();
        set<Id> contactId = new set<Id>();
        set<Id> eventId = new set<Id>();
        
        for(opportunity opp : triggerNew){

            boolean flag = false;
            if(trigger.isUpdate)
            if(opp.accountId != ((map<Id,opportunity>)trigger.oldmap).get(opp.id).accountId ||
                   opp.Primary_Contact__c != ((map<Id,opportunity>)trigger.oldmap).get(opp.id).Primary_Contact__c ||
                   opp.Product__c != ((map<Id,opportunity>)trigger.oldmap).get(opp.id).Product__c
                )
                    flag = true;
            
            system.debug(flag );
            
            if(opp.accountId !=null && ((trigger.isUpdate && flag  ) || trigger.isInsert))
                accountId.add(opp.accountId);
            if(opp.Primary_Contact__c!=null && ((trigger.isUpdate && flag ) || trigger.isInsert))
                contactId.add(opp.Primary_Contact__c);
            if(opp.Product__c !=null && ((trigger.isUpdate && flag ) || trigger.isInsert))
                eventId.add(opp.Product__c);                
        }
        set<string> setOfDuplicate = new set<string>();
        map<string,integer> mapOfCountOpportunity = new  map<string,integer>();
         if(accountId.size()>0 || contactId.size()>0 || eventId.size()>0){
            for(opportunity opp : [select id,Primary_Contact__c,Product__c,accountId,Account_Record_Type__c from opportunity where accountId in:accountId and
                Primary_Contact__c in:contactId and
                Product__c in:eventId]
                ){
                if(opp.Account_Record_Type__c =='Executive')
                    setOfDuplicate.add(opp.accountId+''+opp.Primary_Contact__c+''+opp.Product__c);
                
                if(opp.Account_Record_Type__c =='Supplier'){
                    string key = opp.accountId+''+opp.Primary_Contact__c+''+opp.Product__c;
                    if(!mapOfCountOpportunity.keyset().contains(key))
                        mapOfCountOpportunity.put(key,0);
                    mapOfCountOpportunity.put(key,mapOfCountOpportunity.get(key)+1);
                }
            }
            
            for(opportunity opp : triggerNew){
                string key = opp.accountId+''+opp.Primary_Contact__c+''+opp.Product__c;
                if(setOfDuplicate.contains(key))
                    opp.addError('An Opportunity for this event and contact already exists. You may not create a duplicate.');
                if(mapOfCountOpportunity.keyset().contains(key))
                    opp.Supplier_Opp_Count__c = mapOfCountOpportunity.get(key)+1;
                else
                    opp.Supplier_Opp_Count__c = null;
            }
        }
    }
}