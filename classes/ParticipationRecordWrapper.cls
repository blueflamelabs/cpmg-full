/*
* THIS CLASS WILL BE USED TO CONSOLIDATE Participation Records (Event_Account_Details__c) and the  Questions answers for the event directory.
* IT's PURPOSE IS TO PROVIDE A SINGLE POINT OF ACCESS FOR THE PR, Short Company Description and Long Company Description.
* WE ALSO ARE POPULATING THE CATEGORY SELECTION AS WELL.
*/

public with sharing class ParticipationRecordWrapper {
	public Event_Account_Details__c participationRecord {get;set;}
	public Contact_Event_Answers__c longProfile {get;set;}
	public Contact_Event_Answers__c shortProfile {get;set;}
	public Contact_Event_Answers__c categoryQuestion {get;set;}
	public Map<String,Object> jsonMap {get;set;}
	public String accountURL {get;set;}
	
	public ParticipationRecordWrapper(Event_Account_Details__c participationRecord, Contact_Event_Answers__c longProfile, Contact_Event_Answers__c shortProfile, Contact_Event_Answers__c categoryQuestion) {
		this.participationRecord = participationRecord;
		this.longProfile = longProfile;
		this.shortProfile = shortProfile;
		this.categoryQuestion = categoryQuestion;
		this.accountURL = participationRecord.Account__r.Website;

		setJSONString();
	}

	public void setJSONString(){
		jsonMap = new Map<String,Object>();
		Map<String,Object> descAnswers = new Map<String,Object>();
		descAnswers.put('short', (String.isNotBlank(shortProfile.Answer__c))?shortProfile.Answer__c:'');
		descAnswers.put('long', (String.isNotBlank(longProfile.Answer__c))?longProfile.Answer__c:'');
		descAnswers.put('category',(String.isNotBlank(categoryQuestion.Answer__c))?categoryQuestion.Answer__c:'');
		jsonMap.put('company',(String.isNotBlank(participationRecord.Company_Display_Name__c))?participationRecord.Company_Display_Name__c:participationRecord.Account__r.Name);
		jsonMap.put('location',participationRecord.Location__c);
		jsonMap.put('desc',descAnswers);
		jsonMap.put('url',accountURL); //EncodingUtil.URLENCODE(Utilities.formatExternalLinkULR(participationRecord.Account__r.Website),'UTF-8');   //participationRecord.Account__r.Website
		jsonMap.put('title',participationRecord.Primary_Contact__r.title);
		jsonMap.put('name', participationRecord.Primary_Contact__r.Name);
	}
}