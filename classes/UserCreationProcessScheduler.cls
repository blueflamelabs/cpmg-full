global class UserCreationProcessScheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        
        UserCreationProcessBatch userCreationBatch = new UserCreationProcessBatch();
        database.executeBatch(userCreationBatch,1);
        
        

    }
}