@IsTest
    public class registrationQuestionsController_Test{
         static testmethod void testmethod1(){
         TestDataGenerator.createInactiveTriggerSettings();
         Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
         Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

         Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
         c.Has_Event__c = true;
         update c;
         Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
        
        //Start Defining Event "Product" related info.
        Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec@cpmg.com',null);
        User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier@cpmg.com',null);

        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');

        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

        Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,Date.today(),ead,prod ,true); 
       //Start Questions
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),false,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav4Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option4','Select4');
            Question_Answers_Values__c qav5Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option5','Select5');
            Question_Answers_Values__c qav6Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option6','Select6');
        //Supplier
        Question__c supplierQuestion = TestDataGenerator.createQuestion(true,'Test Supplier Question',2000,false,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);

        //Executive Golf
        Question__c execActivityQuestion = TestDataGenerator.createQuestion(true,'Test Golf Pickilist',200,true,'Activity',questionRTypeMap.get('Question').getRecordTypeId(),false,true);
       
        //Create Event Questions
        Event_Question__c eQuestion = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);
        eQuestion.Display_Type__c='Areas of Interest';
        //update eQuestion;
        //UserPermissions uPermissions = 

        Test.startTest();
            registrationQuestionsController regQuestionController= new registrationQuestionsController();
            regQuestionController.currentEvent = prod;
            regQuestionController.currentContact = c;
            regQuestionController.currentConfirmation = conf;
            regQuestionController.eventId = prod.Id;
            regQuestionController.contactId = c.Id;
            regQuestionController.currentContactRecordType = 'Executive';
            regQuestionController.phase ='Category';
            List<QuestionWrapper> questionList = regQuestionController.getQuestionList();
            regQuestionController.saveAnswers();
        Test.stopTest();
    } 
     static testmethod void testmethod2(){
      //Implement test code
        TestDataGenerator.createInactiveTriggerSettings();
        Id supplierAccRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id supplierConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();

        Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account',supplierAccRtype);

        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) supplierConRtype);
        c.Has_Event__c = true;
        update c;
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
        
        //Start Defining Event "Product" related info.
        Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec@cpmg.com',null);
        User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier@cpmg.com',null);

        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');

        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

        Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Preliminary',opp,Date.today(),ead,prod,true);
        
        //Start Questions
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),false,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav4Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option4','Select4');
            Question_Answers_Values__c qav5Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option5','Select5');
            Question_Answers_Values__c qav6Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option6','Select6');
        //Supplier
        Question__c supplierQuestion = TestDataGenerator.createQuestion(true,'Test Supplier Question',2000,false,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);

        //Executive Golf
        Question__c execActivityQuestion = TestDataGenerator.createQuestion(true,'Test Golf Pickilist',200,true,'Activity',questionRTypeMap.get('Question').getRecordTypeId(),false,true);
        
        //Create Event Questions
        Event_Question__c eQuestion1 = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',picklistQuestion,true,null,null);
        //Event_Question__c eQuestion2 = TestDataGenerator.createEventQuestion(true,'The Supplier Text Question',prod,'Category',supplierQuestion,true,null,null);
        //Event_Question__c eQuestion = TestDataGenerator.createEventQuestion(true,'The Mulit Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);
        //eQuestion.Display_Type__c='Primary Area of Interest';
        //update eQuestion ;
        //Event_Question__c eQuestion3 = TestDataGenerator.createEventQuestion(true,'The Mulit Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);
        //eQuestion3.Display_Type__c='Areas of Interest';
        //update eQuestion3 ;
       
        //Event_Question__c eQuestion1 = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',picklistQuestion,true,null,null);
        UserPermissions uPermissions = new UserPermissions('Supplier',true);
        
        Question_Answers_Values__c objquAns = new Question_Answers_Values__c();
        objquAns.Name = 'test';
        objquAns.Question__c = execActivityQuestion.id;
        insert objquAns;
        
        
        Test.startTest();
            registrationQuestionsController regQuestionController= new registrationQuestionsController();
            regQuestionController.currentEvent = prod;
            regQuestionController.currentContact = c;
            regQuestionController.currentConfirmation = conf;
            regQuestionController.eventId = prod.Id;
            regQuestionController.contactId = c.Id;
            regQuestionController.currentContactRecordType = 'Supplier';
            regQuestionController.phase ='Category';
            //regQuestionController.isPrimaryAreaofInterest=true;
            //regQuestionController.EventQuestionID=eQuestion.id;
           
            List<QuestionWrapper> questionList = regQuestionController.getQuestionList();
            
           
        Test.stopTest();
    }
    
    static testmethod void testmethod2Else(){
      //Implement test code
        TestDataGenerator.createInactiveTriggerSettings();
        Id supplierAccRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id supplierConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();

        Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account',supplierAccRtype);

        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) supplierConRtype);
        c.Has_Event__c = true;
        update c;
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
        
        //Start Defining Event "Product" related info.
        Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec@cpmg.com',null);
        User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier@cpmg.com',null);

        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');

        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

        Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Preliminary',opp,Date.today(),ead,prod,true);
        
        //Start Questions
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),false,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav4Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option4','Select4');
            Question_Answers_Values__c qav5Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option5','Select5');
            Question_Answers_Values__c qav6Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option6','Select6');
        //Supplier
        Question__c supplierQuestion = TestDataGenerator.createQuestion(true,'Test Supplier Question',2000,false,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);

        //Executive Golf
        Question__c execActivityQuestion = TestDataGenerator.createQuestion(true,'Test Golf Pickilist',200,true,'Activity',questionRTypeMap.get('Question').getRecordTypeId(),false,true);
        
        //Create Event Questions
         Event_Question__c eQuestion = TestDataGenerator.createEventQuestion(true,'The Mulit Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);
        eQuestion.Display_Type__c='Primary Area of Interest';
        //update eQuestion ;
        //Event_Question__c eQuestion3 = TestDataGenerator.createEventQuestion(true,'The Mulit Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);
        //eQuestion3.Display_Type__c='Areas of Interest';
        //update eQuestion3 ;
       //Event_Question__c eQuestion1 = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',picklistQuestion,true,null,null);
        //Event_Question__c eQuestion2 = TestDataGenerator.createEventQuestion(true,'The Supplier Text Question',prod,'Category',supplierQuestion,true,null,null);
        
        //Event_Question__c eQuestion1 = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',picklistQuestion,true,null,null);
        UserPermissions uPermissions = new UserPermissions('Supplier',true);
        
        Question_Answers_Values__c objquAns = new Question_Answers_Values__c();
        objquAns.Name = 'test';
        objquAns.Question__c = execActivityQuestion.id;
        insert objquAns;
        
        
        Test.startTest();
            registrationQuestionsController regQuestionController= new registrationQuestionsController();
            regQuestionController.currentEvent = prod;
            regQuestionController.currentContact = c;
            regQuestionController.currentConfirmation = conf;
            regQuestionController.eventId = prod.Id;
            regQuestionController.contactId = c.Id;
            regQuestionController.currentContactRecordType = 'Supplier';
            regQuestionController.phase ='Category';
            //regQuestionController.isPrimaryAreaofInterest=true;
            //regQuestionController.EventQuestionID=eQuestion.id;
           
            List<QuestionWrapper> questionList = regQuestionController.getQuestionList();
           
        Test.stopTest();
    }
    
    static testMethod void testMethod3(){
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();

        //AccountFormSettings
        TestDataGenerator.createAccountFormSettings();

        //Contact Form Settings:
        TestDataGenerator.createContactFormSettings();

        //Order Form Display settings
        TestDataGenerator.createOrderFormSettings();

        //Registration Settings
        TestDataGenerator.createRegistrationSettings();

        //Need the Order Status Custom Setting
        TestDataGenerator.createOrderStatusCustomSetting();
        
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execCommUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExecUser@cpmg.com',c.Id);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));

        Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);

        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');


        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        List<Order> ordersJustCreated = new List<Order>{conf};
        conf.Primary_Event_Contact__c = false;

        Document d1 = TestDataGenerator.createDocument('test1','text/plain','test1');
        Document d2 = TestDataGenerator.createDocument('test2','text/plain','test2');
        Document d3 = TestDataGenerator.createDocument('test3','text/plain','test3');
        Document d4 = TestDataGenerator.createDocument('test4','text/plain','test4');

        Image__c img1 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d1.Id,partRecord,true,false,true);
        Image__c img2 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d2.Id,partRecord,true,false,true);
        Image__c img3 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d3.Id,partRecord,true,false,true);
        Image__c img4= TestDataGenerator.createImage(true,a,c,conf,prod,(String) d4.Id,partRecord,true,false,true);

        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),false,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav4Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option4','Select4');
            Question_Answers_Values__c qav5Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option5','Select5');
            Question_Answers_Values__c qav6Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option6','Select6');
        //Supplier
        Question__c supplierQuestion = TestDataGenerator.createQuestion(true,'Test Supplier Question',2000,false,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);

        //Executive Golf
        Question__c execActivityQuestion = TestDataGenerator.createQuestion(true,'Test Golf Pickilist',200,true,'Activity',questionRTypeMap.get('Question').getRecordTypeId(),false,true);
        
        //Create Event Questions
        Event_Question__c eQuestion = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);

        PageReference pageRef = Page.EventRegistration2;
        pageRef.getParameters().put('eId', prod.Id);


        Test.startTest();
            System.runAs(execCommUser){
                Test.setCurrentPageReference(pageRef);
                EventRegistration2Controller eRegController = new EventRegistration2Controller();
                String anchorPart = eRegController.anchorPart;
                String subAnchorPart = eRegController.subAnchorPart;
                String siteURL = eRegController.getSiteURL();
                UserPermissions uPermissions = eRegController.permissions;
                Boolean displayContact = eRegController.displayEditContactModal;
                //Boolean displayAccount = eRegController.displayProfileAnswersModal;

        registrationQuestionsController regQuestionController= new registrationQuestionsController();
            regQuestionController.currentEvent = prod;
            regQuestionController.currentContact = c;
            regQuestionController.currentConfirmation = conf;
            regQuestionController.eventId = prod.Id;
            regQuestionController.contactId = c.Id;
            regQuestionController.currentContactRecordType = 'Executive';
            regQuestionController.parentForm = eRegController.getThis();
            regQuestionController.phase ='Category';
            List<QuestionWrapper> questionList = regQuestionController.getQuestionList();
            
            regQuestionController.parentForm.registerQuestionsCompController(regQuestionController);

            Integer numController =regQuestionController.parentForm.getNumControllers();
            Set<registrationQuestionsController> regQuestionControlSet = MultiComponentForm.getControllers();

            regQuestionController.parentForm.saveAllComponents();

            }
        Test.stopTest();
    }
    static testMethod void testMethod4(){
        // Implement test code
        TestDataGenerator.createInactiveTriggerSettings();
        Id supplierAccRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id supplierConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        
        Event myEvent = new Event(ActivityDate=Date.today().addDays(3), IsAllDayEvent = true);
        
        Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account',supplierAccRtype);

        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) supplierConRtype);
        c.Has_Event__c = true;
        update c;
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
        
        //Start Defining Event "Product" related info.
        Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec@cpmg.com',null);
        User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier@cpmg.com',null);

        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');

        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

        Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Preliminary',opp,Date.today(),ead,prod,true);
        conf.Primary_Event_Contact__c = false;
        
        //Start Questions
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),false,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav4Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option4','Select4');
            Question_Answers_Values__c qav5Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option5','Select5');
            Question_Answers_Values__c qav6Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option6','Select6');
        //Supplier
        Question__c supplierQuestion = TestDataGenerator.createQuestion(true,'Test Supplier Question',2000,false,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);

        //Executive Golf
        Question__c execActivityQuestion = TestDataGenerator.createQuestion(true,'Test Golf Pickilist',200,true,'Activity',questionRTypeMap.get('Question').getRecordTypeId(),false,false);
    
        
        //Create Event Questions
        Event_Question__c eQuestion = TestDataGenerator.createEventQuestion(true,'The Mulit Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);
        // Event_Question__c eQuestion1 = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',picklistQuestion,true,null,null);
        //Event_Question__c eQuestion2 = TestDataGenerator.createEventQuestion(true,'The Supplier Text Question',prod,'Category',supplierQuestion,true,null,null);
        //Event_Question__c eQuestion1 = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',picklistQuestion,true,null,null);
        eQuestion.Display_Type__c = 'Primary Area of Interest';
        //UserPermissions uPermissions = new UserPermissions('Supplier',true);
        
        Event_Account_Details__c record = new Event_Account_Details__c(Event__c=myEvent.Id, Opportunity__c=opp.Id);
        insert record;
        
        Contact_Event_Answers__c eventAnswers = new Contact_Event_Answers__c(Question__c = eQuestion.Id, Participation_Record__c = record.Id,Confirmation__c = conf.Id);
        insert eventAnswers;
        Question_Answers_Values__c objquAns = new Question_Answers_Values__c();
        objquAns.Name = 'test';
        objquAns.Question__c = execActivityQuestion.id;
        insert objquAns;
        
        Test.startTest();
        List<QuestionWrapper> questionList = New List<QuestionWrapper>();
        QuestionWrapper objq = new QuestionWrapper(execActivityQuestion,eQuestion,eventAnswers,c.id,10);
        objq.answerList = new List<String>{'value1','value2'};
        objq.singleAnswer = objquAns.id;
        objq.subAnswer = 'test';
        objq.subPicklist = 'test';
        Map<String,Boolean> objMap = new Map<String,Boolean>();
        objMap.put(objquAns.id,true);
        objq.golfHelperMap = objMap;
        questionList.add(objq);
       
        UserPermissions uPermissions = new UserPermissions('Supplier',true);
            registrationQuestionsController regQuestionController= new registrationQuestionsController();
            //regQuestionController.selectedValues = 'test';
            regQuestionController.currentEvent = prod;
            regQuestionController.name='Test';
            regQuestionController.readOnly = false;
            regQuestionController.questionType='Executive';
            regQuestionController.permissions = uPermissions;
            regQuestionController.currentContact = c;
            regQuestionController.currentConfirmation = conf;
            regQuestionController.eventId = prod.Id;
            regQuestionController.contactId = c.Id;
            regQuestionController.currentContactRecordType = 'Executive';
            regQuestionController.phase ='Category';
            registrationQuestionsController.permissionsCheck = uPermissions;
            
            regQuestionController.getQuestionList();
            
            registrationQuestionsController.prepareAnswers(questionList,conf);
            execActivityQuestion.Multi_Select__c = true;
            
            registrationQuestionsController.prepareAnswers(questionList,conf);
            objq.answerList = null;
            
            registrationQuestionsController.prepareAnswers(questionList,conf);
            objq.singleAnswer = 'Select Option';
            objq.subAnswer = '';
            objq.subPicklist = '';
            registrationQuestionsController.prepareAnswers(questionList,conf);
            
        Test.stopTest();
    }
   
    static testMethod void  combine_SelectOption_test(){
            String[] ansList = new List<String>{'value1','value2'};

            Test.startTest();
                //registrationQuestionsController regQuestController = new registrationQuestionsController();
                String s = RegistrationQuestionsController.combineSelectOptions(ansList); 
            Test.stopTest();
    }
    static testMethod void  setAnswers_test(){
            String[] ansList = new List<String>{'value1','value2'};

            Test.startTest();
                registrationQuestionsController regQuestController = new registrationQuestionsController();
                regQuestController.setAnswers(ansList); 
            Test.stopTest();
    } 
    static testMethod void  getAnswers_test(){
            String[] ansList = new List<String>{'value1','value2'};

            Test.startTest();
                registrationQuestionsController regQuestController = new registrationQuestionsController();
                regQuestController.answers = ansList;
                List<String> s =regQuestController.getAnswers(); 
            Test.stopTest();
    }

        static testMethod void  combine_SelectOptionForLabel_test(){
            String[] ansList = new List<String>{'value1','value2'};
            Map<String,String> picklistMap = New Map<String,String>{'value1'=>'val1','value2'=>'val2'};

            Test.startTest();
                //registrationQuestionsController regQuestController = new registrationQuestionsController();
                String s = RegistrationQuestionsController.combineSelectOptionsForLabel(ansList, picklistMap); 
            Test.stopTest();
    }
    
}