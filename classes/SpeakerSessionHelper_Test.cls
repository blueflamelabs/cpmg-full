@isTest
private class SpeakerSessionHelper_Test {

	@isTest
	private static void getSpeakersForEvent_Test() {
		//PRepare Test Data.
		TestDataGenerator.createInactiveTriggerSettings();

		//Event Setup
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
		prod.Start_Date__c = Date.today().addDays(10);
		prod.Family ='BuildPoint';
		prod.Event_Navigation_Color__c = '90135d';
		prod.Event_Tint_Color__c = 'EEEEEE';
		update prod;

		//Create Speaker Sessions
		Speaker_Session__c speakSession1 = TestDataGenerator.createSpeakerSessions(true,String.valueOf( System.Now()), System.Now(),'Speaker Session 1', prod,'Overview Test','Http://www.google.com','Presenter 1','Presentation Title');
		Speaker_Session__c speakSession2 = TestDataGenerator.createSpeakerSessions(true,String.valueOf( System.Now().addDays(-11)), System.Now().addDays(-1),'Speaker Session 2', prod,'Overview Test','Http://www.google.com','Presenter 2','Presentation Title For Session 2');

		Test.startTest();
			Speaker_Session__c[] speakers = SpeakerSessionHelper.getSpeakersForEvent(new Set<Id>{prod.Id});
		Test.stopTest();
			//The sort should be by earliest date. So the Query should re-order, and the second speaker sessions starts earlier.
			//So we are testing to make sure that the "First Speaker Session" is the earliest one (i.e. the speakSessions2).
			System.assertEquals(speakers[0].Id, speakSession2.Id);

	}

	static testmethod void getJSONSpeakerMap(){
				//PRepare Test Data.
		TestDataGenerator.createInactiveTriggerSettings();

		//Event Setup
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
		prod.Start_Date__c = Date.today().addDays(10);
		prod.Family ='BuildPoint';
		prod.Event_Navigation_Color__c = '90135d';
		prod.Event_Tint_Color__c = 'EEEEEE';
		update prod;

		//Create Speaker Sessions
		Speaker_Session__c speakSession1 = TestDataGenerator.createSpeakerSessions(true,String.valueOf( System.Now()), System.Now(),'Speaker Session 1', prod,'Overview Test','Http://www.google.com','Presenter 1','Presentation Title');

		Test.startTest();
			Map<String,Object> jsonSpeaker = SpeakerSessionHelper.getJSONSpeakerMap(speakSession1);
		Test.stopTest();
			System.assertEquals(speakSession1.Overview_Text__c,jsonSpeaker.get('overview'));
			System.assertEquals(speakSession1.Overview_URL__c,jsonSpeaker.get('overviewURL'));
			System.assertEquals(speakSession1.Title__c,jsonSpeaker.get('title'));
			System.assertEquals(speakSession1.Name,jsonSpeaker.get('name'));
			System.assertEquals(speakSession1.Presenter__c,jsonSpeaker.get('presenter'));
			System.assertEquals(speakSession1.Date_and_Time_Display__c,jsonSpeaker.get('datedisplay'));

	}
}