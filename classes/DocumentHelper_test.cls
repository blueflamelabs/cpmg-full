@isTest
private class DocumentHelper_test {
	
	@isTest static void testGetDocumentMap() {

		Document myDocument = new Document(
			Name='testDocument',
			Body=Blob.valueOf('test body'),
			FolderId= UserInfo.getUserId());
		insert myDocument;

		Set<Id> docIds = new Set<Id>();
		docIds.add(myDocument.Id);

		test.startTest();
		Map<Id,Document> result =  DocumentHelper.getDocumentMap(docIds);
		System.assertEquals(result.get(myDocument.Id).Id, myDocument.Id);
		test.stopTest();
	}
	
	@isTest static void testCreateDoc() {
		
		Blob body = Blob.valueOf('test body');
		String contType =  'text/plain';
		String name = 'testDocument2';
		String folderID = (String)UserInfo.getUserId();

		Document result = DocumentHelper.createDoc(body, contType, folderID, name);

		System.assertEquals(name, result.name);
	}
	
}