@isTest
private class ParticipationRecordWrapper_Test {
    /*
        Test Data Needed:
            1.) Participation Record
            2.) Contact Event Answer (Category)
            3.) Contact Event Answer (Long Description)
            4.) Contact Event Answer (Short Description)
    
    */
    @isTest static void ParticipationRecordWrapper_Constructor_PR_All_Questions() {
        // Prepare Test Data.
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(10);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        //Wrapper wants a website.
        execAcc.Website = 'https://www.google.com';
        update execAcc;
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod);

        //Create Question
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Category Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c longQuestion = TestDataGenerator.createQuestion(true,'Long Description',2000,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
        Question__c shortQuestion = TestDataGenerator.createQuestion(true,'Short Description',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            
        //Create the 3 Event Questions
        Event_Question__c categoryEQ = TestDataGenerator.createEventQuestion(true,'Category',prod,'Category',picklistQuestion,true,1,null);
        Event_Question__c longEQ = TestDataGenerator.createEventQuestion(true,'Company Long Description',prod,'Company',longQuestion,true,1,'Company Long Description');
        Event_Question__c shortEQ = TestDataGenerator.createEventQuestion(true,'Company Short Description',prod,'Company',shortQuestion,true,1,'Company Short Description');

        //Create Contact Answer Questions
        Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,categoryEQ,execCon,'Select2;Select3',null,null);
        Contact_Event_Answers__c longAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,longEQ,execCon,'Long Answer',null,null);
        Contact_Event_Answers__c shortAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,shortEQ,execCon,'Short Answer',null,null);

        Test.startTest();
            ParticipationRecordWrapper prWrapper = new ParticipationRecordWrapper(execPRecod,longAnswer,shortAnswer,categoryAnswer);
        Test.stopTest();
        
    }

}