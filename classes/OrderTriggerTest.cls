@isTest
private class OrderTriggerTest {

    private static Account supplierAccount;
    private static  Contact supplierContact;
    private static  Account executiveAccount;
    private static  Contact executiveContact;
    private static  Contact executiveContact2;
    private static  Contact supplierContact2;

    private static  Product2 event;
    private static  Contract supplierContract;
    private static  Contract executiveContract;


    private static void setupData() {
        TestDataGenerator.createCommSettings();
        TestDataGenerator.createOrderStatusCustomSetting();
        supplierAccount = new Account(Name = 'Supplier', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId());
        insert supplierAccount;
        supplierContact = new Contact(FirstName = 'test', LastName = 'Supplier', AccountId = supplierAccount.Id, RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId());
        insert supplierContact;
        supplierContact2 = new Contact(FirstName = 'test',LastName = 'Supplier2', AccountId = supplierAccount.Id, RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId());
        insert supplierContact2;

        executiveAccount = new Account(Name = 'Executive', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId());
        insert executiveAccount;
        executiveContact = new Contact(FirstName = 'test',LastName = 'Executive', AccountId = executiveAccount.Id, RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId());
        insert executiveContact;
        executiveContact2 = new Contact(FirstName = 'test',LastName = 'Executive2', AccountId = executiveAccount.Id, RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId());
        insert executiveContact2;

        event = new Product2(Name = 'Event', Start_Date__c = System.today(),User_Link_Expiration_for_Suppliers__c =2,User_Link_Expiration_for_Executives__c=2);
        insert event;
        executiveContract = new Contract(Name = 'Contract', AccountId = executiveAccount.Id);
        insert executiveContract;
        supplierContract = new Contract(Name = 'Contract', AccountId = supplierAccount.Id);
        insert supplierContract;
    }
    @isTest
    private static void testExecutiveOrder(){

        setupData();


        System.Test.startTest();

        Event_Account_Details__c ead = new Event_Account_Details__c();
        ead.Event__c = event.Id;
        ead.Account__c = executiveAccount.Id;
       // ead.Package__c = 'PackageEAD01';
        insert ead;

        //// Create Order record
        Order ord = new Order();
        ord.AccountId = executiveAccount.Id;
        ord.BillToContactId = executiveContact.Id;
        ord.ContractId = executiveContract.Id;
        ord.Order_Event__c = event.Id;
        ord.Status = Order.Status.getDescribe().getPicklistValues().get(0).getValue();//should be Draft or Premilinerary
        ord.EffectiveDate = Date.today();
        ord.RecordTypeId =  Schema.SObjectType.Order.getRecordTypeInfosByName().get('Attendee').getRecordTypeId();
        ord.Primary_Event_Contact__c = true;
        insert ord;

        Product2 currentEvent = getEvent(event.Id);
        System.assertEquals(0, currentEvent.Supplier_Registrants__c);
        System.assertEquals(1, currentEvent.Executive_Registrants__c);

        System.Test.stopTest();
    }
    @isTest
    private static void testSupplierOrder() {
        setupData();


        System.Test.startTest();

        Event_Account_Details__c ead = new Event_Account_Details__c();
        ead.Event__c = event.Id;
        ead.Account__c = supplierAccount.Id;
       // ead.Package__c = 'PackageEAD01';
        insert ead;

        //// Create Order record
        Order ord = new Order();
        ord.AccountId = supplierAccount.Id;
        ord.BillToContactId = supplierContact.Id;
        ord.ContractId = supplierContract.Id;
        ord.Order_Event__c = event.Id;
        ord.Status = Order.Status.getDescribe().getPicklistValues().get(0).getValue();//should be Draft or Premilinerary
        ord.EffectiveDate = Date.today();
        ord.RecordTypeId =  Schema.SObjectType.Order.getRecordTypeInfosByName().get('Attendee').getRecordTypeId();
        ord.Primary_Event_Contact__c = true;
        insert ord;

        Product2 currentEvent = getEvent(event.Id);
        System.assertEquals(1, currentEvent.Supplier_Registrants__c);
        System.assertEquals(0, currentEvent.Executive_Registrants__c);

        System.Test.stopTest();
    }
        @isTest
    private static void testSupplierOrderConfirmed() {
        setupData();


       

        Event_Account_Details__c ead = new Event_Account_Details__c();
        ead.Event__c = event.Id;
        ead.Account__c = supplierAccount.Id;
        //ead.Package__c = 'PackageEAD01';
        insert ead;

        //// Create Order record
        Order ord = new Order();
        ord.AccountId = supplierAccount.Id;
        ord.BillToContactId = supplierContact.Id;
        //ord.ContractId = supplierContract.Id;
        ord.Order_Event__c = event.Id;
        ord.Status = Order.Status.getDescribe().getPicklistValues().get(0).getValue();//should be Draft or Premilinerary
        ord.EffectiveDate = Date.today();
        ord.RecordTypeId =  Schema.SObjectType.Order.getRecordTypeInfosByName().get('Attendee').getRecordTypeId();
        ord.Primary_Event_Contact__c = true;
        insert ord;

        Product2 currentEvent = getEvent(event.Id);
        System.assertEquals(1, currentEvent.Supplier_Registrants__c);
        System.assertEquals(0, currentEvent.Executive_Registrants__c);

        System.Test.startTest();
         Order upOrd = new Order();
         upOrd.id = ord.Id;
         upOrd.Status = 'Complete';
         update upOrd;
        System.Test.stopTest();
    }

    @isTest
    private static void testMultipleExecutiveOrder(){

        setupData();




        Event_Account_Details__c ead = new Event_Account_Details__c();
        ead.Event__c = event.Id;
        ead.Account__c = executiveAccount.Id;
        //ead.Package__c = 'PackageEAD01';
        insert ead;
        System.debug('Executive Account: '+ executiveAccount);
        //// Create Order record
        Order ord = new Order();
        ord.AccountId = executiveAccount.Id;
        ord.BillToContactId = executiveContact.Id;
       // ord.ContractId = executiveContract.Id;
        ord.Order_Event__c = event.Id;
        ord.Status = Order.Status.getDescribe().getPicklistValues().get(0).getValue();//should be Draft or Premilinerary
        ord.EffectiveDate = Date.today();
        ord.RecordTypeId =  Schema.SObjectType.Order.getRecordTypeInfosByName().get('Attendee').getRecordTypeId();

        Order ord2 = new Order();
        ord2.AccountId = executiveAccount.Id;
        ord2.BillToContactId = executiveContact2.Id;
       // ord.ContractId = executiveContract.Id;
        ord2.Order_Event__c = event.Id;
        ord2.Status = Order.Status.getDescribe().getPicklistValues().get(0).getValue();//should be Draft or Premilinerary
        ord2.EffectiveDate = Date.today();
        ord2.RecordTypeId =  Schema.SObjectType.Order.getRecordTypeInfosByName().get('Attendee').getRecordTypeId();

        System.debug('Order 2: '+ ord2);

        System.Test.startTest();
        insert ord2;
        insert ord;

        Product2 currentEvent = getEvent(event.Id);
        System.assertEquals(0, currentEvent.Supplier_Registrants__c);
        System.assertEquals(2, currentEvent.Executive_Registrants__c);

        System.Test.stopTest();
    }
    @isTest
    private static void testMultipleSupplierOrder() {
        setupData();

        Event_Account_Details__c ead = new Event_Account_Details__c();
        ead.Event__c = event.Id;
        ead.Account__c = supplierAccount.Id;
       // ead.Package__c = 'PackageEAD01';
        insert ead;

        //// Create Order record
        Order ord = new Order();
        ord.AccountId = supplierAccount.Id;
        ord.BillToContactId = supplierContact.Id;
        ord.ContractId = supplierContract.Id;
        ord.Order_Event__c = event.Id;
        ord.Status = Order.Status.getDescribe().getPicklistValues().get(0).getValue();//should be Draft or Premilinerary
        ord.EffectiveDate = Date.today();
        ord.RecordTypeId =  Schema.SObjectType.Order.getRecordTypeInfosByName().get('Attendee').getRecordTypeId();
        ord.Primary_Event_Contact__c = true;

        Order ord2 = new Order();
        ord2.AccountId = supplierAccount.Id;
        ord2.BillToContactId = supplierContact2.Id;
        ord2.ContractId = supplierContract.Id;
        ord2.Order_Event__c = event.Id;
        ord2.Status = Order.Status.getDescribe().getPicklistValues().get(0).getValue();//should be Draft or Premilinerary
        ord2.EffectiveDate = Date.today();
        ord2.RecordTypeId =  Schema.SObjectType.Order.getRecordTypeInfosByName().get('Attendee').getRecordTypeId();
        ord2.Primary_Event_Contact__c = true;
    System.Test.startTest(); 
        insert ord;
        insert ord2;

        Product2 currentEvent = getEvent(event.Id);
        System.assertEquals(2, currentEvent.Supplier_Registrants__c);
        System.assertEquals(0, currentEvent.Executive_Registrants__c);

        System.Test.stopTest();
    }

    @isTest
    private static void testMultipleConfirmedOrder() {
        setupData();

        Event_Account_Details__c ead = new Event_Account_Details__c();
        ead.Event__c = event.Id;
        ead.Account__c = supplierAccount.Id;
       // ead.Package__c = 'PackageEAD01';
        insert ead;

        //// Create Order record
        Order ord = new Order();
        ord.AccountId = supplierAccount.Id;
        ord.BillToContactId = supplierContact.Id;
        ord.ContractId = supplierContract.Id;
        ord.Order_Event__c = event.Id;
        ord.Status = Order.Status.getDescribe().getPicklistValues().get(0).getValue();//should be Draft or Premilinerary
        ord.EffectiveDate = Date.today();
        ord.RecordTypeId =  Schema.SObjectType.Order.getRecordTypeInfosByName().get('Attendee').getRecordTypeId();
        ord.Primary_Event_Contact__c = true;

        Order ord2 = new Order();
        ord2.AccountId = supplierAccount.Id;
        ord2.BillToContactId = supplierContact2.Id;
        ord2.ContractId = supplierContract.Id;
        ord2.Order_Event__c = event.Id;
        ord2.Status = Order.Status.getDescribe().getPicklistValues().get(0).getValue();//should be Draft or Premilinerary
        ord2.EffectiveDate = Date.today();
        ord2.RecordTypeId =  Schema.SObjectType.Order.getRecordTypeInfosByName().get('Attendee').getRecordTypeId();
        ord2.Primary_Event_Contact__c = true;
    
        insert ord;
        insert ord2;

        Product2 currentEvent = getEvent(event.Id);
        System.assertEquals(2, currentEvent.Supplier_Registrants__c);
        System.assertEquals(0, currentEvent.Executive_Registrants__c);

        System.Test.startTest(); 
        Order upOrd = new Order(Id = ord.Id, Status = 'Complete');
        Order upOrd2 = new Order(Id = ord2.Id, status = 'Complete');
        List<Order> upOrders = new List<Order>();
        upOrders.add(upOrd);
        upOrders.add(upOrd2);

        update upOrders;
        System.Test.stopTest();
    }

    @isTest
    private static void testDeletOrder() {
        setupData();

        Event_Account_Details__c ead = new Event_Account_Details__c();
        ead.Event__c = event.Id;
        ead.Account__c = supplierAccount.Id;
        //ead.Package__c = 'PackageEAD01';
        insert ead;

        //// Create Order record
        Order ord = new Order();
        ord.AccountId = supplierAccount.Id;
        ord.BillToContactId = supplierContact.Id;
        ord.ContractId = supplierContract.Id;
        ord.Order_Event__c = event.Id;
        ord.Status = Order.Status.getDescribe().getPicklistValues().get(0).getValue();//should be Draft or Premilinerary
        ord.EffectiveDate = Date.today();
        ord.RecordTypeId =  Schema.SObjectType.Order.getRecordTypeInfosByName().get('Attendee').getRecordTypeId();
        ord.Primary_Event_Contact__c = true;

        Order ord2 = new Order();
        ord2.AccountId = supplierAccount.Id;
        ord2.BillToContactId = supplierContact2.Id;
        ord2.ContractId = supplierContract.Id;
        ord2.Order_Event__c = event.Id;
        ord2.Status = Order.Status.getDescribe().getPicklistValues().get(0).getValue();//should be Draft or Premilinerary
        ord2.EffectiveDate = Date.today();
        ord2.RecordTypeId =  Schema.SObjectType.Order.getRecordTypeInfosByName().get('Attendee').getRecordTypeId();
        ord2.Primary_Event_Contact__c = true;
    
        insert ord;
        insert ord2;

        Product2 currentEvent = getEvent(event.Id);
        System.assertEquals(2, currentEvent.Supplier_Registrants__c);
        System.assertEquals(0, currentEvent.Executive_Registrants__c);

        System.Test.startTest(); 
       
        Order upOrd2 = new Order(Id = ord2.Id);
        
        delete upOrd2;

        Product2 currentEvent2 = getEvent(event.Id);
        System.assertEquals(1, currentEvent2.Supplier_Registrants__c);
        System.assertEquals(0, currentEvent2.Executive_Registrants__c);

        System.Test.stopTest();
    }

    private static Product2 getEvent(Id eventId) {
        Product2 currentevent = [SELECT Id, Executive_Registrants__c, Supplier_Registrants__c FROM Product2 WHERE Id =: eventId];
        return currentevent;
    }

    static testMethod void testOrderBulk_NoOPP(){
        TestDataGenerator.createActiveTriggerSettings();
       
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();

        //AccountFormSettings
        TestDataGenerator.createAccountFormSettings();

        //Contact Form Settings:
        TestDataGenerator.createContactFormSettings();

        //Order Form Display settings
        TestDataGenerator.createOrderFormSettings();

        //Registration Settings
        TestDataGenerator.createRegistrationSettings();

        //Need the Order Status Custom Setting
        TestDataGenerator.createOrderStatusCustomSetting();
        
        Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
        Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(false,'Test1','Last Name','testAcc1@account.com',a,suppConRtype);
        Contact c2 = TestDataGenerator.createTestContact(false,'Test2','Last Name','testAcc2@account.com',a,suppConRtype);
        Contact c3 = TestDataGenerator.createTestContact(false,'Test3','Last Name','testAcc3@account.com',a,suppConRtype);
        Contact c4 = TestDataGenerator.createTestContact(false,'Test4','Last Name','testAcc4@account.com',a,suppConRtype);
        Contact c5 = TestDataGenerator.createTestContact(false,'Test5','Last Name','testAcc5@account.com',a,suppConRtype);
        Contact c6 = TestDataGenerator.createTestContact(false,'Test6','Last Name','testAcc6@account.com',a,suppConRtype);
        Contact c7 = TestDataGenerator.createTestContact(false,'Test7','Last Name','testAcc7@account.com',a,suppConRtype);
        Contact c8 = TestDataGenerator.createTestContact(false,'Test8','Last Name','testAcc8@account.com',a,suppConRtype);
        Contact c9 = TestDataGenerator.createTestContact(false,'Test8','Last Name','testAcc9@account.com',a,suppConRtype);
        Contact c10 = TestDataGenerator.createTestContact(false,'Test10','Last Name','testAcc10@account.com',a,suppConRtype);
        Contact c11 = TestDataGenerator.createTestContact(false,'Test11','Last Name','testAcc11@account.com',a,suppConRtype);
        Contact c12 = TestDataGenerator.createTestContact(false,'Test12','Last Name','testAcc12@account.com',a,suppConRtype);
        List<Contact> contacts = new List<Contact>{c,c2,c3,c4,c5,c6,c7};
        insert contacts;

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execCommUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExecUser@cpmg.com',c.Id);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
    

        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod,'Renewal');
        
        
        Id suppPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, suppPartRecordType);

        Order conf = TestDataGenerator.createConfirmation(false,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        conf.OpportunityId = null;
        Order conf2 = TestDataGenerator.createConfirmation(false,a,c2,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        conf2.OpportunityId = null;
        Order conf3 = TestDataGenerator.createConfirmation(false,a,c3,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        conf3.OpportunityId = null;
        Order conf4 = TestDataGenerator.createConfirmation(false,a,c4,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        conf4.OpportunityId = null;
        Order conf5 = TestDataGenerator.createConfirmation(false,a,c5,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        conf5.OpportunityId = null;
        Order conf6 = TestDataGenerator.createConfirmation(false,a,c6,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        conf6.OpportunityId = null;
        Order conf7 = TestDataGenerator.createConfirmation(false,a,c7,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        conf7.OpportunityId = null;
        Order conf8 = TestDataGenerator.createConfirmation(false,a,c8,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        conf8.OpportunityId = null;
        Order conf9 = TestDataGenerator.createConfirmation(false,a,c9,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        conf9.OpportunityId = null;
        Order conf10 = TestDataGenerator.createConfirmation(false,a,c10,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        conf10.OpportunityId = null;
        Order conf11 = TestDataGenerator.createConfirmation(false,a,c11,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        conf11.OpportunityId = null;
        Order conf12 = TestDataGenerator.createConfirmation(false,a,c12,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        conf12.OpportunityId = null;
        List<Order> ordersJustCreated = new List<Order>{conf, conf2, conf3, conf4, conf5, conf6, conf7,conf8,conf9,conf10,conf11,conf12};
        Set<Id> ordIds = new Set<Id>();
        for(Order o: ordersJustCreated){
            System.assertEquals(o.OpportunityId,null);
        }
        test.startTest();
            insert ordersJustCreated;
            for(Order o : ordersJustCreated){
                ordIds.add(o.Id);

            }
        test.stopTest();
            List<Order> ords = [Select Id,OpportunityID FROM Order WHERE Id in :ordIds];

            For(Order o: ords){
                SYstem.assertEquals(opp.Id, o.OpportunityId);
            }

    }
}