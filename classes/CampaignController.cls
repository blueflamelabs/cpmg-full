public with sharing class CampaignController {
  
    @AuraEnabled 
    public static String RecName(Id contactId){
    	String Name = [Select Id,Name from Contact where Id =:contactId].Name;
        return Name;
    }
        
  @AuraEnabled 
    public static List<CampaignListWrapper> fetchCampaignWrapper(Id contactId,String SelectedCampaign,String SelectedCampaignEvent,String sortField, boolean isAsc,String tabID){
    system.debug('@@sortField'+sortField);
    system.debug('@@isAsc'+isAsc);
       Set<String> setOfCampaignId=new Set<String>();
       contact cont=[SELECT Id,RecordType.Name FROM contact where Id=:contactId];
       String contactRecordType=cont.RecordType.Name;
       String recordTypename='All';
       String campStatus='Completed';
       User usr=[select id from user where id=:userInfo.getUserId()];
      
       String CurntUserId=usr.id;
                    
       system.debug('CurntUserId '+CurntUserId);
       List<CampaignMember> allCampaignMemberList =[SELECT Id,Name,CampaignId,ContactId,LeadId FROM CampaignMember where ContactId=:contactId];       
       List<CampaignListWrapper> lstCampaignListWrapper = new List<CampaignListWrapper>();
       List<Campaign> objCampaignList ;
       for(CampaignMember campMember : allCampaignMemberList){
           setOfCampaignId.add(campMember.CampaignId);
       }
      if(tabID=='tab1'){
               String sSoql='Select Id,name,RecordType.Name,OwnerId,Event__r.End_Date__c,Status From Campaign Where IsActive= true';
                sSoql+=' and OwnerId =:CurntUserId';
                sSoql+=' and Status!=:campStatus';
                sSoql+=' and Event__r.End_Date__c >=today';
                sSoql+=' and (RecordType.name=:contactRecordType'; 
                sSoql+=' OR RecordType.name=:recordTypename)';
                
                if (sortField != '') {
                 sSoql += ' order by ' + sortField;
                    if (isAsc) {
                        sSoql += ' asc';
                     } else {
                        sSoql += ' desc';
                     }
               }  
               system.debug('@@query '+sSoql);
              
               if(SelectedCampaign == 'My Campaigns' && SelectedCampaignEvent == 'Upcoming Event Campaigns'){
                    objCampaignList = Database.query(sSoql);
               }
               if(SelectedCampaign == 'My Campaigns' && SelectedCampaignEvent == 'All Active Campaigns'){ 
                   String target = ' and Status!=:campStatus and Event__r.End_Date__c >=today';
                   String replacement = '';
                   String sSoqlNew = sSoql.replace(target, replacement);   
                   objCampaignList = Database.query(sSoqlNew);
               }
               if(SelectedCampaign == 'All Campaigns' && SelectedCampaignEvent == 'Upcoming Event Campaigns'){
                   String target = ' and OwnerId =:CurntUserId';
                   String replacement = '';
                   String sSoqlNew = sSoql.replace(target, replacement);
                   objCampaignList = Database.query(sSoqlNew);
               }
               if(SelectedCampaign == 'All Campaigns' && SelectedCampaignEvent == 'All Active Campaigns'){
                   String target = ' and OwnerId =:CurntUserId and Status!=:campStatus and Event__r.End_Date__c >=today';
                   String replacement = '';
                   String sSoqlNew = sSoql.replace(target, replacement);                  
                   objCampaignList = Database.query(sSoqlNew);
                    
               }
                for(Campaign camp : objCampaignList){
                   if(!setOfCampaignId.contains(camp.id)){
                         lstCampaignListWrapper.add(new CampaignListWrapper(false,false,camp)); 
                     }
                                
                } 
               
             }else{
                objCampaignList=null;
                   String sSoql='Select Id,name,RecordType.Name,OwnerId,Event__r.End_Date__c,Status From Campaign Where IsActive= true';
                    sSoql+=' and Event__r.End_Date__c >=today';
                    sSoql+=' and (RecordType.name=:contactRecordType'; 
                    sSoql+=' OR RecordType.name=:recordTypename)';
                   system.debug('sortField  ' +sortField+' '+tabID+' '+isAsc);
                    if (sortField != '') {
                     sSoql += ' order by ' + sortField;
                        if (isAsc) {
                            sSoql += ' asc';
                         } else {
                            sSoql += ' desc';
                         }
                    }
                    objCampaignList = Database.query(sSoql); 
                    for(Campaign camp : objCampaignList){
                        if(setOfCampaignId.contains(camp.id)){
                         lstCampaignListWrapper.add(new CampaignListWrapper(false,true,camp));
                          
                        }     
                    }
                      
             }  
              List<Contact> conRec = [select id,isCheckPage__c from contact where id = :contactId];
                for(Contact objCon : conRec){
                    objCon.isCheckPage__c = false;
                }
               update conRec; 
        
        return lstCampaignListWrapper; 
    }
    
    /* wrapper class */  
    public class CampaignListWrapper{
        @AuraEnabled public boolean isChecked {get;set;}
        @AuraEnabled public boolean isDisabled {get;set;}
        @AuraEnabled public Campaign objCampaign{get;set;}
        public CampaignListWrapper(boolean isChecked,boolean isDisabled, Campaign objCampaign){
            this.isChecked = isChecked;
            this.isDisabled= isDisabled;
            this.objCampaign= objCampaign;
        }
    }
    @AuraEnabled
    public static void insertRecord(List<String> lstRecordId,Id contactId) {
        List<CampaignMember> lstCampaignMember  = new List<CampaignMember>();     
        for(Campaign camp : [select id,Name from Campaign where id IN : lstRecordId]){
            CampaignMember campMember=new CampaignMember();
            campMember.CampaignId=camp.id;
            campMember.ContactId=contactId;
            lstCampaignMember.add(campMember);
        }        
        if(lstCampaignMember.size() > 0){
            Insert lstCampaignMember;
        }
    }
    @AuraEnabled
    public static void removeRecord(List<String> lstRecordId,Id contactId) {
        List<CampaignMember> objCamMember = [Select id,CampaignId,ContactId from CampaignMember where CampaignId IN : lstRecordId and ContactId =:contactId];
        if(objCamMember.size() > 0)
                delete objCamMember;
    }
    @AuraEnabled
    public static List<CampaignListWrapper> findByName(String searchKey,Id contactId,String SelectedCampaign,String SelectedCampaignEvent,String tabId){       
       String names = '%'+searchKey + '%';
       String RecordTypes = '%'+searchKey + '%';
       String Tag = '%'+searchKey + '%';
       contact cont=[SELECT Id,RecordType.Name FROM contact where Id=:contactId];
       String contactRecordType=cont.RecordType.Name;
       String recordTypename='All';
       Set<String> setOfCampaignId=new Set<String>();
       List<CampaignMember> allCampaignMemberList =[SELECT Id,Name,CampaignId,ContactId,LeadId FROM CampaignMember where ContactId=:contactId];       
       List<CampaignListWrapper> lstCampaignListWrapper = new List<CampaignListWrapper>();
        for(CampaignMember campMem : allCampaignMemberList){
                   if(campMem.CampaignId!=null){
                       setOfCampaignId.add(campMem.CampaignId);
                   }
          }
              
        if(tabId=='tab1'){
            List<Campaign> objCampaignList = [Select Id,name,RecordType.Name,
                                                 OwnerId,Event__r.End_Date__c,Status From Campaign Where IsActive= true 
                                                 and (name like :names OR RecordType.Name like : RecordTypes) 
                                                 and Event__r.End_Date__c >=today 
                                                 and (RecordType.Name=:contactRecordType OR RecordType.Name=:recordTypename)
                                                  Order by Name ASC];
          
               for(Campaign camp : objCampaignList){
                   lstCampaignListWrapper.add(new CampaignListWrapper(false,false,camp));                     
                }           
          }else{
              List<Campaign> objCampaignList = [Select Id,name,RecordType.Name,
                                                 OwnerId,Event__r.End_Date__c,Status From Campaign Where IsActive= true 
                                                 and (name like :names OR RecordType.Name like : RecordTypes) 
                                                 and Event__r.End_Date__c >=today 
                                                 and (RecordType.Name=:contactRecordType OR RecordType.Name=:recordTypename)
                                                 and id in:setOfCampaignId
                                                  Order by Name ASC];
          
               for(Campaign camp : objCampaignList){
                       lstCampaignListWrapper.add(new CampaignListWrapper(false,false,camp));                       
                }    
         } 
        return lstCampaignListWrapper; 
    }
    
    @AuraEnabled
    public static String getBaseUrl() {
        string urler=URL.getSalesforceBaseUrl().toExternalForm();
        system.debug('hi' +urler);
        return urler;
     }     
    @AuraEnabled
       public static String getUIThemeDescription() {
       return UserInfo.getUiThemeDisplayed();
     }
   @AuraEnabled
       public static List<CampaignListWrapper> fetchRemovefromCampaigns(Id contactId){
           List<CampaignListWrapper> lstCampaignListWrapper = new List<CampaignListWrapper>();
           Set<Id> setOfCampaignId=new Set<Id>();
           List<CampaignMember> allCampaignMemberList =[SELECT Id,Name,CampaignId,ContactId,LeadId FROM CampaignMember where ContactId=:contactId]; 
               for(CampaignMember campMem : allCampaignMemberList){
                   if(campMem.CampaignId!=null){
                       setOfCampaignId.add(campMem.CampaignId);
                   }
               }
          List<Campaign> allCampaignList =[SELECT Id,Name,RecordType.Name FROM Campaign where id in:setOfCampaignId];
          if(allCampaignList.size()>0){
              for(Campaign camp : allCampaignList){
                 lstCampaignListWrapper.add(new CampaignListWrapper(false,true,camp)); 
               }            
         } 
        return lstCampaignListWrapper;
   } 
  
}