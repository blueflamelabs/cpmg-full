public with sharing class EventListingAndDetailsController {
    public Id userId {get; set;}
    public Id contactId {get; set;}
    public Id accountId {get; set;}
    public User currentUser {get; set;}
    public List<Product2> listEvents {get; set;}
    public List<Product2> previousListEvents {get;set;}
    public List<ProductWrapper> previousEvents {get;set;}
    public List<ProductWrapper> currentEvents {get;set;}
    public Set<Id> eventIds {get; set;}
    public String noEventMessage {get; set;}
    public Boolean interventionFlag {get; set;}
    public Boolean isSupplier {get;set;}
    //string userEmail = UserInfo.getUserEmail();
    Private set<string> setStaticResource = new set<string>();
    public EventListingAndDetailsController(){
        noEventMessage = 'THERE ARE NO EVENTS TO REGISTER FOR AT THE PRESENT TIME.';


        userId = UserInfo.getUserId();
      
        currentUser = [SELECT Id, AccountId, ContactId,Contact.RecordType.Name, Username, Name FROM User WHERE Id =: userId];
        isSupplier = (currentUser != null && String.isNotBlank(currentUser.Contact.RecordType.Name) && currentUser.Contact.RecordType.Name == 'Supplier')?true:false;
        accountId = currentUser.AccountId;
        contactId = currentUser.ContactId;
        
        for(StaticResource  sr: [Select id, name from StaticResource where Name like 'CPMG_Events_Page%'
        OR Name like 'CPMG_Events_Page_Hotel%'
        OR Name like 'CPMG_Header_Transparent%'
        ]){
            setStaticResource.add(sr.name);
        }
        
        getEventsByContactId();
        getInterventionFlag();
        if(listEvents != null)getCurrentEvents();
        if(previousListEvents != null) getPreviousEvents();
        
    }
    public void getInterventionFlag(){
       if(accountId != null){
         Account acc = [SELECT Intervention_Flag__c FROM Account WHERE Id =: accountId];
         interventionFlag = acc.Intervention_Flag__c;
       }
       if(contactId != null){
         Contact con = [SELECT Intervention_Flag__c FROM Contact WHERE Id =: contactId];
         interventionFlag = con.Intervention_Flag__c;
       }
    }
    public void getEventsByContactId(){
        eventIds = new Set<Id>();
        for (Order order : [SELECT Order_Event__c,Status FROM Order WHERE BillToContactId =: contactId and Status != 'Cancelled']){
            eventIds.add(order.Order_Event__c);
        }
        listEvents = EventHelper.getAllCurrentEventsForCommunity(eventIds);
        previousListEvents = EventHelper.getAllPreviousEventsForCommunity(eventIds);
        system.debug('USER___ listEvents ' + listEvents.size());
       
    }
    private void getCurrentEvents(){

        currentEvents = new List<ProductWrapper>();
        
        for(Product2 event: listEvents){
           if(event.End_Date__c != null){   
              if(((isSupplier && event.Registration_Part_1_Open_For_Supplier__c>date.Today()) || (!isSupplier && event.Registration_Part_1_Open_For_Executive__c>date.Today()))
                 && !event.isActive && event.start_date__c > date.Today()
              ){
                  
              }
              else {    
                      ProductWrapper newProd = new ProductWrapper(event,isSupplier);
                      currentEvents.add(newProd);
                      
                      if(!setStaticResource.contains(newProd.CPMG_Events_Page_Label)){
                          newProd.isCPMG_Events_Page_Label  = false;
                      }
                      if(!setStaticResource.contains(newProd.CPMG_Events_Page_Hotel_Label )){
                          newProd.isCPMG_Events_Page_Hotel_Label  = false;
                      }
                      if(!setStaticResource.contains(newProd.CPMG_Header_Transparent_Label )){
                          newProd.isCPMG_Header_Transparent_Label  = false;
                      }
                      if((isSupplier && event.Registration_Part_1_Open_For_Supplier__c>date.Today()) || (!isSupplier && event.Registration_Part_1_Open_For_Executive__c>date.Today()))
                          newProd.isActive = false;
                  }
                  
            }
        }
    }
    private void getPreviousEvents(){
        previousEvents = new List<ProductWrapper>();
        for(Product2 event : previousListEvents){
            if(event.End_date__c != null){
                ProductWrapper newProd = new ProductWrapper(event,isSupplier);
                previousEvents.add(new ProductWrapper(event,isSupplier));
                
                  if(!setStaticResource.contains(newProd.CPMG_Events_Page_Label )){
                      newProd.isCPMG_Events_Page_Label  = false;
                  }
                  if(!setStaticResource.contains(newProd.CPMG_Events_Page_Hotel_Label )){
                      newProd.isCPMG_Events_Page_Hotel_Label  = false;
                  }
                  if(!setStaticResource.contains(newProd.CPMG_Header_Transparent_Label )){
                      newProd.isCPMG_Header_Transparent_Label  = false;
                  }
            }
        }
    }

}