public without sharing class ContactEventAnswerHelper {

  public static Map<Id,List<Contact_Event_Answers__c>> idContactEventAnswersMap {get;set;}
 
  public static Map<Id, Contact_Event_Answers__c> getEventQuestionAnswers(Set<Id> p_questionIds,  ID participationRecordId) {  //Id contactID, REPURPOSED TO USE THE PARTICIPATION RECORD
    System.debug('Get Current Answers Called');
    Map<Id, Contact_Event_Answers__c> answersByQ = new Map<Id, Contact_Event_Answers__c>();
    
    for (Contact_Event_Answers__c a : [SELECT Id,Answer__c, Question__c, Question__r.Question__r.Max_Char_Count__c, Picklist_IDs__c,
                                            Contact__c, Name,Participation_Record__c,Confirmation__c, Sub_Question_Answer__c
                                            FROM Contact_Event_Answers__c
                                            WHERE Question__c IN: p_questionIds
                                            //AND Contact__c =: contactID 
                                            AND Participation_Record__c = :participationRecordId]) 
    {
         answersByQ.put(a.Question__c, a);
         //System.debug('Contact Answer: '+a);
    }
    return answersByQ;
  }
  /*public static Map<Id, Contact_Event_Answers__c> getEventQuestionAnswers(Set<Id> p_questionIds, Id partId) {
    System.debug('Get Current Answers Called');
    Map<Id, Contact_Event_Answers__c> answersByQ = new Map<Id, Contact_Event_Answers__c>();
    
    for (Contact_Event_Answers__c a : [SELECT Id, Answer__c, Question__c, Question__r.Question__r.Max_Char_Count__c, Picklist_IDs__c,
                                            Contact__c, Name,Participation_Record__c,Confirmation__c, Sub_Question_Answer__c
                                            FROM Contact_Event_Answers__c
                                            WHERE Question__c IN: p_questionIds
                                            AND Participation_Record__c =: partId ]) 
    {
         answersByQ.put(a.Question__c, a);
         //System.debug('Contact Answer: '+a);
    }
    return answersByQ;
  } */
  /*
   * THIS GETTER WILL UTILIZE THE PARTICIPATION RECORD IDS TO IDENTIFY THE SHORT AND LONG COMPANY DESCRIPTIONS,
   * THIS MAP WILL BE KEYED OFF A COMBO KEY OF THE PRId and 'Short' or 'Long' for the respective Short and Long
   * Company Description.
  */
  public static Map<String, Contact_Event_Answers__c> getLongAndShortProfiles(Set<Id> EventIds, Set<String> shortLongSet){
        Map<String, Contact_Event_Answers__c> answersByPRAndQType = new Map<String,Contact_Event_Answers__c>();
        for(Contact_Event_Answers__c  a: [SELECT Id,Answer__c, Question__c, Participation_Record__c, Confirmation__c, Question__r.Event__c, Question__r.Display_Type__c, Question__r.Phase__c
                          ,Question__r.Question_Display__c, Question__r.Question__c  
                                            FROM Contact_Event_Answers__c WHERE Question__r.Event__c In :eventIds and  (Question__r.Display_Type__c IN :shortLongSet OR Question__r.Phase__c = 'Category')]){
            //THIS ACCOMODATES FOR THE COMPANY SHORT AND LONG DESCRIPTION
            String tempKey = ContactEventAnswerHelper.determineTempKey(a, shortLongSet);
            
            answersByPRAndQType.put(tempKey,a);

        }
        System.debug('$$$$$$$$$$$4 answersByPRAndQType '+answersByPRAndQType);
        return answersByPRAndQType;
  }
  
  
  public static Map<String, Contact_Event_Answers__c> getLongAndShortProfiles1(Set<Id> EventIds, Set<String> shortLongSet){
        Map<String, Contact_Event_Answers__c> answersByPRAndQType = new Map<String,Contact_Event_Answers__c>();
        for(Contact_Event_Answers__c  a: [SELECT Id,Answer__c, Question__c, Participation_Record__c, Confirmation__c, Question__r.Event__c, Question__r.Display_Type__c, Question__r.Phase__c
                          ,Question__r.Question_Display__c, Question__r.Question__c  
                                            FROM Contact_Event_Answers__c WHERE Question__r.Event__c In :eventIds and  (Question__r.Display_Type__c IN :shortLongSet OR Question__r.Phase__c = 'Category') and Question__r.Display_Type__c != 'Primary Area of Interest']){
            //THIS ACCOMODATES FOR THE COMPANY SHORT AND LONG DESCRIPTION
            String tempKey = ContactEventAnswerHelper.determineTempKey(a, shortLongSet);
            
            answersByPRAndQType.put(tempKey,a);

        }
        System.debug('$$$$$$$$$$$4 answersByPRAndQType '+answersByPRAndQType);
        return answersByPRAndQType;
  }

  public static Map<Id,List<Contact_Event_Answers__c>> getProfileQuestionAnswers(Set<Id> eventIds, Set<Id> partRecordId){
        idContactEventAnswersMap = new Map<Id,List<Contact_Event_Answers__c>>();

        for(Contact_Event_Answers__c answer : [SELECT Id,Answer__c, Question__c,Participation_Record__c,Confirmation__c, Question__r.Event__c, Question__r.Display_Type__c, Question__r.Phase__c,
                                                  Question__r.Question_Display__c,QUestion__r.Question__c 
                                                  FROM Contact_Event_Answers__c Where Question__r.Event__c In :eventIds and Question__r.Include_In_Part_2_Profile__c = true]){
          List<Contact_Event_Answers__c> tempList = new List<Contact_Event_Answers__c>();
          if(idContactEventAnswersMap.containsKey(answer.Participation_Record__c)){
              tempList = idContactEventAnswersMap.get(answer.Participation_Record__c);
              tempList.add(answer);
              idContactEventAnswersMap.put(answer.Participation_Record__c, tempList);
            }else{
              tempList.add(answer);
              idContactEventAnswersMap.put(answer.Participation_Record__c,tempList);
            }

        }

        return idContactEventAnswersMap;
  }
  public static String determineTempKey(Contact_Event_Answers__c answer, Set<String> shortLongSet){
        String tempKey = answer.Participation_Record__c;
        if(shortLongSet.contains(answer.Question__r.Display_Type__c)){
            //WE KNow this answer is for either short description or long description
            if(answer.Question__r.Display_Type__c.contains('Short')){
                tempKey += 'Short';
            }
            if(answer.Question__r.Display_Type__c.contains('Long')){
                tempKey += 'Long';
            }
        }
        if(answer.Question__r.Phase__c == 'Category'){
            tempKey+= 'Category';
        }
        return tempKey;
  }

  public static Contact_Event_Answers__c getAnswerByPRandQType(Id prID, String qType, Map<String,Contact_Event_Answers__c> answersByPRAndQType){
      //System.debug('ANswers Map: '+answersByPRAndQType);
      //System.debug('Key: '+ (String)prId +qType);
      if(answersByPRAndQType.containsKey((STRING)prId+qType)){
        return answersByPRAndQType.get(prId+qType);
      } else{
        return new Contact_Event_Answers__c();
      }
  }

  public static Map<String,Contact_Event_Answers__c> getShortAnswerQuestionsByPR(Set<id> eventIds){
    Map<String,Contact_Event_Answers__c> prIdContactEventAnswersMap = new Map<String,Contact_Event_Answers__c>();
    for(Contact_Event_Answers__c answer : [SELECT Id,Answer__c, Question__c, Participation_Record__c, Question__r.Event__c, Question__r.Display_Type__c, Question__r.Phase__c,
                                            Question__r.Question__c
                                            FROM Contact_Event_Answers__c WHERE Question__r.Event__c  in :eventIds and Question__r.Display_Type__c = 'Company Short Description']){
        if(!prIdContactEventAnswersMap.containsKey(answer.Participation_Record__c) && answer.Participation_Record__c != null){
          if(String.isNotBlank(answer.Answer__c)){
            prIdContactEventAnswersMap.put(answer.Participation_Record__c, answer);
          } else{
            prIdContactEventAnswersMap.put(answer.Participation_Record__c, new Contact_Event_Answers__c(answer__c = ''));
          }
        }
    }
    return prIdContactEventAnswersMap;
  }
}