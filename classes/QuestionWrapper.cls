public class QuestionWrapper {
    /*
        This is a wrapper class for displaying the questions, potential answers, and user defined questions per event.

    */
    public Question__c question { get; set; }
    public Event_Question__c eventQuestion { get; set; }
    public Contact_Event_Answers__c eventAnswer { get; set; }
    public String PDFvalue{set;get;}

    public List<String> answerList { get; set; }
    public List<SelectOption> questionOptions { get; set; }
    public Integer maxChars {get;set;}
    public Integer questOrder {get;set;}
  public String answer {get;set;}
  //public Boolean selected{get;set;}
  public String subAnswer {get;set;}
  public String subPicklist {get;set;}
  public String singleAnswer {get;set;}
  public List<SelectOption> golfOptions {get;set;}
  public Map<String,String> picklistIdValueMap {get;set;}
  public Map<String,Boolean> pdfHelperMap {get;set;}
  public Map<String,Boolean> golfHelperMap {get;set;}
  public String primarySelectedValue{get;set;}
  public String primarySelectedName{get;set;}
  //public String otherAnswer {get;set;}


    public QuestionWrapper(Question__c p_question, Event_Question__c p_eventQuestion, Contact_Event_Answers__c p_eventAnswer, Id p_contactId, Integer questionNum) {
      this.question = p_question;
      this.eventQuestion = p_eventQuestion;
      this.eventAnswer = p_eventAnswer;
      this.maxChars = (p_question.Max_Char_Count__c != null) ? Integer.valueOf(p_question.Max_Char_Count__c):255;
        this.questOrder = questionNum;
      this.answerList = new List<String>();
        this.subAnswer='';
        this.primarySelectedValue = '';
        this.primarySelectedName = '';
      this.subPicklist=null;
      //We need to key off the value populated in the label of the selectOption
      pdfHelperMap = new Map<String,Boolean>();
      golfHelperMap = new Map<String,Boolean>();

      //this.singleAnswer='';
     // this.picklistIdValueMap = new Map<String,String>();
      if(golfOptions == null)this.golfOptions = getGolfOptions();
        //this.otherAnswer='';
    //  System.debug('Event AnswerList: '+ eventAnswer);
    //  System.debug('Max Char: '+p_question.Max_Char_Count__c);
   //   try{
   
        if (eventAnswer == NULL) {
          eventAnswer = new Contact_Event_Answers__c();
          eventAnswer.Contact__c = p_contactId;
                  eventAnswer.Name = question.Name;
                  eventAnswer.Question__c = eventQuestion.Id;
            //if(String.isNotBlank(eventAnswer.Sub_Question_Answer__c)) subAnswer=eventAnswer.Sub_Question_Answer__c;
        }
        //

        /*if(!question.Multi_Select__c && String.isNotBlank(eventAnswer.Picklist_IDs__c)){
            if(String.isNotBlank(eventAnswer.Picklist_IDs__c)){
                singleAnswer = eventAnswer.Picklist_IDs__c;
        //Need to add the single answer to the pdfHelper Map;
        pdfHelperMap.put(eventAnswer.Picklist_IDs__c,true);
            }
        } */

        if ( question.Picklist__c && question.Picklist__c != null) {
          this.picklistIdValueMap = new Map<String,String>();

         questionOptions = new List<SelectOption>();
          //Add the N/A Option for single option picklists / radio buttons.
          if(!question.Multi_Select__c){
             if(question.Question_Type__c != 'Activity')
                  questionOptions.add(new SelectOption('N/A','N/A'));
              //add in a False for Golf Helper Map w/ N/A option
              golfHelperMap.put('N/A',false);
              if(String.isNotBlank(eventAnswer.Picklist_IDs__c)){
                singleAnswer = eventAnswer.Picklist_IDs__c;

               // pdfHelperMap.put(eventAnswer.Picklist_IDs__c,true);
              }

          }
          
          
           
          for (Question_Answers_Values__c option : question.Question_Answers_Values__r) {
              picklistIdValueMap.put(option.Id, (Option.Name != null)? option.Name: option.Value__c);
              
              if(option.Golf__c ){
                System.debug('Option '+ option);
                golfHelperMap.put(option.id,true);
              }else{
                golfHelperMap.put(option.id,false);
              }
              //SelectOption so = new SelectOption((String)option.Value__c, option.Name);
              //SelectOption so = new SelectOption(option.Id, option.Name); //Changing to Option.Value__c for the label
              SelectOption so = new SelectOption(option.Id, (Option.Value__c != null)? option.Value__c: option.Name); //Changing to Option.Value__c for the label
              so.setEscapeItem(true);
              questionOptions.add( so);
          }
          System.debug('######golfHelperMapgolfHelperMap######### '+golfHelperMap);
          if(question.Question_Type__c == 'Activity')
              questionOptions.add(new SelectOption('N/A','N/A'));

          if ( String.isNotBlank(eventAnswer.Picklist_ids__c) && eventAnswer.Picklist_Ids__c != null && question.Multi_Select__c) {
            answerList = eventAnswer.Picklist_ids__c.split(';');
            //System.debug('Answer List Size: '+ answerList.size());
          } else if(String.isNotBlank(eventAnswer.Picklist_ids__c) && eventAnswer.Picklist_Ids__c != null && !question.Multi_Select__c){
            answerList.add(eventAnswer.Picklist_ids__c);
          } else {
            answerList = new List<String>();
          }
          
          if(p_eventQuestion.Display_Type__c == 'Primary Area of Interest'){
              singleAnswer = eventAnswer.Picklist_ids__c;
              if(singleAnswer != null){
                  List<Question_Answers_Values__c> objQues = [select id,Golf__c from Question_Answers_Values__c where Id =: singleAnswer];
                  if(objQues.size() > 0){
                      if(objQues[0].Golf__c ){
                          golfHelperMap.put(singleAnswer,true);
                      }else{
                          golfHelperMap.put(singleAnswer,false);
                      }
                  }
                  
              }
              
          }    
          
          

        //NOW THAT WE HAVE BUILT UP THE SELECT OPTIONS, and THE POTENTIAL ANSWERS. WE CAN Build the PDF Helper
        //We need a set so we can see if the picklistId exists within the AnswerList.
        Set<String> tempAnswer = new Set<String>(answerList);

        for(SelectOption so: questionOptions){
            //This is for displaying the checkbox images on the PDF Document.
            if(tempAnswer.contains(so.getValue())){
                pdfHelperMap.put(so.getValue(),true);
            }else{
              pdfHelperMap.put(so.getValue(),false);
            }
            //This is for helping with "a Golf QUestion"
        }

          if(String.isNotBlank(eventAnswer.Sub_Question_Answer__c)){
            this.subAnswer = eventAnswer.Sub_Question_Answer__c;
          }
         
        }
        
        
        
        if(String.isNotBlank(eventAnswer.Sub_Question_Answer__c) && question.Includes_Golf__c){
          subPicklist = eventAnswer.Sub_Question_Answer__c;
        }
        if(String.isNotBlank(eventAnswer.Sub_Question_Answer__c)){
            subAnswer = eventAnswer.Sub_Question_Answer__c;
        }
        //System.debug('#####eventAnswer.Primary_are_of_interest__c######### '+eventAnswer.Primary_are_of_interest__c);
        //eventAnswer.Primary_are_of_interest__c = 'Doing great';
          
        System.debug('%%%%%%%%%%%%%5 '+singleAnswer);
        System.debug('Golf Helper Map: '+ golfHelperMap);

       //if(questionOptions != null) System.debug('The Select Option List: '+ questionOptions);
   //     System.debug('PDF HELPER: '+ pdfHelperMap);
     //   System.debug('Event AnswerList: '+ eventAnswer);
    //  }catch(Exception ex){
     //   System.debug('Error: '+ ex.getMessage()+'. Stack Trace: '+ ex.getStackTraceString());
    //  }
    }

    public void setAnswerList(String answer){
      this.answerList.add(answer);
    }
    public void setSubAnswer(String sub){
      this.subAnswer = sub;
    }
    public void setSubPicklist(String pList){
      this.subPicklist = pList;
    }
    public String getSubPicklist(){
      return subPicklist;
    }

    public List<SelectOption> getGolfOptions(){
      List<SelectOption> opts = new List<SelectOption>();
      opts.add(new SelectOption('----','N/A'));
      //opts.add(new SelectOption('No','No'));
      opts.add(new SelectOption('Right','Right'));
      opts.add(new SelectOption('Left','Left'));
      return opts;
    }

    public Integer getMaxChars(){
      return maxChars;
    }
}