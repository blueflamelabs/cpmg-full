@isTest
public class OrderEventActive_test {
    public static testMethod void unitTest(){
    
       Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true,'Test Account');
        
        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        //conf.Primary_Event_Contact__c = false;
        //update conf;
        PageReference pageRef = Page.OrderEventActive;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(conf);
        ApexPages.currentPage().getParameters().put('Id',conf.id);
        
        OrderEventActive ordEveObj = new OrderEventActive (sc);
        ordEveObj.updateOrder();
        //ordEveObj.replaceOrder();
        ordEveObj.updateOrderCancel();
    }
    public static testMethod void unitTest1(){
       Id AccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
       Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true,'Test Account',AccountRtype);
        
        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);
        Opportunity opp1 = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        partRecord.Total_number_of_confirmations__c =0;
        update partRecord;
        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        Order conf1 = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp1,opp1.CloseDate,partRecord,prod,true);
        list<Order> listOrderUncheck = new list<Order>();
        listOrderUncheck.add(Conf1);
        listOrderUncheck.add(Conf);
        
        Id exeEventQuRtype =  Schema.SObjectType.Event_Question__c.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Event_Question__c eventQuestion = new Event_Question__c();
       //eventQuestion.Question__c = mulitPicklistQuestion.Id;
        eventQuestion.Event__c = prod.Id;
        eventQuestion.Name = 'The Picklist Event Quest';
        eventQuestion.Display_Order__c =1;
        eventQuestion.Display_Type__c = 'Company Short Description';
        eventQuestion.Phase__c = 'Company';
        eventQuestion.Required__c = false;
        eventQuestion.RecordTypeId  = exeEventQuRtype;
        insert eventQuestion;
       
        Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,Conf1,partRecord,eventQuestion,c,'Select2;Select3',null,null);
        PageReference pageRef = Page.OrderEventActive;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(conf);
        ApexPages.currentPage().getParameters().put('Id',conf1.id);
        
        OrderEventActive ordEveObj = new OrderEventActive (sc);
        ordEveObj.listOrderUncheck.addAll(listOrderUncheck);
        ordEveObj.repIndex = 1;
        ordEveObj.currentOrderStatus = 'Attending Event Contact';
        ordEveObj.replaceOrder();
    }

}