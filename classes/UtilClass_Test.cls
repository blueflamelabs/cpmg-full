@isTest
private class UtilClass_Test {

	@isTest
	private static void randomString_Test() {
		Integer random = 4;
		Test.startTest();
			String s = UtilClass.GenerateRandomString(4);
			System.assertEquals(random,s.length());
		Test.stopTest();
	}

	static testMethod void getUserPermissions_Test(){
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execCommUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExecUser@cpmg.com',c.Id);

        Test.startTest();
            System.runAs(execCommUser){
                UtilClass.getUserPermissions();
            }
        Test.stopTest();
    }

     static testMethod void test_externalWebLinkNoHttp(){
        String url = 'google.com';
        test.startTest();
            String returnedVal = UtilClass.formatExternalLinkULR(url);
        test.stopTest();
        System.assertEquals('http://google.com', returnedVal);
    }
    static testMethod void test_externalWebLinkWithHttp(){
        String url = 'http://google.com';
        test.startTest();
            String returnedVal = UtilClass.formatExternalLinkULR(url);
        test.stopTest();
        System.assertEquals('http://google.com', returnedVal);
    }

    static testMethod void transformToProfileWrapper_Test(){

        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(40);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        Account execAcc2 = TestDataGenerator.createTestAccount(true,'Second Exec Account', execAccRType);
        Account execAcc3 = TestDataGenerator.createTestAccount(true,'Zeta Exec Account', execAccRType);
        //Wrapper wants a website.
        execAcc.Website = 'https://www.google.com';
        update execAcc;
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);
        Contact execCon2 = TestDataGenerator.createTestContact(true,'Test Exec2','Last Account','testexec2@test.com',execAcc2, execConRType);
        Contact execCon3 = TestDataGenerator.createTestContact(true,'Test Exec3','Last Account','testexec3@test.com',execAcc3, execConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);
        Opportunity execOpp2 = TestDataGenerator.createTestOpp(true,'Exec Test Opp2',Date.today(),execAcc2,'Closed Won',prod);
        Opportunity execOpp3 = TestDataGenerator.createTestOpp(true,'Exec Test Opp2',Date.today(),execAcc3,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);
        Event_Account_Details__c execPRecod2 = TestDataGenerator.createParticipationRecord(true,prod,execAcc2,execOpp2,execCon2,execPRRType);
        Event_Account_Details__c execPRecod3 = TestDataGenerator.createParticipationRecord(true,prod,execAcc3,execOpp3,execCon3,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod);
        Order execConf2 = TestDataGenerator.createConfirmation(true,execAcc2,execCon2,'Attending Event Contact',execOpp3,execOpp.CloseDate,execPRecod2,prod);
        Order execConf3 = TestDataGenerator.createConfirmation(true,execAcc3,execCon3,'Attending Event Contact',execOpp3,execOpp.CloseDate,execPRecod2,prod);   

        List<Event_Account_Details__c> partRecords = new List<Event_Account_Details__c>{execPRecod, execPRecod2,execPRecod3};
        test.startTest();
            UtilClass.transformToProfileWrapper(partRecords);
        test.stopTest();

    }
}