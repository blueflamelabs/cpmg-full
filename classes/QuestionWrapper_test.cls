@isTest
private class QuestionWrapper_test {
    
    @isTest static void testQuestionMultiSelect() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id excuAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
        Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);

        Contact attendContact = TestDataGenerator.createTestContact(true,'Test','Attending Con', 'testAttendingCon@account.com',a,suppConRtype);
        Contact userCreateContact = TestDataGenerator.createTestContact(true,'Test1','Attending Co1n', 'testAttendingCon1@account.com',a,suppConRtype);
        
        //Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
         Product2 event = new Product2(Name='testProduct', User_Link_Expiration_for_Executives__c= 2, User_Link_Expiration_for_Suppliers__c = 2);
        insert event;
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',event);

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,event,a,opp,c, execPartRecordType);
        partRecord.Registration_Part_1_Submitted__c = true;
        update partRecord;

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,event);
        //***************************************************************************************
        Question__c p_question = new Question__c(Multi_Select__c = true, Includes_Golf__c = true);
        insert p_question;

        //Product2 event = new Product2(Name='testProduct', User_Link_Expiration_for_Executives__c= 2, User_Link_Expiration_for_Suppliers__c = 2);
       //insert event;

        string phase = 'testPhase';
        Event_Question__c p_eventQuestion = new Event_Question__c(Event__c=event.Id, Phase__c=phase, Question__c=p_question.Id);
       // p_eventQuestion.Display_Type__c='Primary Area of Interest';
        insert p_eventQuestion;
        

        Contact_Event_Answers__c p_eventAnswer = new Contact_Event_Answers__c(Sub_Question_Answer__c='test',Confirmation__c = conf.Id, Picklist_IDs__c='asdfaesw');
        insert p_eventAnswer;

        Contact myContact = new Contact();
        Integer questionNum = 5;                        

        QuestionWrapper questionWrapper = new QuestionWrapper(p_question, p_eventQuestion, p_eventAnswer, myContact.Id, questionNum);

        test.startTest();
        questionWrapper.setSubAnswer('testtest');
        questionWrapper.setSubPicklist('testtset');
        questionWrapper.setAnswerList('test');
        questionWrapper.answer = 'answer';
        System.assertEquals(p_question.Id, questionWrapper.question.Id);
        System.assertEquals(p_eventQuestion.Id, questionWrapper.eventQuestion.Id);
        System.assertEquals(p_eventAnswer.Id, questionWrapper.eventAnswer.Id);
        System.assertNotEquals(null, questionWrapper.maxChars);
        System.assertNotEquals(null, questionWrapper.questOrder);
        System.assertNotEquals(null, questionWrapper.subAnswer);
        System.assertNotEquals(null, questionWrapper.subPicklist);
        System.assertNotEquals(null, questionWrapper.answerList);
        System.assertNotEquals(null, questionWrapper.golfOptions);
        System.assertNotEquals(null, questionWrapper.getGolfOptions());
        System.assertNotEquals(null, questionWrapper.getSubPicklist());
        System.assertNotEquals(null, questionWrapper.getMaxChars());
        System.assertNotEquals(null, questionWrapper.answer);
        System.assertNotEquals(null, questionWrapper.getGolfOptions());
        test.stopTest();
    }
    
    @isTest static void testQuestionNotMultiSelect() {

        Question__c p_question = new Question__c(Picklist__c=true, Multi_Select__c = false, Includes_Golf__c = true);
        insert p_question;

        Question_Answers_Values__c  questionAnswer = new Question_Answers_Values__c(Name='test',Question__c=p_question.Id, Value__c='test', Golf__c = true);
        insert questionAnswer;

        Question__c newQuestion = [SELECT Id, Picklist__c, Multi_Select__C,Question_Type__c , Includes_Golf__C, Max_Char_Count__c,
            (SELECT ID, Name, Value__c,Golf__c FROM Question_Answers_Values__r) 
            FROM Question__c where Id = :p_question.Id];

        Product2 event = new Product2(Name='testProduct', User_Link_Expiration_for_Executives__c= 2, User_Link_Expiration_for_Suppliers__c = 2);
        insert event;

        string phase = 'testPhase';
        Event_Question__c p_eventQuestion = new Event_Question__c(Event__c=event.Id, Phase__c=phase, Question__c=newQuestion.Id);
        insert p_eventQuestion;

        Contact_Event_Answers__c p_eventAnswer = new Contact_Event_Answers__c(Sub_Question_Answer__c='test', Picklist_IDs__c='test;asdfasd;asdfa');
        insert p_eventAnswer;

        Account acct = new Account(Name='SFDC Account');
        insert acct;

        ID acctID = acct.ID;

        Contact myContact = new Contact(
        FirstName='Joe',
        LastName='Smith',
        Phone='415.555.1212',
        AccountId=acctID);

        insert myContact;
        Integer questionNum = 5;                        

        QuestionWrapper questionWrapper = new QuestionWrapper(newQuestion, p_eventQuestion, p_eventAnswer, myContact.Id, questionNum);

        test.startTest();
        System.assertEquals(p_eventAnswer.Picklist_IDs__c, questionWrapper.singleAnswer);
        test.stopTest();
    }
    /*
    @isTest static void testMultiSelectTruePickListTrue(){
        
        Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id excuAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
        Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);

        Contact attendContact = TestDataGenerator.createTestContact(true,'Test','Attending Con', 'testAttendingCon@account.com',a,suppConRtype);
        Contact userCreateContact = TestDataGenerator.createTestContact(true,'Test1','Attending Co1n', 'testAttendingCon1@account.com',a,suppConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        partRecord.Registration_Part_1_Submitted__c = true;
        update partRecord;

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        
        Contact_Event_Answers__c p_eventAnswer = new Contact_Event_Answers__c(Sub_Question_Answer__c ='test',Confirmation__c = conf.id, Picklist_IDs__c= questionAnswer.Id+');
        insert p_eventAnswer;

        Question__c p_question = new Question__c(Picklist__c=true, Multi_Select__c = true, Includes_Golf__c = true);
        insert p_question;

        Question_Answers_Values__c  questionAnswer = new Question_Answers_Values__c(Name='test',Question__c=p_question.Id, Value__c='test value', Golf__c = false);
        insert questionAnswer;

        Question__c newQuestion = [SELECT Id, Picklist__c, Multi_Select__C, Includes_Golf__C, Max_Char_Count__c,
            (SELECT ID, Name, Value__c,Golf__c FROM Question_Answers_Values__r) 
            FROM Question__c where Id = :p_question.Id];

        
        Product2 event = new Product2(Name='testProduct', User_Link_Expiration_for_Executives__c= 2, User_Link_Expiration_for_Suppliers__c = 2);
        insert event;

        string phase = 'testPhase';
        Event_Question__c p_eventQuestion = new Event_Question__c(Event__c=event.Id, Phase__c=phase, Question__c=newQuestion.Id);
        
        insert p_eventQuestion;

        Account acct = new Account(Name='SFDC Account');
        insert acct;

        ID acctID = acct.ID;

        Contact myContact = new Contact(
        FirstName='Joe',
        LastName='Smith',
        Phone='415.555.1212',
        AccountId=acctID);

        insert myContact;
        Integer questionNum = 5;                        

        QuestionWrapper questionWrapper = new QuestionWrapper(newQuestion, p_eventQuestion, p_eventAnswer, myContact.Id, questionNum);

        test.startTest();
        System.assertNotEquals(null, questionWrapper.answerList);
        test.stopTest();
    }*/

    @isTest static void testEventAnswerNull(){
        Question__c p_question = new Question__c(Picklist__c=true, Multi_Select__c = false, Includes_Golf__c = true);
        insert p_question;

        Product2 event = new Product2(Name='testProduct', User_Link_Expiration_for_Executives__c= 2, User_Link_Expiration_for_Suppliers__c = 2);
        insert event;

        string phase = 'testPhase';
        Event_Question__c p_eventQuestion = new Event_Question__c(Event__c=event.Id, Phase__c=phase, Question__c=p_question.Id);
        insert p_eventQuestion;

        Account acct = new Account(Name='SFDC Account');
        insert acct;

        ID acctID = acct.ID;

        Contact myContact = new Contact(
        FirstName='Joe',
        LastName='Smith',
        Phone='415.555.1212',
        AccountId=acctID);

        insert myContact;
        Integer questionNum = 5;                        

        QuestionWrapper questionWrapper = new QuestionWrapper(p_question, p_eventQuestion, null, myContact.Id, questionNum);

        test.startTest();
        System.assertNotEquals(null, questionWrapper.eventAnswer);
        test.stopTest();
    }
    
}