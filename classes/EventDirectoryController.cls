public class EventDirectoryController {
    private Set<Id> eventIds {get; set;}
    public DirectoryWrapper dirWrapper {get;set;}
    private List<Event_Account_Details__c> participationRecordsForEvent {get;set;}
    private Set<String> questionTypes {get;set;}
    private Map<String,Contact_Event_Answers__c> answersByPRAndQType {get;set;}
    private List<Speaker_Session__c> eventSpeakers {get;set;}
    private List<Event_Staff__c> staffForEvent {get;set;}
    public List<Account> objAccountListSupplier{set;get;}
    public List<Contact> objContactListExecutive{set;get;}
    

    public User currentUser {get; set;}

    public String navColor {get; set;}

    public String eventId {get; set;}

    public Product2 targetEvent {get;set;}

    public EventDirectoryController(){
        questionTypes = new Set<String>{'Company Short Description','Company Long Description'};
        try{
            String userId = UserInfo.getUserId(); //'00563000000YS32AAG';
            system.debug(userId);
            System.debug('USER___ userId ' + userId);
            List<User> currentUsers = [SELECT Id, AccountId, ContactId, Contact.RecordType.Name, Username, Name FROM User WHERE Id =: userId];
           targetEvent = getEvent();
           navColor = targetEvent.Event_Navigation_Color__c;

            
            if(String.isBlank(eventId) && targetEvent.Id != null){
                eventId = targetEvent.Id;
            }
            eventIds = new Set<Id>{eventId};

            if(currentUsers.size() == 0){
                String errMessage = 'User record is not found';
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, errMessage));
                return;
            }
            currentUser = currentUsers[0];
            instantiateDirectory(eventIds, questionTypes);

        }catch(Exception ex){
            System.debug('Error: '+ ex.getMessage()+'. Stack Trace: '+ ex.getStackTraceString());
        }
  
    }

    public void instantiateDirectory(Set<Id> eventIds, Set<String> questionTypes){
        dirWrapper = new DirectoryWrapper();
        getParticipationRecordsByEvent(eventIds);
        getParticipationRecordQuestionAnswers(eventIds,questionTypes);
        getEventStaff(eventIds);
        getSpeakers(eventIds);
        prepareParticipationRecordWrappers(participationRecordsForEvent, answersByPRAndQType);
        prepareEventStaff(staffForEvent);
        prepareSpeakers(eventSpeakers);
        dirWrapper.JSONSerializeAllCollections();
        System.debug('THE JSON FOR Executive List: '+ dirWrapper.executiveJSON);
        System.debug('THE JSON FOR Supplier List: '+dirWrapper.supplierJSON);
        System.debug('THE JSON FOR Staff List: '+dirWrapper.staffJSON);
        System.debug('THE JSON FOR Speaker List: '+dirWrapper.speakerJSON);
    }

    private void getParticipationRecordsByEvent(Set<Id> eventIds){
      /* participationRecordsForEvent = new List<Event_Account_Details__c>();
       Set<Id> AccountDetailsId = new Set<Id>();
       Map<Id,Event_Account_Details__c> participationRecords = new Map<Id,Event_Account_Details__c>([SELECT Id,Account__c, Account__r.Name,  Account__r.Description, Account__R.website, Event__c, Location__c, Primary_Contact__c,
            Primary_Contact__r.Name, Primary_Contact__r.Title, Primary_Contact__r.Account.Name, Primary_Contact__r.RecordTypeID,
            Primary_Contact__r.RecordType.Name, Primary_Contact__r.Account.Website, Account__r.RecordTypeID, Account__r.RecordType.Name,
            Company_Display_Name__c 
            FROM Event_Account_Details__c WHERE Event__c IN : eventIds  and Account__r.Type <> 'Staff' ORDER BY Account__r.Name ASC]);
            
        List<Order> objOrderList = [Select id,Status,Event_Account_Detail__c From Order where Status != 'Cancelled' and Event_Account_Detail__c IN : participationRecords.keySet()];
        for(Order objOrder : objOrderList){
            AccountDetailsId.add(objOrder.Event_Account_Detail__c);
        }
        for(Id objAccDeId : AccountDetailsId){
            participationRecordsForEvent.add(participationRecords.get(objAccDeId));
        }*/
        participationRecordsForEvent = ParticipationRecordHelper.getAllParticipationRecordsByEvent(EventIds);
        //List<Event_Account_Details__c> objPRForEvent = ParticipationRecordHelper.getAllParticipationRecordsByEvent(EventIds);
        System.debug('###############3 '+participationRecordsForEvent.size());
        
    }
    private void getParticipationRecordQuestionAnswers(Set<Id> eventIds, Set<String> questionTypes){
        answersByPRAndQType = ContactEventAnswerHelper.getLongAndShortProfiles(EventIds, questionTypes);
    }

    private void getEventStaff(Set<id> eventIds){
        staffForEvent = EventStaffHelper.getEventStaffByEvent(eventIds);
        System.debug('Staff for Event: '+ staffForEvent);
    }

    private void getSpeakers(Set<Id> eventIds){
        eventSpeakers = SpeakerSessionHelper.getSpeakersForEvent(eventIds);
        System.debug('eventSpeakers for Event: '+ eventSpeakers);
    }

    private void prepareParticipationRecordWrappers(List<Event_Account_Details__c> participationRecords, Map<String,Contact_Event_Answers__c> prByQTypeAnswerMap){
        Map<Id,Event_Account_Details__c> objEventAccountDetails = new Map<Id,Event_Account_Details__c>();
        Map<Id,Event_Account_Details__c> objEventAccountExecutive = new Map<Id,Event_Account_Details__c>();
        for(Event_Account_Details__c pRecord : participationRecords){
            Contact_Event_Answers__c longDesc = ContactEventAnswerHelper.getAnswerByPRandQType(pRecord.ID, 'Long', prByQTypeAnswerMap);
            Contact_Event_Answers__c shortDesc  = ContactEventAnswerHelper.getAnswerByPRandQType(pRecord.ID, 'Short', prByQTypeAnswerMap);
            Contact_Event_Answers__c categoryAnswer  = ContactEventAnswerHelper.getAnswerByPRandQType(pRecord.ID, 'Category', prByQTypeAnswerMap);
            //Need to differentiate between Supplier PR's and Executive PRs;
            if(pRecord.Account__r.RecordType.Name == 'Executive'){
                objEventAccountExecutive.put(pRecord.id,pRecord);
                //dirWrapper.addExecutive(createParticipationRecordWrapper(pRecord, longDesc, shortDesc, categoryAnswer));
            }else{
                objEventAccountDetails.put(pRecord.id,pRecord);
            }
        }
       Map<String,Event_Account_Details__c> objEventAccountexecutiveList = new Map<String,Event_Account_Details__c>();
       List<Order> objOrderListex = [select id,Company_Display_Name__c,BillToContactId,Event_Account_Detail__c,Status,Company_Name__c from Order where Event_Account_Detail__c IN : objEventAccountExecutive.keySet() 
                                       and Status != 'Cancelled' and Primary_Event_Contact__c = true order by Company_Name__c ASC];
       for(Order objOrder : objOrderListex){
           objEventAccountexecutiveList.put(objOrder.Event_Account_Detail__c,objEventAccountExecutive.get(objOrder.Event_Account_Detail__c));
       } 
       for(Event_Account_Details__c pRecord : objEventAccountexecutiveList.values()){
            Contact_Event_Answers__c longDesc = ContactEventAnswerHelper.getAnswerByPRandQType(pRecord.ID, 'Long', prByQTypeAnswerMap);
            Contact_Event_Answers__c shortDesc  = ContactEventAnswerHelper.getAnswerByPRandQType(pRecord.ID, 'Short', prByQTypeAnswerMap);
            Contact_Event_Answers__c categoryAnswer  = ContactEventAnswerHelper.getAnswerByPRandQType(pRecord.ID, 'Category', prByQTypeAnswerMap);
            //Need to differentiate between Supplier PR's and Executive PRs;
            if(pRecord.Account__r.RecordType.Name == 'Executive' && pRecord.Account__r.Type != 'Vendor'){
               dirWrapper.addExecutive(createParticipationRecordWrapper(pRecord, longDesc, shortDesc, categoryAnswer));
            }
        } 
       
       Set<Id> objEventAccID = new Set<Id>();
       Set<Id> objEventAccIDCompany = new Set<Id>();
       //Need Differentiate Supplier
       Map<String,Event_Account_Details__c> objEventAccountDetailsList = new Map<String,Event_Account_Details__c>();
       List<Order> objOrderList = [select id,Company_Display_Name__c,BillToContactId,Event_Account_Detail__c,Status,Company_Name__c from Order where Event_Account_Detail__c IN : objEventAccountDetails.keySet() and Status != 'Cancelled' and Primary_Event_Contact__c = true];
       for(Order objOrder : objOrderList){
           if(String.isNotBlank(objOrder.Company_Display_Name__c)){
               if(!objEventAccountDetailsList.keySet().Contains(objOrder.Company_Display_Name__c)){
                     objEventAccountDetailsList.put(objOrder.Company_Display_Name__c,objEventAccountDetails.get(objOrder.Event_Account_Detail__c));  
               }else{
                   objEventAccID.add(objEventAccountDetailsList.get(objOrder.Company_Display_Name__c).id);
                   objEventAccID.add(objOrder.Event_Account_Detail__c);
               }      
           }  
           else{
               if(!objEventAccountDetailsList.keySet().Contains(objOrder.Company_Name__c)){
                   objEventAccountDetailsList.put(objOrder.Company_Name__c,objEventAccountDetails.get(objOrder.Event_Account_Detail__c));
               }else{
                   objEventAccIDCompany.add(objEventAccountDetailsList.get(objOrder.Company_Name__c).id);
                   objEventAccIDCompany.add(objOrder.Event_Account_Detail__c);
               }  
           }  
       } 
       System.debug('$$$$$$$objEventAccIDCompany$$$4 '+objEventAccIDCompany);
       List<Contact_Event_Answers__c> objContactEventAns = [SELECT Id,Answer__c, Question__c, Participation_Record__c, Confirmation__c,Confirmation__r.Company_Display_Name__c, Question__r.Event__c, 
                                                           Question__r.Display_Type__c, Question__r.Phase__c,
                                                           Question__r.Question_Display__c, Question__r.Question__c
                                                           FROM Contact_Event_Answers__c WHERE Question__r.Event__c In :eventIds and Participation_Record__c IN : objEventAccID order by LastModifiedDate ASC];
                                                           
       for(Contact_Event_Answers__c objConEvent : objContactEventAns){
         if(String.isNotBlank(objConEvent.Answer__c))
           objEventAccountDetailsList.put(objConEvent.Confirmation__r.Company_Display_Name__c,objEventAccountDetails.get(objConEvent.Participation_Record__c));
       }  
       
       
       List<Contact_Event_Answers__c> objContactEventAnsCompany = [SELECT Id,Answer__c, Question__c, Participation_Record__c, Confirmation__c,Confirmation__r.Company_Name__c, Question__r.Event__c, 
                                                           Question__r.Display_Type__c, Question__r.Phase__c,
                                                           Question__r.Question_Display__c, Question__r.Question__c
                                                           FROM Contact_Event_Answers__c WHERE Question__r.Event__c In :eventIds and Participation_Record__c IN : objEventAccIDCompany order by LastModifiedDate ASC];
                                                           
       for(Contact_Event_Answers__c objConEvent : objContactEventAnsCompany){
          if(String.isNotBlank(objConEvent.Answer__c))
           objEventAccountDetailsList.put(objConEvent.Confirmation__r.Company_Name__c,objEventAccountDetails.get(objConEvent.Participation_Record__c));
       }  
           
                                                        
       for(Event_Account_Details__c pRecord : objEventAccountDetailsList.values()){
            Contact_Event_Answers__c longDesc = ContactEventAnswerHelper.getAnswerByPRandQType(pRecord.ID, 'Long', prByQTypeAnswerMap);
            Contact_Event_Answers__c shortDesc  = ContactEventAnswerHelper.getAnswerByPRandQType(pRecord.ID, 'Short', prByQTypeAnswerMap);
            Contact_Event_Answers__c categoryAnswer  = ContactEventAnswerHelper.getAnswerByPRandQType(pRecord.ID, 'Category', prByQTypeAnswerMap);
            //Need to differentiate between Supplier PR's and Executive PRs;
            if(pRecord.Account__r.RecordType.Name == 'Supplier' && pRecord.Account__r.Type != 'Vendor'){
               dirWrapper.addSupplier(createParticipationRecordWrapper(pRecord, longDesc, shortDesc, categoryAnswer));
            }
        }
        
    }

    private void prepareEventStaff(List<Event_Staff__c> staffForEvent){
        for(Event_Staff__c eStaff : staffForEvent){
            dirWrapper.addStaff(eStaff);
        }
    }

    private void prepareSpeakers(List<Speaker_Session__c> speakersForEvent){
        for(Speaker_Session__c speaker : speakersForEvent){
            dirWrapper.addSpeaker(speaker);
        }
    }

    private static ParticipationRecordWrapper createParticipationRecordWrapper(Event_Account_Details__c participationRecord, Contact_Event_Answers__c longProfile, Contact_Event_Answers__c shortProfile, Contact_Event_Answers__c categoryQuestion){
        return new ParticipationRecordWrapper(participationRecord, longProfile, shortProfile, categoryQuestion);
    }

    private Product2 getEvent(){
                Id event_id_param = ApexPages.currentPage().getParameters().get('eId');
                Product2 target_product = NULL;
                //target_product = NULL;
                if (event_id_param != null) {
                    target_product = EventHelper.getEvent(event_id_param);

                } else {
                    Id userId = UserInfo.getUserId();
                    User currentUser = [SELECT Id, Name, ContactId, Contact.RecordTypeId FROM User where Id = :userId];
                    if(currentUser.contactId != null){
                        List<Order> orders = [SELECT Id, Order_Event__c FROM Order WHERE (Order_Event__r.Start_Date__c >= Today AND BillToContactId =:currentUser.ContactId) OR (BillToContactId =:currentUser.ContactId) ORDER BY Order_Event__r.Start_Date__c ASC LIMIT 1];
                        if (orders.size() > 0) {
                            Id pId = orders.get(0).Order_Event__c;
                            target_product = EventHelper.getEvent(pId);
                        }
                    }
                if(target_product == NULL) {
                        target_product = EventHelper.getClosestEventByDate();
                    }
                }
                return target_product;
    }  
}