public without sharing class ProfileCOmponentController {

	public List<Schema.FieldSetMember> accountFormFields{get{
			if(accountFormFields == null){
				accountFormFields = SObjectType.Account.FieldSets.User_Specified_Fields.getFields();
			}
			return accountFormFields;
		}set;}
	public List<Schema.FieldSetMember> contactFormFields{get{
			if(contactFormFields == null){
				contactFormFields = SObjectType.Contact.FieldSets.Self_User_Specified_Fields.getFields();
			}
			return contactFormFields;
		}set;}
	
  	public Map<String, Account_Form_Display_Settings__c> accountUserSpecifiedFieldNames { get{
  		if(accountUserSpecifiedFieldNames == null){
  			accountUserSpecifiedFieldNames = New Map<String,Account_Form_Display_Settings__c>(Account_Form_Display_Settings__c.getAll());
  			for(Schema.FieldSetMember field :accountFormFields){
    
	      		if(!accountUserSpecifiedFieldNames.containsKey(field.getFieldPath())) {
	        		accountUserSpecifiedFieldNames.put(field.getFieldPath(), new Account_Form_Display_Settings__c(Include_in_Part_2_Profile__c=false,Read_Only__c = false, Field_Display_Name__c = field.getLabel()));
	      		}
        //System.debug('Field: ' + accountUserSpecifiedFieldNames.get(field.getFieldPath()));
    		}
  		}
  		return accountUserSpecifiedFieldNames;
  		} set; }
  	public Map<String, Contact_Form_Display_Settings__c> contactUserSpecifiedFieldNames { get{
  		if(contactUserSpecifiedFieldNames == null){
  			contactUserSpecifiedFieldNames = New Map<String,Contact_Form_Display_Settings__c>(Contact_Form_Display_Settings__c.getAll());
  			for(Schema.FieldSetMember field :contactFormFields){
    
	      		if(!contactUserSpecifiedFieldNames.containsKey(field.getFieldPath())) {
	        		contactUserSpecifiedFieldNames.put(field.getFieldPath(), new Contact_Form_Display_Settings__c(Include_in_Part_2_Profile__c=false,Read_Only__c = false, Field_Display_Name__c = field.getLabel()));
	      		}
        //System.debug('Field: ' + accountUserSpecifiedFieldNames.get(field.getFieldPath()));
    		}
  		}
  		return contactUserSpecifiedFieldNames;
  		} set; }

	public ProfileCOmponentController() {
 
    
	}

	public Map<String, Account_Form_Display_Settings__c>  getAccountUserSpecifiedFieldNames(){
		return accountUserSpecifiedFieldNames;
	}

	public Map<String, Contact_Form_Display_Settings__c> getContactUserSpecifiedFieldNames(){
		return contactUserSpecifiedFieldNames;
	}
}