public with sharing class DirectoryWrapper {
	
	//public List<ParticipationRecordWrapper> executiveDirectoryList {get;set;}
	//public List<ParticipationRecordWrapper> supplierDirectoryList {get;set;}
	public List<Map<String,Object>> executiveDirectoryList {get;set;}
	public List<Map<String,Object>> supplierDirectoryList {get;set;}
	public List<Map<String,Object>> cpmgStaffList{ get;set;}
	public List<Map<String,Object>> speakerList {get;set;}
	public String executiveJSON {get;set;}
	public String supplierJSON {get;set;}
	public String staffJSON {get;set;}
	public String speakerJSON {get;set;}

	public DirectoryWrapper() {
		//executiveDirectoryList = new List<ParticipationRecordWrapper>();
		//supplierDirectoryList = new List<ParticipationRecordWrapper>();
		executiveDirectoryList = new List<Map<String,Object>>();
		supplierDirectoryList = new List<Map<String,Object>>();
		cpmgStaffList = new List<Map<String,Object>>();
		speakerList = new List<Map<String,Object>>();
	}

	public void addExecutive(ParticipationRecordWrapper pRecordWrapperExecutive){
		executiveDirectoryList.add(pRecordWrapperExecutive.jsonMap);
	}

	public void addSupplier(ParticipationRecordWrapper pRecordWrapperSupplier){
		supplierDirectoryList.add(pRecordWrapperSupplier.jsonMap);
	}

	public void addStaff(Event_Staff__c eStaff){
		cpmgStaffList.add(EventStaffHelper.eventStaffJSON(eStaff));
	}

	public void addSpeaker(Speaker_Session__c speaker){
		speakerList.add(SpeakerSessionHelper.getJSONSpeakerMap(speaker));
	}



	public void JSONSerializeAllCollections(){
		JSON.createGenerator(true);
		executiveJSON = JSON.serialize(executiveDirectoryList);
		supplierJSON = JSON.serialize(supplierDirectoryList);
		staffJSON = JSON.serialize(cpmgStaffList);
		speakerJSON = JSON.serialize(speakerList);
	}

	
}