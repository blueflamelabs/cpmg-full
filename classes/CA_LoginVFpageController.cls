global with sharing class CA_LoginVFpageController {
    public String email {get;set;}
    public String emailError {get;set;}
    public boolean disablePasswordLabel {get;set;}
    public string Login_password {get;set;}
    public boolean disableLoginButton{get;set;}
    public boolean disableLoginButton1{get;set;}
    public Boolean redirect {public get; private set;}
    public String redirectURL {public get; private set;}
    global String originStartUrl{get;set;}
    
    public string Tstvar{get;set;}
    public Boolean isPasswordIncorrect {get;set;}
    public Boolean flag = false;
    public string orderEventId;
    public CA_LoginVFpageController() {
        redirect = false;
        isPasswordIncorrect = false;
        email = '';
        emailError = '';
        Login_password = '';
        disableLoginButton = false;
        disableLoginButton1 = true;
        disablePasswordLabel = false;
        Tstvar ='';
        orderEventId = '';
    }
    
    public void CheckMailId() {
        system.debug('Check mail ::: ' + email);
        string UName;
        integer count =0;
        
        string Loginparameter = ApexPages.currentPage().getParameters().get('Parameter1');
        system.debug('Parameter value----> ' + Loginparameter);
        
        list<LoginParameter__c> LoginParameterList = new list<LoginParameter__c>();
        LoginParameterList = [SELECT ID,Name,Executive_Email__c,Supplier_Email__c,Parameter__c from LoginParameter__c where Parameter__c =:Loginparameter];
        system.debug('List of the Custon setting--->' + LoginParameterList);
        if(email != '')
        {
            list<USER> userList = new list<User>( [SELECT ID, FirstName, Name, ContactId, Username FROM User WHERE Username =: email] );
            list<Contact> contactList = new list<Contact>( [SELECT Id, FirstName, LastName, RecordType.DeveloperName, Name FROM Contact WHERE Email=:email] );
            /*set<ID> userId = new set<ID>(); 
            if (userList != null)
            {
            for (USER us : userList)
            {
            if (us.Username == email) {
            userId.add(us.Id);
            UName = us.FirstName;
            }
            }
            }
            System.debug('UserId :::' + userId);*/
            System.debug( '----> Contact Record: '+contactList );
            if( contactList.isEmpty() && userList.isEmpty() ){
                emailError = 'We don’t recognize that email, please try again or contact CPMG for help. (Are you using your corporate email?)';
                disableLoginButton = false;
                disablePasswordLabel = false;
                disableLoginButton1 = true;
            }else if( userList.isEmpty() && !contactList.isEmpty() ){
                Contact objContact = contactList.get(0); 
                System.debug( '----> Contact Record Type: '+objContact.RecordType.DeveloperName );
                emailError = 'Hi '+objContact.FirstName+ ', thank you for visiting our website but we don’t have you registered for any upcoming events. '+
                            'Please contact '+ ( objContact.RecordType.DeveloperName == 'Supplier' ? LoginParameterList[0].Supplier_Email__c : LoginParameterList[0].Executive_Email__c ) +
                            ' for more help.';
                disableLoginButton = false;
                disableLoginButton1 = true;
                disablePasswordLabel = false;         
            }else if (Site.isValidUsername(email) && userList.size() > 0) {
                Community_Settings__c commSett = Community_Settings__c.getOrgDefaults();
                list<ORDER> OrderList = [SELECT ID, OwnerId, BillToContactId, Order_Event__c FROM Order WHERE BillToContactId =:userList[0].ContactId AND Event_Start_Date__c >= :commSett.First_Event_Date__c AND Event_End_Date__c >= TODAY];
                System.debug('here are the orders' + OrderList);
               list<ORDER> oldOrderList = [SELECT ID, OwnerId, BillToContactId, Order_Event__c FROM Order WHERE BillToContactId =:userList[0].ContactId AND Event_Start_Date__c >= :commSett.First_Event_Date__c AND Event_End_Date__c < TODAY]; 
                
                if (OrderList.size() > 0) {
                    set<Id> EventId = new set<Id>();
                    
                    for(ORDER ord : OrderList)
                    {
                        EventId.add(ord.Order_Event__c);
                    }
                    system.debug('-----Here is the Size of the Event Id set-- ' + EventId.size());
                    if(EventId.size() == 1 && oldOrderList.size()==0)
                        {
                            system.debug('The code is here---');
                            orderEventId = string.valueof(OrderList[0].Order_Event__c);

                        }
                        if(EventId.size() == 1 && oldOrderList.size()>0){
                            emailError = '';
                            disableLoginButton = true;
                            disableLoginButton1 = false;
                            disablePasswordLabel = true;
                            flag = true;
                        }
                    if(EventId.size() > 1)
                    {
                        list<Product2> ProductList = new list<Product2>();
                        ProductList = [SELECT Id, End_Date__c FROM Product2 WHERE Id IN:EventId];
                        system.debug('Product List with enddate ' + ProductList);
                        /*Here checking the end date of the Events.*/
                        for(Product2 prod : ProductList)
                        {
                            if(prod.End_Date__c >= date.today())
                            {
                                /*Here we are calculating the Events number who all are having dates in future */
                                count++;
                                
                            }
                        }
                        if(count >= 2)
                        {
                            system.debug('--------> We need to redirect it to event page <------');
                            emailError = '';
                            disableLoginButton = true;
                            disableLoginButton1 = false;
                            disablePasswordLabel = true;
                            flag = true;
                        }
                        else
                        {
                            emailError = '';
                            disableLoginButton = true;
                            disableLoginButton1 = false;
                            disablePasswordLabel = true;
                            flag = false;
                        }
                    }
                    else
                    {
                        emailError = '';
                        disableLoginButton = true;
                        disableLoginButton1 = false;
                        disablePasswordLabel = true;
                    }
                }
                else {
                
                     if(oldOrderList.size() > 0) {
                        disableLoginButton = true;
                         disableLoginButton1 = false;
                         disablePasswordLabel = true;
                         flag = true;
                    }else{
                        Contact cont = [SELECT ID, RecordTypeId FROM Contact WHERE ID =:userList[0].ContactId];
                        
                        RecordType recordType =[select DeveloperName, ID from RecordType where ID =: cont.RecordTypeId];
                        
                        string RecordTypeName = recordType.DeveloperName;
                        system.debug('Name of the Record type--->' + RecordTypeName);
                        if(userList[0].FirstName != null)
                        {
                            if(RecordTypeName == 'Supplier')
                            {
                                if(LoginParameterList.size() > 0)
                                {
                                    emailError = 'Hi '+userList[0].FirstName+ ', thank you for visiting our website but we don’t have you registered for any upcoming events. Please contact '+LoginParameterList[0].Supplier_Email__c +' for more help.';
                                }
                                
                            }
                            else
                            {
                                if(LoginParameterList.size() > 0)
                                {
                                    emailError = 'Hi '+userList[0].FirstName+ ', thank you for visiting our website but we don’t have you registered for any upcoming events. Please contact '+LoginParameterList[0].Executive_Email__c +' for more help.';
                                }
                                
                            }
                        }
                        else
                        {
                            if(RecordTypeName == 'Supplier')
                            {
                                if(LoginParameterList.size() > 0)
                                {
                                    emailError = 'Hi, thank you for visiting our website but we don’t have you registered for any upcoming events. Please contact '+LoginParameterList[0].Supplier_Email__c +' for more help.';
                                }
                            }
                            else
                            {
                                if(LoginParameterList.size() > 0)
                                {
                                    emailError = 'Hi, thank you for visiting our website but we don’t have you registered for any upcoming events. Please contact '+LoginParameterList[0].Executive_Email__c +' for more help.';
                                }
                                
                            }
                        }
                        
                        disableLoginButton = false;
                        disableLoginButton1 = true;
                        disablePasswordLabel = false;
                 }
             }
            } else {
                emailError = 'We don’t recognize that email, please try again or contact CPMG for help. (Are you using your corporate email?)';
                disableLoginButton = false;
                disablePasswordLabel = false;
                disableLoginButton1 = true;
            }
        }
        else
        {
            emailError = '';
            disableLoginButton = false;
            disableLoginButton1 = true;
            disablePasswordLabel = false;
        }
    }
    public pageReference LoginContrl() {
        isPasswordIncorrect = false;
        CheckMailId();
        if( String.isNotBlank( emailError ) ){
            return null;
        }
        
        system.debug('Password ' + Login_password);
        system.debug('Email Id ::' + email);
        originStartUrl = System.currentPageReference().getParameters().get('startURL');
        if(originStartUrl != null)
        {
            if(originStartUrl.contains('secur/RemoteAccessAuthorizationPage'))
            {

            PageReference redirectUserPage = new PageReference('/RedirectUser');
            redirectUserPage.getParameters().put('startURL', originStartUrl);
            String startUrl = redirectUserPage.getUrl();

            startUrl =  ApexPages.currentPage().getHeaders().get('startUrl');
            system.debug('refer startUrl : ' + startUrl);

            PageReference loginResult =  Site.login(email, Login_password, originStartUrl);
            return loginResult;
            }
            system.debug('originStartUrl : ' + originStartUrl);
            return null;
        }
        else
        {
            Tstvar = 'UserName::::'+email+':::::Password::::'+Login_password+'::::';
            string SingleEventURL = '/CommunitiesHomePage?eId=' + orderEventId;
            pageReference pg = Site.login(email,Login_password,SingleEventURL);
            //Below 3 lines are written for code coverage only.
            if(Test.isRunningTest() && pg == NULL && Apexpages.currentPage().getParameters().get('testloginmethod') != NULL){
                pg = new pageReference('/test.com');
            }
            // Test Coverage code ends here    
            Tstvar += String.valueOf(pg);
            if(pg != null && String.valueOf(pg) != 'null') {
                isPasswordIncorrect = false;
                emailError = '';
                Tstvar += ':::Inside pg';
                system.debug('Sucess---->');
                redirect = true;
                if(flag == true)
                {
                    pageReference eventpage = Site.login(email,Login_password,'/Events');
                    //Below 3 lines are written for code coverage only.
                    if(Test.isRunningTest() && Apexpages.currentPage().getParameters().get('testloginmethod') != NULL){
                        eventpage = new pageReference('/test.com');
                    }
                    // Test Coverage code ends here
                    redirectURL = eventpage.getUrl();
                }
                else
                {
                    redirectURL = pg.getUrl();
                }
                //redirectURL = pg.getUrl();
                //pg.setRedirect(true);
                //return pg;
            } else {
                isPasswordIncorrect = true;
                Tstvar += ':::Inside else pg';
                //pg.setRedirect(false);
                emailError = 'Your password is incorrect. Please try again or use the Forgot Password link.';
            }
            return null;
        }
    }
    public PageReference forgotPassword() {
        String ForgotPasswordPage = Label.ForgotPassword;
        PageReference pg = new PageReference(ForgotPasswordPage);
        pg.setRedirect(true);
        return pg;
    }
}