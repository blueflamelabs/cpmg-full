/************************************************/
/*
Who  : Vscidev
When : 06/18/2018
What : To create user and send welcome mails.
*/
/************************************************/
public with sharing class UserAndEmailCreationProcess {
    private boolean allowMail = false;
    private Set<Id> billToContactIds {get;set;}
    private Map<Id,User> contactIdUserMap {get;set;}
    private Map<Id,Contact> contactMap{get;set;}
    private List<User> usersToCreate {get;set;}
    private Community_Settings__c commSettings {get;set;}
    private list<Order> existingUserOrder {get;set;}
    private list<Order> newUserOrder {get;set;}
    private List<contact> contactsToUpdate {get;set;}
    Private Database.SaveResult[] results {get;set;}
    Private product2 Event{get;set;}
    Private list<Order> listOrder{get;set;}
    public string  userType{get;set;}
    Private String EventStaffEmail{set;get;}
    public set<string> eventStaffemailSet{get;set;}
    private Boolean isMailNotSend{set;get;}
    private List<order> objListorderUpdate{set;get;}
    private boolean orgAlreadyProcess = true;
    Blob cryptoKey = Blob.valueOf(system.label.cryptoKey);

    public UserAndEmailCreationProcess(string eventId,string userType){
        isMailNotSend = false;
        eventStaffemailSet = new set<string>();
        objListorderUpdate = new List<order>();
        this.userType = userType;
        Event = [select Id,Executive_Users_Created__c,Supplier_Users_Created__c,Allow_Welcome_Emails_for_Suppliers__c,Allow_Welcome_Emails_for_Executives__c  from product2 where id =:eventId];
        listOrder = [select Order_Event__r.User_Link_Expiration_for_Suppliers__c,Order_Event__r.User_Link_Expiration_for_Executives__c,Welcome_Email_Link_Expiration__c,Welcome_Email_Link__c,Send_Email__c,id,BillToContactId,Opportunity.Account_Record_Type__c,status,owner.email  from order where Order_Event__c =:eventId and opportunity.Account_Record_Type__c =:userType];
        objListorderUpdate.addAll(listOrder);
        
        if(userType == 'Supplier')
            Event.Supplier_Users_Created__c = true;
        else if(userType == 'Executive')
            Event.Executive_Users_Created__c = true;
            
        executeUserCreationProcess();
    }
    public UserAndEmailCreationProcess(String OrderId){
        isMailNotSend = false;
        eventStaffemailSet = new set<string>();
        Community_Settings__c commSettings = Community_Settings__c.getOrgDefaults();
        Date todaydate = Date.valueOf(System.now());
        Date startDate = commSettings.First_Event_date__c;        
        listOrder = [select Order_Event__r.User_Link_Expiration_for_Suppliers__c,Order_Event__r.User_Link_Expiration_for_Executives__c,Welcome_Email_Link_Expiration__c,Welcome_Email_Link__c,Order_Event__r.Registration_Part_1_Open_For_Executive__c,Order_Event__r.Registration_Part_1_Open_For_Supplier__c,BillToContact.recordType.name,Order_Event__r.Start_Date__c,Order_Event__c,Send_Email__c,
                    id,BillToContactId,Opportunity.Account_Record_Type__c,status,owner.email  
                    from order where id =:OrderId and Order_Event__r.Start_Date__c >=: startDate];
       if(listOrder.size() > 0){              
            Event = [select Id,Executive_Users_Created__c,Supplier_Users_Created__c,Allow_Welcome_Emails_for_Suppliers__c,Allow_Welcome_Emails_for_Executives__c  from product2 where id =:listOrder[0].Order_Event__c];
            this.userType = listOrder[0].Opportunity.Account_Record_Type__c;
            if(userType == 'Supplier')
                Event.Supplier_Users_Created__c = true;
            else if(userType == 'Executive')
                Event.Executive_Users_Created__c = true;
            
            executeUserCreationProcess();
      }  
    }
    public UserAndEmailCreationProcess(String OrderId,Boolean isMailNotSend){
        this.isMailNotSend = isMailNotSend;
        System.debug('#######isMailNotSend###333 '+isMailNotSend);
        System.debug('#######OrderId###333 '+OrderId);
        eventStaffemailSet = new set<string>();
        Community_Settings__c commSettings = Community_Settings__c.getOrgDefaults();
        Date todaydate = Date.valueOf(System.now());
        Date startDate = commSettings.First_Event_date__c;        
        listOrder = [select Order_Event__r.User_Link_Expiration_for_Suppliers__c,Order_Event__r.User_Link_Expiration_for_Executives__c,Welcome_Email_Link_Expiration__c,Welcome_Email_Link__c,Order_Event__r.Registration_Part_1_Open_For_Executive__c,Order_Event__r.Registration_Part_1_Open_For_Supplier__c,BillToContact.recordType.name,Order_Event__r.Start_Date__c,Order_Event__c,Send_Email__c,
                    id,BillToContactId,Opportunity.Account_Record_Type__c,status,owner.email  
                    from order where id =:OrderId and Order_Event__r.Start_Date__c >=: startDate];
       if(listOrder.size() > 0){              
            Event = [select Id,Executive_Users_Created__c,Supplier_Users_Created__c,Allow_Welcome_Emails_for_Suppliers__c,Allow_Welcome_Emails_for_Executives__c  from product2 where id =:listOrder[0].Order_Event__c];
            this.userType = listOrder[0].Opportunity.Account_Record_Type__c;
            if(userType == 'Supplier')
                Event.Supplier_Users_Created__c = true;
            else if(userType == 'Executive')
                Event.Executive_Users_Created__c = true;
            
            executeUserCreationProcess();
      }  
    }
    public UserAndEmailCreationProcess(List<Order> BatchOrderList){
        objListorderUpdate = new List<order>();
        isMailNotSend = false;
        eventStaffemailSet = new set<string>();
        listOrder = BatchOrderList;
        Event = [select Id,Executive_Users_Created__c,Supplier_Users_Created__c,Allow_Welcome_Emails_for_Suppliers__c,Allow_Welcome_Emails_for_Executives__c  from product2 where id =:listOrder[0].Order_Event__c];
        this.userType = listOrder[0].Opportunity.Account_Record_Type__c;
        objListorderUpdate.add(listOrder[0]);
        if(userType == 'Supplier')
            Event.Supplier_Users_Created__c = true;
        else if(userType == 'Executive')
            Event.Executive_Users_Created__c = true;
        executeUserCreationProcess();
    }
    
    public void executeUserCreationProcess(){
        if(objListorderUpdate != null && objListorderUpdate.size() > 0 && isMailNotSend == false){
            for(Order objorder : objListorderUpdate){
                objorder.Send_Email__c = true;
            }
            update objListorderUpdate;
        }
           
        commSettings = Community_Settings__c.getOrgDefaults();
        newUserOrder = new list<Order>();
        existingUserOrder = new list<Order>();
        billToContactIds = new Set<Id>();
        usersToCreate =new List<User>();
        contactsToUpdate = new List<contact>();
        system.debug(event.Allow_Welcome_Emails_for_Executives__c +'********'+userType);
        // isMailNotSend 
        if( (event.Allow_Welcome_Emails_for_Suppliers__c && userType=='Supplier')
            || (event.Allow_Welcome_Emails_for_Executives__c && userType=='Executive')
        ){
            allowMail = true;
        }
        
        if(isMailNotSend == true)
            allowMail = true;
        for(Order ord : listOrder){
            billToContactIds.add(ord.BillToContactId);
        }
        contactIdUserMap = UserHelper.getContactIdUserMap(billToContactIds);
        Map<Id,User> objConIdUserMap = new Map<Id,User>();
         List<User> objUser = [SELECT id, ContactId, UserName, Email,LastLoginDate FROM User where contactId IN :billToContactIds and LastLoginDate = null];
        for(User objU : objUser){
            objConIdUserMap.put(objU.ContactId,objU);
        }      
       
        contactMap =new Map<Id,Contact>([SELECT Id, User_Create_Confirmation__c,Email, FirstName, LastName,Phone, AccountId, Account.Type,RecordTypeId FROM Contact Where Id IN :billToContactIds]);
        String ChangePassword = Label.ChangePassword;
        String LoginUserPage = Label.LoginPage;
        if(UserInfo.getProfileId() != commSettings.Community_User_Profile__c)
            for(Order ord : listOrder){
                if(contactMap.containsKey(ord.BillToContactId)){
                  Contact con = contactMap.get(ord.BillToContactId);
                  //if(con.User_Create_Confirmation__c == null)contactsToUpdate.add(new Contact(Id = ord.BillToContactId, User_Create_Confirmation__c = ord.Id));                  
                  if(contactIdUserMap != null && !contactIdUserMap.containsKey(ord.BillToContactId) && con.Email != null){
                    if(userType == 'Supplier' && ord.Order_Event__r.User_Link_Expiration_for_Suppliers__c != null)
                        ord.Welcome_Email_Link_Expiration__c = Date.today().adddays(Integer.valueOf(ord.Order_Event__r.User_Link_Expiration_for_Suppliers__c));
                    else if(userType == 'Executive'  && ord.Order_Event__r.User_Link_Expiration_for_Executives__c != null)
                        ord.Welcome_Email_Link_Expiration__c = Date.today().adddays(Integer.valueOf(ord.Order_Event__r.User_Link_Expiration_for_Executives__c));
                    
                   String LinkExpir='';
                   if(ord.Welcome_Email_Link_Expiration__c !=null)
                        LinkExpir = String.valueOf(ord.Welcome_Email_Link_Expiration__c).replace('-','');
                    
                    System.debug('#########LinkExpirLinkExpir###### '+LinkExpir);
                    Blob tokenData = Blob.valueOf('{"contactId":"'+ord.BillToContactId+'","confirmationId":"'+ord.Id+'","token":"'+LinkExpir+'"}');
                    Blob encryptedData = Crypto.encryptWithManagedIV('AES128',cryptoKey,tokenData);
                    String token = EncodingUtil.base64Encode(encryptedData);
                    token  = EncodingUtil.urlEncode(token, 'UTF-8');
                    ord.Welcome_Email_Link__c = ChangePassword+token; //ChangePassword+ord.BillToContactId+'&ConfirmationId='+ord.Id+'&Token='+LinkExpir;

                    usersToCreate.add(UserHelper.CreateUser(commSettings.Community_User_Profile__c,
                                        (con.firstName != null)?con.firstName:null, 
                                         con.LastName, 
                                        (con.Phone != null)?con.Phone: null,
                                         con.Email,
                                         con.Id)
                                     );
                      newUserOrder.add(ord);                 
                  }else if(objConIdUserMap != null && objConIdUserMap.keyset().contains(ord.BillToContactId)){
                      if(userType == 'Supplier' && ord.Order_Event__r.User_Link_Expiration_for_Suppliers__c != null)
                            ord.Welcome_Email_Link_Expiration__c = Date.today().adddays(Integer.valueOf(ord.Order_Event__r.User_Link_Expiration_for_Suppliers__c));
                       else if(userType == 'Executive' && ord.Order_Event__r.User_Link_Expiration_for_Executives__c != null)
                            ord.Welcome_Email_Link_Expiration__c = Date.today().adddays(Integer.valueOf(ord.Order_Event__r.User_Link_Expiration_for_Executives__c));
                      
                       String LinkExpir='';
                       if(ord.Welcome_Email_Link_Expiration__c !=null)
                            LinkExpir = String.valueOf(ord.Welcome_Email_Link_Expiration__c).replace('-','');
                      
                      System.debug('#########LinkExpirLinkExpir###### '+LinkExpir);
                      Blob tokenData = Blob.valueOf('{"contactId":"'+ord.BillToContactId+'","confirmationId":"'+ord.Id+'","token":"'+ LinkExpir+'"}');
                      Blob encryptedData = Crypto.encryptWithManagedIV('AES128',cryptoKey,tokenData);
                      String token = EncodingUtil.base64Encode(encryptedData);
                      token  = EncodingUtil.urlEncode(token, 'UTF-8');
                      ord.Welcome_Email_Link__c = ChangePassword+token;// ChangePassword+ord.BillToContactId+'&ConfirmationId='+ord.Id+'&Token='+LinkExpir;
                      
                      newUserOrder.add(ord);
                  }else if(con.Email != null){                      
                       ord.Welcome_Email_Link_Expiration__c = null;
                       ord.Welcome_Email_Link__c = LoginUserPage;
                       existingUserOrder.add(Ord);
                   }
                }
               
            }
       
       if(listOrder.size() > 0)
           update listOrder;
       if(Event != null)
              update Event;     
      // if(contactsToUpdate.size()>0) 
          // Update contactsToUpdate;  
          
       Set<Id> objConId = new Set<Id>();
       for(User objUserCon : usersToCreate){
           objConId.add(objUserCon.ContactId);
       }
       if(objConId.size() > 0){
           List<Contact> objConList = [Select Id,AccountId from Contact where Id IN : objConId];
           Set<Id> objAccId = new Set<Id>();
           for(Contact objCon : objConList){
               objAccId.add(objCon.AccountId);
           }
           if(objAccId.size() > 0){
               List<Account> objAccList = [Select Id,Name,OwnerId from Account where Id IN : objAccId AND (Owner.UserRoleId =null OR Owner.isActive = false)];
               for(Account objAcc : objAccList){
                   if(String.isNotBlank(commSettings.Default_Owner__c)){
                       objAcc.OwnerId = commSettings.Default_Owner__c;
                   }
               }
               DataBase.update(objAccList,false);
           }
       }
       commitNewUsers();
       if(allowMail || test.isrunningTest())
           sendEmail();
            
    }
    private void commitNewUsers(){
        System.debug('$$$$$usersToCreate$$$$$$$$ '+usersToCreate);
        if(usersToCreate.size()>0){
          results = database.insert(usersToCreate,false);
          String emailContent = SaveResultEmailClass.processSaveResults(results, usersToCreate);
          Messaging.SingleEmailMessage message =  SingleMailMessage.createSingleMailMessage(new String[]{commSettings.Community_Notification_Email__c}, emailContent, 'User Creation Report');
          try{
              Messaging.sendEmail(new Messaging.SingleEmailMessage[]{ message },false);
          }catch(Exception ex){
              System.debug('######## '+ex.getMessage());
          }
        }
  }
  public void sendEmail(){
     Map<String,String> objMapOrderIDMail = new Map<String,String>();
     boolean isUseOrg = false;
     EventStaffEmail = null;
     String OwnerName = '';
     String RoleName = '';

     if(userType == 'Supplier')
         RoleName = 'Support';
     else if(userType == 'Executive')
         RoleName = 'Recruiter';
         
     List<Event_Staff__c> objEventStaff = [select id,User__r.IsActive,User__r.Email,User__r.Name,Role__c,CPMG_Event__c,Display_to_Suppliers__c  from Event_Staff__c where CPMG_Event__c =: event.Id and Role__c =: RoleName and User__r.IsActive=true and User__r.Email not in:eventStaffemailSet limit 1];
     if(objEventStaff.size() > 0){             
        EventStaffEmail = objEventStaff[0].User__r.Email;
        OwnerName = objEventStaff[0].User__r.Name;
        eventStaffemailSet.add(EventStaffEmail);
     }
     Map<Id,Id> objContactorOrderId = new Map<Id,Id>();
     map<string,OrgWideEmailAddress> mapOfOrgWideEmailAddress = new map<string,OrgWideEmailAddress>();
     for(OrgWideEmailAddress owea : [select Id,Address,DisplayName from OrgWideEmailAddress ]){
         mapOfOrgWideEmailAddress.put(owea.Address,owea);
     }
     list<Messaging.SingleEmailMessage> listOfEmails = new list<Messaging.SingleEmailMessage>();
     for(Community_Welcome_Emails__mdt communitywel : [select id,Sender_Name__c,Channel__c,Default_Email_Sender__c,Email_Template_ID__c,Status__c,User_Type__c from Community_Welcome_Emails__mdt]){
          for(Order ord : newUserOrder){
              objContactorOrderId.put(ord.BillToContactId,ord.id);
              if(ord.Opportunity.Account_Record_Type__c == communitywel.Channel__c &&
                 communitywel.User_Type__c =='New User' &&
                 ord.status == communitywel.Status__c
              ){
                  string owaId;
                  if(mapOfOrgWideEmailAddress.keyset().contains(EventStaffEmail))
                      owaId = mapOfOrgWideEmailAddress.get(EventStaffEmail).id;
                  else if(mapOfOrgWideEmailAddress.keyset().contains(communitywel.Default_Email_Sender__c) && orgAlreadyProcess){
                      isUseOrg = true;
                      owaId = mapOfOrgWideEmailAddress.get(communitywel.Default_Email_Sender__c).id;
                      //OwnerName = mapOfOrgWideEmailAddress.get(communitywel.Default_Email_Sender__c).DisplayName;
                     OwnerName = communitywel.Sender_Name__c;
                  }else
                    OwnerName = userinfo.getName();     
                      
                  listOfEmails.add(createEmailMessage(ord.BillToContactID,(string)ord.id,communitywel.Email_Template_ID__c,owaId));
                  objMapOrderIDMail.put(String.valueOf(ord.id),OwnerName);
              }
              
          }
          for(Order ord : existingUserOrder){
              objContactorOrderId.put(ord.BillToContactId,ord.id);
              if(ord.Opportunity.Account_Record_Type__c == communitywel.Channel__c &&
                 communitywel.User_Type__c =='Existing User' &&
                 ord.status == communitywel.Status__c
              ){
                  string owaId;
                  if(mapOfOrgWideEmailAddress.keyset().contains(EventStaffEmail))
                      owaId = mapOfOrgWideEmailAddress.get(EventStaffEmail).id;    
                  else if(mapOfOrgWideEmailAddress.keyset().contains(communitywel.Default_Email_Sender__c) && orgAlreadyProcess){
                      isUseOrg = true;
                      owaId = mapOfOrgWideEmailAddress.get(communitywel.Default_Email_Sender__c).id;
                      //OwnerName = mapOfOrgWideEmailAddress.get(communitywel.Default_Email_Sender__c).DisplayName;
                      OwnerName = communitywel.Sender_Name__c;
                  }else
                      OwnerName = userinfo.getName(); 
                  listOfEmails.add(createEmailMessage(ord.BillToContactID,(string)ord.id,communitywel.Email_Template_ID__c,owaId));
                  objMapOrderIDMail.put(String.valueOf(ord.id),OwnerName);
              }
          }              
     }
     if(isUseOrg){
         orgAlreadyProcess = false;
     }
     if(listOfEmails.size()>0){
          List<Order> objOrderEventSupportUpdate = [select id,Send_Email__c,Event_Recruiter__c,Event_Support__c from Order where Id IN : objMapOrderIDMail.keySet()];
          for(Order objorder : objOrderEventSupportUpdate){
              if(userType == 'Supplier') 
                   objorder.Event_Support__c = objMapOrderIDMail.get(objorder.id);
              else if(userType == 'Executive')
                   objorder.Event_Recruiter__c = objMapOrderIDMail.get(objorder.id);
                   
             if(isMailNotSend == true && userType == 'Supplier')
                 objorder.Event_Support__c = '';
             else if(isMailNotSend == true && userType == 'Executive')
                 objorder.Event_Recruiter__c = '';
             
          } 
          update objOrderEventSupportUpdate ;
          try{
              Set<Id> objContactId = new Set<Id>();
             if(isMailNotSend == false){
              Messaging.SendEmailResult [] objresponse = Messaging.sendEmail(listOfEmails);
              for(Messaging.SendEmailResult objResponseMail : objresponse){
                  if(!objResponseMail.IsSuccess()){
                      Messaging.SendEmailError [] errArr = objResponseMail.getErrors();                  
                      for(Messaging.SendEmailError objError : errArr){                  
                          
                          Id objConId = (Id)objError.getTargetObjectId();
                          objContactId.add(objConId);
                      }
                  }
              }
              for(Id objConId : objContactId){
                  if(objContactorOrderId.containsKey(objConId))
                      objContactorOrderId.Remove(objConId);
              }
             List<Order> objOrderUpdate = [select id,Send_Email__c,Event_Support__c from Order where Id IN : objContactorOrderId.values()];
             for(Order objorder : objOrderUpdate){
                 objorder.Send_Email__c = true;
             } 
             update objOrderUpdate;
           }
          }catch(exception e){
              if(e.getmessage().contains('Organization-Wide Email Address has not be verified for use'))
                  sendEmail();
          }
      }
 }
 public Messaging.SingleEmailMessage createEmailMessage(string contactId,string orderId,string TemplateId,string owaId){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSaveAsActivity(true);
        if(owaId !=null)
            mail.setOrgWideEmailAddressId(owaId);
        mail.setTargetObjectId(contactId);
        mail.setWhatId(orderId);
        mail.setTemplateId(TemplateId);
        return mail;
 }
}