@isTest
public class EventDirectoryControllerTest {
    
    static testMethod void eventDirectory_Constructor_Test_No_FILTER(){ 
    
        //Create Test Data
        TestDataGenerator.createInactiveTriggerSettings();
        //Create CPMG_Notification Settings
        TestDataGenerator.createCPMGNotifiedUsers();
        TestDataGenerator.createOrderStatusCustomSetting();
            
        //Set Up Event
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        update prod;
        //Set Up EventStaff
        Profile standardUser = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

        //Create Speaker Sessions
        Speaker_Session__c speakSession1 = TestDataGenerator.createSpeakerSessions(true,String.valueOf( System.Now()), System.Now(),'Speaker Session 1', prod,'Overview Test','Http://www.google.com','Presenter 1','Presentation Title');
        Speaker_Session__c speakSession2 = TestDataGenerator.createSpeakerSessions(true,String.valueOf( System.Now().addDays(-11)), System.Now().addDays(-1),'Speaker Session 2', prod,'Overview Test','Http://www.cpmgInc.com','Presenter 2','Presentation Title For Session 2');  

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod,true);
        

        //Set Up Suppliers
        Id supplierAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id supplierConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account supAcc = TestDataGenerator.createTestAccount(true,'Test Sup Account', supplierAccRType);
        Contact supCon = TestDataGenerator.createTestContact(true,'Test Sup','Last Account','testsup123@test.com',supAcc,supplierConRType);

        User supUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Sup','Last Account','603-555-5555',supCon.Email, supCon.Id);

        Opportunity supOpp = TestDataGenerator.createTestOpp(true,'Supp Test Opp',Date.today(),supAcc,'Closed Won',prod);

        Id supplierPRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c supPRecod = TestDataGenerator.createParticipationRecord(true,prod,supAcc,supOpp,supCon,supplierPRType);

        Order supConf = TestDataGenerator.createConfirmation(true,supAcc,supCon,'Preliminary',supOpp,supOpp.CloseDate,supPRecod,prod,true);

        //Second Supplier
        Account supAcc2 = TestDataGenerator.createTestAccount(true,'Test Supplier Account2', supplierAccRType);
        Contact supCon2 = TestDataGenerator.createTestContact(true,'Test Supplier2','Last Account','testsupp2@test.com',supAcc2,supplierConRType);

        
        User supUser2 = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Exec','Last Account','603-555-5555',supCon2.Email, supCon2.Id);

        Opportunity supOpp2 = TestDataGenerator.createTestOpp(true,'Supplier2 Test Opp',Date.today(),supAcc2,'Closed Won',prod);

        Event_Account_Details__c supPRecod2 = TestDataGenerator.createParticipationRecord(true,prod,supAcc2,supOpp2,supCon2,supplierPRType);

        Order supConf2 = TestDataGenerator.createConfirmation(true,supAcc2,supCon2,'Preliminary',supOpp2,supOpp2.CloseDate,supPRecod2,prod,true);

        //Set Up the Page to the Community Home Page
        PageReference pageRef = Page.EventDirectory;
        pageRef.getParameters().put('eId', prod.Id);
        

        //PERFORM THE TESTING
        Test.startTest();
            System.runAs(execUser){
                Test.setCurrentPageReference(pageRef);
                EventDirectoryController eDirectory = new EventDirectoryController();

                System.assertEquals(eDirectory.eventId, prod.Id);
            }
        Test.stopTest();

    }
    
    static testMethod void eventDirectory_Test_FILTER(){                    
   
            //Create Test Data
        TestDataGenerator.createInactiveTriggerSettings();
        //Create CPMG_Notification Settings
        TestDataGenerator.createCPMGNotifiedUsers();
        TestDataGenerator.createOrderStatusCustomSetting();
            
        //Set Up Event
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        update prod;
        //Set Up EventStaff
        Profile standardUser = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter');
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson');
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support');

    

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod,true);
        

        //Set Up Suppliers
        Id supplierAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id supplierConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account supAcc = TestDataGenerator.createTestAccount(true,'Test Sup Account', supplierAccRType);
        Contact supCon = TestDataGenerator.createTestContact(true,'Test Sup','Last Account','testsup123@test.com',supAcc,supplierConRType);

        User supUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Sup','Last Account','603-555-5555',supCon.Email, supCon.Id);

        Opportunity supOpp = TestDataGenerator.createTestOpp(true,'Supp Test Opp',Date.today(),supAcc,'Closed Won',prod);

        Id supplierPRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c supPRecod = TestDataGenerator.createParticipationRecord(true,prod,supAcc,supOpp,supCon,supplierPRType);

        Order supConf = TestDataGenerator.createConfirmation(true,supAcc,supCon,'Preliminary',supOpp,supOpp.CloseDate,supPRecod,prod,true);

        //Second Supplier
        Account supAcc2 = TestDataGenerator.createTestAccount(true,'Test Supplier Account2', supplierAccRType);
        Contact supCon2 = TestDataGenerator.createTestContact(true,'Test Supplier2','Last Account','testsupp2@test.com',supAcc2,supplierConRType);

        
        User supUser2 = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Exec','Last Account','603-555-5555',supCon2.Email, supCon2.Id);

        Opportunity supOpp2 = TestDataGenerator.createTestOpp(true,'Supplier2 Test Opp',Date.today(),supAcc2,'Closed Won',prod);

        Event_Account_Details__c supPRecod2 = TestDataGenerator.createParticipationRecord(true,prod,supAcc2,supOpp2,supCon2,supplierPRType);

        Order supConf2 = TestDataGenerator.createConfirmation(true,supAcc2,supCon2,'Preliminary',supOpp2,supOpp2.CloseDate,supPRecod2,prod,true);

        //Create Speaker Sessions
        Speaker_Session__c speakSession1 = TestDataGenerator.createSpeakerSessions(true,String.valueOf( System.Now()), System.Now(),'Speaker Session 1', prod,'Overview Test','Http://www.google.com','Presenter 1','Presentation Title');
        Speaker_Session__c speakSession2 = TestDataGenerator.createSpeakerSessions(true,String.valueOf( System.Now().addDays(-11)), System.Now().addDays(-1),'Speaker Session 2', prod,'Overview Test','Http://www.cpmgInc.com','Presenter 2','Presentation Title For Session 2');

        //Set Up the Page to the Directory Home Page
        PageReference pageRef = Page.EventDirectory;
        pageRef.getParameters().put('eId', prod.Id);
        

        //PERFORM THE TESTING
        Test.startTest();
            System.runAs(execUser){
                Test.setCurrentPageReference(pageRef);
                EventDirectoryController eDirectory = new EventDirectoryController();

                System.assertEquals(eDirectory.eventId, prod.Id);
            }
        Test.stopTest();
        
    }
    
    static testmethod void eventDirectory_Supplier_Test_No_Eid(){
        //Create Test Data
        TestDataGenerator.createInactiveTriggerSettings();
        //Create CPMG_Notification Settings
        TestDataGenerator.createCPMGNotifiedUsers();
        TestDataGenerator.createOrderStatusCustomSetting();
            
        //Set Up Event
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        update prod;
        //Set Up EventStaff
        Profile standardUser = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

    

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod,true);
        

        //Set Up Suppliers
        Id supplierAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id supplierConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account supAcc = TestDataGenerator.createTestAccount(true,'Test Sup Account', supplierAccRType);
        Contact supCon = TestDataGenerator.createTestContact(true,'Test Sup','Last Account','testsup123@test.com',supAcc,supplierConRType);

        User supUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Sup','Last Account','603-555-5555',supCon.Email, supCon.Id);

        Opportunity supOpp = TestDataGenerator.createTestOpp(true,'Supp Test Opp',Date.today(),supAcc,'Closed Won',prod);

        Id supplierPRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c supPRecod = TestDataGenerator.createParticipationRecord(true,prod,supAcc,supOpp,supCon,supplierPRType);

        Order supConf = TestDataGenerator.createConfirmation(true,supAcc,supCon,'Preliminary',supOpp,supOpp.CloseDate,supPRecod,prod,true);

        //Second Supplier
        Account supAcc2 = TestDataGenerator.createTestAccount(true,'Test Supplier Account2', supplierAccRType);
        Contact supCon2 = TestDataGenerator.createTestContact(true,'Test Supplier2','Last Account','testsupp2@test.com',supAcc2,supplierConRType);

        
        User supUser2 = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Exec','Last Account','603-555-5555',supCon2.Email, supCon2.Id);

        Opportunity supOpp2 = TestDataGenerator.createTestOpp(true,'Supplier2 Test Opp',Date.today(),supAcc2,'Closed Won',prod);

        Event_Account_Details__c supPRecod2 = TestDataGenerator.createParticipationRecord(true,prod,supAcc2,supOpp2,supCon2,supplierPRType);

        Order supConf2 = TestDataGenerator.createConfirmation(true,supAcc2,supCon2,'Preliminary',supOpp2,supOpp2.CloseDate,supPRecod2,prod,true);

        //Create Speaker Sessions
        Speaker_Session__c speakSession1 = TestDataGenerator.createSpeakerSessions(true,String.valueOf( System.Now()), System.Now(),'Speaker Session 1', prod,'Overview Test','Http://www.google.com','Presenter 1','Presentation Title');
        Speaker_Session__c speakSession2 = TestDataGenerator.createSpeakerSessions(true,String.valueOf( System.Now().addDays(-11)), System.Now().addDays(-1),'Speaker Session 2', prod,'Overview Test','Http://www.cpmgInc.com','Presenter 2','Presentation Title For Session 2');


        //Set Up the Page to the Community Home Page
        PageReference pageRef = Page.EventDirectory;

        

        //PERFORM THE TESTING
        Test.startTest();
            System.runAs(supUser2){
                Test.setCurrentPageReference(pageRef);
                EventDirectoryController eDirectory = new EventDirectoryController();

                System.assertEquals(eDirectory.eventId, prod.Id);
                //System.assertEquals(eDirectory.fRole, 'Executive');
            }
        Test.stopTest();
    } 

    static testmethod void eventDirectory_Supplier_Test_No_Eid_NO_ORDER(){
        //Create Test Data
        TestDataGenerator.createInactiveTriggerSettings();
        //Create CPMG_Notification Settings
        TestDataGenerator.createCPMGNotifiedUsers();
        TestDataGenerator.createOrderStatusCustomSetting();
            
        //Set Up Event
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(10);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        update prod;
        //Set Up EventStaff
        Profile standardUser = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);
    

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod,true);
        

        //Set Up Suppliers
        Id supplierAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id supplierConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account supAcc = TestDataGenerator.createTestAccount(true,'Test Sup Account', supplierAccRType);
        Contact supCon = TestDataGenerator.createTestContact(true,'Test Sup','Last Account','testsup123@test.com',supAcc,supplierConRType);

        User supUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Sup','Last Account','603-555-5555',supCon.Email, supCon.Id);

        Opportunity supOpp = TestDataGenerator.createTestOpp(true,'Supp Test Opp',Date.today(),supAcc,'Closed Won',prod);

        Id supplierPRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c supPRecod = TestDataGenerator.createParticipationRecord(true,prod,supAcc,supOpp,supCon,supplierPRType);

        Order supConf = TestDataGenerator.createConfirmation(true,supAcc,supCon,'Preliminary',supOpp,supOpp.CloseDate,supPRecod,prod,true);

        //Second Supplier
        Account supAcc2 = TestDataGenerator.createTestAccount(true,'Test Supplier Account2', supplierAccRType);
        Contact supCon2 = TestDataGenerator.createTestContact(true,'Test Supplier2','Last Account','testsupp2@test.com',supAcc2,supplierConRType);

        
        User supUser2 = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Exec','Last Account','603-555-5555',supCon2.Email, supCon2.Id);

        Opportunity supOpp2 = TestDataGenerator.createTestOpp(true,'Supplier2 Test Opp',Date.today(),supAcc2,'Closed Won',prod);

        //Event_Account_Details__c supPRecod2 = TestDataGenerator.createParticipationRecord(true,prod,supAcc2,supOpp2,supCon2,supplierPRType);

        //Order supConf2 = TestDataGenerator.createConfirmation(true,supAcc2,supCon2,'Preliminary',supOpp2,supOpp2.CloseDate,supPRecod2,prod,true);

        //Set Up the Page to the Community Home Page
        PageReference pageRef = Page.EventDirectory;

        

        //PERFORM THE TESTING
        Test.startTest();
            System.runAs(supUser2){
                Test.setCurrentPageReference(pageRef);
                EventDirectoryController eDirectory = new EventDirectoryController();

                System.assertEquals(eDirectory.eventId, prod.Id);
                //System.assertEquals(eDirectory.fRole, 'Executive');
            }
        Test.stopTest();
    }
    
    static testmethod void eventDirectory_CPMG_USER_Test_No_FILTER(){
        //Create Test Data
        TestDataGenerator.createInactiveTriggerSettings();
        //Create CPMG_Notification Settings
        TestDataGenerator.createCPMGNotifiedUsers();
        TestDataGenerator.createOrderStatusCustomSetting();
            
        //Set Up Event
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(10);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        update prod;
        //Set Up EventStaff
        Profile standardUser = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);   

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod,true);
        

        //Set Up Suppliers
        Id supplierAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id supplierConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account supAcc = TestDataGenerator.createTestAccount(true,'Test Sup Account', supplierAccRType);
        Contact supCon = TestDataGenerator.createTestContact(true,'Test Sup','Last Account','testsup123@test.com',supAcc,supplierConRType);

        User supUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Sup','Last Account','603-555-5555',supCon.Email, supCon.Id);

        Opportunity supOpp = TestDataGenerator.createTestOpp(true,'Supp Test Opp',Date.today(),supAcc,'Closed Won',prod);

        Id supplierPRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c supPRecod = TestDataGenerator.createParticipationRecord(true,prod,supAcc,supOpp,supCon,supplierPRType);

        Order supConf = TestDataGenerator.createConfirmation(true,supAcc,supCon,'Preliminary',supOpp,supOpp.CloseDate,supPRecod,prod,true);

        //Second Supplier
        Account supAcc2 = TestDataGenerator.createTestAccount(true,'Test Supplier Account2', supplierAccRType);
        Contact supCon2 = TestDataGenerator.createTestContact(true,'Test Supplier2','Last Account','testsupp2@test.com',supAcc2,supplierConRType);

        
        User supUser2 = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test Exec','Last Account','603-555-5555',supCon2.Email, supCon2.Id);

        Opportunity supOpp2 = TestDataGenerator.createTestOpp(true,'Supplier2 Test Opp',Date.today(),supAcc2,'Closed Won',prod);

        //Event_Account_Details__c supPRecod2 = TestDataGenerator.createParticipationRecord(true,prod,supAcc2,supOpp2,supCon2,supplierPRType);

        //Order supConf2 = TestDataGenerator.createConfirmation(true,supAcc2,supCon2,'Preliminary',supOpp2,supOpp2.CloseDate,supPRecod2,prod,true);

        //Set Up the Page to the Community Home Page
        PageReference pageRef = Page.EventDirectory;
        pageRef.getParameters().put('eId',prod.Id);

        

        //PERFORM THE TESTING
        Test.startTest();
            System.runAs(execUserCPMG){
                Test.setCurrentPageReference(pageRef);
                EventDirectoryController eDirectory = new EventDirectoryController();

                System.assertEquals(eDirectory.eventId, prod.Id);
                //System.assertEquals(eDirectory.fRole, null);
            }
        Test.stopTest();
    }  
}