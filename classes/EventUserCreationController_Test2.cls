@isTest
private class EventUserCreationController_Test2 {
   static testMethod void supplier_Contact_TestDiv() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id excuAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',excuAccRtype);
        Id excConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,excConRtype);

        Contact attendContact = TestDataGenerator.createTestContact(true,'Test','Attending Con', 'testAttendingCon@account.com',a,excConRtype);
        Contact userCreateContact = TestDataGenerator.createTestContact(true,'Test1','Attending Co1n', 'testAttendingCon1@account.com',a,excConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod,'Returning without Renewal');

        Id execacceventType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execacceventType);
        partRecord.Registration_Part_1_Submitted__c = true;
        update partRecord;

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        Order attendingConf = TestDataGenerator.createConfirmation(true,a,attendContact,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        //User objUser = TestDataGenerator.CreateUser(true,userinfo.getprofileid(),'TestFirstName','TestLastname','7894561235','testusercreate@gmail.com',userCreateContact.id);
        User objUser = new User();
        objUser.id = UserInfo.getUserId();
        Event_Staff__c  objEventStaff = TestDataGenerator.createEventStaff(true,prod,objUser,'Support',false,1);
        
        attendingConf.status = 'Attending Event Contact';
        update attendingConf;
        UserAndEmailCreationProcess  objUserandEmail = new UserAndEmailCreationProcess(new List<Order>{attendingConf});
        objUserandEmail.sendEmail();
        objUserandEmail.executeUserCreationProcess();
        
        ApexPages.currentPage().getParameters().put('id', prod.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(prod);
        
    
         ApexPages.currentPage().getParameters().put('id', attendingConf.Id);
            ApexPages.StandardController sc1 = new ApexPages.StandardController(attendingConf);
            EventUserCreationController eventUserController1 = new EventUserCreationController(sc1);
            eventUserController1.RegenerateLinkMailndUserCreationProcess();
            prod.IsActive = true;
            update prod; 
            eventusercontroller1.RegenerateLinkMailndUserCreationProcess();
            prod.IsActive = false;
            prod.Executive_Users_Created__c = false;
            update prod;
            eventusercontroller1.RegenerateLinkMailndUserCreationProcess();
        
            Test.startTest();
            prod.IsActive = false;
            update prod;
            eventusercontroller1.ResendMailndUserCreationProcess();
            prod.Executive_Users_Created__c = false;
            update prod;
            eventusercontroller1.ResendMailndUserCreationProcess();
            prod.Executive_Users_Created__c = true;
            prod.Allow_Welcome_Emails_for_Executives__c = false;
            update prod;
            eventusercontroller1.ResendMailndUserCreationProcess();
            
          Test.stopTest();
   }
   /* static testMethod void supplier_Contact_Test1() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id excuAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',excuAccRtype);
        Id excConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,excConRtype);

        Contact attendContact = TestDataGenerator.createTestContact(true,'Test','Attending Con', 'testAttendingCon@account.com',a,excConRtype);
        Contact userCreateContact = TestDataGenerator.createTestContact(true,'Test1','Attending Co1n', 'testAttendingCon1@account.com',a,excConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod,'Returning without Renewal');

        Id execacceventType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execacceventType);
        partRecord.Registration_Part_1_Submitted__c = true;
        update partRecord;

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        Order attendingConf = TestDataGenerator.createConfirmation(true,a,attendContact,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        //User objUser = TestDataGenerator.CreateUser(true,userinfo.getprofileid(),'TestFirstName','TestLastname','7894561235','testusercreate@gmail.com',userCreateContact.id);
        User objUser = new User();
        objUser.id = UserInfo.getUserId();
        Event_Staff__c  objEventStaff = TestDataGenerator.createEventStaff(true,prod,objUser,'Support',false,1);
        
        attendingConf.status = 'Attending Event Contact';
        update attendingConf;
        UserAndEmailCreationProcess  objUserandEmail = new UserAndEmailCreationProcess(new List<Order>{attendingConf});
        

        
        ApexPages.currentPage().getParameters().put('id', prod.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(prod);
        
      
            EventUserCreationController eventUserController = new EventUserCreationController(sc);
            eventUserController.executiveUserCreationProcess();
            //eventUserController.supplierUserCreationProcess(); 
            prod.IsActive = false;
            update prod;
            eventUserController.executiveUserCreationProcess();
            eventUserController.supplierUserCreationProcess();
            prod.IsActive = true;
            update prod;
        Test.startTest();   
            eventUserController.executiveUserCreationProcess();
            eventUserController.supplierUserCreationProcess();
            prod.Allow_Welcome_Emails_for_Executives__c = false;
            prod.Allow_Welcome_Emails_for_Suppliers__c = false;
            update prod;
            eventUserController.executiveUserCreationProcess();
            eventUserController.supplierUserCreationProcess();             
            prod.Allow_Welcome_Emails_for_Executives__c = true;
            prod.Allow_Welcome_Emails_for_Suppliers__c = true;
            update prod;
            eventUserController.executiveUserCreationProcess();
            eventUserController.supplierUserCreationProcess();
          Test.stopTest();             
           
    }*/
   /*static testMethod void supplier_Contact_Test2() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id excuAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
        Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);

        Contact attendContact = TestDataGenerator.createTestContact(true,'Test','Attending Con', 'testAttendingCon@account.com',a,suppConRtype);
        Contact userCreateContact = TestDataGenerator.createTestContact(true,'Test1','Attending Co1n', 'testAttendingCon1@account.com',a,suppConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod,'Returning without Renewal');

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        partRecord.Registration_Part_1_Submitted__c = true;
        update partRecord;

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        Order attendingConf = TestDataGenerator.createConfirmation(true,a,attendContact,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        //User objUser = TestDataGenerator.CreateUser(true,userinfo.getprofileid(),'TestFirstName','TestLastname','7894561235','testusercreate@gmail.com',userCreateContact.id);
        User objUser = new User();
        objUser.id = UserInfo.getUserId();
        Event_Staff__c  objEventStaff = TestDataGenerator.createEventStaff(true,prod,objUser,'Support',false,1);        
        attendingConf.status = 'Attending Event Contact';
        update attendingConf;        
        
        
            Test.startTest();
            ApexPages.StandardController sc1 = new ApexPages.StandardController(conf);
            EventUserCreationController eventUserController1 = new EventUserCreationController(sc1);
            ApexPages.currentPage().getParameters().put('id', conf.Id);
            prod.Supplier_Users_Created__c = true;
            prod.Supplier_Users_Created__c = false;           
            update prod;
            eventusercontroller1.ResendMailndUserCreationProcess();            
            eventusercontroller1.RegenerateLinkMailndUserCreationProcess();
             
             
            prod.Allow_Welcome_Emails_for_Suppliers__c = false;           
            update prod;
            eventusercontroller1.ResendMailndUserCreationProcess();
        
            
            prod.Supplier_Users_Created__c = true;           
            update prod;            
           
          Test.stopTest();
    }*/
    
   /* static testMethod void supplier_Contact_Test21() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id excuAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
        Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);

        Contact attendContact = TestDataGenerator.createTestContact(true,'Test','Attending Con', 'testAttendingCon@account.com',a,suppConRtype);
        Contact userCreateContact = TestDataGenerator.createTestContact(true,'Test1','Attending Co1n', 'testAttendingCon1@account.com',a,suppConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod,'Returning without Renewal');

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        partRecord.Registration_Part_1_Submitted__c = true;
        update partRecord;

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        Order attendingConf = TestDataGenerator.createConfirmation(true,a,attendContact,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        //User objUser = TestDataGenerator.CreateUser(true,userinfo.getprofileid(),'TestFirstName','TestLastname','7894561235','testusercreate@gmail.com',userCreateContact.id);
        User objUser = new User();
        objUser.id = UserInfo.getUserId();
        Event_Staff__c  objEventStaff = TestDataGenerator.createEventStaff(true,prod,objUser,'Support',false,1);        
        attendingConf.status = 'Attending Event Contact';
        update attendingConf;        
        
        
            Test.startTest();
            ApexPages.StandardController sc1 = new ApexPages.StandardController(conf);
            EventUserCreationController eventUserController1 = new EventUserCreationController(sc1);
            ApexPages.currentPage().getParameters().put('id', conf.Id);
            prod.Supplier_Users_Created__c = true;
            prod.Supplier_Users_Created__c = false;           
            update prod;
            eventusercontroller1.ResendMailndUserCreationProcess();            
            eventusercontroller1.RegenerateLinkMailndUserCreationProcess();
             
             
            prod.Allow_Welcome_Emails_for_Suppliers__c = false;           
            update prod;
            eventusercontroller1.ResendMailndUserCreationProcess();
        
            
           
            prod.isActive = false; 
            prod.Allow_Welcome_Emails_for_Executives__c = false;           
            update prod;            
            prod.User_Link_Expiration_for_Executives__c = null; 
            update prod;
            
          Test.stopTest();
    }*/
    
    
    static testMethod void supplier_Contact_Test3Div() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id excuAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
        Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);

        Contact attendContact = TestDataGenerator.createTestContact(true,'Test','Attending Con', 'testAttendingCon@account.com',a,suppConRtype);
        Contact userCreateContact = TestDataGenerator.createTestContact(true,'Test1','Attending Co1n', 'testAttendingCon1@account.com',a,suppConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod,'Returning without Renewal');

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        partRecord.Registration_Part_1_Submitted__c = true;
        update partRecord;

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        Order attendingConf = TestDataGenerator.createConfirmation(true,a,attendContact,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        //User objUser = TestDataGenerator.CreateUser(true,userinfo.getprofileid(),'TestFirstName','TestLastname','7894561235','testusercreate@gmail.com',userCreateContact.id);
        User objUser = new User();
        objUser.id = UserInfo.getUserId();
        Event_Staff__c  objEventStaff = TestDataGenerator.createEventStaff(true,prod,objUser,'Support',false,1);        
        attendingConf.status = 'Attending Event Contact';
        update attendingConf;        
        
        PageReference pageRef = Page.SupplierEventCreationPage;
        pageRef.getParameters().put('id', prod.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(prod);
        Test.setCurrentPageReference(pageRef);
        
        Test.startTest();
        ApexPages.StandardController sc1 = new ApexPages.StandardController(conf);
            EventUserCreationController eventUserController1 = new EventUserCreationController(sc1);
            ApexPages.currentPage().getParameters().put('id', conf.Id);
            prod.IsActive = true;
            update prod;
            eventusercontroller1.ResendMailndUserCreationProcess(); 
            eventusercontroller1.RegenerateLinkMailndUserCreationProcess();
            
            Test.stopTest();
     }
       static testMethod void supplier_Contact_Test4Div() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Id excuAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
        Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);

        Contact attendContact = TestDataGenerator.createTestContact(true,'Test','Attending Con', 'testAttendingCon@account.com',a,suppConRtype);
        Contact userCreateContact = TestDataGenerator.createTestContact(true,'Test1','Attending Co1n', 'testAttendingCon1@account.com',a,suppConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod,'Returning without Renewal');

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        partRecord.Registration_Part_1_Submitted__c = true;
        update partRecord;

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        Order attendingConf = TestDataGenerator.createConfirmation(true,a,attendContact,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        //User objUser = TestDataGenerator.CreateUser(true,userinfo.getprofileid(),'TestFirstName','TestLastname','7894561235','testusercreate@gmail.com',userCreateContact.id);
        User objUser = new User();
        objUser.id = UserInfo.getUserId();
        Event_Staff__c  objEventStaff = TestDataGenerator.createEventStaff(true,prod,objUser,'Support',false,1);        
        attendingConf.status = 'Attending Event Contact';
        update attendingConf;        
        
        PageReference pageRef = Page.SupplierEventCreationPage;
        pageRef.getParameters().put('id', prod.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(prod);
        Test.setCurrentPageReference(pageRef);
        
        Test.startTest();
        ApexPages.StandardController sc1 = new ApexPages.StandardController(conf);
            EventUserCreationController eventUserController1 = new EventUserCreationController(sc1);
            ApexPages.currentPage().getParameters().put('id', conf.Id);
            
            prod.IsActive = false;
            prod.Supplier_Users_Created__c = false;
            prod.Allow_Welcome_Emails_for_Suppliers__c = false;
            update prod;
            eventusercontroller1.RegenerateLinkMailndUserCreationProcess();
            eventusercontroller1.ResendMailndUserCreationProcess();
            prod.IsActive = false;
            prod.Supplier_Users_Created__c = true;
            prod.Allow_Welcome_Emails_for_Suppliers__c = false;
            update prod;
            eventusercontroller1.ResendMailndUserCreationProcess();
            Test.stopTest();
     }
}