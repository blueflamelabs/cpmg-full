public without sharing class EventFeedController {
	public Id eventId { get; set; }
	public Boolean isSupplier { get; set; }
   
	public EventFeedController() {
	}

/*	public List<FeedItemDto> getFeedPosts() {
		List<FeedItem> FeedItems = pullPosts(eventId, isSupplier);
		List<FeedItemDto> feedItemDtos = new List<FeedItemDto>();
		Set<Id> contentIds = new Set<Id>();
		for (FeedItem fi : FeedItems) {
			contentIds.add(fi.RelatedRecordId);
		}
		Map<Id, ContentDocument> contentVersions = new Map<Id, ContentDocument>(pullContent(contentIds));
		for (FeedItem fi : FeedItems) {
			feedItemDtos.add(new FeedItemDto(fi, contentVersions.get(fi.RelatedRecordId)));
		}

		return feedItemDtos;
	} */
    //THIS MAY NOT EVEN BE NEEDED
    public Product2 getCurrEvent(){
       Product2 currEvent = EventHelper.getEvent(eventId);
        return currEvent;
    } 
/*	private static List<FeedItem> pullPosts(Id eventId, Boolean isSupplier) {
		Set<Id> groupIds = determineAvailableGroups(isSupplier);
		groupIds.add(eventId);
		List<FeedItem> posts = [
			SELECT Id, ParentId, Type, Revision, LastEditById, LastEditDate, CommentCount, LikeCount, Title, Body, LinkUrl, IsRichText,
				RelatedRecordId, InsertedById, NetworkScope, Visibility, BestCommentId, HasContent, HasLink, Status
			FROM FeedItem
			WHERE ParentId IN :groupIds
			AND Type IN ('ContentPost', 'TextPost')
			AND Visibility = 'AllUsers'
			ORDER BY CreatedDate DESC];

		return posts;
	}
	private static Set<Id> determineAvailableGroups(Boolean isSupplier) {
		Set<String> groupNames = new Set<String> { 'All Connecting Point Marketing' };
		if (isSupplier) {
			groupNames.add('Supplier');
		} else {
			groupNames.add('Executive');
		}
		List<CollaborationGroup> groups = [SELECT Id, Name FROM CollaborationGroup WHERE Name IN :groupNames];
		Set<Id> resultIds = new Set<Id>();

		for (CollaborationGroup cg : groups) {
			resultIds.add(cg.Id);
		}
		return resultIds;
	}
	private static List<ContentDocument> pullContent(Set<Id> versionIds) {
		System.debug(LoggingLevel.ERROR, versionIds);
		List<ContentDocument> posts = [
			SELECT Id, ContentSize, FileType, FileExtension FROM ContentDocument
			WHERE LatestPublishedVersionId IN :versionIds];
		System.debug(LoggingLevel.ERROR, posts);
		return posts;
	}

	public class FeedItemDto {
		public Boolean hasContent { get; set; }
		public String body { get; set; }
		public String title { get; set; }
		public Id relatedRecordId { get; set; }
		public String thumbLink { get; set; }
		public String downloadLink { get; set; }

		public FeedItemDto(FeedItem p_feeditem, ContentDocument p_version) {
			Set<String> previewAvailableFor = new Set<String> {
				'jpg', 'jpeg', 'png', 'gif'
			};
			hasContent  = p_feeditem.HasContent;
			body = p_feeditem.Body;
			relatedRecordId = p_feeditem.RelatedRecordId;
			title =  p_feeditem.Title;
			if (hasContent) {
				thumbLink  = Site.getBaseUrl() + '/sfc/servlet.shepherd/version/renditionDownload?rendition=THUMB720BY480&versionId=' + relatedRecordId;
				downloadLink  = Site.getBaseUrl() + '/sfc/servlet.shepherd/version/download/'+ relatedRecordId +'?asPdf=false&operationContext=CHATTER';
			}
		} */
}