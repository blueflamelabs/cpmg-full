public without sharing class OrderHandler {
    private static Id executiveRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
    private static Id supplierRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
    private Set<Id> productIds {get;set;}
    private Set<Id> contactIDs {get;set;}
    private Map<Id,Contact> contactMap {get;set;}
    private Map<Id,Product2> productMap {get;set;}
    private List<Order> newOrders {get;set;}
    private List<Order> ordersForUserProcess {get;set;}
    //private UserCreationProcess ordUserProcess {get;set;}
    private Map<String,Event_Staff__c> eventStaffMap {get;set;}
    private Community_Settings__c commSettings {get;set;}


    //BEFORE INSERT
    public void executeConfirmationOwnerProcess(List<order> newOrders){
        this.newOrders = newOrders;
        commSettings = Community_Settings__c.getOrgDefaults();
        determineConfirmationOwnerProcessIds();
        getEventStaffMapByRole();
        getContactMap();
        determineConfirmationOwner();
    }

    public void determineConfirmationOwnerProcessIds(){

        productIds = new Set<Id>();
        contactIds = new Set<Id>();
        for(Order ord: newOrders){
            if(ord.Order_Event__c != null && !productIds.contains(ord.Order_Event__c)) productIds.add(ord.Order_Event__c);
            if(!contactIds.contains(ord.BillToContactId)) contactIds.add(ord.BillToContactId);
        }
    }
    private void getEventStaffMapByRole(){
        if(productIds.size() > 0){
            eventStaffMap = EventStaffHelper.getEventStaffMapByRoles(productIds); //THis is keyed off the Role__c+EventId
        }
    }
    private void determineConfirmationOwner(){
         final String execRole = 'Recruiter';
         final String supplierRole = 'Support';
         
         Set<Id> objExeUserId = new Set<Id>();
         Set<Id> objSuppUserId = new Set<Id>();
          for(Order ord: newOrders){
            if(contactMap != null && contactMap.containsKey(ord.BillToContactId)){
                Contact con = contactMap.get(ord.BillToContactId);
                if(con.RecordTypeId == executiveRecordTypeId){
                    if(eventStaffMap != null && eventStaffMap.containsKey(execRole+ord.Order_Event__c)){
                       objExeUserId.add(eventStaffMap.get(execRole+ord.Order_Event__c).User__c);
                    }
                }
                else if(con.RecordTypeId == supplierRecordTypeId){
                    if(eventStaffMap != null && eventStaffMap.containsKey(supplierRole+ord.Order_Event__c)){
                        objSuppUserId.add(eventStaffMap.get(supplierRole+ord.Order_Event__c).User__c);
                    }
                }
            }
         }
         Map<Id,Boolean> objExeUserActive = new Map<Id,Boolean>();
         List<User> objUserEx = [Select Id,Name,isActive from User where Id IN : objExeUserId];
         for(User objU : objUserEx){
             objExeUserActive.put(objU.Id,objU.isActive);
         }
         Map<Id,Boolean> objSuppUserActive = new Map<Id,Boolean>();
         List<User> objUserSupp = [Select Id,Name,isActive from User where Id IN : objSuppUserId];
         for(User objU : objUserSupp){
             objSuppUserActive.put(objU.Id,objU.isActive);
         }
         
         for(Order ord: newOrders){
            if(contactMap != null && contactMap.containsKey(ord.BillToContactId)){
                Contact con = contactMap.get(ord.BillToContactId);
                ord.OwnerID = UserInfo.getUserId();
                if(con.RecordTypeId == executiveRecordTypeId){
                    if(eventStaffMap != null && eventStaffMap.containsKey(execRole+ord.Order_Event__c)){
                        if(UserInfo.getProfileId() != commSettings.Community_User_Profile__c) {
                           if(objExeUserActive.keySet().Contains(eventStaffMap.get(execRole+ord.Order_Event__c).User__c)){
                               Boolean isActiveUser = objExeUserActive.get(eventStaffMap.get(execRole+ord.Order_Event__c).User__c);
                               if(isActiveUser == true){
                                    ord.OwnerId = eventStaffMap.get(execRole+ord.Order_Event__c).User__c;   
                               }else{
                                   ord.addError('The Event Support Staff member for this event is inactive. Please notify your System Administrator to update this and then create your confirmation.');
                               }
                           }
                            
                        }
                        ord.Email_Notification_User__c = eventStaffMap.get(execRole+ord.Order_Event__c).User__c;
                    }
                }
                else if(con.RecordTypeId == supplierRecordTypeId){
                    if(eventStaffMap != null && eventStaffMap.containsKey(supplierRole+ord.Order_Event__c)){
                        if(UserInfo.getProfileId() != commSettings.Community_User_Profile__c){
                            if(objSuppUserActive.keySet().Contains(eventStaffMap.get(supplierRole+ord.Order_Event__c).User__c)){
                                Boolean isActiveUser = objSuppUserActive.get(eventStaffMap.get(supplierRole+ord.Order_Event__c).User__c);
                                if(isActiveUser == true){
                                     ord.OwnerId = eventStaffMap.get(supplierRole+ord.Order_Event__c).User__c;   
                                }else{
                                    ord.addError('The Event Support Staff member for this event is inactive. Please notify your System Administrator to update this and then create your confirmation.');
                                }
                            }
                        }
                        ord.Email_Notification_User__c = eventStaffMap.get(supplierRole+ord.Order_Event__c).User__c;
                    }
                }
            }
         }
    }

    ////BEGIN AFTER INSERT
    // Changed by VSCIDev
  /*  public void executeUserCreationForActiveEvents(List<Order> newOrders){
        this.newOrders = newOrders;
        ordUserProcess = new UserCreationProcess();
        //First determine if the Confirmation (Order) should be used.
        //THe Only Orders that will result in an email, and user (If it doesn't exist being created) are those that
        //1.) Order.Order_Event__c (Product2) isActive
        //2.) Order.BillToContact.Account.Type != Vendor
        getOrderIdReferences();
        getContactMap();
        getProductMap();
        prepareOrderListForUserAndEmailProcess();
        if(ordersForUserProcess.size() > 0) ordUserProcess.executeUserCreationProcess(ordersForUserProcess);
        
    }
    private void getOrderIdReferences(){
        productIds = new Set<Id>();
        contactIds = new Set<Id>();
        for(Order ord : newOrders){
            if(!productIds.contains(ord.Order_Event__c)) productIds.add(ord.Order_Event__c);
            if(!contactIds.contains(ord.BillToContactId)) contactIds.add(ord.BillToContactId);
        }

    }*/
    private void getContactMap(){
        
        if(contactIds.size()>0){
            contactMap = ContactHelper.getContactMap(contactIds);
        }
    }
   /* private void getProductMap(){

        if(productIds.size() >0){
            productMap = EventHelper.getEventMap(productIds);
        }
    }
    private void prepareOrderListForUserAndEmailProcess(){
        ordersForUserProcess = new List<Order>();
        for(Order ord: newOrders){
            if(productMap != null && contactMap!= null&& productMap.containsKey(ord.Order_Event__c) && contactMap.containsKey(ord.BillToContactId)){
                Product2 event = productMap.get(ord.Order_Event__c);
                Contact con = contactMap.get(ord.BillToContactId);
                //Vendor's denote SPEAKERS, they do not get Customer Portal Users. SHould only auto-create when the "Event" (Product2) isActive
                if(con.Account.Type != 'Vendor' && event.isActive && !ord.Send_Email__c){
                    ordersForUserProcess.add(ord);
                }
            }
        }
    }*/

    public void updateTotalRegistrants(List<Order> listOrders){

        //collect all ids
        Set<Id> accountIds =  new Set<Id>();
        Set<Id> productIds =  new Set<Id>();
        for (Order order: listOrders) {
            accountIds.add(order.AccountId);
            productIds.add(order.Order_Event__c);
        }



        //Summary count of Orders where Contact record type on Order
        //Set<String> confirmedOrderStates = new Set<String>{ 'Confirmed', 'Complete' };

            Set<String>  confirmedOrderStates = new Set<String>();
            if(String.isNotBlank(Order_Statuses__c.getOrgDefaults().Event_Contact_picklist_values__c)){
                for(String s: Order_Statuses__c.getOrgDefaults().Event_Contact_picklist_values__c.split('\\|')){
                    if(!confirmedOrderStates.contains(s)) confirmedOrderStates.add(s);
                }
            }


        Map<Id, Integer> executiveRegistrantsByProductIds = new Map<Id, Integer>();
        Map<Id, Integer> supplierRegistrantsByProductIds = new Map<Id, Integer>();
        Map<Id, Map<Id, Integer>> totalRegistrantsByAccountIds = new Map<Id, Map<Id, Integer>>();
        Map<Id, Map<Id, Integer>> totalConfirmedRegistrantsByAccountIds = new Map<Id, Map<Id, Integer>>();
        List<Order> orders = [SELECT BillToContactId, BillToContact.RecordTypeId, Order_Event__c, AccountId, Status
            FROM Order WHERE Order_Event__c IN: productIds AND BillToContactId != NULL AND Order_Event__c != NULL AND AccountId != NULL];
        for (Order order: orders) {
            if (order.BillToContact.RecordTypeId == executiveRecordTypeId) {
                Integer executiveRegistrants = 0;
                 if (executiveRegistrantsByProductIds.containsKey(order.Order_Event__c)) {
                    executiveRegistrants = executiveRegistrantsByProductIds.get(order.Order_Event__c);
                }
                executiveRegistrants++;
                executiveRegistrantsByProductIds.put(order.Order_Event__c, executiveRegistrants);
            } else if (order.BillToContact.RecordTypeId == supplierRecordTypeId) {
                Integer supplierRegistrants = 0;
                if (supplierRegistrantsByProductIds.containsKey(order.Order_Event__c)) {
                    supplierRegistrants = supplierRegistrantsByProductIds.get(order.Order_Event__c);
                }
                supplierRegistrants++;
                supplierRegistrantsByProductIds.put(order.Order_Event__c, supplierRegistrants);
            }
            //count total registrants for account
            Integer totalRegistrantsForCurrentProduct = 0;
            Map<Id, Integer> accountRegistrants = new Map<Id, Integer>();
            if (totalRegistrantsByAccountIds.containsKey(order.Order_Event__c)) {
                accountRegistrants = totalRegistrantsByAccountIds.get(order.Order_Event__c);
                if (accountRegistrants.containsKey(order.AccountId)) {
                    totalRegistrantsForCurrentProduct = accountRegistrants.get(order.AccountId);
                }
            }
            totalRegistrantsForCurrentProduct++;
            accountRegistrants.put(order.AccountId, totalRegistrantsForCurrentProduct);
            totalRegistrantsByAccountIds.put(order.Order_Event__c, accountRegistrants);

            if (confirmedOrderStates.contains(order.Status)) {
                Integer totalConfirmedForCurrentProduct = 0;
                Map<Id, Integer> accountConfirmedRegistrants = new Map<Id, Integer>();
                if (totalConfirmedRegistrantsByAccountIds.containsKey(order.Order_Event__c)) {
                    accountConfirmedRegistrants = totalConfirmedRegistrantsByAccountIds.get(order.Order_Event__c);
                    if (accountConfirmedRegistrants.containsKey(order.AccountId)) {
                        totalConfirmedForCurrentProduct = accountConfirmedRegistrants.get(order.AccountId);
                    }
                }
                totalConfirmedForCurrentProduct++;
                accountConfirmedRegistrants.put(order.AccountId, totalConfirmedForCurrentProduct);
                totalConfirmedRegistrantsByAccountIds.put(order.Order_Event__c, accountConfirmedRegistrants);
            }
        }

        //update exec/supplier registrants number
        List<Product2> products = [SELECT Id, Executive_Registrants__c, Supplier_Registrants__c FROM Product2 WHERE Id IN: productIds];
        for (Product2 product: products) {
            Integer executiveRegistrants = 0;
            Integer supplierRegistrants = 0;

            if (executiveRegistrantsByProductIds.containsKey(product.Id)) {
                executiveRegistrants = executiveRegistrantsByProductIds.get(product.Id);
            }
            if (supplierRegistrantsByProductIds.containsKey(product.Id)) {
                supplierRegistrants = supplierRegistrantsByProductIds.get(product.Id);
            }

            product.Executive_Registrants__c = executiveRegistrants;
            product.Supplier_Registrants__c = supplierRegistrants;
        }
        update products;

        //update number of registrants
        List<Event_Account_Details__c> eventAccDetails = [SELECT Id, Event__c, Account__c FROM Event_Account_Details__c WHERE Account__c IN:accountIds AND Event__c IN:productIds];
        for (Event_Account_Details__c ead : eventAccDetails) {
          /*  ead.Total_number_of_confirmations__c = 0;
            if (totalRegistrantsByAccountIds.containsKey(ead.Event__c)) {
                Map<Id, Integer> accountRegistrants = totalRegistrantsByAccountIds.get(ead.Event__c);
                if (accountRegistrants.containsKey(ead.Account__c)) {
                    ead.Total_number_of_confirmations__c = accountRegistrants.get(ead.Account__c);
                }
            } */
            ead.Total_number_of_confirmations__c = 0;
            if (totalConfirmedRegistrantsByAccountIds.containsKey(ead.Event__c)) {
                Map<Id, Integer> accountRegistrants = totalConfirmedRegistrantsByAccountIds.get(ead.Event__c);
                if (accountRegistrants.containsKey(ead.Account__c)) {
                    ead.Total_number_of_confirmations__c = accountRegistrants.get(ead.Account__c);
                }
            }
        }
        update eventAccDetails;
    }
}