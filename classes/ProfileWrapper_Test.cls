@isTest
private class ProfileWrapper_Test {
    
    @isTest static void profileWrapper_Constructor_Test() {
        // Implement test code
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(40);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        //Wrapper wants a website.
        execAcc.Website = 'https://www.google.com';
        update execAcc;
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod);

        //Create Question
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Category Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c longQuestion = TestDataGenerator.createQuestion(true,'Long Description',2000,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
        Question__c shortQuestion = TestDataGenerator.createQuestion(true,'Short Description',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            
        //Create the 3 Event Questions
        Event_Question__c categoryEQ = TestDataGenerator.createEventQuestion(true,'Category',prod,'Category',picklistQuestion,true,1,null);
        Event_Question__c longEQ = TestDataGenerator.createEventQuestion(true,'Company Long Description',prod,'Company',longQuestion,true,1,'Company Long Description');
        Event_Question__c shortEQ = TestDataGenerator.createEventQuestion(true,'Company Short Description',prod,'Company',shortQuestion,true,1,'Company Short Description');

        //Create Contact Answer Questions
        Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,categoryEQ,execCon,'Select2;Select3',null,null);
        Contact_Event_Answers__c longAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,longEQ,execCon,'Long Answer',null,null);
        Contact_Event_Answers__c shortAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,shortEQ,execCon,'Short Answer',null,null);

        Test.startTest();
            ProfileWrapper profWrapper = new ProfileWrapper( execPRecod, shortAnswer , categoryAnswer,new List<Contact_Event_Answers__c> {longAnswer});
        Test.stopTest();
    }

    @isTest static void profileWrapper_Comparable_Test() {
        // Implement test code
        TestDataGenerator.createInactiveTriggerSettings();

        //Event Setup
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.Start_Date__c = Date.today().addDays(40);
        prod.Family ='BuildPoint';
        prod.Event_Navigation_Color__c = '90135d';
        prod.Event_Tint_Color__c = 'EEEEEE';
        update prod;

        Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
        User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
        User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
        User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


        Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
        Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
        Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

        //Set Up Executive User
        Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
        Account execAcc2 = TestDataGenerator.createTestAccount(true,'Second Exec Account', execAccRType);
        Account execAcc3 = TestDataGenerator.createTestAccount(true,'Zeta Exec Account', execAccRType);
        //Wrapper wants a website.
        execAcc.Website = 'https://www.google.com';
        update execAcc;
        Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);
        Contact execCon2 = TestDataGenerator.createTestContact(true,'Test Exec2','Last Account','testexec2@test.com',execAcc2, execConRType);
        Contact execCon3 = TestDataGenerator.createTestContact(true,'Test Exec3','Last Account','testexec3@test.com',execAcc3, execConRType);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

        Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);
        Opportunity execOpp2 = TestDataGenerator.createTestOpp(true,'Exec Test Opp2',Date.today(),execAcc2,'Closed Won',prod);
        Opportunity execOpp3 = TestDataGenerator.createTestOpp(true,'Exec Test Opp2',Date.today(),execAcc3,'Closed Won',prod);

        Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);
        Event_Account_Details__c execPRecod2 = TestDataGenerator.createParticipationRecord(true,prod,execAcc2,execOpp2,execCon2,execPRRType);
        Event_Account_Details__c execPRecod3 = TestDataGenerator.createParticipationRecord(true,prod,execAcc3,execOpp3,execCon3,execPRRType);

        Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod);
        Order execConf2 = TestDataGenerator.createConfirmation(true,execAcc2,execCon2,'Attending Event Contact',execOpp3,execOpp.CloseDate,execPRecod2,prod);
        Order execConf3 = TestDataGenerator.createConfirmation(true,execAcc3,execCon3,'Attending Event Contact',execOpp3,execOpp.CloseDate,execPRecod2,prod);

        //Create Question
        Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
        Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Category Pickilist',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
            Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
            Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
        Question__c longQuestion = TestDataGenerator.createQuestion(true,'Long Description',2000,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
        Question__c shortQuestion = TestDataGenerator.createQuestion(true,'Short Description',200,true,'Category',questionRTypeMap.get('Question').getRecordTypeId(),true,false);
            
        //Create the 3 Event Questions
        Event_Question__c categoryEQ = TestDataGenerator.createEventQuestion(true,'Category',prod,'Category',picklistQuestion,true,1,null);
        Event_Question__c longEQ = TestDataGenerator.createEventQuestion(true,'Company Long Description',prod,'Company',longQuestion,true,1,'Company Long Description');
        Event_Question__c shortEQ = TestDataGenerator.createEventQuestion(true,'Company Short Description',prod,'Company',shortQuestion,true,1,'Company Short Description');

        //Create Contact Answer Questions
        Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,categoryEQ,execCon,'Select2;Select3',null,null);
        Contact_Event_Answers__c longAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,longEQ,execCon,'Long Answer',null,null);
        Contact_Event_Answers__c shortAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,shortEQ,execCon,'Short Answer',null,null);

        //Create Contact Answer Questions
        Contact_Event_Answers__c categoryAnswer2 = TestDataGenerator.createQuestionAnswers(true,execConf2,execPRecod2,categoryEQ,execCon2,'Select;Select3',null,null);
        Contact_Event_Answers__c longAnswer2 = TestDataGenerator.createQuestionAnswers(true,execConf2,execPRecod2,longEQ,execCon2,'Long Answer',null,null);
        Contact_Event_Answers__c shortAnswer2 = TestDataGenerator.createQuestionAnswers(true,execConf2,execPRecod2,shortEQ,execCon2,'Short Answer',null,null);

        //Create Contact Answer Questions
        Contact_Event_Answers__c categoryAnswer3 = TestDataGenerator.createQuestionAnswers(true,execConf3,execPRecod3,categoryEQ,execCon3,'Select;Select3',null,null);
        Contact_Event_Answers__c longAnswer3 = TestDataGenerator.createQuestionAnswers(true,execConf3,execPRecod3,longEQ,execCon3,'Long Answer',null,null);
        Contact_Event_Answers__c shortAnswer3 = TestDataGenerator.createQuestionAnswers(true,execConf3,execPRecod3,shortEQ,execCon3,'Short Answer',null,null);

        ProfileWrapper profWrapper = new ProfileWrapper( execPRecod, shortAnswer , categoryAnswer,new List<Contact_Event_Answers__c> {longAnswer});
        profWrapper.compName='Test Exec Account';
        ProfileWrapper profWrapperDup = new ProfileWrapper( execPRecod, shortAnswer , categoryAnswer,new List<Contact_Event_Answers__c> {longAnswer});
        profWrapperDup.compName='Test Exec Account';
        ProfileWrapper profWrapper2 = new ProfileWrapper( execPRecod2, shortAnswer2 , categoryAnswer2,new List<Contact_Event_Answers__c> {longAnswer2});
        profWrapper2.compName = 'Second Exec Account';
        ProfileWrapper profWrapper3 = new ProfileWrapper( execPRecod3, shortAnswer3 , categoryAnswer3,new List<Contact_Event_Answers__c> {longAnswer3});
        profWrapper3.compName = 'Zeta Exec Account';

        List<ProfileWrapper> profWrapperList = new List<ProfileWrapper>{profWrapperDup, profWrapper, profWrapper2, profWrapper3};
                

        Test.startTest();
            profWrapperList.sort();
        Test.stopTest();
    }
    

}