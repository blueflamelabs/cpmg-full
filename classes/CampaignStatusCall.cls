public class CampaignStatusCall {

    public CampaignStatusCall() {

    }

   public Id  campaignId{get;set;}
   Public Boolean shouldRedirect{set;get;}
   
   public CampaignStatusCall(ApexPages.StandardController stdController) {
        this.campaignId= (Id)stdController.getId();
        List<Campaign> objCamList = [Select id,isReopen__c from Campaign where Id =:campaignId];
        List<CampaignMemberStatus> objCamMebStatus = [SELECT CampaignId,HasResponded,Label FROM CampaignMemberStatus where HasResponded = true and CampaignId =:campaignId];
        if(objCamMebStatus.size() > 1 && objCamList[0].isReopen__c == false)
            shouldRedirect = true;
    }
    
    public void updateCampaign(){
        String CamId = ApexPages.currentPage().getParameters().get('Id');
        List<Campaign> objCamList = [Select id,isReopen__c from Campaign where Id =:CamId];
        for(Campaign objCam : objCamList){
            objCam.isReopen__c = true;
        }
        if(objCamList.size() > 0){
            update objCamList;
        }
    }
}