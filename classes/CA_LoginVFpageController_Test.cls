@istest
public class CA_LoginVFpageController_Test 
{
    @istest static void WithoutOrderItemsChangedContactRecordTypeIII()
    {
       
        Test.setCurrentPageReference(new PageReference('Page.CA_LoginVFpage')); 
        System.currentPageReference().getParameters().put('Parameter1','SDF' );
        list<LoginParameter__c>  customsettingobjList= new list<LoginParameter__c>();
        
        LoginParameter__c customsettingobj = new LoginParameter__c(Name = 'Custom setting',
                                                                   Executive_Email__c ='Executive@vennsci.com',
                                                                   Supplier_Email__c = 'Supplier@vennsci.com',
                                                                   Parameter__c = 'SDF');
        //insert customsettingobj;
        customsettingobjList.add(customsettingobj);
        insert customsettingobjList;
        
        Account acct = new Account(Name = 'dom');
        insert acct;
        
        String recType = [Select Name From RecordType Where SobjectType = 'Contact' and DeveloperName = 'Executive'].Id;
        //String recType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact cont = new Contact(firstname = 'Test' ,LastName = 'Executive',
                                   AccountId = acct.Id, Email='puser000@amamama.com');
        insert cont;
        if(cont.Id != null)
        {
            Contact contupdate = [SELECT id, RecordTypeId FROM Contact WHERE Id=: cont.Id];
            
            contupdate.RecordTypeId =recType;
            update contupdate;
        }
        
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'CPMG Community Portal User'];
        
        CA_LoginVFpageController obj = new CA_LoginVFpageController();
        obj.email = 'puser000@amamama.com';
        obj.CheckMailId();
        
        User u = new User(
            
            ProfileId = pf.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ContactId = cont.Id
        );
        insert u;
        
        Test.startTest();
            obj = new CA_LoginVFpageController();
            obj.email = 'puser000@amamama.com';
            obj.CheckMailId();
            
            System.ResetPasswordResult resetResult = System.resetPassword( u.Id, false );
            obj.Login_password = resetResult.getPassword();
            obj.LoginContrl();
        Test.stopTest();
    }
    
    @istest static void ReturnURL()
    {
       CA_LoginVFpageController obj = new CA_LoginVFpageController();
        String email1='asd@gmail.com';
        String Login_password1='wer33';
        //CA_LoginVFpageController ControllerClass = new CA_LoginVFpageController();
        obj.email = email1;
        obj.Login_password = Login_password1;
        Apexpages.currentPage().getParameters().put('testloginmethod', 'Yes');
        pageReference pref = obj.LoginContrl();
        
    }
    
        @istest static void ReturnURL_AnotherURL()
    {
       CA_LoginVFpageController obj = new CA_LoginVFpageController();
        String email1='asd@gmail.com';
        String Login_password1='wer33';
        //CA_LoginVFpageController ControllerClass = new CA_LoginVFpageController();
        obj.email = email1;
        obj.Login_password = Login_password1;
        obj.flag = true;
        Apexpages.currentPage().getParameters().put('testloginmethod', 'Yes');
        pageReference pref = obj.LoginContrl();
        
    }
    
    @istest static void ReturnURL_2()
    {
       CA_LoginVFpageController obj = new CA_LoginVFpageController();
        String email1='asd@gmail.com';
        String Login_password1='wer33';
        //CA_LoginVFpageController ControllerClass = new CA_LoginVFpageController();
        obj.email = email1;
        obj.Login_password = Login_password1;
        pageReference pref = obj.LoginContrl();
        
    }
    @istest static void NoEmailString()
    {
       CA_LoginVFpageController obj = new CA_LoginVFpageController();
        obj.email = '';
        obj.CheckMailId();
        
    }
    
    @istest static void ReturnURL2()
    {
       CA_LoginVFpageController obj = new CA_LoginVFpageController();
        pageReference pref = obj.forgotPassword();
        
    }
    
    @istest static void WithOrderItems1_Events()
    {
        Account acct = new Account(Name = 'dom');
        insert acct;
        
        Contact cont = new Contact(firstname = 'Test' ,LastName = 'VennsciTest',
                                   AccountId = acct.Id, Email='puser000@amamama.com');
        insert cont;
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'CPMG Community Portal User'];
       
        
        User u = new User(
            
            ProfileId = pf.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ContactId = cont.Id
        );
        insert u;
        PriceBook2 pb = New PriceBook2(Id = test.getStandardPricebookId(), isActive = true);
        update pb;
        
        Product2 prod = new Product2(Name = 'CGMP', End_Date__c = date.today(),Start_Date__c = date.today(),User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert prod;
        
        Order order1 = new Order(Pricebook2Id = pb.Id,
                                 AccountId =acct.Id,
                                 BillToContactId = cont.Id,
                                 EffectiveDate = date.today(),
                                 status = 'Complete',
                                 Order_Event__c = prod.Id);
        insert order1;
        
        Product2 prod2 = new Product2(Name = 'CGMP2', End_Date__c = (date.today()+2), Start_Date__c = date.today(),User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert prod2;
        system.assert(prod2.Id != null);
        
        Order order2 = new Order(Pricebook2Id = pb.Id,
                                 AccountId =acct.Id,
                                 BillToContactId = cont.Id,
                                 EffectiveDate = date.today(),
                                 status = 'Complete',
                                 Order_Event__c = prod2.Id);
        insert order2;
        
        list<LoginParameter__c>  customsettingobjList= new list<LoginParameter__c>();
        
        LoginParameter__c customsettingobj = new LoginParameter__c(Name = 'Custom setting',
                                                                   Executive_Email__c ='Executive@vennsci.com',
                                                                   Supplier_Email__c = 'Supplier@vennsci.com',
                                                                   Parameter__c = 'SDF');
        //insert customsettingobj;
        customsettingobjList.add(customsettingobj);
        insert customsettingobjList;
        
        
        Test.startTest();
             
            Test.setCurrentPageReference(new PageReference('Page.CA_LoginVFpage')); 
            System.currentPageReference().getParameters().put('Parameter1','SDF' );
            
            CA_LoginVFpageController obj = new CA_LoginVFpageController();
            obj.email = 'r000@amamama.com';
            obj.CheckMailId();
            
            obj.email = 'puser000@amamama.com';
            obj.CheckMailId();
            
            System.ResetPasswordResult resetResult = System.resetPassword( u.Id, false );
            obj.Login_password = resetResult.getPassword();
            obj.LoginContrl();
        Test.StopTest();     
    }
    
    @istest static void WithOrderItems_EventsTwo()
    {
        Account acct = new Account(Name = 'dom');
        insert acct;
        
        Contact cont = new Contact(firstname = 'Test' ,LastName = 'VennsciTest',
                                   AccountId = acct.Id);
        insert cont;
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'CPMG Community Portal User'];
        
        User u = new User(
        
            ProfileId = pf.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ContactId = cont.Id
        );
        insert u;
        PriceBook2 pb = New PriceBook2(Id = test.getStandardPricebookId(), isActive = true);
        update pb;
        
       Product2 prod = new Product2(Name = 'CGMP', End_Date__c = date.today(),Start_Date__c = date.today(),User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert prod;
        
        Order order1 = new Order(Pricebook2Id = pb.Id,
                                 AccountId =acct.Id,
                                 BillToContactId = cont.Id,
                                 EffectiveDate = date.today(),
                                 status = 'Complete',
                                 Order_Event__c = prod.Id);
        insert order1;
        
        Product2 prod2 = new Product2(Name = 'CGMP2', End_Date__c = (date.today()+2), Start_Date__c = date.today(),User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert prod2;
        system.assert(prod2.Id != null);
        
        Order order2 = new Order(Pricebook2Id = pb.Id,
                                 AccountId =acct.Id,
                                 BillToContactId = cont.Id,
                                 EffectiveDate = date.today(),
                                 status = 'Complete',
                                 Order_Event__c = prod2.Id);
        insert order2;
        
        CA_LoginVFpageController obj = new CA_LoginVFpageController();
        obj.email = 'puser000@amamama.com';
        obj.CheckMailId();
    }
    
     @istest static void WithOrderItems_EventsOne()
    {
        Account acct = new Account(Name = 'dom');
        insert acct;
        
        Contact cont = new Contact(firstname = 'Test' ,LastName = 'VennsciTest',
                                   AccountId = acct.Id);
        insert cont;
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'CPMG Community Portal User'];
        
        
        User u = new User(
            
            ProfileId = pf.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ContactId = cont.Id
        );
        insert u;
        PriceBook2 pb = New PriceBook2(Id = test.getStandardPricebookId(), isActive = true);
        update pb;
        
       Product2 prod = new Product2(Name = 'CGMP', End_Date__c = date.today(),Start_Date__c = date.today(),User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert prod;
        
        Order order1 = new Order(Pricebook2Id = pb.Id,
                                 AccountId =acct.Id,
                                 BillToContactId = cont.Id,
                                 EffectiveDate = date.today(),
                                 status = 'Complete',
                                 Order_Event__c = prod.Id);
        insert order1;
        
        Product2 prod2 = new Product2(Name = 'CGMP2', End_Date__c = (date.today()-2), Start_Date__c = date.today(),User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert prod2;
        system.assert(prod2.Id != null);
        
        Order order2 = new Order(Pricebook2Id = pb.Id,
                                 AccountId =acct.Id,
                                 BillToContactId = cont.Id,
                                 EffectiveDate = date.today(),
                                 status = 'Complete',
                                 Order_Event__c = prod2.Id);
        insert order2;
        
        CA_LoginVFpageController obj = new CA_LoginVFpageController();
        obj.email = 'puser000@amamama.com';
        obj.CheckMailId();
    }
    
    @istest static void WithOrderItems_EventsOnlyOne()
    {
        Account acct = new Account(Name = 'dom');
        insert acct;
        
        Contact cont = new Contact(firstname = 'Test' ,LastName = 'VennsciTest',
                                   AccountId = acct.Id);
        insert cont;
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'CPMG Community Portal User'];
        
        
        User u = new User(
            
            ProfileId = pf.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ContactId = cont.Id
        );
        insert u;
        PriceBook2 pb = New PriceBook2(Id = test.getStandardPricebookId(), isActive = true);
        update pb;
        
       Product2 prod = new Product2(Name = 'CGMP', End_Date__c = date.today(),Start_Date__c = date.today(),User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert prod;
        
        Order order1 = new Order(Pricebook2Id = pb.Id,
                                 AccountId =acct.Id,
                                 BillToContactId = cont.Id,
                                 EffectiveDate = date.today(),
                                 status = 'Complete',
                                 Order_Event__c = prod.Id);
        insert order1;
        
        
        
        CA_LoginVFpageController obj = new CA_LoginVFpageController();
        obj.email = 'puser000@amamama.com';
        obj.CheckMailId();
    }
    @istest static void WithoutOrderItems()
    {
        
        Test.setCurrentPageReference(new PageReference('Page.CA_LoginVFpage')); 
        System.currentPageReference().getParameters().put('Parameter1','SDF' );
        list<LoginParameter__c>  customsettingobjList= new list<LoginParameter__c>();
        
        LoginParameter__c customsettingobj = new LoginParameter__c(Name = 'Custom setting',
                                                                   Executive_Email__c ='Executive@vennsci.com',
                                                                   Supplier_Email__c = 'Supplier@vennsci.com',
                                                                   Parameter__c = 'SDF');
        //insert customsettingobj;
        customsettingobjList.add(customsettingobj);
        insert customsettingobjList;
        
        Account acct = new Account(Name = 'dom');
        insert acct;
        
        Contact cont = new Contact(firstname = 'Test' ,LastName = 'VennsciTest',
                                   AccountId = acct.Id);
        insert cont;
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'CPMG Community Portal User'];
        
        
        User u = new User(
            
            ProfileId = pf.Id,
            LastName = 'last',
            FirstName ='First',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ContactId = cont.Id
        );
        insert u;
        
        Test.startTest();
        CA_LoginVFpageController obj = new CA_LoginVFpageController();
        obj.email = 'puser000@amamama.com';
        obj.CheckMailId();
        Test.stopTest();
    }
    @istest static void WithoutOrderItemsChangedContactRecordTypeII()
    {
       
        Test.setCurrentPageReference(new PageReference('Page.CA_LoginVFpage')); 
        System.currentPageReference().getParameters().put('Parameter1','SDF' );
        list<LoginParameter__c>  customsettingobjList= new list<LoginParameter__c>();
        
        LoginParameter__c customsettingobj = new LoginParameter__c(Name = 'Custom setting',
                                                                   Executive_Email__c ='Executive@vennsci.com',
                                                                   Supplier_Email__c = 'Supplier@vennsci.com',
                                                                   Parameter__c = 'SDF');
        //insert customsettingobj;
        customsettingobjList.add(customsettingobj);
        insert customsettingobjList;
        
        Account acct = new Account(Name = 'dom');
        insert acct;
        
        String recType = [Select Name From RecordType Where SobjectType = 'Contact' and DeveloperName = 'Executive'].Id;
        //String recType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact cont = new Contact(firstname = 'Test' ,LastName = 'Executive',
                                   AccountId = acct.Id);
        insert cont;
        if(cont.Id != null)
        {
            Contact contupdate = [SELECT id, RecordTypeId FROM Contact WHERE Id=: cont.Id];
            
            contupdate.RecordTypeId =recType;
            update contupdate;
        }
        
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'CPMG Community Portal User'];
        
        
        User u = new User(
            
            ProfileId = pf.Id,
            LastName = 'last',
            FirstName ='First',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ContactId = cont.Id
        );
        insert u;
        
        Test.startTest();
        CA_LoginVFpageController obj = new CA_LoginVFpageController();
        obj.email = 'puser000@amamama.com';
        obj.CheckMailId();
        Test.stopTest();
    }
    
    @istest static void WithoutOrderItemsChangedContactRecordType()
    {
        
        Test.setCurrentPageReference(new PageReference('Page.CA_LoginVFpage')); 
        System.currentPageReference().getParameters().put('Parameter1','SDF' );
        list<LoginParameter__c>  customsettingobjList= new list<LoginParameter__c>();
        
        LoginParameter__c customsettingobj = new LoginParameter__c(Name = 'Custom setting',
                                                                   Executive_Email__c ='Executive@vennsci.com',
                                                                   Supplier_Email__c = 'Supplier@vennsci.com',
                                                                   Parameter__c = 'SDF');
        //insert customsettingobj;
        customsettingobjList.add(customsettingobj);
        insert customsettingobjList;
        
        Account acct = new Account(Name = 'dom');
        insert acct;
        
        
        
        Contact cont = new Contact(firstname = 'Test' ,LastName = 'VennsciTest',
                                   AccountId = acct.Id);
        insert cont;
        
        Profile pf = [SELECT Id FROM Profile WHERE Name = 'CPMG Community Portal User'];
        
        
        User u = new User(
            
            ProfileId = pf.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com',
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ContactId = cont.Id
        );
        insert u;
        
        Test.startTest();
        CA_LoginVFpageController obj = new CA_LoginVFpageController();
        obj.email = 'puser000@amamama.com';
        obj.CheckMailId();
        Test.stopTest();
     
    }
}