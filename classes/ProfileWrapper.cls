public with sharing class ProfileWrapper implements Comparable {

	public Contact_Event_Answers__c shortDescription {get;set;}
	public Contact_Event_Answers__c selectedCategories {get;set;}
	public Event_Account_Details__c partRecord {get;set;}
	public List<Contact_Event_Answers__c> profileQuestions {get;set;}
	public String compName {get;set;}
	public String compWebsite {get;set;}
	
	
	public ProfileWrapper(Event_Account_Details__c partRecord, Contact_Event_Answers__c shortDescription, Contact_Event_Answers__c selectedCategories, 
						List<Contact_Event_Answers__c> profileQuestions ) {
		this.partRecord = partRecord;
		this.selectedCategories = selectedCategories;
		this.shortDescription = shortDescription;
		this.profileQuestions = profileQuestions;
		this.compName = (String.isNotBlank(partRecord.Company_Display_Name__c))?partRecord.Company_Display_Name__c:partRecord.Account__r.Name;
		this.compWebsite = UtilClass.formatExternalLinkULR(partRecord.Account__r.Website);
	}

	public integer compareTo(Object compareTo){
		ProfileWrapper compareToProfile = (ProfileWrapper)compareTo;
        if (compName.toLowerCase() == compareToProfile.compName.toLowerCase()) {
          return 0;
        } else if (compName.toLowerCase() > compareToProfile.compName.toLowerCase()) {
          return 1;
        } else if(compName.toLowerCase() < compareToProfile.compName.toLowerCase()){
        	return -1;
        } else{
        	return 0;
        }
	}
}