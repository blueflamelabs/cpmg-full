public with sharing class GenerateOpportunitiesController {
   
    public String campaignId{set;get;}
    public String totalPercentage{set;get;}
    String batchId;
    boolean batchProcessed;
    public GenerateOpportunitiesController(ApexPages.StandardController std) {
        campaignId = (Id)std.getId();
    }
    public PageReference StartBactJob() {
        //execute RecordTypeAccessFinder batch
        GenerateOpportunitiesControllerBatch GenerateOpp = new GenerateOpportunitiesControllerBatch(campaignId);
        batchId = database.executeBatch(GenerateOpp,200);
        system.debug('************batchid:'+batchid);
        batchProcessed = true;
        return null;
    }
   
    public GenerateOpportunitiesController(){
        batchProcessed = false;    
    }
    public String getMessage(){
        if(batchProcessed == true){
            Double itemsProcessed;
            Double totalItems ;
            for(AsyncApexJob a : [select TotalJobItems, Status, NumberOfErrors, MethodName, JobType, JobItemsProcessed, Id, 
                                   CreatedDate, CreatedById, CompletedDate, ApexClassId, ApexClass.Name From AsyncApexJob where ApexClass.Name = 'GenerateOpportunitiesControllerBatch' order by CreatedDate desc limit 1]){
                itemsProcessed = a.JobItemsProcessed;
                totalItems = a.TotalJobItems;
            }
            //Determine the percent complete based on the number of batches complete and set message
            if(totalItems == 0){
                //A little check here as we don't want to divide by 0.
                return 'Opportunity generation in progress 0% Complete.';
            }else{
                totalPercentage = String.valueof(((itemsProcessed  / totalItems) * 100.0).intValue());
                return 'Opportunity generation in progress ' +totalPercentage +'% Complete.';
            }

        } else {
            return 'Opportunity generation in progress.';
        }
    }
}