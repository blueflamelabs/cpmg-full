@isTest
private class RegistrationComponentController_Test {
	
	@isTest static void regConstructor_test() {
		// Implement test code
		//Test the constructor
		test.startTest();
			RegistrationComponentController regController = new RegistrationComponentController();
		test.stopTest();
	}
	
	@isTest static void currentEventSet_Test() {
		// Implement test code
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true);
		RegistrationComponentController regController = new RegistrationComponentController();

		test.startTest();
			regController.currentEvent = prod;
		test.StopTest();
	}
	
}