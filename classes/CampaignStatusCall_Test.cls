@isTest
public class CampaignStatusCall_Test{
        public static testMethod void CampaignStatusTest(){
            Id execAccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Account a = TestDataGenerator.createTestAccount(true,'Test Account',execAccountRtype);
        
            Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
        
            Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
            
            Campaign camp = new Campaign();
            camp.Name = 'test';
            camp.isReopen__c = false;
            camp.Event__c = prod.id;
            insert camp;
            
            
            CampaignMemberStatus cms = new CampaignMemberStatus();
            cms.Label = 'test';
            cms.CampaignId = camp.id;
            cms.HasResponded = true;
            insert cms;
            
            CampaignMemberStatus cmst = new CampaignMemberStatus();
            cmst.Label = 'test1';
            cmst.CampaignId = camp.id;
            cmst.HasResponded = true;
            insert cmst;
            
            CampaignStatusCall csc = new CampaignStatusCall();
            
             ApexPages.currentPage().getParameters().put('Id',camp.id);
            ApexPages.StandardController sc = new ApexPages.StandardController(camp);
            CampaignStatusCall testAccPlan = new CampaignStatusCall(sc);
             testAccPlan.updateCampaign();
 }
        
}