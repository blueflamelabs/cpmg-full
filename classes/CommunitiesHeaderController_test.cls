@isTest
private class CommunitiesHeaderController_test {

    @isTest
    private static void test_CommHeader_with_Eid() {
        // Implement test code
        Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

        Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');
        //List<sObject> ls = Test.loadData(StaticResource.sObjectType, 'CPMG_Events_Page_2017');

        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
        //c.Has_Event__c = true;
        //update c;
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
        prod.Venue__c='Test Venue';
       // prod.Start_Date__c = Date.today().addDays(5);
        prod.Venue_and_Location__c ='This is a Spot';
        prod.Location__c ='Test Location';
        prod.LinkedIn__c ='Test LinkedId';
        prod.Twitter__c ='Test Twitter';
        //prod.Year__c =String.valueOf(Date.today().year());
        prod.Header_Date_Range__c = 'January 21 - February 24, 2019';
        prod.EventHeaderLogo__c='BuildPoint';
        prod.Family='BuildPoint';
        //update prod;
        //Start Defining Event "Product" related info.
        Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);
        
        User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);
        
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        //List<Order> ordersJustCreated = new List<Order>{conf};
        PageReference pageRef = Page.CommunitiesHomePage;
        pageRef.getParameters().put('eId',prod.Id);
        Test.setCurrentPageReference(pageRef);

        // create an instance of the controller

    
        Test.startTest();
           System.runAs(commUser){
                CommunitiesHeaderController comHeader = new CommunitiesHeaderController();  
                comHeader.passedEvent = prod;
                comHeader.getHead();
                
               } 
        Test.stopTest();
    }
    @isTest
    private static void test_CommHeader_without_Eid() {
        // Implement test code
        Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

        Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
        c.Has_Event__c = true;
        update c;
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
        prod.Venue__c='Test Venue';
        prod.Venue_and_Location__c ='This is a Spot';
        prod.Location__c ='Test Location';
        prod.LinkedIn__c ='Test LinkedId';
        prod.Twitter__c ='Test Twitter';
        prod.Year__c =String.valueOf(Date.today().year());
        prod.Header_Date_Range__c = 'January 21 - February 24, 2019';
        prod.EventHeaderLogo__c='BuildPoint';
        prod.Family='BuildPoint';
        prod.Start_Date__c = Date.today().addDays(5);
        update prod;
        
        //Start Defining Event "Product" related info.
        Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', null);
        Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord= TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);
        User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);
       Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod,true);
        
        PageReference pageRef = Page.CommunitiesHomePage;
        //pageRef.getParameters().put('eId',prod.Id);
        Test.setCurrentPageReference(pageRef);

        // create an instance of the controller

    
        Test.startTest();
         System.runAs(commUser){
                CommunitiesHeaderController comHeader = new CommunitiesHeaderController();
                comHeader.getHead();
               } 
        Test.stopTest();
        }
  @isTest
    private static void test_CommHeader_from_Order() {
        // Implement test code
        Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

        Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
        c.Has_Event__c = true;
        update c;
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
        prod.Venue__c='Test Venue';
        prod.Venue_and_Location__c ='This is a Spot';
        prod.Location__c ='Test Location';
        prod.LinkedIn__c ='Test LinkedId';
        prod.Twitter__c ='Test Twitter';
        //prod.Year__c =String.valueOf(Date.today().year());
        prod.Header_Date_Range__c = 'January 21 - February 24, 2019';
        prod.EventHeaderLogo__c='BuildPoint';
        prod.Family='BuildPoint';
        //prod.Start_Date__c = Date.today().addDays(5);
        update prod;
        
        //Start Defining Event "Product" related info.
        //Profile communityProfile = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        //User commUser = TestDataGenerator.CreateUser(true,communityProfile.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);

        
        //Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', null);

        //Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        //Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

        //Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,Date.today(),ead);
        


        PageReference pageRef = Page.CommunitiesHomePage;
        Test.setCurrentPageReference(pageRef);

        // create an instance of the controller
        
        Test.startTest();
           
                CommunitiesHeaderController comHeader = new CommunitiesHeaderController();
                comHeader.getHead();
               
                PageReference pageRef1 = Page.CommunitiesHomePage;
                pageRef1.getParameters().put('eId',prod.Id);
                Test.setCurrentPageReference(pageRef1);
                //CommunitiesHeaderController comHeader1 = new CommunitiesHeaderController();
                //comHeader1.getHead();
               
        Test.stopTest();
        }
}