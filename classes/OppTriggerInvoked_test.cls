@isTest
public class OppTriggerInvoked_test{
     

      public static testmethod void testOppTriggerInvoked1(){
         
        Product2 prod = new Product2(Name = 'Test Event', User_Link_Expiration_for_Executives__c = 2, User_Link_Expiration_for_Suppliers__c = 2, Start_Date__c =System.Today(),  ProductCode =' TE 17'); //
        insert prod;

        Account a = new Account(Name = 'Test Account 1', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId());
        insert a;
        Contact c = new Contact(FirstName ='Test', LastName ='Executive', AccountId = a.Id,RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId(), email ='test@test.com');
        insert c; 
        Campaign camp = new Campaign(Name ='Test Event Campaign', StartDate = System.Today(), Event__c = prod.Id );
        insert camp;

        Opportunity opp = new Opportunity(name='Test opp', AccountId = a.Id,Product__c = prod.Id,Primary_Contact__c = c.Id,stageName = 'Closed Won', CloseDate =System.Today(), CampaignId = camp.Id);
        insert opp;
        
        Opportunity opp1 = new Opportunity(Name = 'Test opp',AccountId = a.Id, Product__c = prod.Id, stageName = 'Closed Won', CloseDate =System.Today(), CampaignId = camp.Id);
        insert opp1;
        
        OpportunityContactRole ocr1 = new OpportunityContactRole(OpportunityId = opp1.Id, ContactId =c.Id, Role ='Other', isPrimary =true);
        insert ocr1;
      
        Test.startTest();
            ApexPages.StandardController oppStandardController = new ApexPages.StandardController(opp1);
            OppTriggerInvoked objoti = new OppTriggerInvoked (oppStandardController);
            RecursiveVariable.duplicateVariable = true;
            objoti.updateoppty();
        Test.stopTest();
      }   
       public static testmethod void testOppTriggerInvoked2(){
         
        Product2 prod = new Product2(Name = 'Test Event', User_Link_Expiration_for_Executives__c = 2, User_Link_Expiration_for_Suppliers__c = 2, Start_Date__c =System.Today(),  ProductCode =' TE 17'); //
        insert prod;

        Account a = new Account(Name = 'Test Account 1', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId());
        insert a;
        Contact c = new Contact(FirstName ='Test', LastName ='Executive', AccountId = a.Id,RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId(), email ='test@test.com');
        insert c; 
        Campaign camp = new Campaign(Name ='Test Event Campaign', StartDate = System.Today(), Event__c = prod.Id );
        insert camp;

        Opportunity opp = new Opportunity(name='Test opp', AccountId = a.Id,Product__c = prod.Id,Primary_Contact__c = c.Id,stageName = 'Closed Won', CloseDate =System.Today(), CampaignId = camp.Id);
        insert opp;
        
        Opportunity opp1 = new Opportunity(Name = 'Test opp',AccountId = a.Id, Product__c = prod.Id, stageName = 'Closed Won', CloseDate =System.Today(), CampaignId = camp.Id);
        insert opp1;
        
        OpportunityContactRole ocr1 = new OpportunityContactRole(OpportunityId = opp1.Id, ContactId =c.Id, Role ='Other', isPrimary =true);
        insert ocr1;
        
        Test.startTest();
            ApexPages.StandardController oppStandardController = new ApexPages.StandardController(opp1);
            OppTriggerInvoked objoti = new OppTriggerInvoked (oppStandardController);
            RecursiveVariable.duplicateVariable = true;
            objoti.updateoppty();
        Test.stopTest();
      }   
}