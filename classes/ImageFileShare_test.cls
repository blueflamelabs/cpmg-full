@isTest
public class ImageFileShare_test {
    
    @isTest static void testImageFileShare() {
        
       Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
        
       Id conRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact objCon = TestDataGenerator.createTestContact(true,'Test FName','Last Name','testexec@test.com',a,conRecType);

        Profile profilId = [SELECT Id FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User userCPMG = TestDataGenerator.CreateUser(true,profilId.Id,'Test','Exec','2225252525','test1Exec1234@cpmg.com',objCon.Id);
        
        Product2 objProd = TestDataGenerator.createTestProduct(true,'Test Name',True);
        
        Event_Staff__c objEventStf = TestDataGenerator.createEventStaff(true,objProd,userCPMG,'Developer');
        
        Opportunity objOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),a,'Closed Won',objProd,'Onsite Renewal');
        
       Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,objProd,a,objOpp,objCon, execPartRecordType);

        Image__c myImage = new Image__c(Image_Type__c='Headshot',
                                        File_ID__c = 'abc123',
                                        Participation_Record__c = partRecord.id,
                                        Contact__c = objcon.Id);
        insert myImage;
        myImage.File_ID__c = 'abc1234';
        update myImage;
        ContentVersion contentVersion = new ContentVersion(Title = 'Penguins',
                                                           PathOnClient = 'Penguins.jpg',
                                                           VersionData = Blob.valueOf('Test Content'),
                                                           IsMajorVersion = true);
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = a.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        cdl.Visibility = 'AllUsers';
        update cdl;
        

        test.startTest();
        ImageFileShareCTRL imageShaCTRL = new ImageFileShareCTRL();
        test.stopTest();
    }
 }