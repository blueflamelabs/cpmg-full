public without sharing class EventHelper {

    public static Product2 getEvent(Id eventId) {
        return [SELECT Action_Items_Header__c, Attire_Details__c, Charity_Goal__c, Charity_Logo_URL__c, Charity_Org_URL__c, Charity_Raised__c,
                    
                    End_Date__c,
                    EventHeaderLogo__c, 
                    //Event_Map_URL__c,
                     Event_Navigation_Color__c, 
                    //Event_Owner__c, Event_Owner__r.Contact.Title,
                    //Event_Owner__r.Email, Event_Owner__r.FirstName, Event_Owner__r.LastName, Event_Owner__r.Name, Event_Owner__r.Phone,
                    Executive_Other_Information__c, 
                    Part_1_Instructions_for_Executives__c, 
                    Part_2_Instructions_for_Executives__c,
                    Executive_Post_Event_Survey__c, 
                    Executive_Survey_End_Date__c, 
                    Executive_Survey_Start_Date__c, 
                    Exec_Tool_Box_Content__c,
                    ProductCode,
                    Family, Ground_Transportation_Details__c, Ground_Transportation__c, Id, LinkedIn__c, Location__c, Name, News_Header__c,News__c,
                    //Phase_1_Is_Opened__c, Phase_2_Is_Opened__c,
                    Registration_Part_1_Open_For_Executive__c,
                    Registration_Part_1_Open_For_Supplier__c,
                    Registration_Part_1_Close_For_Executive__c,
                    Registration_Part_1_Close_For_Supplier__c,
                    Registration_Part_2_Open_For_Executive__c,
                    Registration_Part_2_Open_For_Supplier__c,
                    Registration_Part_2_Close_For_Executive__c,
                    Registration_Part_2_Close_For_Supplier__c, 
                    Registration_Part_1_End__c, 
                    Registration_Part_1_Start__c,
                    Registration_Part_2_End__c, 
                    Registration_Part_2_Start__c, 
                    //Sales_Manager__c, Sales_Manager__r.Email, 
                    Start_Date__c,
                    PublicSiteUrl__c,
                    Part_1_Instructions_for_Suppliers__c, 
                    Part_2_Instructions_for_Suppliers__c,
                    Part_1_Instructions_Attending_Only__c, 
                    Supplier_Post_Event_Survey__c,
                    Supplier_Tab_Label__c,
                    Executive_Tab_Label__c,
                    Speakers_Tab_Label__c,
                    CPMG_Staff_Tab_Label__c,
                    Directory_Intro__c,
                    Charity_URL_End_Date__c,
                    Hotel_Reservation_URL__c,
                    Hotel_Reservation_Start_Date__c,
                    Hotel_Reservation_End_Date__c,
                    Activity_Button_Label_1__c,
                    Activity_Button_Label_2__c,
                    Activity_Button_URL_1__c,
                    Activity_Button_URL_2__c,
                    Optional_Activities_Arrival_Date__c,
                    Logo_Section_Instructions__c,
                    Planning_Series__c,
                    //Presentation_Request_Start_Date__c,
                    Supplier_Survey_End_Date__c, 
                    Supplier_Survey_Start_Date__c, 
                    Supplier_Tool_Box_Content__c, Schedule_URL__c, Scheduler_Available_Exec__c,
                    Scheduler_Available_Supplier__c, Tool_Box_Header__c, Display_Charity_Goal__c,isActive,
                    Total_Badges__c, Total_Board_Rooms__c, Total_One_on_Ones__c, Total_Theatres__c, Travel_Details__c, Twitter__c,Event_Directory_PDF_URL__c,
                    Venue_Url__c, Venue__c, Year__c, Executive_Sample_Profile_Url__c, Supplier_Sample_Profile_Url__c, 
                    Hotel_Details_Header__c, Travel_Details_Header__c,Attire_Details_Header__c,Flight_Information_Header__c,Ground_Transportation_Header__c,
                    header_Date_Range__c, Supplier_Renewal_Kit_URL__c, Boardroom_Request_Available_Date__c, Event_Tint_Color__c, Directory_Available__c, Maps_Available__c,
                    Header_Location_Text__c,
                    Header_Venue_Text__c, 
                    Travel_Available__c 
                    //Scheduler_Available_Exec__c, Scheduler_Available_Supplier__c
                FROM Product2
                WHERE Id = :eventId
                ORDER BY Start_Date__c
                LIMIT 1];
    }

    public static Product2 getClosestEventByDate(){
        return [SELECT Id, Name, Twitter__c, Start_Date__c, 
                        End_Date__c,
                         Charity_Logo_URL__c, Charity_Org_URL__c, Charity_Goal__c,
                      // Sales_Manager__c, Sales_Manager__r.Email,
                       Charity_Raised__c, 
                       Registration_Part_1_End__c, 
                       Registration_Part_1_Start__c,
                       Registration_Part_2_End__c, 
                       Registration_Part_2_Start__c,
                       Registration_Part_1_Open_For_Executive__c,
                       Registration_Part_1_Open_For_Supplier__c,
                       Registration_Part_1_Close_For_Executive__c,
                       Registration_Part_1_Close_For_Supplier__c,
                       Registration_Part_2_Open_For_Executive__c,
                       Registration_Part_2_Open_For_Supplier__c,
                       Registration_Part_2_Close_For_Executive__c,
                       Registration_Part_2_Close_For_Supplier__c,  
                       Executive_Post_Event_Survey__c, 
                       Executive_Survey_Start_Date__c,
                       Executive_Survey_End_Date__c, 
                       Supplier_Post_Event_Survey__c, 
                       Supplier_Survey_End_Date__c,
                       Directory_Intro__c, 
                       Supplier_Survey_Start_Date__c,
                       Supplier_Tab_Label__c,
                       Executive_Tab_Label__c,
                       Speakers_Tab_Label__c,
                       CPMG_Staff_Tab_Label__c,
                       Part_2_Instructions_for_Suppliers__c, 
                       News_Header__c,
                       Schedule_URL__c,
                       ProductCode,
                       PublicSiteUrl__c,
                       Event_Directory_PDF_URL__c,
                       Charity_URL_End_Date__c,
                       Optional_Activities_Arrival_Date__c,
                       Logo_Section_Instructions__c,
                       Planning_Series__c,
                       Travel_Available__c,
                      // Presentation_Request_Start_Date__c,
                       Family,Ground_Transportation_Details__c, Event_Navigation_Color__c, Action_Items_Header__c, Tool_Box_Header__c, Exec_Tool_Box_Content__c, Supplier_Tool_Box_Content__c,
                       Venue__c, Venue_Url__c, Hotel_Reservation_URL__c, Location__c, Year__c, LinkedIn__c, Executive_Sample_Profile_Url__c, Supplier_Sample_Profile_Url__c,
                       Hotel_Details_Header__c, Travel_Details_Header__c,Attire_Details_Header__c,Flight_Information_Header__c,Ground_Transportation_Header__c,
                       Display_Charity_Goal__c, isActive, header_Date_Range__c, Supplier_Renewal_Kit_URL__c, Boardroom_Request_Available_Date__c, Event_Tint_Color__c,
                       Scheduler_Available_Exec__c, Scheduler_Available_Supplier__c, Directory_Available__c,Maps_Available__c, Header_Location_Text__c,Header_Venue_Text__c  
                       FROM Product2 WHERE Start_Date__c >= Today ORDER BY Start_Date__c ASC LIMIT 1];

                       
    }

    public static List<Product2> getAllCurrentEventsForCommunity(Set<Id> eventIds){
        Community_Settings__c commSett = Community_Settings__c.getOrgDefaults();

        return [SELECT Id, Name, Start_Date__c, 
                            End_Date__c, 
                            Venue__c, 
                            Registration_Part_1_Start__c,
                            Registration_Part_1_End__c, 
                            Registration_Part_2_Start__c, 
                            Registration_Part_2_End__c,
                            Registration_Part_1_Open_For_Executive__c,
                            Registration_Part_1_Open_For_Supplier__c,
                            Registration_Part_1_Close_For_Executive__c,
                            Registration_Part_1_Close_For_Supplier__c,
                            Registration_Part_2_Open_For_Executive__c,
                            Registration_Part_2_Open_For_Supplier__c,
                            Registration_Part_2_Close_For_Executive__c,
                            Registration_Part_2_Close_For_Supplier__c,
                            Optional_Activities_Arrival_Date__c,
                            Logo_Section_Instructions__c,
                            Planning_Series__c,
                            Travel_Available__c,
                           // Requirements_Due__c,  REMOVED
                            //Registration_Change_End__c,  REMOVED
                            //Event_Portal_URL__c, 
                            Logo__c, //Event_Owner__c, Event_Owner__r.Name, Event_Owner__r.Email, Event_Owner__r.Phone,
                            Family,//Event_Owner__r.Contact.Title, 
                            //Phase_1_Is_Opened__c, Phase_2_Is_Opened__c, 
                            Event_Navigation_Color__c,
                            Twitter__c, LinkedIn__c,
                            PublicSiteUrl__c, EventHeaderLogo__c, Venue_Url__c, Hotel_Reservation_URL__c, Location__c, Year__c, Display_Charity_Goal__c, isActive, header_Date_Range__c, Event_Tint_Color__c,
                            Scheduler_Available_Exec__c, Scheduler_Available_Supplier__c, Maps_Available__c,Header_Location_Text__c,Header_Venue_Text__c 
                        FROM Product2 WHERE Id IN :eventIds and Start_Date__c >= :commSett.First_Event_Date__c AND End_Date__c >= TODAY  ORDER BY Start_Date__c ASC];

                               
    }
    public static List<Product2> getAllPreviousEventsForCommunity(Set<Id> eventIds){
        Community_Settings__c commSett = Community_Settings__c.getOrgDefaults();

        return [SELECT Id, Name, Start_Date__c, 
                            End_Date__c, 
                            Venue__c, 
                            Registration_Part_1_Start__c,
                            Registration_Part_1_End__c, 
                            Registration_Part_2_Start__c, 
                            Registration_Part_2_End__c,
                            Registration_Part_1_Open_For_Executive__c,
                            Registration_Part_1_Open_For_Supplier__c,
                            Registration_Part_1_Close_For_Executive__c,
                            Registration_Part_1_Close_For_Supplier__c,
                            Registration_Part_2_Open_For_Executive__c,
                            Registration_Part_2_Open_For_Supplier__c,
                            Registration_Part_2_Close_For_Executive__c,
                            Registration_Part_2_Close_For_Supplier__c,
                            Optional_Activities_Arrival_Date__c,
                            Logo_Section_Instructions__c,
                            Planning_Series__c,
                            Travel_Available__c,
                           // Requirements_Due__c,  REMOVED
                            //Registration_Change_End__c,  REMOVED
                            //Event_Portal_URL__c, 
                            Logo__c, //Event_Owner__c, Event_Owner__r.Name, Event_Owner__r.Email, Event_Owner__r.Phone,
                            Family,//Event_Owner__r.Contact.Title, 
                            //Phase_1_Is_Opened__c, Phase_2_Is_Opened__c, 
                            Event_Navigation_Color__c, Twitter__c, LinkedIn__c,
                            PublicSiteUrl__c, EventHeaderLogo__c, Venue_Url__c, Hotel_Reservation_URL__c, Location__c, Year__c, Display_Charity_Goal__c, isActive, header_Date_Range__c, Event_Tint_Color__c,
                            Scheduler_Available_Exec__c, Scheduler_Available_Supplier__c, Maps_Available__c,Header_Location_Text__c,Header_Venue_Text__c 
                        FROM Product2 WHERE Id IN :eventIds and Start_Date__c >= :commSett.First_Event_Date__c AND End_Date__c <= TODAY  ORDER BY Start_Date__c DESC];

                               
    }

    public static Map<Id,Product2> getEventMap(Set<Id> eventIds){
        Community_Settings__c commSett = Community_Settings__c.getOrgDefaults();
        return new Map<Id,Product2>([SELECT Id, isActive FROM Product2 WHERE ID in :eventIds and Start_Date__c >= :commSett.First_Event_Date__c]);
    }
}