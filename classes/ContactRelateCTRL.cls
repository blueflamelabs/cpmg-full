public class ContactRelateCTRL{

    Public String AcctId{set;get;}
    Public String ConRecId{set;get;}
    public ContactRelateCTRL(){
        AcctId = ApexPages.currentPage().getParameters().get('Id');
        ConRecId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(ApexPages.currentPage().getParameters().get('ReTypeId')).getRecordTypeId();
    }
}