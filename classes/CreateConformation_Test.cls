@isTest
public class CreateConformation_Test{
@isTest
    static void CreateConformationTest(){
       Id execAccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
       Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true,'Test Account',execAccountRtype);
        
        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
        
        
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(-1), Date.today().addMonths(-3), Date.today().addMonths(-2));
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod ,'Renewal');
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        
        OpportunityContactRole oppconrole = new OpportunityContactRole();
        oppconrole.ContactId=c.id;
        oppconrole.OpportunityId=opp.id;
        oppconrole.IsPrimary=true;
        insert oppconrole;
        
      CreateConformation.showConformationRecord(opp.id);
    }
    @isTest
    static void CreateConformationTest1(){
       Id execAccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
       Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true,'Test Account',execAccountRtype);
        
        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(-1), Date.today().addMonths(-3), Date.today().addMonths(-2));
        prod.Supplier_Renewal_Badge_Add_On_Rule__c ='Always Receive Renewal Badge Add On';
        update prod;
        prod.Supplier_Renewal_Badge_Add_On_Rule__c ='Must Attend Another Event';
        update prod;
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod ,'Renewal');
       
        
        OpportunityContactRole oppconrole = new OpportunityContactRole();
        oppconrole.ContactId=c.id;
        oppconrole.OpportunityId=opp.id;
        oppconrole.IsPrimary=true;
        insert oppconrole;
        
      CreateConformation.showConformationRecord(opp.id);
      
    }
    @isTest
    static void CreateConformationTest2(){
       Id execAccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
       Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true,'Test Account',execAccountRtype);
        
        Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
       
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(-1), Date.today().addMonths(-3), Date.today().addMonths(-2));
        
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod ,'Renewal');
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        //Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        
        OpportunityContactRole oppconrole = new OpportunityContactRole();
        oppconrole.ContactId=c.id;
        oppconrole.OpportunityId=opp.id;
        oppconrole.IsPrimary=true;
        insert oppconrole;
        
      CreateConformation.showConformationRecord(opp.id);
    }
 }