/*
*Created By     : 
*Created Date   : 22/07/2019 
*Description    : 
*Test Class Name: CancellationsOrderCTRL
Modification History :
============================================================================================
V.No    Date            By                  Description
============================================================================================
*/
@IsTest
public class CancellationsOrderCTRL_Test{
    static testMethod void CancellationsOrderCTRL_testMethod(){
     
         Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
        
        Id conRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact objCon = TestDataGenerator.createTestContact(true,'Test FName','Last Name','testexec@test.com',a,conRecType);

        Profile profilId = [SELECT Id FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User userCPMG = TestDataGenerator.CreateUser(true,profilId.Id,'Test','Exec','2225252525','test1Exec1234@cpmg.com',objCon.Id);
        
        Product2 objProd = TestDataGenerator.createTestProduct(true,'Test Name',True);
        
        Event_Staff__c objEventStf = TestDataGenerator.createEventStaff(true,objProd,userCPMG,'Developer');
        
        Opportunity objOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),a,'Closed Won',objProd,'Onsite Renewal');
        
       Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,objProd,a,objOpp,objCon, execPartRecordType);
        
        partRecord.Status__c = 'Cancelled';
        update partRecord;
       
        User userRecord = new User(Alias = 'new123',  
                                              EmailEncodingKey='UTF-8',
                                              LastName='Testing',
                                              LanguageLocaleKey='en_US', 
                                              LocaleSidKey='en_US', 
                                              ProfileId = UserInfo.getProfileId(),
                                              Email = 'test1.liveston@asdf.com',
                                              Username = 'test.liveston@asdf.com', 
                                              TimeZoneSidKey='Asia/Dubai'
                                              );
            insert userRecord ;
            
          Id CampaignConRtype = Schema.SObjectType.Campaign.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Campaign camp = new Campaign();
            camp.Name = 'test';
            camp.isReopen__c = false;
            camp.IsActive = true;
            camp.Event__c = objProd.id;
            camp.OwnerId = userRecord.id;
            camp.Status = 'Planned';
            camp.RecordTypeId = CampaignConRtype;
            insert camp;
            
            Lead lead = new Lead();
            lead.FirstName ='test';
            lead.LastName = 'test';
            lead.Company ='syn';
            insert lead;
            
            
            CampaignMember cm = new CampaignMember();
            //cm.AccountId = a.id;
            cm.CampaignId = camp.id;
            cm.ContactId = objCon.id;
            cm.LeadId = lead.id;
            insert cm;
            
            CampaignMemberStatus cms = new CampaignMemberStatus();
            cms.CampaignId = camp.id;
            cms.HasResponded = true;
            cms.Label = 'Test';
            insert cms;
            
         Test.startTest();
            Set<Id> oppId = new Set<id>();
            oppId.add(objOpp.id);
            //CancellationsOrderCTRL cancelOrd = new CancellationsOrderCTRL();
            CancellationsOrderCTRL.canOrder(oppId);
            CancellationsOrderCTRL.RespondedCamMemberStatus(oppId);
         Test.stopTest();  
    }
}