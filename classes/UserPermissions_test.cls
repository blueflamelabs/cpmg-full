@isTest
private class UserPermissions_test {
    
    @isTest static void testEmptyConstructor() {
        UserPermissions up = new UserPermissions();

        up.part1IsOpen = true;
        up.part1IsReadOnly = true;
        up.part2IsOpen = true;
        up.part2IsReadOnly = true;

        test.startTest();
        System.assertEquals(true, up.part1IsOpen);
        System.assertEquals(true, up.part1IsReadOnly);
        System.assertEquals(true, up.part2IsOpen);
        System.assertEquals(true, up.part2IsReadOnly);
        System.assertEquals(false, up.isReadOnly);
        System.assertEquals(false, up.isSupplier);
        System.assertEquals(false, up.isExecutive);
        System.assertEquals(false, up.isAccountOwner);
        System.assertEquals(false, up.canEditAccountFields);
        System.assertEquals(false, up.canEditContactFields);
        System.assertEquals(false, up.canManageContacts);
        test.stopTest();
    }
    
    @isTest static void testNonEmptyConstructor() {

        Boolean isAccountOwner = true;
        String p_recordType = 'Supplier';
        UserPermissions up = new UserPermissions(p_recordType, isAccountOwner);

        test.startTest();
        System.assertEquals(false, up.isReadOnly);
        System.assertEquals(true, up.isSupplier);
        System.assertEquals(false, up.isExecutive);
        System.assertEquals(isAccountOwner, up.isAccountOwner);
        System.assertEquals(true, up.canEditAccountFields);
        System.assertEquals(true, up.canEditContactFields);
        System.assertEquals(true, up.canManageContacts);
        test.stopTest();

    }

    @isTest(SeeAllData=true)
    static void testGetPublicPhotoFolder(){

        Folder myFolder = [SELECT Id, Name FROM Folder Limit 1];
        UserPermissions up = new UserPermissions();
        string p_publicFolderName = myFolder.Name;
        Folder result = up.getPublicPhotoFolder(p_publicFolderName);

        test.startTest();
        System.assertEquals(myFolder.Id, result.Id);
        test.stopTest();
    }

    @isTest static void testGetEventStaff(){
        test.startTest();
        getEventStafffTester('Executive');
        getEventStafffTester('Supplier');
        test.stopTest();
    }


    private static void getEventStafffTester(String p_recordType){
        Boolean isAccountOwner = true;
        UserPermissions up = new UserPermissions(p_recordType, isAccountOwner);

        Product2 testProduct = new Product2(Name='testProduct', Start_Date__c = Datetime.now().addDays(2).date(),User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert testProduct;

        Event_Staff__c eventStaff = new Event_Staff__c(CPMG_Event__c = testProduct.Id, User__c = UserInfo.getUserId(), 
            Role__c = 'Recruiter');
        insert eventStaff;
        
        up.getEventStaff(testProduct.Id);
        
    }

}