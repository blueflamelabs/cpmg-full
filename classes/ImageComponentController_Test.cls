@isTest
private class ImageComponentController_Test {

    @isTest
    private static void imageComponent_Initialize_Test() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));

        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        List<Order> ordersJustCreated = new List<Order>{conf};

        Document d1 = TestDataGenerator.createDocument('test1','text/plain','test1');
        Document d2 = TestDataGenerator.createDocument('test2','text/plain','test2');
        Document d3 = TestDataGenerator.createDocument('test3','text/plain','test3');
        Document d4 = TestDataGenerator.createDocument('test4','text/plain','test4');

        Image__c img1 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d1.Id,partRecord,true,false,true);
        Image__c img2 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d2.Id,partRecord,true,false,true);
        Image__c img3 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d3.Id,partRecord,true,false,true);
        Image__c img4= TestDataGenerator.createImage(true,a,c,conf,prod,(String) d4.Id,partRecord,true,false,true);

        Test.startTest();
            System.runAs(execUser){
                ImageComponentController imgController = new ImageComponentController();
                imgController.imageSection = 'Logo';
                imgController.currentContact = c;
                imgController.currentEvent = prod;
                imgController.currentConfirmation = conf;
                imgController.userID = execUser.Id;
            //imgController.currentContact = c.Id;
        
                imgController.getCurrentUploadPhotos();
            }
        Test.stopTest();
    }

    @isTest
    private static void imageComponent_Initialize_Supplier_Test() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));

        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        List<Order> ordersJustCreated = new List<Order>{conf};

        Document d1 = TestDataGenerator.createDocument('test1','text/plain','test1');
        Document d2 = TestDataGenerator.createDocument('test2','text/plain','test2');
        Document d3 = TestDataGenerator.createDocument('test3','text/plain','test3');
        Document d4 = TestDataGenerator.createDocument('test4','text/plain','test4');

        Image__c img1 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d1.Id,partRecord,false,false,true);
        Image__c img2 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d2.Id,partRecord,false,false,false);
        Image__c img3 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d3.Id,partRecord,false,false,false);
        Image__c img4= TestDataGenerator.createImage(true,a,c,conf,prod,(String) d4.Id,partRecord,false,false,false);

        Test.startTest();
            System.runAs(execUser){
                ImageComponentController imgController = new ImageComponentController();
                imgController.imageSection = 'Contact';
                imgController.currentContact = c;
                imgController.currentEvent = prod;
                imgController.currentConfirmation = conf;
                imgController.userID = execUser.Id;
                //imgController.publ
            //imgController.currentContact = c.Id;
        
                imgController.getCurrentUploadPhotos();
            }
        Test.stopTest();
    }

    @isTest
    private static void imageComponent_upsertImage_Supplier_Test() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));

        Product2 prodPrev = TestDataGenerator.createTestProduct(true,'Test Event Active2',true,Date.today().addDays(-20),Date.today().addDays(-25),Date.today().addMonths(-3), Date.today().addMonths(-2));


        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        List<Order> ordersJustCreated = new List<Order>{conf};

        Document d1 = TestDataGenerator.createDocument('test1','text/plain','test1');
        Document d2 = TestDataGenerator.createDocument('test2','text/plain','test2');
        Document d3 = TestDataGenerator.createDocument('test3','text/plain','test3');
        Document d4 = TestDataGenerator.createDocument('test4','text/plain','test4');

        Image__c img1 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d1.Id,partRecord,false,false,true);
        Image__c img2 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d2.Id,partRecord,false,false,false);
        Image__c img3 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d3.Id,partRecord,false,false,false);
        Image__c img4= TestDataGenerator.createImage(true,a,c,conf,prodPrev,(String) d4.Id,partRecord,false,false,false);

        PageReference pageRef = Page.EventRegistration2;
        pageRef.getParameters().put('eId', prod.Id);
        //ApexPages.StandardController sc = new ApexPages.StandardController(prod);
        

        ImageComponentController imgController = new ImageComponentController();
        imgController.imageSection = 'Contact';
                imgController.currentContact = c;
                imgController.currentEvent = prod;
                imgController.currentConfirmation = conf;
                imgController.userID = execUser.Id;
                imgController.getCurrentUploadPhotos();

        Test.startTest();
            System.runAs(execUser){
            Test.setCurrentPageReference(pageRef);
                Blob userBlob = Blob.valueOf('THIS IS SOMETHING');
                imgController.contactPhotoFile =userBlob;
                imgController.contactPhotoFileName ='Uploaded File';
                imgController.contactPhotoFileContentType = 'application/txt';
                imgController.selectImage();
                //imgController.publ
            //imgController.currentContact = c.Id;
        
                //imgController.getCurrentUploadPhotos();
            }
        Test.stopTest();
    }

    @isTest
    private static void imageComponent_upsertImage_DiffEvent_Supplier_Test() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));

        Product2 prodPrev = TestDataGenerator.createTestProduct(true,'Test Event Active2',true,Date.today().addDays(-20),Date.today().addDays(-25),Date.today().addMonths(-3), Date.today().addMonths(-2));


        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        List<Order> ordersJustCreated = new List<Order>{conf};

        Document d1 = TestDataGenerator.createDocument('test1','text/plain','test1');
        Document d2 = TestDataGenerator.createDocument('test2','text/plain','test2');
        Document d3 = TestDataGenerator.createDocument('test3','text/plain','test3');
        Document d4 = TestDataGenerator.createDocument('test4','text/plain','test4');

        Image__c img1 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d1.Id,partRecord,false,false,false);
        Image__c img2 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d2.Id,partRecord,false,false,false);
        Image__c img3 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d3.Id,partRecord,false,false,false);
        Image__c img4= TestDataGenerator.createImage(true,a,c,conf,prodPrev,(String) d4.Id,partRecord,false,false,true);

        PageReference pageRef = Page.EventRegistration2;
        pageRef.getParameters().put('eId', prod.Id);
        //ApexPages.StandardController sc = new ApexPages.StandardController(prod);
        

        ImageComponentController imgController = new ImageComponentController();
        imgController.imageSection = 'Contact';
                imgController.currentContact = c;
                imgController.currentEvent = prod;
                imgController.currentConfirmation = conf;
                imgController.userID = execUser.Id;
                imgController.getCurrentUploadPhotos();

        Test.startTest();
            System.runAs(execUser){
            Test.setCurrentPageReference(pageRef);
                //Blob userBlob = Blob.valueOf('THIS IS SOMETHING');
                //imgController.contactPhotoFile =userBlob;
                //imgController.contactPhotoFileName ='Uploaded File';
                //imgController.contactPhotoFileContentType = 'application/txt';
                imgController.selectImage();
                //imgController.publ
            //imgController.currentContact = c.Id;
        
                //imgController.getCurrentUploadPhotos();
            }
        Test.stopTest();
    }

    static testMethod void testMultiComponent(){
        testDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();

        //AccountFormSettings
        TestDataGenerator.createAccountFormSettings();

        //Contact Form Settings:
        TestDataGenerator.createContactFormSettings();

        //Order Form Display settings
        TestDataGenerator.createOrderFormSettings();

        //Registration Settings
        TestDataGenerator.createRegistrationSettings();

        //Need the Order Status Custom Setting
        TestDataGenerator.createOrderStatusCustomSetting();

        TestDataGenerator.createOtheContactFormSettings();
        
        Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
        Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);

        Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
        
        User execUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExec@cpmg.com',c.Id);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));

        Product2 prodPrev = TestDataGenerator.createTestProduct(true,'Test Event Active2',true,Date.today().addDays(-20),Date.today().addDays(-25),Date.today().addMonths(-3), Date.today().addMonths(-2));


        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);

        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        List<Order> ordersJustCreated = new List<Order>{conf};

        Document d1 = TestDataGenerator.createDocument('test1','text/plain','test1');
        Document d2 = TestDataGenerator.createDocument('test2','text/plain','test2');
        Document d3 = TestDataGenerator.createDocument('test3','text/plain','test3');
        Document d4 = TestDataGenerator.createDocument('test4','text/plain','test4');

        Image__c img1 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d1.Id,partRecord,false,false,false);
        Image__c img2 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d2.Id,partRecord,false,false,false);
        Image__c img3 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d3.Id,partRecord,false,false,false);
        Image__c img4= TestDataGenerator.createImage(true,a,c,conf,prodPrev,(String) d4.Id,partRecord,false,false,true);

        PageReference pageRef = Page.EventRegistration2;
        pageRef.getParameters().put('eId', prod.Id);
        //ApexPages.StandardController sc = new ApexPages.StandardController(prod);
        





        Test.startTest();
            System.runAs(execUser){
                Test.setCurrentPageReference(pageRef);
                EventRegistration2Controller eRegController = new EventRegistration2Controller();
                String anchorPart = eRegController.anchorPart;
                String subAnchorPart = eRegController.subAnchorPart;
                String siteURL = eRegController.getSiteURL();
                UserPermissions uPermissions = eRegController.permissions;
                Boolean displayContact = eRegController.displayEditContactModal;
                Boolean displayAccount = eRegController.displayProfileAnswersModal;

                ImageComponentController imgController = new ImageComponentController();
                imgController.imageSection = 'Contact';
                imgController.currentContact = c;
                imgController.currentEvent = prod;
                imgController.currentConfirmation = conf;
                imgController.userID = execUser.Id;
                imgController.getCurrentUploadPhotos();
                imgController.parentForm = eRegController.getThis();

            
                imgController.parentForm.registerImageCompController(imgController);

                Integer numController =imgController.parentForm.getImageControllersSize();
                Set<ImageComponentController> imgControllerSet = MultiComponentForm.getImageControllers();

                imgController.parentForm.saveAllImages();

            }
        Test.stopTest();
    }
}