@isTest
private class EventAccountDetailsHandler_test {
    
    @isTest static void eventAccountDetails_insert_test() {
        // Implement test code
        Trigger_Settings__c tSettings = new Trigger_Settings__c(Event_Account_Detail_Trigger__c = false);
        insert tSettings;
        Account a = new Account(Name = 'Test Account 1', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId());
        insert a;

        Product2 prod = new Product2(Name = 'Test Event', Start_Date__c =System.Today(), ProductCode =' TE 17',User_Link_Expiration_for_Executives__c=10,User_Link_Expiration_for_Suppliers__c=10); 
        insert prod;
        //Create Event Sponsorship Records
        Event_Sponsorships__c eSponsor = new Event_Sponsorships__c(Name='TE Sponsor 1', Event__c = prod.Id, Price__c =1500, Purchased__c =false);
        Event_Sponsorships__c eSponsor2 = new Event_Sponsorships__c(Name='TE Sponsor 2', Event__c = prod.Id, Price__c =3000, Purchased__c =false);
        insert eSponsor;
        insert eSponsor2;


        Event_Package_Inventory__c package1 = new Event_Package_Inventory__c(Event__c = prod.Id, Name ='Package TE A', Price__c= 1000.00, Number_of_Badges__c =1,Number_of_Board_Rooms__c = 1, Number_of_Theatres__c=1 );
        insert package1;

        Event_Account_Details__c ead = new Event_Account_Details__c(Account__c = a.Id, Event__c = prod.Id, Package_Selection__c = package1.Id );
        Event_Account_Details__c ead2 = new Event_Account_Details__c(Account__c = a.Id, Event__c = prod.Id, Package_Selection__c = package1.Id, Badge_Add_Ons__c = 1 );
        List<Event_Account_Details__c> eadList = new List<Event_Account_Details__c>();
        eadList.add(ead);
        eadList.add(ead2);
        test.startTest();
            insert eadList;
        test.stopTest();
    }
    
    @isTest static void eventAccountDetails_update_test() {
        // Implement test code
        Trigger_Settings__c tSettings = new Trigger_Settings__c(Event_Account_Detail_Trigger__c = false);
        insert tSettings;
        Account a = new Account(Name = 'Test Account 1', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId());
        insert a;

        Product2 prod = new Product2(Name = 'Test Event', Start_Date__c =System.Today(), ProductCode =' TE 17',User_Link_Expiration_for_Executives__c=10,User_Link_Expiration_for_Suppliers__c=10); 
        insert prod;
        //Create Event Sponsorship Records
        Event_Sponsorships__c eSponsor = new Event_Sponsorships__c(Name='TE Sponsor 1', Event__c = prod.Id, Price__c =1500, Purchased__c =false);
        Event_Sponsorships__c eSponsor2 = new Event_Sponsorships__c(Name='TE Sponsor 2', Event__c = prod.Id, Price__c =3000, Purchased__c =false);
        insert eSponsor;
        insert eSponsor2;

        Event_Package_Inventory__c package1 = new Event_Package_Inventory__c(Event__c = prod.Id, Name ='Package TE A', Price__c= 1000.00, Number_of_Badges__c =1,Number_of_Board_Rooms__c = 1, Number_of_Theatres__c=1 );
        insert package1;

        Event_Account_Details__c ead = new Event_Account_Details__c(Account__c = a.Id, Event__c = prod.Id, Package_Selection__c = package1.Id );
        insert ead;

        Event_Account_Details__c upEAD = new Event_Account_Details__c(Id = ead.Id, Badge_Add_Ons__c = 1);
        test.startTest();
            update upEAD;
        test.stopTest();
    }
    
    @isTest static void eventAccountDetails_delete_test() {
        // Implement test code
        Trigger_Settings__c tSettings = new Trigger_Settings__c(Event_Account_Detail_Trigger__c = false);
        insert tSettings;
        Account a = new Account(Name = 'Test Account 1', RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId());
        insert a;

        Product2 prod = new Product2(Name = 'Test Event', Start_Date__c =System.Today(), ProductCode =' TE 17',User_Link_Expiration_for_Executives__c=10,User_Link_Expiration_for_Suppliers__c=10); 
        insert prod;
        //Create Event Sponsorship Records
        Event_Sponsorships__c eSponsor = new Event_Sponsorships__c(Name='TE Sponsor 1', Event__c = prod.Id, Price__c =1500, Purchased__c =false);
        Event_Sponsorships__c eSponsor2 = new Event_Sponsorships__c(Name='TE Sponsor 2', Event__c = prod.Id, Price__c =3000, Purchased__c =false);
        insert eSponsor;
        insert eSponsor2;

        Event_Package_Inventory__c package1 = new Event_Package_Inventory__c(Event__c = prod.Id, Name ='Package TE A', Price__c= 1000.00, Number_of_Badges__c =1,Number_of_Board_Rooms__c = 1, Number_of_Theatres__c=1 );
        insert package1;

        Event_Account_Details__c ead = new Event_Account_Details__c(Account__c = a.Id, Event__c = prod.Id, Package_Selection__c = package1.Id );
        insert ead;

        Event_Account_Details__c delEAD = new Event_Account_Details__c(id = ead.Id);

        test.startTest();
            delete delEAD;
        test.stopTest();
    }
}