public with sharing class DocumentHelper {
	/*public DocumentHelper() {
		
	}*/

	public static Map<Id,Document> getDocumentMap(Set<Id> docIds){
		return new Map<Id,Document>([SELECT BodyLength, ContentType, Id, Name, Type FROM Document where Id In :docIds ]);
	}

	public static Document createDoc(Blob body, String contType, String folderID, String name){
		return new Document(
			Body= body,
			ContentType = contType,
			FolderId = folderID,
			isInternalUseOnly = false,
			isPublic = true,
			Name = name
			);
	}
}