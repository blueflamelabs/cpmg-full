/*****************************************/
/**
*  @Who     Arun Kumar
*  @When    24/07/2018
*  @What    Test Class for cover EventUserCreationController,ChangePasswordController1,ChangePasswordCTRL,ChangePasswordController,
*           
*/
/****************************************/
@IsTest
public class OpportunitiesCloneController_Test{
    @isTest
    private static void OpportunitiesCloneController_Method1() {
        TestDataGenerator.createInactiveTriggerSettings();
        Community_Settings__c commSett = TestDataGenerator.createCommSettings();
        
        Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
        Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
        Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);

        Contact attendContact = TestDataGenerator.createTestContact(true,'Test','Attending Con', 'testAttendingCon@account.com',a,suppConRtype);
        
        Product2 prod = TestDataGenerator.createTestProduct(true,'CreateEventName',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
        prod.year__c='2018';
        prod.Family='BuildPoint';
        update prod ;
       
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);
       
        OpportunityContactRole ocr1 = new OpportunityContactRole(OpportunityId = opp.Id, ContactId =c.Id, Role ='Other', isPrimary =true);
        insert ocr1;
        
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);
        partRecord.Registration_Part_1_Submitted__c = true;
        update partRecord;

        Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
        Order attendingConf = TestDataGenerator.createConfirmation(true,a,attendContact,'Complete',opp,opp.CloseDate,partRecord,prod);
        
        

       /* PageReference pageRef = Page.SupplierEventCreationPage;
        pageRef.getParameters().put('id', prod.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(prod);
        Test.setCurrentPageReference(pageRef);*/
        
        Test.startTest();
              ApexPages.StandardController oppStandardController = new ApexPages.StandardController(opp);
            OpportunitiesCloneController oppCloneContrl = new OpportunitiesCloneController(oppStandardController);
            oppCloneContrl.isContactRoleClone='false';
            oppCloneContrl.CloneOpportunity();
            oppCloneContrl.isContactRoleClone='true';
            oppCloneContrl.CloneOpportunity();
            
            
        Test.stopTest();
    }
}