/*
*Created By     : 
*Created Date   : 16/07/2019 
*Description    : This Scheduler class run daily that checks the CPMG Event to see if the End date = yesterday.
*Test Class Name: 
Modification History :
============================================================================================
V.No    Date            By                  Description
============================================================================================
*/
global class SchedulerNeverAttendedEventProcess implements Schedulable {
   
   global void execute(SchedulableContext SC) {
     
       neverAttendedEventProcess chechEvent = new neverAttendedEventProcess(); 
       
       chechEvent.cancelEvent();
              
   }
}