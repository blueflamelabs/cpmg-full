/*
*Created By     : 
*Created Date   : 22/07/2019 
*Description    : 
*Test Class Name: ScheduleAlertNewPostOrderPlanningseries
Modification History :
============================================================================================
V.No    Date            By                  Description
============================================================================================
*/
@isTest
public class ScheduleAlertNewPlanningseries_Test{

    public static testMethod void ScheduleAlertNewPostOrderPlanningseries_TestMethod(){
    
        Id execAccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account a = TestDataGenerator.createTestAccount(true,'Test Account',execAccountRtype );
       
        Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Contact c = testdatagenerator.createtestcontact(true,'test','test','test@gmail.com',a,execConRtype);
       
        Product2 objProd = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
       
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'CPMG Community Portal User' LIMIT 1];
        User objUser = TestDataGenerator.CreateUser(true ,profileId.id, 'Test', 'test','bc123','test@gmail.com',c.id);
       
        Event_Staff__c objEventStf = TestDataGenerator.createEventStaff(true,objProd,objUser,'Developer');
        
        Opportunity objOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),a,'Closed Won',objProd,'Onsite Renewal');
        
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,objProd,a,objOpp,c, execPartRecordType);
        
        
       Id execConRtype1 = Schema.SObjectType.Planning_Series__c.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
       Planning_Series__c ps = new Planning_Series__c();
           ps.Written_By__c =objUser.id;
           ps.Event__c = objProd.id;
           ps.Title__c = 'test';
           ps.Posted_Date__c =system.Today();
           ps.Event__c = objProd.id;
           ps.RecordTypeId =execConRtype1;
           insert ps;
        Order objOrder = new Order(AccountId = a.Id, BillToContact = c, 
                                   Status = 'Cancelled',
                                   Event_Account_Detail__c = partRecord.id, 
                                   OpportunityId = objOpp.Id, 
                                   Primary_Event_Contact__c = true, 
                                   EffectiveDate = objOpp.CloseDate,
                                   Alert_New_Post__c = false,
                                   Order_Event__c = objProd.Id);
        insert objOrder;
        //objOrder.Alert_New_Post__c = true;
        //update objOrder;
        
      Test.StartTest();
          Set<Id> objId = new Set<Id>();
          ScheduleAlertNewPostOrderPlanningseries sch = new ScheduleAlertNewPostOrderPlanningseries();
          ScheduleAlertNewPostOrderPlanningseries.updateAlertPost();
          ScheduleAlertNewPostOrderPlanningseries.updateOrderbyPlaningSeries(objId);
          String jobId = System.schedule('jobName', '0 5 * * * ?',sch);
               
      Test.StopTest();
    }
 }