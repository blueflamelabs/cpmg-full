@isTest
private class MeetingRequestWrapper_Test {
	
	@isTest static void MeetingRequestWrapper_Constructor_Test() {
		// Implement test code
		TestDataGenerator.createInactiveTriggerSettings();

		//Event Setup
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Active Event',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));
		prod.Start_Date__c = Date.today().addDays(40);
		prod.Family ='BuildPoint';
		prod.Event_Navigation_Color__c = '90135d';
		prod.Event_Tint_Color__c = 'EEEEEE';
		update prod;

		Profile standardUser = [SELECT Id FROM Profile where Name ='Standard User' LIMIT 1];
		User execUserCPMG = TestDataGenerator.CreateUser(true,standardUser.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
		User supplierUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);
		User supportUserCPMG = TestDataGenerator.CreateUser(true, standardUser.Id,'Test','Support',null,'testSupport1234@cpmg.com',null);


		Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUserCPMG,'Recruiter',true,1);
		Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUserCPMG,'Salesperson',true,2);
		Event_Staff__c supportStaff = TestDataGenerator.createEventStaff(true,prod,supportUserCPMG,'Support',true,3);

		//Set Up Executive User
		Id execAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Id execConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

		Id suppAccRType =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Id suppConRType =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();

		Account execAcc = TestDataGenerator.createTestAccount(true,'Test Exec Account', execAccRType);
		Account supplierAcc1 = TestDataGenerator.createTestAccount(true,'Test Supplier Account 1', suppAccRType);
		Account supplierAcc2 = TestDataGenerator.createTestAccount(true,'Test Supplier Account 2', suppAccRType);
		Account supplierAcc3 = TestDataGenerator.createTestAccount(true,'Test Supplier Account 3', suppAccRType);
		//Wrapper wants a website.
		execAcc.Website = 'https://www.google.com';
		update execAcc;
		Contact execCon = TestDataGenerator.createTestContact(true,'Test Exec','Last Account','testexec@test.com',execAcc, execConRType);
		Contact suppCon1 = TestDataGenerator.createTestContact(true,'Test Supplier 1','Last Account 2','testsupplier1@test.com',supplierAcc1, suppConRType);
		Contact suppCon2 = TestDataGenerator.createTestContact(true,'Test Supplier 2','Last Account 3','testsupplier2@test.com',supplierAcc2, suppConRType);
		Contact suppCon3 = TestDataGenerator.createTestContact(true,'Test Supplier 3','Last Account 3','testsupplier3@test.com',supplierAcc3, suppConRType);

		Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		User execUser = TestDataGenerator.CreateUser(true,commUserProfId.id,'Test Exec','Last Account','603-555-5555','testexec@test.com', execCon.Id);

		Opportunity execOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),execAcc,'Closed Won',prod);
		Opportunity suppOpp1 = TestDataGenerator.createTestOpp(true,'Supplier Test Opp 1',Date.today(),supplierAcc1,'Closed Won',prod);
		Opportunity suppOpp2 = TestDataGenerator.createTestOpp(true,'Supplier Test Opp2',Date.today(),supplierAcc2,'Closed Won',prod);
		Opportunity suppOpp3 = TestDataGenerator.createTestOpp(true,'Supplier Test Opp3',Date.today(),supplierAcc3,'Closed Won',prod);

		Id execPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
		Id supplierPRRType =Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
		Event_Account_Details__c execPRecod = TestDataGenerator.createParticipationRecord(true,prod,execAcc,execOpp,execCon,execPRRType);
		Event_Account_Details__c suppPRecod1 = TestDataGenerator.createParticipationRecord(true,prod,supplierAcc1,suppOpp1,suppCon1,supplierPRRType);
		Event_Account_Details__c suppPRecod2 = TestDataGenerator.createParticipationRecord(true,prod,supplierAcc2,suppOpp2,suppCon2,supplierPRRType);
		Event_Account_Details__c suppPRecod3 = TestDataGenerator.createParticipationRecord(true,prod,supplierAcc3,suppOpp3,suppCon3,supplierPRRType);

		Order execConf = TestDataGenerator.createConfirmation(true,execAcc,execCon,'Attending Event Contact',execOpp,execOpp.CloseDate,execPRecod,prod);

		//Create Question
		Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
		Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Category Pickilist',200,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),true,false);
			Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
			Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
			Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
		Question__c longQuestion = TestDataGenerator.createQuestion(true,'Long Description',2000,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),true,false);
		Question__c shortQuestion = TestDataGenerator.createQuestion(true,'Short Description',200,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),true,false);
			
		//Create the 3 Event Questions
		Event_Question__c categoryEQ = TestDataGenerator.createEventQuestion(true,'Category',prod,'Category',picklistQuestion,true,1,null);
		Event_Question__c longEQ = TestDataGenerator.createEventQuestion(true,'Company Long Description',prod,'Company',longQuestion,true,1,'Company Long Description');
		Event_Question__c shortEQ = TestDataGenerator.createEventQuestion(true,'Company Short Description',prod,'Company',shortQuestion,true,1,'Company Short Description');

		//Create Contact Answer Questions
		Contact_Event_Answers__c categoryAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,categoryEQ,execCon,'Select2;Select3',null,null);
		Contact_Event_Answers__c longAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,longEQ,execCon,'Long Answer',null,null);
		Contact_Event_Answers__c shortAnswer = TestDataGenerator.createQuestionAnswers(true,execConf,execPRecod,shortEQ,execCon,'Short Answer',null,null);

		ProfileWrapper profWrapper = new ProfileWrapper( execPRecod, shortAnswer , categoryAnswer,new List<Contact_Event_Answers__c> {longAnswer});
		Event_Ranking__c ranking = TestDataGenerator.createEventRanking(true, prod, execPRecod,  suppPRecod1, 1, execCon, 'Boardroom and One-on-One', 'Test Comment' );
		Event_Ranking__c ranking2 = TestDataGenerator.createEventRanking(true, prod, execPRecod,  suppPRecod2, 3, execCon, 'Boardroom', 'Test Comment2' );
		Event_Ranking__c ranking3 = TestDataGenerator.createEventRanking(true, prod, execPRecod,  suppPRecod3, 2, execCon, 'One-on-One', 'Test Comment3' );
		Event_Ranking__c ranking4 = TestDataGenerator.createEventRanking(true, prod, execPRecod,  suppPRecod3, 2, execCon, 'One-on-One', 'Test Comment4' );
		Event_Ranking__c ranking5 = TestDataGenerator.createEventRanking(true, prod, execPRecod,  suppPRecod2, null, execCon, 'Boardroom', 'Test Comment5' );

		Test.startTest();
			MeetingRequestWrapper mWrapper = new MeetingRequestWrapper( profWrapper , ranking );
			MeetingRequestWrapper mWrapper2 = new MeetingRequestWrapper (profWrapper, ranking2);
			MeetingRequestWrapper mWrapper3 = new MeetingRequestWrapper (profWrapper, ranking3);
			MeetingRequestWrapper mWrapper4 = new MeetingRequestWrapper (profWrapper, ranking4);
			MeetingRequestWrapper mWrapper5 = new MeetingRequestWrapper (profWrapper, ranking5);

			List<MeetingRequestWrapper> mWrappers = new List<MeetingRequestWrapper>{mWrapper, mWrapper2, mWrapper3, mWrapper4, mWrapper5};
			mWrappers.sort();
		Test.stopTest();
	}
	
	
	
}