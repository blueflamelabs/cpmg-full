public without sharing class CommunitiesTravelController {

    //public EventRegistration2Controller.UserPermissions permissions { get; set; }
    public UserPermissions permissions { get; set; }

   // public List<Order> userOrders {get;set;}
    public Id userId {get; set;}
    public User currentUser {get; set;}
    //public Contact userContact {get;set;}
    public Set<Id> prodEventIds {get;set;}
    public Product2 eventProd {get;set;}
    private Boolean isEmpty = false;
    private String message {get;set;}
    //public String headerLogo {get; set;}
    public String navColor {get; set;}
   // public Boolean useDefaultHeaderLogo {get; set;}
    private String eventIdFromURL {get;set;}
    public Order currentContactEventOrder { get; set; }

   // public List<Contact_Event_Answers__c> yourActivities { get; set; }

    public CommunitiesTravelController() {
	try{
        eventIdFromURL = ApexPages.currentPage().getParameters().get('eId');

        UserId = UserInfo.getUserId();
        currentUser = [SELECT Id, ContactId, Contact.RecordType.DeveloperName FROM User where ID = :userID];
		System.debug('Current User: ' + currentUser);
        initializeTravelPage();

        currentContactEventOrder = OrderHelper.getConfirmation(currentUser.ContactId, eventProd.Id);
		System.debug('The Contact ORder: '+ currentContactEventOrder);
		//try{
		boolean isEventContact = false;
		if(currentContactEventOrder != null){
		  isEventContact = OrderHelper.isEventContact(currentContactEventOrder, Order_Statuses__c.getOrgDefaults());
		}
		//System.debug('User Record Type: '+ currentUser.Contact.RecordType.DeveloperName);
		//System.debug('Event Contact: '+ isEventContact);
       // permissions = new EventRegistration2Controller.UserPermissions(currentUser.Contact.RecordType.DeveloperName, isEventContact);
        permissions = new UserPermissions(currentUser.Contact.RecordType.DeveloperName, isEventContact);
		//}catch(Exception ex){
		//	System.debug('Error: '+ex.getMessage()+'. Stack Trace: '+ ex.getStackTraceString());
		//}
		
		}catch(exception ex){
			System.debug('Stack Trace: '+ ex.getStackTraceString());
			System.debug('Error: '+ex.getMessage());
		}
    }


    public void initializeTravelPage(){
        getUserOrdersProducts();
       // getHeaderLogo();

      /*  if (eventProd != NULL) {
            yourActivities = [SELECT Id, Activity_Details__c, Question__r.Question__r.Question_Description__c, Answer__c
                FROM Contact_Event_Answers__c
                WHERE Confirmed__c = true
                    AND Question__r.Question__r.Question_Type__c = 'Activity'
                    AND Question__r.Event__c = :eventProd.Id
                ];
        } */
    }

    private void getUserOrdersProducts(){
        //Get all Orders associated with the current contact
        prodEventIds = new Set<Id>();
        message ='';
        for(Order o: [SELECT Id, BillToContactID ,Order_Event__c from Order where BillToContactID = :currentUser.ContactId]){
            if(o.Order_Event__c != null) prodEventIds.add(o.Order_Event__c);
        }
        System.debug('The Event Products Ids from orders: '+ prodEventIds);

        if(String.isNotBlank(eventIdFromURL)){
            eventProd = EventHelper.getEvent(eventIdFromURL);


        } else {
            List<Order> orders = [SELECT Id, Order_Event__c FROM Order WHERE (Order_Event__r.Start_Date__c >= Today AND BillToContactId =:currentUser.ContactId) OR (BillToContactId =:currentUser.ContactId) ORDER BY Order_Event__r.Start_Date__c ASC LIMIT 1];
            System.debug(LoggingLevel.ERROR, orders);
            if (orders.size() > 0) {
                Id pId = orders.get(0).Order_Event__c;
                eventProd = EventHelper.getEvent(pId);

            } else {

                eventProd = EventHelper.getClosestEventByDate();
            }
        } 
        // Used to display a Message, if the Event Products List is empty.
        if(eventProd == null){
            isEmpty = true;
            message = 'Travel details are currently unavailable for this Event. Please contact Connecting Point to get your Travel Details updated.';
            navColor='';
        } else {
            navColor = eventProd.Event_Navigation_Color__c;
        }
		System.debug('The Event Product: '+ eventProd);
    }

    public Boolean getIsEmpty(){
        return isEmpty;
    }

    public String getMessage(){
        return message;
    }
}