@isTest
private class GeneralUtilities_Test {
	
	@isTest static void testFormatedUSPhoneNumber() {
		String num = '(555) 555-5555';
		System.assert(GeneralUtilities.isValidPhoneNumber(num));
	}
	
	@isTest static void testUnformattedUSPhoneNumber() {
		String num = '5555555555';
		System.assert(GeneralUtilities.isValidPhoneNumber(num));
	}

	@isTest static void testFormattedInternationalPhoneNumber() {
		String num = '19-49-89-636-48018';
		System.assert(GeneralUtilities.isValidPhoneNumber(num));
	}

	@isTest static void testInvalidPhoneNumber() {
		String num = 'fivefivefivefivefivefivefivefivefivefive';
		System.assert(GeneralUtilities.isValidPhoneNumber(num) == false);
	}

	@isTest static void testFormatterPreformatted() {
		String num = '(555) 555-5555';
		System.assert(GeneralUtilities.getFormattedPhoneNumber(num) == num);
	}

	@isTest static void testFormatterUnformatted() {
		String num = '5555555555';
		System.assert(GeneralUtilities.getFormattedPhoneNumber(num) == '(555) 555-5555');
	}

	static testmethod void sendNotificationEmail_Test(){
		Account a = TestDataGenerator.createTestAccount(true,'Test Account');
		Contact c = TestDataGenerator.createTestContact(true,'First','Test Contact','firstcontact@test.com',a);

		Test.startTest();
			GeneralUtilities.sendNotificationEmail(a.Id,'Test Subject','Test Body Text',c.email);
		Test.stopTest();
	}

	static testMethod void getSObjectSelectDatabaseQuery(){

		Account a = TestDataGenerator.createTestAccount(true,'Test Account');
		Contact con = TestDataGenerator.createTestContact(true,'First','Test Contact','firstcontact@test.com',a);
		List<Schema.FieldSetMember> conFieldSet = SObjectType.Contact.FieldSets.Self_User_Specified_Fields.getFields();
		Set<String> otherFields = new Set<String>{'Birthdate'};
		String sObjectToQuery ='Contact';
		String whereClause = 'Id=\''+con.Id+'\'';

		Test.startTest();
			String queryString = GeneralUtilities.getSObjectSelectDatabaseQuery(conFieldSet,otherFields,sObjectToQuery,whereClause);
		Test.stopTest();

	}

	static testMethod void getRequiredFieldsFromFieldSet_Test(){
		Account a = TestDataGenerator.createTestAccount(true,'Test Account');
		Contact con = TestDataGenerator.createTestContact(true,'First','Test Contact','firstcontact@test.com',a);
		List<Schema.FieldSetMember> conFieldSet = SObjectType.Contact.FieldSets.Self_User_Specified_Fields.getFields();
		Test.startTest();
			Map<String,String> requiredFieldsMap = GeneralUtilities.getRequiredFieldsFromFieldSet(conFieldSet);
		Test.stopTest();	
	}

	static testMethod void validateRequiredFields_Test(){
		Account a = TestDataGenerator.createTestAccount(true,'Test Account');
		Contact con = TestDataGenerator.createTestContact(true,'First','Test Contact','firstcontact@test.com',a);
		List<Schema.FieldSetMember> conFieldSet = SObjectType.Contact.FieldSets.Self_User_Specified_Fields.getFields();
		Map<String,String> requiredFieldsMap = GeneralUtilities.getRequiredFieldsFromFieldSet(conFieldSet);

		Test.startTest();
			try{
			Boolean validate = GeneralUtilities.validateRequiredFields(requiredFieldsMap, (Sobject) con);	
			} catch(Exception ex){
				String mes = ex.getMessage();
				System.assertNotEquals(mes, null);
			}
		Test.stopTest();	
	}
	
}