public with sharing class EventAccountDetailsTriggerHandler {
	private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    public EventAccountDetailsHandler handler {get;set;}
    
    public EventAccountDetailsTriggerHandler(boolean isExecuting, integer size) {
        m_isExecuting = isExecuting;
        BatchSize = size;
        handler = new EventAccountDetailsHandler();
    }
    
    public void OnAfterInsert(Map<Id,Event_Account_Details__c> newEventAccountDetailMap) {		
       if(!EventAccountDetailsHandler.hasExecuteProductSoldProcess) handler.executeProductSoldProcess(newEventAccountDetailMap);
    }
    
    public void OnAfterDelete(Map<Id,Event_Account_Details__c> oldEventAccountDetailMap) {
        if(!EventAccountDetailsHandler.hasExecuteProductSoldProcess) handler.executeProductSoldProcess(oldEventAccountDetailMap);
    }

    public void OnBeforeUpdate(Event_Account_Details__c[] newEventAccountDetails, Map<Id, Event_Account_Details__c> oldRecords) {      
        //if(!EventAccountDetailsHandler.hasSumFields) handler.sumFields(newEventAccountDetails, oldRecords);
    } 

    public void OnAfterUpdate(Map<Id,Event_Account_Details__c> newEventAccountDetailsMap) {
        if(!EventAccountDetailsHandler.hasExecuteProductSoldProcess) handler.executeProductSoldProcess(newEventAccountDetailsMap);
    }
}