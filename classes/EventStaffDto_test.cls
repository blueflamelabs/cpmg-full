@isTest
private class EventStaffDto_test {
	
	@isTest static void test_method_one() {
		String name = 'testname';
		String email = 'test@test.com';

		EventStaffDto esdto = new EventStaffDto(email, name);

		esdto.staffTitle = 'test';		
		esdto.staffPhone = 'test';
		test.startTest();
		System.assertEquals(esdto.staffEmail, email);
		System.assertEquals(esdto.staffName, name);
		System.assertNotEquals(esdto.staffTitle, null);
		System.assertNotEquals(esdto.staffPhone, null);
		test.stopTest();

	}
	
}