@isTest
private class OrderHelper_test {
    
    @isTest static void test_is_attending() {

        Account acc = new Account(Name='test');
        insert acc;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        User usr = new User(LastName = 'TestLast',
                     FirstName='TestFirst',
                     Alias = 'test',
                     Email = 'testFirst.testLast@asdf.com',
                     Username = 'testFirst.testLast@asdf.com',
                     ProfileId = profileId.id,
                     TimeZoneSidKey = 'GMT',
                     LanguageLocaleKey = 'en_US',
                     EmailEncodingKey = 'UTF-8',
                     LocaleSidKey = 'en_US'
                     );
            
        insert usr;

        Pricebook2 priceBook = new Pricebook2(Name='testPriceBook');

        Order myOrder = new Order(AccountId = acc.Id, OwnerId = usr.Id, Pricebook2Id = priceBook.Id, Status='Attending');

        Order_Statuses__c orderStatus = new Order_Statuses__c(Attending_Picklist_values__c='Attending');
        test.startTest();
        boolean attending = OrderHelper.isAttending(myOrder, orderStatus);

        System.assertEquals(attending, true);
        test.stopTest();
    }
    
    @isTest static void test_not_attending() {
        Account acc = new Account(Name='test');
        insert acc;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        User usr = new User(LastName = 'TestLast',
                     FirstName='TestFirst',
                     Alias = 'test',
                     Email = 'testFirst.testLast@asdf.com',
                     Username = 'testFirst.testLast@asdf.com',
                     ProfileId = profileId.id,
                     TimeZoneSidKey = 'GMT',
                     LanguageLocaleKey = 'en_US',
                     EmailEncodingKey = 'UTF-8',
                     LocaleSidKey = 'en_US'
                     );
            
        insert usr;

        Pricebook2 priceBook = new Pricebook2(Name='testPriceBook');

        Order myOrder = new Order(AccountId = acc.Id, OwnerId = usr.Id, Pricebook2Id = priceBook.Id, Status='Event Contact');

        Order_Statuses__c orderStatus = new Order_Statuses__c(Attending_Picklist_values__c='Attending');
        test.startTest();
        boolean attending = OrderHelper.isAttending(myOrder, orderStatus);

        System.assertEquals(attending, false);
        test.stopTest();
    }
    
    @isTest static void test_is_event_contact() {
        Account acc = new Account(Name='test');
        insert acc;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        User usr = new User(LastName = 'TestLast',
                     FirstName='TestFirst',
                     Alias = 'test',
                     Email = 'testFirst.testLast@asdf.com',
                     Username = 'testFirst.testLast@asdf.com',
                     ProfileId = profileId.id,
                     TimeZoneSidKey = 'GMT',
                     LanguageLocaleKey = 'en_US',
                     EmailEncodingKey = 'UTF-8',
                     LocaleSidKey = 'en_US'
                     );
            
        insert usr;

        Pricebook2 priceBook = new Pricebook2(Name='testPriceBook');

        Order myOrder = new Order(AccountId = acc.Id, OwnerId = usr.Id, Pricebook2Id = priceBook.Id, Status='Event Contact');

        Order_Statuses__c orderStatus = new Order_Statuses__c(Event_Contact_picklist_values__c='Event Contact');
        test.startTest();
        boolean attending = OrderHelper.isEventContact(myOrder, orderStatus);

        System.assertEquals(attending, true);
        test.stopTest();
    }

        @isTest static void test_is_not_event_contact() {
        Account acc = new Account(Name='test');
        insert acc;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        User usr = new User(LastName = 'TestLast',
                     FirstName='TestFirst',
                     Alias = 'test',
                     Email = 'testFirst.testLast@asdf.com',
                     Username = 'testFirst.testLast@asdf.com',
                     ProfileId = profileId.id,
                     TimeZoneSidKey = 'GMT',
                     LanguageLocaleKey = 'en_US',
                     EmailEncodingKey = 'UTF-8',
                     LocaleSidKey = 'en_US'
                     );
            
        insert usr;

        Pricebook2 priceBook = new Pricebook2(Name='testPriceBook');

        Order myOrder = new Order(AccountId = acc.Id, OwnerId = usr.Id, Pricebook2Id = priceBook.Id, Status='Final-Confirmed');

        Order_Statuses__c orderStatus = new Order_Statuses__c(Event_Contact_picklist_values__c='Event Contact');
        test.startTest();
        boolean attending = OrderHelper.isEventContact(myOrder, orderStatus);

        System.assertEquals(attending, false);
        test.stopTest();
    }

    @isTest static void test_get_confirmation(){
  
  Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
        Account acc = new Account(Name='test');
        insert acc;

        Contact newContact = new Contact (
        FirstName = 'firstName',
        LastName = 'lastName',
        AccountId = acc.Id,
        Email = 'test@testmail.com',
        RecordTypeId = execConRtype
        );

        insert newContact;

        Product2 myEvent = new Product2(Name='test',User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert myEvent;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        User usr = new User(LastName = 'TestLast',
                     FirstName='TestFirst',
                     Alias = 'test',
                     Email = 'testFirst.testLast@asdf.com',
                     Username = 'testFirst.testLast@asdf.com',
                     ProfileId = profileId.id,
                     TimeZoneSidKey = 'GMT',
                     LanguageLocaleKey = 'en_US',
                     EmailEncodingKey = 'UTF-8',
                     LocaleSidKey = 'en_US'
                     );
            
        insert usr;

        Pricebook2 priceBook = new Pricebook2(Name='testPriceBook');

        Order myOrder = new Order(
            AccountId= acc.Id, 
            OwnerId = usr.Id, 
            Pricebook2Id = priceBook.Id, 
            BillToContactId=newContact.Id, 
            Order_Event__c = myEvent.Id,
            EffectiveDate = Date.today(),
            Status = 'Preliminary'
            );
        insert myOrder;
        test.startTest();
        Order result = OrderHelper.getConfirmation(newContact.Id, myEvent.Id);

        System.assertEquals(myOrder.Id, result.Id);
        test.stopTest();
    }


    @isTest static void test_get_user_confirmation(){

    TestDataGenerator.createOrderStatusCustomSetting();
        Account acc = new Account(Name='test');
        insert acc;

        Contact newContact = new Contact (
        FirstName = 'firstName',
        LastName = 'lastName',
        AccountId = acc.Id,
        Email = 'test@testmail.com'
        );

        insert newContact;

        Product2 myEvent = new Product2(Name='test',User_Link_Expiration_for_Executives__c= 2,User_Link_Expiration_for_Suppliers__c = 2);
        insert myEvent;

        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        User usr = new User(LastName = 'TestLast',
                     FirstName='TestFirst',
                     Alias = 'test',
                     Email = 'testFirst.testLast@asdf.com',
                     Username = 'testFirst.testLast@asdf.com',
                     ProfileId = profileId.id,
                     TimeZoneSidKey = 'GMT',
                     LanguageLocaleKey = 'en_US',
                     EmailEncodingKey = 'UTF-8',
                     LocaleSidKey = 'en_US'
                     );
            
        insert usr;

        Pricebook2 priceBook = new Pricebook2(Name='testPriceBook');
        Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),acc,'Closed Won',myEvent);
        Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Suppliers').getRecordTypeId();
        Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,myEvent,acc,opp,newContact, execPartRecordType);
        

        Order myOrder = new Order(
            AccountId= acc.Id, 
            OwnerId = usr.Id, 
            Pricebook2Id = priceBook.Id, 
            BillToContactId=newContact.Id, 
            Order_Event__c = myEvent.Id,
            EffectiveDate = Date.today(),
            Status = 'Preliminary'
            );
        insert myOrder;
        test.startTest();
        List<Order> result =  OrderHelper.getUserConfirmations(myEvent.Id, (String)newContact.RecordTypeId);

        boolean containsMyOrder = false;
        for(Order o : result){
            if(o.Id == myOrder.Id){
                containsMyOrder = true;
                break;
            }
        }

        System.assert(containsMyOrder);
        test.stopTest();

    } 
}