public class ImageFileShareCTRL{
    public static void fileInsertVisibility(List<Image__c> objImageList){
        Set<Id> objPRID = new Set<Id>();
        for(Image__c objImage : objImageList){
            if(String.isNotBlank(objImage.Participation_Record__c)){
                objPRID.add(objImage.Participation_Record__c);
            }
        }
        if(objPRID.Size() > 0){
            List<ContentDocumentLink> objConDocLink = [Select Id,Visibility,LinkedEntityId from ContentDocumentLink where LinkedEntityId IN:objPRID and Visibility != 'AllUsers'];
            for(ContentDocumentLink ConDoc:objConDocLink){
                    ConDoc.Visibility='AllUsers';   
            }
            if(objConDocLink.size() > 0)
                update objConDocLink;
        }
    }
}