global class GenerateOpportunitiesControllerBatch implements Database.Batchable<sObject> {
    
    String campaignId;
    Boolean isOppCreate{set;get;}
    global GenerateOpportunitiesControllerBatch(String campaignId) {
        this.campaignId = campaignId;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String StatusName = 'Not Interested';
        String query = 'SELECT Id, ContactId, Contact.AccountId, Contact.Account.Name, Contact.Name, Campaign.Event__r.Start_Date__c,'
        +'Campaign.Event__r.ProductCode, Campaign.Event__c, Ranking__c FROM CampaignMember WHERE CampaignId =: campaignId AND Status !=: StatusName AND ContactId != NULL';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
       List<CampaignMember> members = (List<CampaignMember>)scope;
        
        Campaign_Opp_Settings__c campaignOppSettings = Campaign_Opp_Settings__c.getInstance();
        Integer daysToEvent = (campaignOppSettings.Days_To_Event__c != NULL) ? campaignOppSettings.Days_To_Event__c.intValue() : 0;
        list<Opportunity> contactToOpportunityId = new list<Opportunity>();
        
        set<String> setOfContactId = new set<String>();
        set<String> setOfEventId = new set<String>();
        for (CampaignMember cm : members){
                setOfContactId.add(cm.ContactId);
            if(cm.Campaign.Event__c != NULL)
                setOfEventId.add(cm.Campaign.Event__c);            
        }
        set<String> setOfAlreadyHaveOpportunity = new set<String>();
        for(opportunity opp : [select id,Primary_Contact__c,Product__c,Ranking__c from opportunity where Primary_Contact__c in:setOfContactId and Product__c in:setOfEventId
                        and Primary_Contact__c !=null and Product__c !=null]
                         ){
            setOfAlreadyHaveOpportunity.add(opp.Primary_Contact__c+''+opp.Product__c);
        }
        for (CampaignMember cm : members){
            if (cm.Campaign.Event__c != NULL && !setOfAlreadyHaveOpportunity.contains(cm.ContactId+''+cm.Campaign.Event__c)){
                Datetime oppDate = (cm.Campaign.Event__r.Start_Date__c != NULL) ? cm.Campaign.Event__r.Start_Date__c.addDays(-daysToEvent) : System.now().addDays(90);
                Opportunity newOpp = new Opportunity();
                newOpp.CampaignId = cm.CampaignId;
                newOpp.Primary_Contact__c= cm.ContactId;
                newOpp.AccountId = cm.Contact.AccountId;
                newOpp.StageName = campaignOppSettings.StageName__c;
                newOpp.CloseDate = oppDate.date();
                newOpp.Name = cm.Campaign.Event__r.ProductCode + ' - ' + cm.Contact.Account.Name  + ' - ' + cm.Contact.Name;
                newOpp.Product__c = cm.Campaign.Event__c;
                newOpp.Ranking__c = cm.Ranking__c;
                contactToOpportunityId.add(newOpp);
            }
        }
        if(contactToOpportunityId.size()>0){
            insert contactToOpportunityId;
            isOppCreate = true;
        }else{
            isOppCreate = false;
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}