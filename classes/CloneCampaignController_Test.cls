/*
*Created By     : 
*Created Date   : 22/07/2019 
*Description    : 
*Class Name     : CloneCampaignController
Modification History :
============================================================================================
V.No    Date            By                  Description
============================================================================================
*/
@IsTest
public class CloneCampaignController_Test{
    @isTest(seeAllData=true) 
    static void testCampaign() {
           
            Product2 prod = new Product2();
            prod.Name = 'test';
            prod.ProductCode = 'HP17';
            prod.IsActive = true;
            prod.Start_Date__c = date.today();
            insert prod;
           
            Campaign campOrignal = new Campaign();
            campOrignal.Name = 'test';
            campOrignal.Event__c = prod.id;
            campOrignal.Type = 'Conference';
            campOrignal.StartDate = date.today();
            insert campOrignal;
            
             List <CampaignMemberStatus> cmStatus = [SELECT id, CampaignID, HasResponded, IsDefault, Label, SortOrder 
                                                  FROM CampaignMemberStatus 
                                                 WHERE CampaignId = :campOrignal.Id]; 
            System.assertEquals(2,cmStatus.size());  
            PageReference pref = Page.CloneCampaignOpportunity; 
            ApexPages.currentPage().getParameters().put('Id',campOrignal.id);
            ApexPages.StandardController sc = new ApexPages.StandardController(campOrignal);
            CloneCampaignController testAccPlan = new CloneCampaignController(sc);
            String eventId=prod.id;
            testAccPlan.createNewCampaign();
        } 
    

       static testmethod void CloneCampaignController_TestMethod(){
        Id execAccountRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
            Account a = TestDataGenerator.createTestAccount(true,'Test Account',execAccountRtype);
        
            Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
        
            //Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today().addDays(2), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
            Product2 prod = new Product2();
            prod.Name = 'test';
            prod.ProductCode = 'HP17';
            prod.IsActive = true;
            prod.Start_Date__c = date.today();
            insert prod;
            
            Opportunity objOpp = TestDataGenerator.createTestOpp(true,'Exec Test Opp',Date.today(),a,'Closed Won',Prod,'Onsite Renewal');
            
            User userRecord = new User(Alias = 'new123',  
                                              EmailEncodingKey='UTF-8',
                                              LastName='Testing',
                                              LanguageLocaleKey='en_US', 
                                              LocaleSidKey='en_US', 
                                              ProfileId = UserInfo.getProfileId(),
                                              Email = 'test1.liveston@asdf.com',
                                              Username = 'test.liveston@asdf.com', 
                                              TimeZoneSidKey='Asia/Dubai'
                                              );
            insert userRecord ;
            
            Campaign campOrignal = new Campaign();
            campOrignal.Name = 'test';
            campOrignal.Event__c = prod.id;
            campOrignal.Type = 'Conference';
            campOrignal.StartDate = date.today();
            insert campOrignal;
            
            Lead lead = new Lead();
            lead.FirstName ='test';
            lead.LastName = 'test';
            lead.Company ='syn';
            insert lead;
            
            CampaignMember cm = new CampaignMember();
            //cm.Name = 'test';
            cm.CampaignId = campOrignal.id;
            cm.ContactId = c.id;
            cm.LeadId = lead.id;
            insert cm;
            
            
           CampaignMemberStatus cmsOrignal = new CampaignMemberStatus();
            cmsOrignal.Label = 'test';
            cmsOrignal.CampaignId = campOrignal.id;
            cmsOrignal.HasResponded = true;
            insert cmsOrignal;
    
          
            
            PageReference pref = Page.CloneCampaignOpportunity; 
            ApexPages.currentPage().getParameters().put('Id',campOrignal.id);
            ApexPages.StandardController sc = new ApexPages.StandardController(campOrignal);
            CloneCampaignController testAccPlan = new CloneCampaignController(sc);
            String eventId=prod.id;
             testAccPlan.createNewCampaign();
             testAccPlan.cloneCampaignNo();
             testAccPlan.cancelButton();
             testAccPlan.cloneCampaignYes();
       }
}