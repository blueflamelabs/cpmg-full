public with sharing class SponsorshipController {
    public Product2 currentEvent {get;set;}
    private List<Purchased_Sponsorship__c> pSponsorships {get;set;}
    private Set<Id> participationRecordIds {get;set;}
    private List<Image__c> sponsorImageRecords {get;set;}
    public String OrgInstanceName{set;get;}
    public List<ImageWrapper> docIds {get{
            if(docIds == null){
                try{
                getDocumentIds();
                }catch(Exception ex){
                    System.debug('Error: '+ ex.getMessage()+'.  Stack Trace: '+ ex.getStackTraceString());
                }
            }
            return docIds;
        }set;}
    
    public SponsorshipController() {
        List<Organization> orgInfo = [SELECT Id, InstanceName FROM Organization];
        if(orgInfo.size() > 0)
            OrgInstanceName = orgInfo[0].InstanceName;
    }
    //This will return all purchased sponsorships from the event, where the corresponding Event Sponsorship
    //Is "Display on Portal"
    private void getPurchasedSponsorships(){
        pSponsorships = PurchasedSponsorshipHelper.getPurchasedSponsorshipsForPortalImages(currentEvent.Id);
    }

    //We need Participation Record Ids because they are referenced on the Images.
    private void getParticipationRecordIds(){
        participationRecordIds = new Set<Id>();
        if(pSponsorships!= null && !pSponsorships.isEmpty()){
            for(Purchased_Sponsorship__c pSponsor : pSponsorships){
                //It has to contain the relationship. Its a master detail
                if(!participationRecordIds.contains(pSponsor.Event_Account_Detail__c)){
                    participationRecordIds.add(pSponsor.Event_Account_Detail__c);
                }
            }
        }
    }

    private void getImageRecords(){
        if(!participationRecordIds.isEmpty()){
            sponsorImageRecords = ImageHelper.getImagesByParticipationRecords(participationRecordIds);
            System.debug('Sponsor Image Records: '+ sponsorImageRecords);
        }
    }

    private List<ImageWrapper> getDocumentIds(){
        System.debug('Getting Doc IDS: ');
        getPurchasedSponsorships();
        getParticipationRecordIds();
        getImageRecords();
        //The File Id is a reference to a Salesforce Document. 
        //This will allow us to display the uploaded image file on the portal.
        docIds = new List<ImageWrapper>();
        if(sponsorImageRecords!= null && !sponsorImageRecords.isEmpty()){
            for(Image__c img : sponsorImageRecords){
                docIds.add(new ImageWrapper(img));
            }
        }
        SYstem.debug('THE DOCUMENT IDS: '+ docIDs);
        return docIds;
    }

    public class ImageWrapper{
        public Image__c img {get;set;}
        //public String companyName {get;set;}
        //public String companyWebsite {get;set;}

        public ImageWrapper(Image__c image){
            img = image;
            //companyName = image.Company__r.Name;
            //companyWebsite = image.Company__r.Website;
        }
    }
}