public class ChangePasswordController1 {
    public boolean isExpiry{set;get;}
    public boolean isMinLength{set;get;}
    public String passwordPolicy{set;get;}
    public String StaticResourceName{set;get;}
    public String StaticResourceYear{set;get;}
    Blob cryptoKey = Blob.valueOf(system.label.cryptoKey);
    string contactId;
    string confmId;
    string tokenString;
    Date Token;
    public ChangePasswordController1(){
        passwordPolicy = String.valueOf(site.getPasswordPolicyStatement());
        
        string crptoDataContactId = ApexPages.currentPage().getParameters().get('contactId');
        System.debug('@@@@@@@@crptoDataContactId@@@@@@@@@@'+crptoDataContactId);
        if(crptoDataContactId!=null){         
            
            Blob data = EncodingUtil.base64Decode(crptoDataContactId);
            Blob decryptedData = Crypto.decryptWithManagedIV('AES128', cryptoKey , data);
            String dryptData = decryptedData.toString();
            Map<String, Object> paramObj =(Map<String, Object>)JSON.deserializeUntyped(dryptData);
            if(paramObj.keyset().contains('contactId'))
                contactId = (string)paramObj.get('contactId'); 
            if(paramObj.keyset().contains('confirmationId'))
                confmId = (string)paramObj.get('confirmationId'); 
            if(paramObj.keyset().contains('token'))
                tokenString = (string)paramObj.get('token'); 



        } 
        if(ConfmId != null){
            List<Order> objOrderList = [Select id,Order_Event__r.Year__c,Order_Event__r.Family from Order 
                    where id =:ConfmId and Order_Event__r.Year__c != null and Order_Event__r.Family != null];
            if(objOrderList.size() > 0){
                String FamilyName = String.valueOf(objOrderList[0].Order_Event__r.Family).replace(' ','_');
                StaticResourceYear = objOrderList[0].Order_Event__r.Year__c+'/'+FamilyName+'.png';
                StaticResourceName = 'CPMG_Events_Page_'+objOrderList[0].Order_Event__r.Year__c;
                List<StaticResource> stylesheetStaticResource = [SELECT id,SystemModStamp,Name FROM StaticResource WHERE Name =:StaticResourceName];
               if(stylesheetStaticResource.size() > 0){
                String url_file_ref = '/resource/'
                    + String.valueOf(((DateTime)stylesheetStaticResource[0].get('SystemModStamp')).getTime())
                    + '/' 
                    + stylesheetStaticResource[0].get('Name')
                    + '/'+StaticResourceYear;
              try{
                PageReference file_ref = new PageReference(url_file_ref);
                String res_json_body = String.valueOf(file_ref.getContent());
               }catch(Exception ex){
                   StaticResourceName = null;
               }
              }else{
                  StaticResourceName = null;
              } 
            }
        }
        if(contactId !=null){
            if(tokenString !=null){
                String dataformatchange = tokenString;
                String dataformatyear = dataformatchange.substring(0,4)+'-';
                String dateformatmonth = dataformatchange.substring(4,6)+'-';
                String dateformatday = dataformatchange.substring(6,dataformatchange.length());
                Token = date.valueof(dataformatyear+dateformatmonth+dateformatday);
            }    
            list<Order> listObjOrder = [select id,Welcome_Email_Link_Expiration__c,BillToContactId from Order where BillToContactId =: contactId
            And Id =:ConfmId And Welcome_Email_Link_Expiration__c =:Token limit 1];
            if(listObjOrder.size()==0){                    
                    isExpiry = true;
            }
        }
    }
    public PageReference redirectUrl(){
        //string contactId = ApexPages.currentPage().getParameters().get('contactId');
        list<user> userList = [select Id,userName from user where contactId =:contactId and isActive = true and LastLoginDate !=null];
        if(userList.size()>0){
            PageReference pageRef = new PageReference('/registration/CA_LoginVFpage');
            return pageRef; 
        }
        return null;
    }
    public PageReference sendEmail(){
        if(passwordPolicy == null)
            passwordPolicy = String.valueOf(site.getPasswordPolicyStatement());
        
       
        if(newPassword == confirmPassword){
            if(contactId !=null){
                
                list<user> userList = [select Id,userName from user where contactId =:contactId and isActive = true]; 
                if(userList.size()>0){
                     try{
                        system.setPassword(userList[0].id,newPassword);
                        return Site.login(userList[0].userName, newPassword,null);
                    }catch(Exception ex){
                        if(ex.getMessage().contains('INVALID_NEW_PASSWORD')){
                           passwordPolicy = String.valueOf(ex.getMessage()).split(':')[1];
                           isMinLength = true;
                        }  
                        return null;
                    }
                }else{
                    list<contact> conList = [SELECT Id, User_Create_Confirmation__c,Email, FirstName, LastName,Phone, AccountId, Account.Type,RecordTypeId FROM Contact Where Id =:contactId ];
                    if(conList.size()>0){
                        string userId;
                        contact Con = conList[0];
                        Community_Settings__c commSettings = Community_Settings__c.getOrgDefaults();
                        user u = UserHelper.CreateUser('00e15000001PPPu',
                                            (con.firstName != null)?con.firstName:null, 
                                             con.LastName, 
                                            (con.Phone != null)?con.Phone: null,
                                             con.Email,
                                             con.Id);
                       
                        try{
                           userId = Site.createportaluser(u,con.accountId,null);
                           if(userId != null)
                               system.setPassword(userId,newPassword);
                        }catch(Site.ExternalUserCreateException ex){
                           system.debug(ex);
                        }
                       
                       if (userId != null){ 
                            if (newPassword != null && newPassword.length() > 1) {
                                return Site.login(con.Email, newPassword , null);
                            }
                            else{
                                PageReference page = System.Page.SiteRegisterConfirm;
                                page.setRedirect(true);
                                return page;
                            }
                      }
                      return null;
                    }
                    else{
                        return null;
                    }
                    
                }      
            }
            else{
                try{
                    return site.changePassword(newPassword ,confirmPassword ,null); 
                 }catch(Exception ex){
                  if(ex.getMessage().contains('INVALID_NEW_PASSWORD')){
                       passwordPolicy = String.valueOf(ex.getMessage()).split(':')[1];
                       isMinLength = true;
                    }  
                    return null;
                 }              
            }
        }else{
            passwordPolicy = 'Passwords do not match. Please enter a matching password in both rows.';
            isMinLength = true;
        }
        return null;
    }


    public String confirmPassword { get; set; }
    public String newPassword { get; set; }
}