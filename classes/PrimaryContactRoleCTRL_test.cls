@isTest
public class PrimaryContactRoleCTRL_test{
      static testmethod void PrimaryContactRoleCTRLtest(){
           Id suppAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
           Account a = TestDataGenerator.createTestAccount(true, 'Test Account',suppAccRtype);
           Id suppConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
           
           Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,suppConRtype);
           Contact attendContact = TestDataGenerator.createTestContact(true,'Test','Attending Con', 'testAttendingCon@account.com',a,suppConRtype);
        
           Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(10),Date.today().addDays(15),Date.today().addMonths(-2), Date.today().addMonths(-1));
           Opportunity objOpp= TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);
           OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = objOpp.Id, ContactId =c.Id, Role ='Primary', isPrimary =true);
           insert ocr;
          
           objOpp.Primary_Contact__c = attendContact.id;
           update objOpp;
           objOpp.Primary_Contact__c = null;
           update objOpp;
           objOpp.Primary_Contact__c = null;
           update objOpp;
       
      }

}