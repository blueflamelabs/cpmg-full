public with sharing class ExceptionController {
	public PageReference redirect(){
        PageReference pageRef = new PageReference('/Events');
        return pageRef;
    }
}