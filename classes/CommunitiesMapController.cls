public without sharing class CommunitiesMapController {

    private final sObject mysObject;
    private String userId;
    private User currentUser;
    private Set<Id> prodEventIds{get; set;}
    public Product2 upcomingProdEvent {get;set;}
   // private ContentDocumentLink mapAssociatedWithOrder;
   // private Document eventMapImage;
    //private Set<Id> contentDocumentIds {get; set;}
    //private ContentDocument eventMap;
    private boolean isError = false;
    private String mapURL;
    private String mapNameString;
    private String errorMessage;
    //public String headerLogo {get; set;}
    public String navColor {get; set;}
    //public Boolean useDefaultHeaderLogo {get; set;}
    private String eventIdFromURL {get;set;}
    private String eventName {get;set;}
    private Id eventId {get;set;}
    public Map<String,Contact_Event_Answers__c> shortAnswerMap {get;set;}
    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
  /* public CommunitiesMapController(ApexPages.StandardController stdController) {
        this.mysObject = (sObject)stdController.getRecord();
        userId = UserInfo.getUserId();
        //get the corresponging
        currentUser = [SELECT Id, ContactId FROM USer where Id = :userId];
    } */
    public List<Event_Account_Details__c> ea_list { get; set; }
    public List<Event_Map_URL__c> map_list { get; set; }
    private Set<Id> prIds {get;set;}

    public String companiesSortOrder { get; set; }
    public String companiesSortField { get; set; }

    public CommunitiesMapController() {
        companiesSortOrder = 'ASC';
        companiesSortField = 'company';
       // this.mysObject = (sObject)stdController.getRecord();
       //userId ='00563000000YS32AAG';
        userId = UserInfo.getUserId();
        //get the corresponging
        errorMessage= 'An Event Layout Map is currently unavailable. Please check back soon to see the Event Map';
        eventIdFromURL = ApexPages.currentPage().getParameters().get('eId');
        System.debug('Event Id From URL: '+ eventIdFromURL);
        try{
        currentUser = [SELECT Id, ContactId FROM User where Id = :userId];
        }catch( Exception ex){
            System.debug('Error: '+ ex.getMessage()+'. On Line: '+ ex.getLineNumber() +'. Stack Trace: '+ ex.getStackTraceString());
        }
        SYstem.debug('The current User: ' + currentUser);
        initializeMapPage();
    }
    private void initializeMapPage(){
        getUserOrderProducts();
       // getProductMapFile();
       // getHeaderLogo();
    }

    private void getUserOrderProducts(){

        prodEventIds= new Set<Id>();
        //get orders associated with the logged in User Contact Id value.
        for(Order o: [SELECT Id, BillToContactId, Order_Event__c from Order where BillToContactId = :currentUser.ContactId]){
            if(o.Order_Event__c != null) prodEventIds.add(o.Order_Event__c);
        }
        System.debug('The Product Event Ids: ' + prodEventIds);
        if(String.isNotBlank(eventIdFromURL)){
            eventId = eventIdFromURL;
        } else{
            eventId = new List<Id>(prodEventIds).get(0);
        }
        //if(prodEventIds.size() > 0)
         //upcomingProdEvent = [SELECT Id, Name from Product2 where ID IN :prodEventIds AND Start_Date__c >= Today ORDER BY Start_Date__c ASC Limit 1];
        //if(prodEventIds.size() > 0)

        try{
            if (String.isNotBlank(eventIdFromURL)){
                upcomingProdEvent = EventHelper.getEvent(eventId);
                //[SELECT Id, Name, Event_Navigation_Color__c, Event_Map_URL__c from Product2 where Id = :eventIdFromURL];
            } else if (prodEventIds.size() > 0)
                upcomingProdEvent = EventHelper.getEvent(eventId);
                //[SELECT Id, Name, Event_Navigation_Color__c, Event_Map_URL__c from Product2 where Id in :prodEventIds];
        }catch(Exception ex){
            System.debug('Error: '+ex.getMessage()+'. On Line: '+ ex.getLineNumber()+'. Stack Trace: '+ex.getStackTraceString());
            isError = true;
            errorMessage= 'An Event Layout Map is currently unavailable. Please check back soon to see the Event Map';
        }
        if(upcomingProdEvent !=null) {
            mapNameString = '%' +upcomingProdEvent.Name.removeStart('Sample--') + ' map%';
            navColor = upcomingProdEvent.Event_Navigation_Color__c;
            eventName= upcomingProdEvent.Name;
            map_list = [
                SELECT Map_URL__c, Map_Title__c FROM Event_Map_URL__c
                WHERE Event__c = :upcomingProdEvent.Id
                ORDER BY Display_Order__c
            ];
            sortCompanies();
            
        }
        if(map_list != null && ea_list != null && map_list.isEmpty() && ea_list.isEmpty()){
            isError = true;
            errorMessage = 'An Event Layout Map is currently unavailable. Please check back soon to see the Event Map';
        }
        System.debug('The Upcoming Product Event: ' + upcomingProdEvent);
        System.debug('The Value for document name filter: '+ mapNameString);
    }

    public void sortCompanies() {
        Id upcomingProdEventId = upcomingProdEvent.Id;
        String query = 'SELECT Location__c, Account__r.Name, Account__r.Description, Company_Display_Sort__c FROM Event_Account_Details__c WHERE Event__c = :upcomingProdEventId AND Account__r.RecordType.Name = \'Supplier\' AND Account__r.Type != \'Vendor\' AND Account__r.Type != \'Staff\'';
        if (companiesSortField == 'company') {
           // query += ' ORDER BY Account__r.Name ';
           query += 'ORDER BY Company_Display_Sort__c ';
        } else {
            query += ' ORDER BY Location__c ';
        }

        query += companiesSortOrder+' NULLS LAST';
        ea_list = Database.query(query);
        getShortAnswerDescriptionsByPR();
    }

    private void getShortAnswerDescriptionsByPR(){
        prIds = new Set<Id>();
        shortAnswerMap = new Map<String,Contact_Event_Answers__c>();
        //get all Identified PR records.
        if(ea_list != null && ea_list.size() > 0){
            for(Event_Account_Details__c partRec : ea_list){
                prIds.add(partRec.Id);
            }
        }
        // Get the Short Answer Descriptions.
        Map<String, Contact_Event_Answers__c> tempMap = new Map<String,Contact_Event_Answers__c>();
        if(prIds.size() > 0){
            shortAnswerMap = ContactEventAnswerHelper.getShortAnswerQuestionsByPR(new Set<Id>{eventId});
            for(Id prId : prIds){
                if(!shortAnswerMap.containsKey((String) prId)){
                    shortAnswerMap.put((String) prId, new Contact_Event_Answers__c(Answer__c =''));
                }
            }
           //System.debug('The Temp Map: '+ shortAnswerMap);
            /*for(Contact_Event_Answers__c answer : tempMap.values()){
                if(String.isNotBlank(answer.Answer__c)){
                    shortAnswerMap.put(answer.Participation_Record__c, answer);
                } else{
                    shortAnswerMap.put(answer.Participation_Record__c, new Contact_Event_Answers__c(Answer__c = ''));
                }
            }
            System.debug('The SHort Answer Map: '+ shortAnswerMap); */
        }
    }

    public String getMapURL(){
        return mapURL;
    }

    public Product2 upcomingProdEvent(){
        return upcomingProdEvent;
    }

    public Boolean getIsError(){
        return isError;
    }

    public String getErrorMessage(){
        return errorMessage;
    }
    public String getEventName(){
        return eventName;
    }

}