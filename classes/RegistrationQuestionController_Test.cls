@isTest
private class RegistrationQuestionController_Test {
	
	@isTest static void build_ExecutiveQuestions_Test() {
		// Implement test code
		TestDataGenerator.createInactiveTriggerSettings();
		Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

		Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

		Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
		c.Has_Event__c = true;
		update c;
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
		
		//Start Defining Event "Product" related info.
		Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
		
		User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec@cpmg.com',null);
		User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier@cpmg.com',null);

		Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
		Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');

		Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

		Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
		Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

		Order conf = TestDataGenerator.createConfirmation(true,a,c,'Preliminary',opp,Date.today(),ead,prod );
		
		//Start Questions
		Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
		Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Pickilist',200,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),false,false);
			Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
			Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
			Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
		Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),true,false);
			Question_Answers_Values__c qav4Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option4','Select4');
			Question_Answers_Values__c qav5Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option5','Select5');
			Question_Answers_Values__c qav6Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option6','Select6');
		//Supplier
		Question__c supplierQuestion = TestDataGenerator.createQuestion(true,'Test Supplier Question',2000,false,'Category',questionRTypeMap.get('Supplier').getRecordTypeId(),true,false);

		//Executive Golf
		Question__c execActivityQuestion = TestDataGenerator.createQuestion(true,'Test Golf Pickilist',200,true,'Activity',questionRTypeMap.get('Executive').getRecordTypeId(),false,true);
		
		//Create Event Questions
		Event_Question__c eQuestion = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);
		//UserPermissions uPermissions = 

		Test.startTest();
			registrationQuestionsController regQuestionController= new registrationQuestionsController();
			regQuestionController.currentEvent = prod;
			regQuestionController.currentContact = c;
			regQuestionController.currentConfirmation = conf;
			regQuestionController.eventId = prod.Id;
			regQuestionController.contactId = c.Id;
			regQuestionController.currentContactRecordType = 'Executive';
			//regQuestionController.parentForm = new MultiComponentForm();
			regQuestionController.phase ='Category';
			List<QuestionWrapper> questionList = regQuestionController.getQuestionList();
		Test.stopTest();
	}

	@isTest static void build_ExecutiveQuestions_wAnswers_Test() {
		// Implement test code
		TestDataGenerator.createInactiveTriggerSettings();
		Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

		Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

		Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
		c.Has_Event__c = true;
		update c;
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
		
		//Start Defining Event "Product" related info.
		Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
		
		User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec@cpmg.com',null);
		User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier@cpmg.com',null);

		Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
		Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');

		Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

		Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
		Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

		Order conf = TestDataGenerator.createConfirmation(true,a,c,'Preliminary',opp,Date.today(),ead,prod);
		
		//Start Questions
		Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
		Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Pickilist',200,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),false,false);
			Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
			Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
			Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
		Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),true,false);
			Question_Answers_Values__c qav4Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option4','Select4');
			Question_Answers_Values__c qav5Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option5','Select5');
			Question_Answers_Values__c qav6Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option6','Select6');
		//Supplier
		Question__c supplierQuestion = TestDataGenerator.createQuestion(true,'Test Supplier Question',2000,false,'Category',questionRTypeMap.get('Supplier').getRecordTypeId(),true,false);

		//Executive Golf
		Question__c execActivityQuestion = TestDataGenerator.createQuestion(true,'Test Golf Pickilist',200,true,'Activity',questionRTypeMap.get('Executive').getRecordTypeId(),false,true);
			Question_Answers_Values__c qav4Golf = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Golf','Golf');
			Question_Answers_Values__c qav5sail = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'sailing','Sailing');
		
		//Create Event Questions
		Event_Question__c picklistEQuestion = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',picklistQuestion,true,null,null);
		Event_Question__c multiEQuestion = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);
		Event_Question__c execActEQuestion = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',execActivityQuestion,true,null,null);
		//UserPermissions uPermissions =
		
		Contact_Event_Answers__c  picklistAnswer = TestDataGenerator.createQuestionAnswers(true,conf,ead,picklistEQuestion,c,'Select;Select2',qav1Picklist.Id+';'+qav2Picklist.Id,null); 
		Contact_Event_Answers__c  execActAnswer = TestDataGenerator.createQuestionAnswers(true,conf,ead,execActEQuestion,c,'Golf',qav4Golf.Id,'Left'); 

		Test.startTest();
			registrationQuestionsController regQuestionController= new registrationQuestionsController();
			regQuestionController.currentEvent = prod;
			regQuestionController.currentContact = c;
			regQuestionController.currentConfirmation = conf;
			regQuestionController.eventId = prod.Id;
			regQuestionController.contactId = c.Id;
			regQuestionController.currentContactRecordType = 'Executive';
			//regQuestionController.parentForm = new MultiComponentForm();
			regQuestionController.phase ='Category';
			List<QuestionWrapper> questionList = regQuestionController.getQuestionList();
		Test.stopTest();
	}
	@isTest static void build_ExecutiveQuestions_wAnswers_Changes_Test() {
		// Implement test code
		TestDataGenerator.createInactiveTriggerSettings();
		Id execConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();

		Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account');

		Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) execConRtype);
		c.Has_Event__c = true;
		update c;
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
		
		//Start Defining Event "Product" related info.
		Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
		
		User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec@cpmg.com',null);
		User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier@cpmg.com',null);

		Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
		Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');

		Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

		Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
		Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

		Order conf = TestDataGenerator.createConfirmation(true,a,c,'Preliminary',opp,Date.today(),ead,prod);
		
		//Start Questions
		Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
		Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Pickilist',200,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),false,false);
			Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
			Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
			Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
		Question__c multiPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),true,false);
			Question_Answers_Values__c qav4Picklist = TestDataGenerator.createQuestionOption(true,multiPicklistQuestion,'Select Option4','Select4');
			Question_Answers_Values__c qav5Picklist = TestDataGenerator.createQuestionOption(true,multiPicklistQuestion,'Select Option5','Select5');
			Question_Answers_Values__c qav6Picklist = TestDataGenerator.createQuestionOption(true,multiPicklistQuestion,'Select Option6','Select6');
		//Supplier
		Question__c supplierQuestion = TestDataGenerator.createQuestion(true,'Test Supplier Question',2000,false,'Category',questionRTypeMap.get('Supplier').getRecordTypeId(),true,false);

		//Executive Golf
		Question__c execActivityQuestion = TestDataGenerator.createQuestion(true,'Test Golf Pickilist',200,true,'Activity',questionRTypeMap.get('Executive').getRecordTypeId(),false,true);
			Question_Answers_Values__c qav4Golf = TestDataGenerator.createQuestionOption(true,multiPicklistQuestion,'Golf','Golf');
			Question_Answers_Values__c qav5sail = TestDataGenerator.createQuestionOption(true,multiPicklistQuestion,'sailing','Sailing');
		
		//Create Event Questions
		Event_Question__c picklistEQuestion = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',picklistQuestion,true,null,null);
		Event_Question__c multiEQuestion = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',multiPicklistQuestion,true,null,null);
		Event_Question__c execActEQuestion = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',execActivityQuestion,true,null,null);
		//UserPermissions uPermissions =
		
		Contact_Event_Answers__c  picklistAnswer = TestDataGenerator.createQuestionAnswers(true,conf,ead,picklistEQuestion,c,'Select',qav1Picklist.Id,null);
		Contact_Event_Answers__c  mulitpicklistAnswer = TestDataGenerator.createQuestionAnswers(true,conf,ead,picklistEQuestion,c,'Select4;Select5',qav4Picklist.Id+';'+qav5Picklist.Id,null);  
		Contact_Event_Answers__c  execActAnswer = TestDataGenerator.createQuestionAnswers(true,conf,ead,execActEQuestion,c,'Golf',qav4Golf.Id,'Left'); 

		registrationQuestionsController regQuestionController= new registrationQuestionsController();
			regQuestionController.currentEvent = prod;
			regQuestionController.currentContact = c;
			regQuestionController.currentConfirmation = conf;
			regQuestionController.eventId = prod.Id;
			regQuestionController.contactId = c.Id;
			regQuestionController.currentContactRecordType = 'Executive';
			//regQuestionController.parentForm = new MultiComponentForm();
			regQuestionController.phase ='Category';
			List<QuestionWrapper> questionList = regQuestionController.getQuestionList();
		
		Test.startTest();
			
			For(QuestionWrapper qw: questionList){
				if(qw.question.id == picklistEQuestion.Id){
					qw.singleAnswer = qav1Picklist.Id;
				}
				if(qw.question.Id == multiEQuestion.Id){
				List<String> ansList = new List<String>{(String)qav4Picklist.Id,(String)qav6Picklist.Id};
					regQuestionController.setAnswers(ansList);
					qw.answerList.add(qav4Picklist.Id);
					qw.answerList.add(qav6Picklist.Id);
				}
				if(qw.question.Id == execActEQuestion.id){
					qw.singleAnswer = qav4Golf.Id;
					qw.answer ='Golf';
					qw.subAnswer='Left';
				}
			}
			regQuestionController.saveAnswers();
		Test.stopTest();
	}
	
	@isTest static void build_SupplierQuestions_Test() {
		// Implement test code
		TestDataGenerator.createInactiveTriggerSettings();
		Id supplierAccRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Id supplierConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();

		Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account',supplierAccRtype);

		Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) supplierConRtype);
		c.Has_Event__c = true;
		update c;
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
		
		//Start Defining Event "Product" related info.
		Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
		
		User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec@cpmg.com',null);
		User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier@cpmg.com',null);

		Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
		Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');

		Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

		Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
		Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

		Order conf = TestDataGenerator.createConfirmation(true,a,c,'Preliminary',opp,Date.today(),ead,prod);
		
		//Start Questions
		Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
		Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Pickilist',200,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),false,false);
			Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
			Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
			Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
		Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),true,false);
			Question_Answers_Values__c qav4Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option4','Select4');
			Question_Answers_Values__c qav5Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option5','Select5');
			Question_Answers_Values__c qav6Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option6','Select6');
		//Supplier
		Question__c supplierQuestion = TestDataGenerator.createQuestion(true,'Test Supplier Question',2000,false,'Category',questionRTypeMap.get('Supplier').getRecordTypeId(),true,false);

		//Executive Golf
		Question__c execActivityQuestion = TestDataGenerator.createQuestion(true,'Test Golf Pickilist',200,true,'Activity',questionRTypeMap.get('Executive').getRecordTypeId(),false,true);
		
		//Create Event Questions
		Event_Question__c eQuestion = TestDataGenerator.createEventQuestion(true,'The Mulit Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);
		Event_Question__c eQuestion1 = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',picklistQuestion,true,null,null);
		Event_Question__c eQuestion2 = TestDataGenerator.createEventQuestion(true,'The Supplier Text Question',prod,'Category',supplierQuestion,true,null,null);
		//Event_Question__c eQuestion1 = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',picklistQuestion,true,null,null);
		UserPermissions uPermissions = new UserPermissions('Supplier',true);

		Test.startTest();
			registrationQuestionsController regQuestionController= new registrationQuestionsController();
			regQuestionController.currentEvent = prod;
			regQuestionController.currentContact = c;
			regQuestionController.currentConfirmation = conf;
			regQuestionController.eventId = prod.Id;
			regQuestionController.contactId = c.Id;
			regQuestionController.currentContactRecordType = 'Supplier';
			//regQuestionController.parentForm = new MultiComponentForm();
			regQuestionController.phase ='Category';
			List<QuestionWrapper> questionList = regQuestionController.getQuestionList();
		Test.stopTest();
	}

	@isTest static void build_SupplierQuestions_wAnswers_Test() {
		// Implement test code
		TestDataGenerator.createInactiveTriggerSettings();
		Id supplierAccRtype = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();
		Id supplierConRtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Supplier').getRecordTypeId();

		Account a = TestDataGenerator.createTestAccount(true,'Executive Test Account',supplierAccRtype);

		Contact c = TestDataGenerator.createTestContact(true,'Test','Exec','t@t.com',a,(String) supplierConRtype);
		c.Has_Event__c = true;
		update c;
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event',true,Date.today(), Date.today().addDays(5), Date.today().addMonths(-3), Date.today().addMonths(-2));
		
		//Start Defining Event "Product" related info.
		Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
		
		User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec@cpmg.com',null);
		User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier@cpmg.com',null);

		Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
		Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');

		Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Opp',Date.today().addYears(-1),a, 'Closed Won', prod);

		Id chainExecRType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
		Event_Account_Details__c ead = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c,chainExecRType);

		Order conf = TestDataGenerator.createConfirmation(true,a,c,'Preliminary',opp,Date.today(),ead,prod);
		
		//Start Questions
		Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
		Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Pickilist',200,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),false,false);
			Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
			Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
			Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
		Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),true,false);
			Question_Answers_Values__c qav4Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option4','Select4');
			Question_Answers_Values__c qav5Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option5','Select5');
			Question_Answers_Values__c qav6Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option6','Select6');
		//Supplier
		Question__c supplierQuestion = TestDataGenerator.createQuestion(true,'Test Supplier Question',2000,false,'Category',questionRTypeMap.get('Supplier').getRecordTypeId(),true,false);

		//Executive Golf
		Question__c execActivityQuestion = TestDataGenerator.createQuestion(true,'Test Golf Pickilist',200,true,'Activity',questionRTypeMap.get('Executive').getRecordTypeId(),false,true);
		
		//Create Event Questions
		Event_Question__c eQuestion = TestDataGenerator.createEventQuestion(true,'The Mulit Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);
		Event_Question__c eQuestion1 = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',picklistQuestion,true,null,null);
		Event_Question__c eQuestion2 = TestDataGenerator.createEventQuestion(true,'The Supplier Text Question',prod,'Category',supplierQuestion,true,null,null);
		//Event_Question__c eQuestion1 = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',picklistQuestion,true,null,null);
		UserPermissions uPermissions = new UserPermissions('Supplier',true);

		Test.startTest();
			registrationQuestionsController regQuestionController= new registrationQuestionsController();
			regQuestionController.currentEvent = prod;
			regQuestionController.name='Test';
			regQuestionController.readOnly = false;
			regQuestionController.questionType='Executive';
			regQuestionController.permissions = uPermissions;
			regQuestionController.currentContact = c;
			regQuestionController.currentConfirmation = conf;
			regQuestionController.eventId = prod.Id;
			regQuestionController.contactId = c.Id;
			regQuestionController.currentContactRecordType = 'Executive';
			//regQuestionController.parentForm = new MultiComponentForm();
			regQuestionController.phase ='Category';
			List<QuestionWrapper> questionList = regQuestionController.getQuestionList();
		Test.stopTest();
	}

	static testMethod void  combine_SelectOption_test(){
			String[] ansList = new List<String>{'value1','value2'};

			Test.startTest();
				//registrationQuestionsController regQuestController = new registrationQuestionsController();
				String s = RegistrationQuestionsController.combineSelectOptions(ansList); 
			Test.stopTest();
	}
	static testMethod void  setAnswers_test(){
			String[] ansList = new List<String>{'value1','value2'};

			Test.startTest();
				registrationQuestionsController regQuestController = new registrationQuestionsController();
				regQuestController.setAnswers(ansList); 
			Test.stopTest();
	}
	static testMethod void  getAnswers_test(){
			String[] ansList = new List<String>{'value1','value2'};

			Test.startTest();
				registrationQuestionsController regQuestController = new registrationQuestionsController();
				regQuestController.answers = ansList;
				List<String> s =regQuestController.getAnswers(); 
			Test.stopTest();
	}

		static testMethod void  combine_SelectOptionForLabel_test(){
			String[] ansList = new List<String>{'value1','value2'};
			Map<String,String> picklistMap = New Map<String,String>{'value1'=>'val1','value2'=>'val2'};

			Test.startTest();
				//registrationQuestionsController regQuestController = new registrationQuestionsController();
				String s = RegistrationQuestionsController.combineSelectOptionsForLabel(ansList, picklistMap); 
			Test.stopTest();
	}

	static testMethod void testMultiComponent(){
		TestDataGenerator.createInactiveTriggerSettings();
		Community_Settings__c commSett = TestDataGenerator.createCommSettings();

		//AccountFormSettings
		TestDataGenerator.createAccountFormSettings();

		//Contact Form Settings:
		TestDataGenerator.createContactFormSettings();

		//Order Form Display settings
		TestDataGenerator.createOrderFormSettings();

		//Registration Settings
		TestDataGenerator.createRegistrationSettings();

		//Need the Order Status Custom Setting
		TestDataGenerator.createOrderStatusCustomSetting();
		
		Id execAccRtype =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Account a = TestDataGenerator.createTestAccount(true, 'Test Account',execAccRtype);
		Id execConRtype =  Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Executive').getRecordTypeId();
		Contact c = TestDataGenerator.createTestContact(true,'Test','Last Name','testAcc@account.com',a,execConRtype);

		Profile commUserProfId = [SELECT ID FROM Profile where Name ='CPMG Community Portal User' LIMIT 1];
		
		User execCommUser = TestDataGenerator.CreateUser(true,commUserProfId.Id,'Test','Exec',null,'testExecUser@cpmg.com',c.Id);
		
		Product2 prod = TestDataGenerator.createTestProduct(true,'Test Event Active',true,Date.today().addDays(-10),Date.today().addDays(-5),Date.today().addMonths(-2), Date.today().addMonths(-1));

		Profile standUserProfId = [SELECT ID FROM Profile where Name ='Standard User' LIMIT 1];
		
		User execUser = TestDataGenerator.CreateUser(true,standUserProfId.Id,'Test','Exec',null,'testExec1234@cpmg.com',null);
		User supplierUser = TestDataGenerator.CreateUser(true, standUserProfId.Id,'Test','Supplier',null,'testSupplier1234@cpmg.com',null);

		Event_Staff__c execStaff = TestDataGenerator.createEventStaff(true,prod,execUser,'Recruiter');
		Event_Staff__c supplierStaff = TestDataGenerator.createEventStaff(true,prod,supplierUser,'Salesperson');


		Opportunity opp = TestDataGenerator.createTestOpp(true,'Test Event Active Opp',Date.today().addMonths(-4),a,'Closed Won',prod);

		Id execPartRecordType = Schema.SObjectType.Event_Account_Details__c.getRecordTypeInfosByName().get('Chain Executives').getRecordTypeId();
		Event_Account_Details__c partRecord = TestDataGenerator.createParticipationRecord(true,prod,a,opp,c, execPartRecordType);

		Order conf = TestDataGenerator.createConfirmation(true,a,c,'Attending Event Contact',opp,opp.CloseDate,partRecord,prod);
		List<Order> ordersJustCreated = new List<Order>{conf};

		Document d1 = TestDataGenerator.createDocument('test1','text/plain','test1');
		Document d2 = TestDataGenerator.createDocument('test2','text/plain','test2');
		Document d3 = TestDataGenerator.createDocument('test3','text/plain','test3');
		Document d4 = TestDataGenerator.createDocument('test4','text/plain','test4');

		Image__c img1 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d1.Id,partRecord,true,false,true);
		Image__c img2 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d2.Id,partRecord,true,false,true);
		Image__c img3 = TestDataGenerator.createImage(true,a,c,conf,prod,(String) d3.Id,partRecord,true,false,true);
		Image__c img4= TestDataGenerator.createImage(true,a,c,conf,prod,(String) d4.Id,partRecord,true,false,true);

		Map<String,RecordTypeInfo> questionRTypeMap = Schema.SObjectType.Question__c.getRecordTypeInfosByName();
		Question__c picklistQuestion = TestDataGenerator.createQuestion(true,'Test Pickilist',200,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),false,false);
			Question_Answers_Values__c qav1Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option','Select');
			Question_Answers_Values__c qav2Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option2','Select2');
			Question_Answers_Values__c qav3Picklist = TestDataGenerator.createQuestionOption(true,picklistQuestion,'Select Option3','Select3');
		Question__c mulitPicklistQuestion = TestDataGenerator.createQuestion(true,'Test Multi Pickilist',200,true,'Category',questionRTypeMap.get('Account').getRecordTypeId(),true,false);
			Question_Answers_Values__c qav4Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option4','Select4');
			Question_Answers_Values__c qav5Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option5','Select5');
			Question_Answers_Values__c qav6Picklist = TestDataGenerator.createQuestionOption(true,mulitPicklistQuestion,'Select Option6','Select6');
		//Supplier
		Question__c supplierQuestion = TestDataGenerator.createQuestion(true,'Test Supplier Question',2000,false,'Category',questionRTypeMap.get('Supplier').getRecordTypeId(),true,false);

		//Executive Golf
		Question__c execActivityQuestion = TestDataGenerator.createQuestion(true,'Test Golf Pickilist',200,true,'Activity',questionRTypeMap.get('Executive').getRecordTypeId(),false,true);
		
		//Create Event Questions
		Event_Question__c eQuestion = TestDataGenerator.createEventQuestion(true,'The Picklist Event Quest',prod,'Category',mulitPicklistQuestion,true,null,null);

		PageReference pageRef = Page.EventRegistration2;
		pageRef.getParameters().put('eId', prod.Id);


		Test.startTest();
			System.runAs(execCommUser){
				Test.setCurrentPageReference(pageRef);
				EventRegistration2Controller eRegController = new EventRegistration2Controller();
				String anchorPart = eRegController.anchorPart;
				String subAnchorPart = eRegController.subAnchorPart;
				String siteURL = eRegController.getSiteURL();
				UserPermissions uPermissions = eRegController.permissions;
				Boolean displayContact = eRegController.displayEditContactModal;
				Boolean displayAccount = eRegController.displayProfileAnswersModal;

		registrationQuestionsController regQuestionController= new registrationQuestionsController();
			regQuestionController.currentEvent = prod;
			regQuestionController.currentContact = c;
			regQuestionController.currentConfirmation = conf;
			regQuestionController.eventId = prod.Id;
			regQuestionController.contactId = c.Id;
			regQuestionController.currentContactRecordType = 'Executive';
			regQuestionController.parentForm = eRegController.getThis();
			regQuestionController.phase ='Category';
			List<QuestionWrapper> questionList = regQuestionController.getQuestionList();
			
			regQuestionController.parentForm.registerQuestionsCompController(regQuestionController);

			Integer numController =regQuestionController.parentForm.getNumControllers();
			Set<registrationQuestionsController> regQuestionControlSet = MultiComponentForm.getControllers();

			regQuestionController.parentForm.saveAllComponents();

			}
		Test.stopTest();
	}
	
}