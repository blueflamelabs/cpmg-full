/**
 * An apex page controller that exposes the change password functionality
 */
public with sharing class ChangePasswordController {
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String verifyNewPassword {get; set;}  
    public String passwordPolicy{set;get;}
    public Boolean isMinLength{set;get;} 
    
    public ChangePasswordController(){
        passwordPolicy = String.valueOf(site.getPasswordPolicyStatement());
    }    
    
    public PageReference changePassword() {
        if(passwordPolicy == null)
            passwordPolicy = String.valueOf(site.getPasswordPolicyStatement());
        if(newPassword.length() < 8){
            String[] passpolicybreak = passwordPolicy.split('\\.');
            passwordPolicy = passpolicybreak[0]+'. \n '+passpolicybreak[1];
            isMinLength = true;
            return null;
        } 
        return Site.changePassword(newPassword, verifyNewPassword, oldpassword);    
    }     
    
 
}