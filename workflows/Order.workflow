<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Existing_Attending_Only_User_Welcome_Email</fullName>
        <description>Existing Attending Only User Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>BillToContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Attending_Only_Existing_User_Welcome_HTML</template>
    </alerts>
    <alerts>
        <fullName>Existing_Event_Contact_User_Welcome_Email</fullName>
        <description>Existing Event Contact User Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>BillToContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Event_Contact_Existing_User_Welcome_HTML</template>
    </alerts>
    <alerts>
        <fullName>Existing_Executive_User_Welcome_Email</fullName>
        <description>Existing Executive User Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>BillToContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Executives_Existing_User_Welcome_HTML</template>
    </alerts>
    <alerts>
        <fullName>New_Attending_Only_User_Welcome_Email</fullName>
        <description>New Attending Only User Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>BillToContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Attending_Only_New_User_Welcome_HTML</template>
    </alerts>
    <alerts>
        <fullName>New_Event_Contact_User_Welcome_Email</fullName>
        <description>New Event Contact User Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>BillToContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Event_Contact_New_User_Welcome_HTML</template>
    </alerts>
    <alerts>
        <fullName>New_Executive_User_Welcome_Email</fullName>
        <description>New Executive User Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>BillToContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Executives_New_User_Welcome_HTML</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Badge_Name</fullName>
        <field>BadgeName__c</field>
        <formula>BillToContact.FirstName+&quot; &quot;+BillToContact.LastName</formula>
        <name>Update Badge Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Welcome_Email_Sent_Date</fullName>
        <description>Updates welcome email start date to &quot;Today&quot;.</description>
        <field>Date_Welcome_Email_Sent__c</field>
        <formula>Today()</formula>
        <name>Update Welcome Email Sent Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Badge Name</fullName>
        <actions>
            <name>Update_Badge_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order.BadgeName__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If blank set badge name to contact name</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Welcome Existing Attending Only</fullName>
        <actions>
            <name>Existing_Attending_Only_User_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Welcome_Email_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sends welcome email to existing supplier attending only users.</description>
        <formula>AND( BillToContact.RecordType.DeveloperName = &apos;Supplier&apos;,Id &lt;&gt;  BillToContact.User_Create_Confirmation__r.Id,Send_Email__c, ISBLANK(Date_Welcome_Email_Sent__c),  TEXT(Status)=&apos;Attending&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Welcome Existing Event Contacts</fullName>
        <actions>
            <name>Existing_Event_Contact_User_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Welcome_Email_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sends welcome email to existing supplier event contact users.</description>
        <formula>AND( BillToContact.RecordType.DeveloperName = &apos;Supplier&apos;,Id &lt;&gt;  BillToContact.User_Create_Confirmation__r.Id,Send_Email__c,ISBLANK(Date_Welcome_Email_Sent__c),  TEXT( Status ) &lt;&gt; &apos;Attending&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Welcome Existing Executives</fullName>
        <actions>
            <name>Existing_Executive_User_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Welcome_Email_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sends welcome email to existing executive users.</description>
        <formula>AND( BillToContact.RecordType.DeveloperName = &apos;Executive&apos;,Id &lt;&gt;  BillToContact.User_Create_Confirmation__r.Id,Send_Email__c,ISBLANK(Date_Welcome_Email_Sent__c))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Welcome New Attending Only</fullName>
        <actions>
            <name>New_Attending_Only_User_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Welcome_Email_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sends welcome email to new supplier attending only users.</description>
        <formula>AND( BillToContact.RecordType.DeveloperName = &apos;Supplier&apos;,Id =  BillToContact.User_Create_Confirmation__r.Id,Send_Email__c,ISBLANK(Date_Welcome_Email_Sent__c),  TEXT(Status)=&apos;Attending&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Welcome New Event Contacts</fullName>
        <actions>
            <name>New_Event_Contact_User_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Welcome_Email_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sends welcome email to new supplier event contact users.</description>
        <formula>AND( BillToContact.RecordType.DeveloperName = &apos;Supplier&apos;,Id =  BillToContact.User_Create_Confirmation__r.Id,Send_Email__c,ISBLANK(Date_Welcome_Email_Sent__c),  TEXT(Status) &lt;&gt; &apos;Attending&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Welcome New Executives</fullName>
        <actions>
            <name>New_Executive_User_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Welcome_Email_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Sends welcome email to new executive users.</description>
        <formula>AND(BillToContact.RecordType.DeveloperName = &apos;Executive&apos;,Id =  BillToContact.User_Create_Confirmation__r.Id,Send_Email__c,ISBLANK(Date_Welcome_Email_Sent__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
