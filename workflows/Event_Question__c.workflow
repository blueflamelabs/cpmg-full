<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Event_Question_Name</fullName>
        <field>Name</field>
        <formula>Event__r.ProductCode
&amp; &quot; - &quot; 
&amp; RecordType.Name
&amp; &quot; - &quot;
&amp; Question__r.Name</formula>
        <name>Update Event Question Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Event Question Naming Convention</fullName>
        <actions>
            <name>Update_Event_Question_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Event_Question__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update the Event Question name to the standard naming convention.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
