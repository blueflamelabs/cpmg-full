<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Notes</fullName>
        <field>Notes__c</field>
        <formula>IF(LEN(Description)&gt;255,LEFT(Description,252)&amp;&quot;...&quot;,Description)</formula>
        <name>Update Notes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Notes</fullName>
        <actions>
            <name>Update_Notes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Description</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Updates notes field with first 255 characters of comments section.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
