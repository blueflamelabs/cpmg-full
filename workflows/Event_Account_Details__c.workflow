<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Part_2_Extension_to_True</fullName>
        <field>Registration_Part_2_Extension__c</field>
        <literalValue>1</literalValue>
        <name>Update Part 2 Extension to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Part 2 Extension Upon Create</fullName>
        <actions>
            <name>Update_Part_2_Extension_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Event_Account_Details__c.Account_Record_Type__c</field>
            <operation>equals</operation>
            <value>Executive</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
